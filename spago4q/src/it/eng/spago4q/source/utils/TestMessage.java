/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.source.utils;

import java.io.Serializable;

public class TestMessage implements Serializable{

	private static final long serialVersionUID = 1210532875353822386L;
	private boolean isOk;
	private String message;
	
	public boolean isOk() {
		return isOk;
	}
	public void setOk(final boolean value) {
		this.isOk = value;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(final String message) {
		this.message = message;
	}
	
	public TestMessage(final boolean isOk, final String message) {
		setOk(isOk);
		setMessage(message);
	}
}
