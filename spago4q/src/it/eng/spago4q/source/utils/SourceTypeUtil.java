/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.source.utils;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.DataSource;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.extractors.ExtractorInterface;

public final class SourceTypeUtil {
	
	private SourceTypeUtil() {}

	public static TestMessage testDataSource(final Integer idDataSource) {
		String message;
		boolean isOk;

		IES4QFKDAO dataSourceDAO;
		try {
			dataSourceDAO = (IES4QFKDAO) DAOFactory.getDAO("EDataSourceDAO");
			DataSource dataSourceBo = (DataSource) dataSourceDAO
			.loadObjectById(idDataSource);
			String extractorClassName = dataSourceBo.getSourceType()
			.getExtractorClass();
			ExtractorInterface extractor = (ExtractorInterface) Class.forName(
					extractorClassName).newInstance();
			message = extractor.dataSourceTest(idDataSource);
			isOk = true;
		} catch (EMFUserError e) {
			message = e.getDescription();
			isOk = false;
		} catch (InstantiationException e) {
			message = "Extractor class problem.<br>" + e.toString();
			isOk = false;
		} catch (IllegalAccessException e) {
			message = "Extractor class problem.<br>" + e.toString();
			isOk = false;
		} catch (ClassNotFoundException e) {
			message = "Extractor class problem.<br>" + e.toString();
			isOk = false;
		}
		TestMessage toReturn = new TestMessage(isOk,message);
		return toReturn;
	}

	public static TestMessage testOperation(final Integer idOperation) {
		String message;
		boolean isOk;

		IES4QFKDAO operationDAO;

		try {
			operationDAO = (IES4QFKDAO) DAOFactory.getDAO("EOperationDAO");
			EOperation operationBo = (EOperation) operationDAO
			.loadObjectById(idOperation);
			String extractorClassName = operationBo.getDataSource().getSourceType()
			.getExtractorClass();
			ExtractorInterface extractor = (ExtractorInterface) Class.forName(
					extractorClassName).newInstance();
			message = extractor.operationTest(idOperation);
			isOk = true;
		} catch (EMFUserError e) {
			message = e.getDescription();
			isOk = false;
		} catch (InstantiationException e) {
			message = "Extractor class problem.<br>" + e.toString();
			isOk = false;
		} catch (IllegalAccessException e) {
			message = "Extractor class problem.<br>" + e.toString();
			isOk = false;
		} catch (ClassNotFoundException e) {
			message = "Extractor class problem.<br>" + e.toString();
			isOk = false;
		}
		TestMessage toReturn = new TestMessage(isOk,message);
		return toReturn;
	}

}
