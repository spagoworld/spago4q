/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.source.utils;

import it.eng.spago.base.SourceBean;
import it.eng.spago4q.bo.SourceType;
import it.eng.spago4q.utilities.DetailS4Q1ObjectUtil;

public class DetailSourceTypeUtil extends DetailS4Q1ObjectUtil {

	private static final String DAO_NAME = "ESourceTypeDAO";
	private static final String OBJ_NAME = "SOURCETYPE";

	@Override
	protected String getDAOName() {
		return DAO_NAME;
	}

	@Override
	protected Object getObjectFromRequest(final SourceBean serviceRequest) {
		SourceType toReturn = new SourceType();
		String name = (String) serviceRequest.getAttribute("name");
		String description = (String) serviceRequest
				.getAttribute("description");
		String extractorClass = (String) serviceRequest
				.getAttribute("extractorclass");

		toReturn.setName(name);
		toReturn.setDescription(description);
		toReturn.setExtractorClass(extractorClass);

		return toReturn;
	}

	@Override
	protected String getObjectName() {
		return OBJ_NAME;
	}

	@Override
	protected void setObjectId(final Object object, final Integer id) {
		((SourceType) object).setId(id);
	}
}
