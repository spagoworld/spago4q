/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.source.utils;

import it.eng.spago.base.SourceBean;
import it.eng.spago4q.bo.DataSource;
import it.eng.spago4q.bo.SourceType;
import it.eng.spago4q.utilities.DetailS4Q2ObjectUtil;

public class DetailDataSourceUtil extends DetailS4Q2ObjectUtil {

	private static final long serialVersionUID = 1524534446885460251L;
	private static final String DAO_NAME = "EDataSourceDAO";
	private static final String OBJ_NAME = "DATASOURCE";

	private TestMessage testMessage = null;

	@Override
	protected String getDAOName() {
		return DAO_NAME;
	}

	@Override
	protected Object getObjectFromRequest(final SourceBean serviceRequest) {
		DataSource toReturn = new DataSource();
		String name = (String) serviceRequest.getAttribute("name");
		String description = (String) serviceRequest
				.getAttribute("description");
		String SourceTypeId = (String) serviceRequest.getAttribute("Type_Id");

		toReturn.setName(name);
		toReturn.setDescription(description);

		SourceType sourceType = new SourceType();

		sourceType.setId(Integer.parseInt(SourceTypeId));

		toReturn.setSourceType(sourceType);

		return toReturn;
	}

	@Override
	public void updateObjectFromRequest(final SourceBean serviceRequest,
			final Integer id) throws Exception {
		super.updateObjectFromRequest(serviceRequest, id);
		String testValue = (String) serviceRequest
				.getAttribute("TESTDATASOURCE");
		if (testValue != null && testValue.toUpperCase().equals("TEST")) {
			testMessage = SourceTypeUtil.testDataSource(id);
		}
	}

	@Override
	public void selectObject(final Integer id, final SourceBean serviceResponse)
			throws Exception {
		super.selectObject(id, serviceResponse);
		if (testMessage != null) {
			serviceResponse.setAttribute("TESTRESULT", testMessage);
		}
	}

	@Override
	protected String getObjectName() {
		return OBJ_NAME;
	}

	@Override
	protected void setObjectId(final Object object, final Integer id) {
		((DataSource) object).setId(id);
	}

}
