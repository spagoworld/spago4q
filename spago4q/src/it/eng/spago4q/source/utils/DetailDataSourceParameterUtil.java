/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.source.utils;

import it.eng.spago.base.SourceBean;
import it.eng.spago4q.bo.DataSource;
import it.eng.spago4q.bo.DataSourceParameter;
import it.eng.spago4q.utilities.DetailS4Q2ObjectUtil;

public class DetailDataSourceParameterUtil extends DetailS4Q2ObjectUtil {

	private static final String DAO_NAME = "EDataSourceParameterDAO";
	private static final String OBJ_NAME = "DATASOURCEPARAMETER";

	@Override
	protected String getDAOName() {
		return DAO_NAME;
	}

	@Override
	protected Object getObjectFromRequest(final SourceBean serviceRequest) {
		DataSourceParameter toReturn = new DataSourceParameter();
		String name = (String) serviceRequest.getAttribute("name");
		String value = (String) serviceRequest.getAttribute("value");
		String criptStr = (String) serviceRequest.getAttribute("cript");

		String dataSourceId = (String) serviceRequest
				.getAttribute("DataSource_Id");

		boolean cript = false;

		if (criptStr != null) {
			cript = true;
		}

		toReturn.setName(name);
		toReturn.setCript(cript);
		toReturn.setValue(value);
		

		DataSource dataSource = new DataSource();

		dataSource.setId(Integer.parseInt(dataSourceId));

		toReturn.setDataSource(dataSource);

		return toReturn;

	}

	@Override
	protected String getObjectName() {
		return OBJ_NAME;
	}

	@Override
	protected void setObjectId(final Object object, final Integer id) {
		((DataSourceParameter) object).setId(id);
	}

}
