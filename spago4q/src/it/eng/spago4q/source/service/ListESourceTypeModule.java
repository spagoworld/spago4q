/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.source.service;

import it.eng.spago.base.SourceBean;
import it.eng.spago.base.SourceBeanException;
import it.eng.spago.error.EMFErrorHandler;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.SourceType;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QDAO;
import it.eng.spagobi.kpi.utils.AbstractConfigurableListModule;

import java.util.List;

import org.apache.log4j.Logger;


public class ListESourceTypeModule extends AbstractConfigurableListModule {

	private static final long serialVersionUID = 3097974469585101085L;
	private static transient Logger logger = Logger.getLogger(ListESourceTypeModule.class);

	protected List getObjectList(final SourceBean request) {
		List result = null;
		String fieldOrder = (String)request.getAttribute("FIELD_ORDER");
		String typeOrder = (String)request.getAttribute("TYPE_ORDER");
		try {
			IES4QDAO eSourceTypeDao = (IES4QDAO)DAOFactory.getDAO("ESourceTypeDAO");
			result = eSourceTypeDao.loadObjectList(fieldOrder, typeOrder);
		} catch (EMFUserError e) {
			logger.error(e);
		}
		return result;
	}

	protected void setRowAttribute(final SourceBean rowSB, final Object obj)
			throws SourceBeanException {
		SourceType aSourceType = (SourceType) obj;
		rowSB.setAttribute("name", aSourceType.getName());
		rowSB.setAttribute("id", aSourceType.getId());
	}
	
	@Override
	public boolean delete(final SourceBean request, final SourceBean response) {
		boolean toReturn = false;
		String id = (String) request.getAttribute("ID");
		try {
			IES4QDAO  iESourceTypeDAO = (IES4QDAO) DAOFactory.getDAO("ESourceTypeDAO");
			toReturn = iESourceTypeDAO.deleteObject(Integer.parseInt(id));
			toReturn = true;
		} catch (NumberFormatException e) {
			EMFErrorHandler engErrorHandler = getErrorHandler();
			engErrorHandler.addError(new EMFUserError(EMFErrorSeverity.WARNING,
					"10012", "component_spago4q_messages"));
		} catch (EMFUserError e) {
			EMFErrorHandler engErrorHandler = getErrorHandler();
			engErrorHandler.addError(e);
		}

		return toReturn;
	}
}
