/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.source.service;

import it.eng.spago4q.source.utils.DetailSourceTypeUtil;
import it.eng.spago4q.utilities.DetailS4Q1Module;
import it.eng.spago4q.utilities.DetailS4Q1ObjectUtil;


public class DetailESourceTypeModule extends DetailS4Q1Module {

	private static final String VALIDATION_PAGE = "SourceTypeDetailPage";

	private DetailSourceTypeUtil detailSourceTypeUtil = new DetailSourceTypeUtil();
	
	@Override
	protected DetailS4Q1ObjectUtil getDetailObjectUtil() {
		return detailSourceTypeUtil;
	}

	@Override
	protected String getValidationPage() {
		return VALIDATION_PAGE;
	}

}
