/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.source.service;

import it.eng.spago.base.SourceBean;
import it.eng.spago.base.SourceBeanException;
import it.eng.spago.error.EMFErrorHandler;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.DataSourceParameter;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spagobi.kpi.utils.AbstractConfigurableListModule;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

public class ListEDataSourceParameterModule extends
		AbstractConfigurableListModule {

	private static final long serialVersionUID = -8109267638099660461L;
	private static transient Logger logger = Logger
			.getLogger(ListEDataSourceModule.class);

	@Override
	public void service(final SourceBean request, final SourceBean response)
			throws Exception {

		super.service(request, response);

		String dataSourceId = (String) request.getAttribute("DATASOURCE_ID");

		HashMap parametersMap = (HashMap) response
				.getAttribute("PARAMETERS_MAP");
		if (parametersMap == null) {
			parametersMap = new HashMap();
			parametersMap.put("DATASOURCE_ID", dataSourceId);
			response.setAttribute("PARAMETERS_MAP", parametersMap);
		} else {
			parametersMap.put("DATASOURCE_ID", dataSourceId);
			response.updAttribute("PARAMETERS_MAP", parametersMap);
		}
	}

	@Override
	protected List getObjectList(final SourceBean request) {
		String fieldOrder = (String) request.getAttribute("FIELD_ORDER");
		String typeOrder = (String) request.getAttribute("TYPE_ORDER");
		List result = null;
		try {
			String dataSourceId = (String) request
					.getAttribute("DATASOURCE_ID");
			if (dataSourceId != null && !dataSourceId.trim().equals("")) {
				IES4QFKDAO iEDataSourceDAO = (IES4QFKDAO) DAOFactory
						.getDAO("EDataSourceParameterDAO");
				result = iEDataSourceDAO.loadObjectList(Integer
						.parseInt(dataSourceId), fieldOrder, typeOrder);
			}
		} catch (EMFUserError e) {
			logger.error(e);
		}
		return result;
	}

	@Override
	protected void setRowAttribute(final SourceBean rowSB, final Object obj)
			throws SourceBeanException {
		DataSourceParameter aDataSource = (DataSourceParameter) obj;
		if (aDataSource != null) {
			rowSB.setAttribute("ID", aDataSource.getId());
			if (aDataSource.getName() != null) {
				rowSB.setAttribute("NAME", aDataSource.getName());
			}
			if (aDataSource.getValue() != null) {
				rowSB.setAttribute("VALUE", aDataSource.getValue());
			}
		}
	}

	@Override
	public boolean delete(final SourceBean request, final SourceBean response) {
		boolean toReturn = false;
		String id = (String) request.getAttribute("ID");
		try {
			IES4QFKDAO dataSourceDAO = (IES4QFKDAO) DAOFactory
					.getDAO("EDataSourceParameterDAO");
			toReturn = dataSourceDAO.deleteObject(Integer.parseInt(id));
			toReturn = true;
		} catch (NumberFormatException e) {
			EMFErrorHandler engErrorHandler = getErrorHandler();
			engErrorHandler.addError(new EMFUserError(EMFErrorSeverity.WARNING,
					"10012", "component_spago4q_messages"));
		} catch (EMFUserError e) {
			EMFErrorHandler engErrorHandler = getErrorHandler();
			engErrorHandler.addError(e);
		}

		return toReturn;
	}

}
