/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.tools.service;

import it.eng.spago.base.SourceBean;
import it.eng.spago.base.SourceBeanException;
import it.eng.spago.dispatching.module.AbstractModule;
import it.eng.spago.error.EMFErrorHandler;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago.validation.EMFValidationError;
import it.eng.spago.validation.coordinator.ValidationCoordinator;
import it.eng.spago4q.exporter.ExporterCoordinator;
import it.eng.spago4q.exporter.ExporterCoordinatorImpl;
import it.eng.spago4q.exporter.ImporterCoordinator;
import it.eng.spago4q.exporter.ImporterCoordinatorImpl;
import it.eng.spago4q.tools.utils.FileExportUtil;
import it.eng.spago4q.tools.utils.FileImportUtil;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

public class ImportExportModule extends AbstractModule {

	private static final long serialVersionUID = 6867451309361498673L;
	private static final String VALIDATION_EXPORT_PAGE = "ExportModule";
	private static final String VALIDATION_IMPORT_PAGE = "ImportModule";
	private static transient Logger logger = Logger.getLogger(ImportExportModule.class);

	public void service(final SourceBean serviceRequest,
			final SourceBean serviceResponse) throws Exception {
		String message = (String) serviceRequest.getAttribute("MESSAGE");

		if (message != null && message.equalsIgnoreCase("Export")) {
			if (!hasValidationError(VALIDATION_EXPORT_PAGE)) {
				exportOperation(serviceRequest, serviceResponse);
			}
		}
		if (message != null && message.equalsIgnoreCase("Import")) {
			if (!hasValidationError(VALIDATION_IMPORT_PAGE)) {
				try {
					importOperation(serviceRequest);
				} catch (EMFUserError e) {
					getErrorHandler().addError(e);
				}
			}
		}
	}

	private void importOperation(final SourceBean serviceRequest)
			throws EMFUserError {
		String idDataSourceString = (String) serviceRequest
				.getAttribute("DATASOURCE");
		Integer idDataSource = Integer.parseInt(idDataSourceString);

		String overwriteString = (String) serviceRequest
				.getAttribute("overwrite");
		boolean overwrite = false;
		if (overwriteString != null
				&& overwriteString.toUpperCase().equalsIgnoreCase("TRUE")) {
			overwrite = true;
		}

		// create a file in the temp folder
		Map<String, String> filesName = FileImportUtil.importOperationFiles(
				serviceRequest, getErrorHandler());

		ImporterCoordinator importerCoordinator = new ImporterCoordinatorImpl();
		if (filesName.get(FileImportUtil.OPERATION_XML) != null
				&& filesName.get(FileImportUtil.DATASOURCE_XML) != null
				&& filesName.get(FileImportUtil.INTERFACETYPE_XML) != null
				&& filesName.get(FileImportUtil.SCRIPT_XML) != null) {
			importerCoordinator.operationImporter(filesName
					.get(FileImportUtil.OPERATION_XML), filesName
					.get(FileImportUtil.DATASOURCE_XML), filesName
					.get(FileImportUtil.INTERFACETYPE_XML), filesName
					.get(FileImportUtil.SCRIPT_XML), idDataSource, overwrite);
		} else {
			EMFUserError error = new EMFUserError(EMFErrorSeverity.ERROR,
					"s4q.incomplete.Archive", "component_spago4q_messages");
			getErrorHandler().addError(error);
		}
	}

	private void exportOperation(final SourceBean serviceRequest,
			final SourceBean serviceResponse) {
		String exportArchiveName = (String) serviceRequest
				.getAttribute("exportFileName");
		String operations = (String) serviceRequest
				.getAttribute("selectedoperations");
		Set<Integer> operationsIds = new HashSet<Integer>();
		if (operations != null && !operations.trim().equals("")) {
			String[] operationArray = operations.split(",");
			for (int i = 0; i < operationArray.length; i++) {
				operationsIds.add(Integer.parseInt(operationArray[i].trim()));
			}
		}
		// init
		Date date = new Date();
		Long milliseconds = date.getTime();

		String exportFolderName = milliseconds.toString();
		String folderPath = FileExportUtil.createExportFolder(exportFolderName);
		folderPath += "/";
		String archivePathToDownload = null;

		if (operationsIds.size() > 0) {
			ExporterCoordinator exporterCoordinator = new ExporterCoordinatorImpl();
			exporterCoordinator.exportOperation(operationsIds, folderPath
					+ FileImportUtil.OPERATION_XML, folderPath
					+ FileImportUtil.DATASOURCE_XML, folderPath
					+ FileImportUtil.INTERFACETYPE_XML, folderPath
					+ FileImportUtil.SCRIPT_XML);
			String[] filesToExport = { FileImportUtil.OPERATION_XML,
					FileImportUtil.DATASOURCE_XML,
					FileImportUtil.INTERFACETYPE_XML, FileImportUtil.SCRIPT_XML };
			// zip
			archivePathToDownload = FileExportUtil.createZipFile(
					exportArchiveName, filesToExport, folderPath);
			FileExportUtil.cleanExportFiles(filesToExport, folderPath);
		}
		if (archivePathToDownload != null) {
			try {
				serviceResponse.setAttribute("EXPORT_FILE", exportArchiveName);
				serviceResponse.setAttribute("EXPORT_DIR", folderPath);
			} catch (SourceBeanException e) {
				logger.debug(e.getLocalizedMessage());
			}
		}
	}

	private boolean hasValidationError(final String validationPage) {
		boolean toReturn = false;
		ValidationCoordinator.validate("PAGE", validationPage, this);
		EMFErrorHandler errorHandler = getErrorHandler();
		Collection errors = errorHandler.getErrors();

		if (errors != null && errors.size() > 0) {
			Iterator iterator = errors.iterator();
			while (iterator.hasNext()) {
				Object error = iterator.next();
				if (error instanceof EMFValidationError) {
					toReturn = true;
				}
			}
		}
		return toReturn;
	}
}
