/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.tools.service;

import it.eng.spago.base.SourceBean;
import it.eng.spago.base.SourceBeanException;
import it.eng.spago.dispatching.module.AbstractModule;
import it.eng.spago.error.EMFErrorHandler;
import it.eng.spago.error.EMFUserError;
import it.eng.spago.validation.EMFValidationError;
import it.eng.spago.validation.coordinator.ValidationCoordinator;
import it.eng.spago4q.extractors.FileImportCoordinator;
import it.eng.spago4q.tools.utils.FileExportUtil;
import it.eng.spago4q.tools.utils.FileImportUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;

public class FileImportModule extends AbstractModule {

	private static final long serialVersionUID = 5508967915168492884L;
	private static final String VALIDATION_PAGE = "FileImportModule";
	public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";

	private static transient Logger logger = Logger
			.getLogger(FileImportModule.class);

	public void service(final SourceBean serviceRequest,
			final SourceBean serviceResponse) throws Exception {

		String message = (String) serviceRequest.getAttribute("MESSAGE");

		if (message != null && message.equalsIgnoreCase("EXECUTE")) {

			if (!hasValidationError()) {
				// get Attribute
				String xPath = (String) serviceRequest.getAttribute("xPath");
				String exportDate = (String) serviceRequest
						.getAttribute("exportDate");
				String exportTime = (String) serviceRequest
				.getAttribute("exportTime");
				
				Date operationDate = new Date();
				if (exportDate != null && !exportDate.trim().equals("")
						&& exportTime != null) {
					try {
						DateFormat dateFormat = new SimpleDateFormat(
								DATE_FORMAT);
						if (exportTime != null){
							operationDate = dateFormat.parse(exportDate.substring(0,10) + " " + exportTime);
						} else {
							operationDate = dateFormat.parse(exportDate);
						}
					} catch (ParseException e) {
						logger.debug(e.getLocalizedMessage());
					}
				}
				String operationID = (String) serviceRequest
						.getAttribute("operation_id");
				// create a file in the temp folder
				String fileName = FileImportUtil.importFile(serviceRequest,
						getErrorHandler());
				// extract the file
				if (fileName != null) {
					try {
						FileImportCoordinator fileImportCoordinator = new FileImportCoordinator();
						fileImportCoordinator.init(Integer
								.parseInt(operationID), fileName, xPath,
								operationDate, FileExportUtil
										.getExportTempFolderPath());
						fileImportCoordinator.run(null);
					} catch (EMFUserError e) {
						getErrorHandler().addError(e);
					} finally {
						FileImportUtil.deleteFile(fileName);
					}
				}
			} else {
				restoreValue(serviceRequest, serviceResponse);
			}
		}
	}

	private boolean hasValidationError() {
		boolean toReturn = false;
		ValidationCoordinator.validate("PAGE", VALIDATION_PAGE, this);
		EMFErrorHandler errorHandler = getErrorHandler();
		Collection errors = errorHandler.getErrors();

		if (errors != null && errors.size() > 0) {
			Iterator iterator = errors.iterator();
			while (iterator.hasNext()) {
				Object error = iterator.next();
				if (error instanceof EMFValidationError) {
					toReturn = true;
				}
			}
		}
		return toReturn;
	}

	private void restoreValue(final SourceBean serviceRequest,
			final SourceBean serviceResponse) throws SourceBeanException {
		String xPath = (String) serviceRequest.getAttribute("xPath");
		String exportDate = (String) serviceRequest.getAttribute("exportDate");
		String operationID = (String) serviceRequest
				.getAttribute("operation_id");
		if (exportDate != null && !exportDate.trim().equals("")) {
			try {
				DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
				Date date = dateFormat.parse(exportDate);
				serviceResponse.setAttribute("exportDate", date);
			} catch (ParseException e) {
				logger.debug(e.getLocalizedMessage());
			}
		}
		serviceResponse.setAttribute("xPath", xPath);
		serviceResponse.setAttribute("operation_id", operationID);
	}
}
