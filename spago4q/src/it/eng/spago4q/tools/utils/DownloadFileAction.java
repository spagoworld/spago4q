/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.tools.utils;

import it.eng.spago.base.SourceBean;
import it.eng.spago.dispatching.action.AbstractHttpAction;
import it.eng.spagobi.commons.utilities.GeneralUtilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class DownloadFileAction extends AbstractHttpAction {

	private static Logger logger = Logger.getLogger(DownloadFileAction.class);

	public void service(final SourceBean request, final SourceBean response)
			throws Exception {
		try {
			freezeHttpResponse();
			HttpServletRequest httpRequest = getHttpRequest();
			HttpServletResponse httpResponse = getHttpResponse();
			String operation = (String) request.getAttribute("OPERATION");
			if ((operation != null)
					&& (operation.equalsIgnoreCase("downloadExportFile"))) {
				manageDownloadExportFile(httpRequest, httpResponse);
			}
		} finally {
			logger.debug("OUT");
		}
	}

	private void manageDownloadExportFile(final HttpServletRequest request,
			final HttpServletResponse response) {
		logger.debug("IN");
		try {
			String exportFileName = (String) request.getParameter("FILE_NAME");
			String folderPath = (String) request.getParameter("FILE_DIR");
			String fileExtension = (String) request
					.getParameter("FILE_EXTENSION");
			manageDownload(exportFileName, fileExtension, folderPath, response,
					true, true);
		} catch (Exception e) {
			logger.error("Error while downloading export file", e);
		} finally {
			logger.debug("OUT");
		}

	}

	private void manageDownload(final String fileName,
			final String fileExtension, final String folderPath,
			final HttpServletResponse response, final boolean deleteFile,
			final boolean deleteDir) {
		logger.debug("IN");
		try {
			File exportedFile = new File(folderPath + "/" + fileName
					+ fileExtension);
			response.setHeader("Content-Disposition", "attachment; filename=\""
					+ fileName + fileExtension + "\";");
			byte[] exportContent = "".getBytes();
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(exportedFile);
				exportContent = GeneralUtilities
						.getByteArrayFromInputStream(fis);
			} catch (IOException ioe) {
				logger.error("Cannot get bytes of the exported file", ioe);
			}
			response.setContentLength(exportContent.length);
			response.getOutputStream().write(exportContent);
			response.getOutputStream().flush();
			if (fis != null) {
				fis.close();
			}
			if (deleteFile) {
				exportedFile.delete();
			}
			if (deleteDir) {
				new File(folderPath).delete();
			}
		} catch (SecurityException se){
			logger.error("Cannot delete file", se);
		} catch (IOException ioe) {
			logger.error("Cannot flush response", ioe);
		} finally {
			logger.debug("OUT");
		}
	}

}
