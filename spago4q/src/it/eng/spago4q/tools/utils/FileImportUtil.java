/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.tools.utils;

import it.eng.spago.base.SourceBean;
import it.eng.spago.configuration.ConfigSingleton;
import it.eng.spago.error.EMFErrorHandler;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago.validation.EMFValidationError;
import it.eng.spagobi.commons.utilities.GeneralUtilities;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.log4j.Logger;

public final class FileImportUtil {
	private static final Logger LOGGER = Logger.getLogger(FileImportUtil.class);
	private static final int MAX_DEFAULT_IMPORT_FILE_SIZE = 5242880;

	public static final String OPERATION_XML = "operation.xml";
	public static final String DATASOURCE_XML = "dataSource.xml";
	public static final String INTERFACETYPE_XML = "interfaceType.xml";
	public static final String SCRIPT_XML = "script.xml";

	private FileImportUtil(){
	}
	
	// saveFile
	public static String importFile(final SourceBean serviceRequest,
			final EMFErrorHandler emfErrorHandler) throws EMFUserError {
		String toReturn = null;
		// read the zip file
		FileItem uploaded = (FileItem) serviceRequest
		.getAttribute("UPLOADED_FILE");
		String contentType = uploaded.getContentType();
		byte[] zipContent = readFile(serviceRequest, emfErrorHandler);

		if(zipContent != null){
			toReturn = readXmlContent(zipContent,contentType);
		}
		return toReturn;
	}

	public static Map<String, String> importOperationFiles(
			final SourceBean serviceRequest,
			final EMFErrorHandler emfErrorHandler) throws EMFUserError {
		// read the zip file
		byte[] zipContent = readFile(serviceRequest, emfErrorHandler);
		Map<String, String> toReturn = readOperationFiles(zipContent);
		return toReturn;
	}

	private static Map<String, String> readOperationFiles(
			final byte[] zipContent) {
		Map<String, String> toReturn = new HashMap<String, String>();
		File tempDirectory = new File(getImportTempFolderPath());
		tempDirectory.mkdir();
		Long timeStamp = new Date().getTime();
		File tempOperationDirectory = new File(getImportTempFolderPath() + "/"
				+ timeStamp.toString());
		tempOperationDirectory.mkdir();
		try {
			ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(
					zipContent));
			ZipEntry entry;
			entry = zis.getNextEntry();
			while (entry != null) {
				String fileName = entry.getName();
				if (fileName != null
						&& (fileName.equals(OPERATION_XML)
								|| fileName.equals(DATASOURCE_XML)
								|| fileName.equals(INTERFACETYPE_XML) || fileName
								.equals(SCRIPT_XML))) {
					File file = new File(tempOperationDirectory, fileName);
					copyInputStream(zis, new BufferedOutputStream(
							new FileOutputStream(file)));
					toReturn.put(fileName, file.getAbsolutePath());
				}
				entry = zis.getNextEntry();
			}
			zis.close();
		} catch (IOException e) {
			LOGGER.warn("Error while import a file from a zip:"
					+ e.getLocalizedMessage());
		}
		return toReturn;

	}

	private static byte[] readFile(final SourceBean serviceRequest,
			final EMFErrorHandler emfErrorHandler) {
		byte[] content = null;
		FileItem uploaded = (FileItem) serviceRequest
				.getAttribute("UPLOADED_FILE");
		if (uploaded != null) {
			if (uploaded.getSize() == 0) {
				EMFValidationError error = new EMFValidationError(
						EMFErrorSeverity.ERROR, "uploadFile", "201");
				emfErrorHandler.addError(error);
			}
			int maxSize = getImportFileMaxSize();
			if (uploaded.getSize() > maxSize) {
				EMFValidationError error = new EMFValidationError(
						EMFErrorSeverity.ERROR, "uploadFile", "202");
				emfErrorHandler.addError(error);
			}
			if (uploaded.getSize() > 0) {
				try {
					content = uploaded.get();
				} catch (Exception e) {
					LOGGER.debug(e.getLocalizedMessage());
				}
			}
		}
		return content;
	}

	private static String readXmlContent(final byte[] zipContent,String contentType) {
		String toReturn = null;
		File tempDirectory = new File(getImportTempFolderPath());
		tempDirectory.mkdir();
		try {
			if(contentType != null && contentType.trim().equals("text/xml")){
				File file = new File(tempDirectory, "s4qimport"
						+ new Date().getTime() + ".xml");
				copyInputStream(new ByteArrayInputStream(
						zipContent), new BufferedOutputStream(
						new FileOutputStream(file)));
				toReturn = file.getAbsolutePath();
			}
			else{
			ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(
					zipContent));
			ZipEntry entry;
			entry = zis.getNextEntry();
			if (entry != null) {
				File file = new File(tempDirectory, "s4qimport"
						+ new Date().getTime() + ".xml");
				copyInputStream(zis, new BufferedOutputStream(
						new FileOutputStream(file)));
				zis.close();
				toReturn = file.getAbsolutePath();
			}
			}
		} catch (IOException e) {
			LOGGER.warn("Error while import a file from a zip:"
					+ e.getLocalizedMessage());
		}
		return toReturn;
	}

	public static boolean deleteFile(final String fileName) {
		boolean success = (new File(fileName)).delete();
		return success;
	}

	private static int getImportFileMaxSize() {
		LOGGER.debug("IN");
		int toReturn = MAX_DEFAULT_IMPORT_FILE_SIZE;
		try {
			ConfigSingleton serverConfig = ConfigSingleton.getInstance();
			SourceBean maxSizeSB = (SourceBean) serverConfig
					.getAttribute("SPAGO4Q.IMPORT.IMPORT_FILE_MAX_SIZE");
			if (maxSizeSB != null) {
				String maxSizeStr = (String) maxSizeSB.getCharacters();
				LOGGER.debug("Configuration found for max import file size: "
						+ maxSizeStr);
				Integer maxSizeInt = new Integer(maxSizeStr);
				toReturn = maxSizeInt.intValue();
			} else {
				LOGGER.debug("No configuration found for max import file size");
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving max import file size", e);
			LOGGER.debug("Considering default value "
					+ MAX_DEFAULT_IMPORT_FILE_SIZE);
			toReturn = MAX_DEFAULT_IMPORT_FILE_SIZE;
		}
		LOGGER.debug("OUT: max size = " + toReturn);
		return toReturn;
	}

	public static String getImportTempFolderPath() {
		LOGGER.debug("IN");
		String toReturn = null;
		try {
			ConfigSingleton serverConfig = ConfigSingleton.getInstance();
			SourceBean importerSB = (SourceBean) serverConfig
					.getAttribute("SPAGO4Q.IMPORT.IMPORT_TEMP_FOLDER");
			toReturn = (String) importerSB.getCharacters();
			toReturn = GeneralUtilities.checkForSystemProperty(toReturn);
			if (!toReturn.startsWith("/") && toReturn.charAt(1) != ':') {
				String root = ConfigSingleton.getRootPath();
				toReturn = root + "/" + toReturn;
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving import temporary folder path",
					e);
		} finally {
			LOGGER.debug("OUT: import temporary folder path = " + toReturn);
		}
		return toReturn;
	}

	/**
	 * Copy input stream.
	 * 
	 * @param in
	 *            the in
	 * @param out
	 *            the out
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static final void copyInputStream(final InputStream in,
			final OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		int len;
		while ((len = in.read(buffer)) >= 0) {
			out.write(buffer, 0, len);
		}
		out.close();
	}
}
