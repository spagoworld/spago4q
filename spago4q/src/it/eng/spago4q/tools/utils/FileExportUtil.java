/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.tools.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;

import it.eng.spago.base.SourceBean;
import it.eng.spago.configuration.ConfigSingleton;
import it.eng.spagobi.commons.utilities.GeneralUtilities;

public final class FileExportUtil {

	private static final Logger LOGGER = Logger.getLogger(FileExportUtil.class);

	private FileExportUtil() {
	};

	public static String getExportTempFolderPath() {
		LOGGER.debug("IN");
		String toReturn = null;
		try {
			ConfigSingleton serverConfig = ConfigSingleton.getInstance();
			SourceBean importerSB = (SourceBean) serverConfig
					.getAttribute("SPAGO4Q.EXPORT.EXPORT_TEMP_FOLDER");
			toReturn = (String) importerSB.getCharacters();
			toReturn = GeneralUtilities.checkForSystemProperty(toReturn);
			if (!toReturn.startsWith("/") && toReturn.charAt(1) != ':') {
				String root = ConfigSingleton.getRootPath();
				toReturn = root + "/" + toReturn;
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving export temporary folder path",
					e);
		} finally {
			LOGGER.debug("OUT: export temporary folder path = " + toReturn);
		}
		return toReturn;
	}

	public static String createExportFolder(final String exportFolderName) {
		File exportDirectory = new File(getExportTempFolderPath());
		if (!exportDirectory.exists()) {
			exportDirectory.mkdir();
		}
		File temporaryFolder = new File(exportDirectory.getAbsolutePath() + "/"
				+ exportFolderName);
		if (!temporaryFolder.exists()) {
			temporaryFolder.mkdir();
		}
		return temporaryFolder.getAbsolutePath();
	}

	public static String createZipFile(final String exportArchiveName,
			final String[] filesToExport, final String folderPath) {
		// Create a buffer for reading the files
		String toReturn = null;
		byte[] buf = new byte[1024];
		try {
			// Create the ZIP file
			String outFilename = folderPath + exportArchiveName + ".zip";
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(
					outFilename));
			// Compress the files
			for (int i = 0; i < filesToExport.length; i++) {
				FileInputStream in = new FileInputStream(folderPath
						+ filesToExport[i]);
				// Add ZIP entry to output stream.
				out.putNextEntry(new ZipEntry(filesToExport[i]));
				// Transfer bytes from the file to the ZIP file
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				// Complete the entry
				out.closeEntry();
				in.close();
			}
			// Complete the ZIP file
			out.close();
			toReturn = outFilename;
		} catch (IOException e) {
			LOGGER.debug(e.getLocalizedMessage());
		}
		return toReturn;
	}

	public static void cleanExportFiles(final String[] filesToExport,
			final String folderPath) {
		for (int i = 0; i < filesToExport.length; i++) {
			File exportFile = new File(folderPath + filesToExport[i]);
			if (exportFile.exists()) {
				exportFile.delete();
			}
		}
	}
}
