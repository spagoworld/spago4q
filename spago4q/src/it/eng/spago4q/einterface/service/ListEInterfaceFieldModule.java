/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.einterface.service;

import it.eng.spago.base.SourceBean;
import it.eng.spago.base.SourceBeanException;
import it.eng.spago.error.EMFErrorHandler;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.InterfaceField;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spagobi.kpi.utils.AbstractConfigurableListModule;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

public class ListEInterfaceFieldModule extends AbstractConfigurableListModule{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1721506662537299465L;
	private static transient Logger logger = Logger
			.getLogger(ListEInterfaceFieldModule.class);

	@Override
	public void service(final SourceBean request, final SourceBean response)
			throws Exception {

		super.service(request, response);

		String sourceTypeId = (String) request.getAttribute("TYPE_ID");

		HashMap parametersMap = (HashMap) response
				.getAttribute("PARAMETERS_MAP");
		if (parametersMap == null) {
			parametersMap = new HashMap();
			parametersMap.put("TYPE_ID", sourceTypeId);
			response.setAttribute("PARAMETERS_MAP", parametersMap);
		} else {
			parametersMap.put("TYPE_ID", sourceTypeId);
			response.updAttribute("PARAMETERS_MAP", parametersMap);
		}
	}

	@Override
	protected List getObjectList(final SourceBean request) {
		String fieldOrder = (String) request.getAttribute("FIELD_ORDER");
		String typeOrder = (String) request.getAttribute("TYPE_ORDER");
		List result = null;
		try {
			String sourceTypeId = (String) request.getAttribute("TYPE_ID");
			if (sourceTypeId != null && !sourceTypeId.trim().equals("")) {
				IES4QFKDAO iEInterfaceFieldDAO = (IES4QFKDAO) DAOFactory
						.getDAO("EInterfaceFieldDAO");
				result = iEInterfaceFieldDAO.loadObjectList(Integer
						.parseInt(sourceTypeId), fieldOrder, typeOrder);
			}
		} catch (EMFUserError e) {
			logger.error(e);
		}
		return result;
	}

	@Override
	protected void setRowAttribute(final SourceBean rowSB, final Object obj)
			throws SourceBeanException {
		InterfaceField aInterfaceField = (InterfaceField) obj;
		rowSB.setAttribute("ID", aInterfaceField.getId());
		rowSB.setAttribute("NAME", aInterfaceField.getName());
		rowSB.setAttribute("TYPE", aInterfaceField.getFieldType());
		rowSB.setAttribute("SENSIBLE", aInterfaceField.getSensible());
		rowSB.setAttribute("KEY",aInterfaceField.getKeyField());
	}

	@Override
	public boolean delete(final SourceBean request, final SourceBean response) {
		boolean toReturn = false;
		String id = (String) request.getAttribute("ID");
		try {
			IES4QFKDAO interfaceFieldDAO = (IES4QFKDAO) DAOFactory
					.getDAO("EInterfaceFieldDAO");
			toReturn = interfaceFieldDAO.deleteObject(Integer.parseInt(id));
			toReturn = true;
		} catch (NumberFormatException e) {
			EMFErrorHandler engErrorHandler = getErrorHandler();
			engErrorHandler.addError(new EMFUserError(EMFErrorSeverity.WARNING,
					"10012", "component_spago4q_messages"));
		} catch (EMFUserError e) {
			EMFErrorHandler engErrorHandler = getErrorHandler();
			engErrorHandler.addError(e);
		}

		return toReturn;
	}

}
