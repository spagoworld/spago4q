/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.einterface.utils;

import it.eng.spago.base.SourceBean;
import it.eng.spago4q.bo.DomainValue;
import it.eng.spago4q.bo.InterfaceField;
import it.eng.spago4q.utilities.DetailS4Q2ObjectUtil;

public class DetailEDomainValueUtil extends DetailS4Q2ObjectUtil {

	private static final long serialVersionUID = 3609143170573514705L;
	private static final String DAO_NAME = "EDomainValueDAO";
	private static final String OBJ_NAME = "EDomainValue";

	@Override
	protected String getDAOName() {
		return DAO_NAME;
	}

	@Override
	protected Object getObjectFromRequest(final SourceBean serviceRequest) {
		DomainValue toReturn = new DomainValue();
		String value = (String) serviceRequest.getAttribute("value");

		String interfaceTypeId = (String) serviceRequest
				.getAttribute("FIELD_ID");

		toReturn.setValue(value);

		InterfaceField interfaceField = new InterfaceField();

		interfaceField.setId(Integer.parseInt(interfaceTypeId));

		toReturn.setInterfaceField(interfaceField);

		return toReturn;
	}

	@Override
	protected String getObjectName() {
		return OBJ_NAME;
	}

	@Override
	protected void setObjectId(final Object object, final Integer id) {
		((DomainValue) object).setId(id);
	}

}
