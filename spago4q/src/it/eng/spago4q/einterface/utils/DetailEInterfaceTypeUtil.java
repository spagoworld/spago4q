/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.einterface.utils;

import it.eng.spago.base.RequestContainer;
import it.eng.spago.base.SessionContainer;
import it.eng.spago.base.SourceBean;
import it.eng.spago.error.EMFUserError;
import it.eng.spago.security.IEngUserProfile;
import it.eng.spago4q.bo.InterfaceType;
import it.eng.spago4q.extractors.FactTableManager;
import it.eng.spago4q.source.utils.TestMessage;
import it.eng.spago4q.utilities.DetailS4Q1ObjectUtil;

public class DetailEInterfaceTypeUtil extends DetailS4Q1ObjectUtil {

	private static final long serialVersionUID = -2976787960422492310L;
	private static final String DAO_NAME = "EInterfaceTypeDAO";
	private static final String OBJ_NAME = "EInterfaceType";
	private static final String CREATE_TABLE = "CREATE";
	private static final String DROP_TABLE = "DROP";

	private TestMessage message;

	@Override
	protected String getDAOName() {
		return DAO_NAME;
	}

	@Override
	protected Object getObjectFromRequest(final SourceBean serviceRequest) {
		InterfaceType toReturn = new InterfaceType();
		String name = (String) serviceRequest.getAttribute("name");
		String description = (String) serviceRequest
				.getAttribute("description");
		String tableName = (String) serviceRequest.getAttribute("tablename");
		String filterClass = (String) serviceRequest.getAttribute("filterClass");
		String sDataSourceId = (String) serviceRequest
				.getAttribute("DATASOURCE");
		Integer dataSourceId = null;

		if (sDataSourceId != null && (!sDataSourceId.equals("-1"))) {
			dataSourceId = Integer.parseInt(sDataSourceId);
		}
		toReturn.setName(name);
		toReturn.setDescription(description);
		toReturn.setTableName(tableName);
		toReturn.setFilterClass(filterClass);
		toReturn.setDataSourceId(dataSourceId);
		return toReturn;
	}

	@Override
	public void updateObjectFromRequest(final RequestContainer requestContainer, final SourceBean serviceRequest,
			final Integer id) throws Exception {
		super.updateObjectFromRequest(requestContainer,serviceRequest, id);
		String tableOperation = (String) serviceRequest
				.getAttribute("TABLEOPERATION");
    	SessionContainer aSessionContainer = requestContainer
				.getSessionContainer();
		SessionContainer permanentSession = aSessionContainer
				.getPermanentContainer();
		IEngUserProfile userProfile = (IEngUserProfile) permanentSession
				.getAttribute(IEngUserProfile.ENG_USER_PROFILE);
    	
		if (tableOperation != null
				&& tableOperation.toUpperCase().equals(CREATE_TABLE)) {
			// create or alter table
			try {
				FactTableManager factTableManager = new FactTableManager(id);
				factTableManager.saveFactTable(id,userProfile);
				message = new TestMessage(true, "FT.create.ok");
			} catch (EMFUserError e) {
				message = new TestMessage(false, "FT.create.error");
				throw e;
			}
		}
		if (tableOperation != null
				&& tableOperation.toUpperCase().equals(DROP_TABLE)) {
			// drop table
			try {
				FactTableManager factTableManager = new FactTableManager(id);
				factTableManager.dropFactTable(id,userProfile);
				message = new TestMessage(true, "FT.drop.ok");
			} catch (EMFUserError e) {
				message = new TestMessage(false, "FT.drop.error");
				throw e;
			}
		}
	}

	@Override
	protected String getObjectName() {
		return OBJ_NAME;
	}

	@Override
	protected void setObjectId(final Object object, final Integer id) {
		((InterfaceType) object).setId(id);
	}

	@Override
	public void selectObject(final Integer id, final SourceBean serviceResponse)
			throws Exception {
		super.selectObject(id, serviceResponse);
		if (message != null) {
			serviceResponse.setAttribute("MESSAGETOSHOW", message);
		}
	}
}
