/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.einterface.utils;

import it.eng.spago.base.SourceBean;
import it.eng.spago4q.bo.InterfaceField;
import it.eng.spago4q.bo.InterfaceType;
import it.eng.spago4q.utilities.DetailS4Q2ObjectUtil;

public class DetailEInterfaceFieldUtil extends DetailS4Q2ObjectUtil {

	private static final String DAO_NAME = "EInterfaceFieldDAO";
	private static final String OBJ_NAME = "EInterfaceField";

	@Override
	protected String getDAOName() {
		return DAO_NAME;
	}

	@Override
	protected Object getObjectFromRequest(final SourceBean serviceRequest) {
		InterfaceField toReturn = new InterfaceField();
		String name = (String) serviceRequest.getAttribute("name");
		String fieldType = (String) serviceRequest.getAttribute("fieldType");
		String sensibileString = (String) serviceRequest
				.getAttribute("sensible");
		String keyFieldString = (String) serviceRequest
		.getAttribute("keyField");
		
		Boolean sensible = false;
		if (sensibileString != null) {
			sensible = true;
		}
		
		Boolean keyField = false;
		if (keyFieldString != null) {
			keyField = true;
		}

		String interfaceTypeId = (String) serviceRequest
				.getAttribute("Type_Id");

		toReturn.setName(name);
		toReturn.setFieldType(fieldType);
		toReturn.setSensible(sensible);
		toReturn.setKeyField(keyField);
		InterfaceType interfaceType = new InterfaceType();

		interfaceType.setId(Integer.parseInt(interfaceTypeId));

		toReturn.setInterfaceType(interfaceType);

		return toReturn;
	}

	@Override
	protected String getObjectName() {
		return OBJ_NAME;
	}

	@Override
	protected void setObjectId(final Object object, final Integer id) {
		((InterfaceField) object).setId(id);
	}
}
