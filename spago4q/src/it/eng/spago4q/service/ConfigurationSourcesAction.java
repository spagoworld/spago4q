/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.service;

import it.eng.spago.base.SourceBean;
import it.eng.spago.dispatching.action.AbstractAction;
import it.eng.spago.tracing.TracerSingleton;
import it.eng.spago4q.utilities.Spago4QConstants;

public class ConfigurationSourcesAction extends AbstractAction {

	public void service(final SourceBean serviceRequest, final SourceBean serviceResponse) throws Exception {
		TracerSingleton.log(Spago4QConstants.LOGGER, TracerSingleton.DEBUG , "ConfigurationSourcesAction");
	}

}
