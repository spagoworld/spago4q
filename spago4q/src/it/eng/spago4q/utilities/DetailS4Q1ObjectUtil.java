/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.utilities;

import java.io.Serializable;

import it.eng.spago.base.RequestContainer;
import it.eng.spago.base.SourceBean;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QDAO;
import it.eng.spagobi.commons.constants.SpagoBIConstants;

public abstract class DetailS4Q1ObjectUtil implements Serializable {

	private static final long serialVersionUID = -2903902150223338107L;

	public void selectObject(final Integer id, final SourceBean serviceResponse)
			throws Exception {
		IES4QDAO iEObjectDAO = (IES4QDAO) DAOFactory.getDAO(getDAOName());
		Object toReturn = iEObjectDAO.loadObjectById(id);
		serviceResponse.setAttribute(getObjectName(), toReturn);

	}

	public void updateObjectFromRequest(
			final RequestContainer requestContainer,
			final SourceBean serviceRequest, final Integer id) throws Exception {
		Object object = getObjectFromRequest(serviceRequest);
		setObjectId(object, id);
		IES4QDAO iEObjectDAO = (IES4QDAO) DAOFactory.getDAO(getDAOName());
		iEObjectDAO.modifyObject(object);
	}

	public void restoreObject(final Integer id,
			final SourceBean serviceRequest, final SourceBean serviceResponse)
			throws Exception {
		Object toReturn = getObjectFromRequest(serviceRequest);
		if (id != null) {
			setObjectId(toReturn, id);
		}
		serviceResponse.setAttribute(getObjectName(), toReturn);
	}

	protected abstract Object getObjectFromRequest(SourceBean serviceRequest);

	public void newObject(final SourceBean serviceRequest,
			final SourceBean serviceResponse) throws Exception {
		Object toCreate = getObjectFromRequest(serviceRequest);
		IES4QDAO iEObjectDAO = (IES4QDAO) DAOFactory.getDAO(getDAOName());
		Integer resourceId = iEObjectDAO.insertObject(toCreate);

		serviceResponse.setAttribute("ID", resourceId);
		serviceResponse.setAttribute("MESSAGE", SpagoBIConstants.DETAIL_SELECT);
		selectObject(resourceId.intValue(), serviceResponse);
	}

	protected abstract void setObjectId(Object object, Integer id);

	protected abstract String getObjectName();

	protected abstract String getDAOName();

}
