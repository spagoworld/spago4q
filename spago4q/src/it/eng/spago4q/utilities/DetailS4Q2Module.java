/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.utilities;

import it.eng.spago.base.SourceBean;
import it.eng.spago.dispatching.module.detail.impl.DefaultDetailModule;
import it.eng.spago.dispatching.service.detail.impl.DelegatedDetailService;
import it.eng.spago.error.EMFErrorHandler;
import it.eng.spago.validation.EMFValidationError;
import it.eng.spago.validation.coordinator.ValidationCoordinator;
import it.eng.spagobi.commons.constants.SpagoBIConstants;

import java.util.Collection;
import java.util.Iterator;

public abstract class DetailS4Q2Module extends DefaultDetailModule {

	protected abstract DetailS4Q2ObjectUtil getDetailObjectUtil();

	protected abstract String getValidationPage();

	public void service(final SourceBean request, final SourceBean response)
			throws Exception {

		boolean validationError = false;
		String message = (String) request.getAttribute("MESSAGE");
		if (message == null) {
			message = SpagoBIConstants.DETAIL_SELECT;
		}

		// VALIDATION
		validationError = hasValidationError(message);

		// DETAIL_SELECT
		if (message.equalsIgnoreCase(SpagoBIConstants.DETAIL_SELECT)) {
			String idObject = (String) request.getAttribute("ID");
			getDetailObjectUtil().selectObject(Integer.parseInt(idObject),
					response);
		}

		// DETAIL_UPDATE
		if (message.equalsIgnoreCase(DelegatedDetailService.DETAIL_UPDATE)) {

			String idObject = (String) request.getAttribute("ID");
			response.setAttribute("ID", idObject);
			response.setAttribute("MESSAGE", SpagoBIConstants.DETAIL_SELECT);
			if (!validationError) {
				getDetailObjectUtil().updateObjectFromRequest(request,
						Integer.parseInt(idObject));
				getDetailObjectUtil().selectObject(Integer.parseInt(idObject),
						response);
			} else {
				getDetailObjectUtil().restoreObject(Integer.parseInt(idObject),
						request, response);
			}
		}

		// DETAIL_INSERT
		if (message.equalsIgnoreCase(DelegatedDetailService.DETAIL_INSERT)) {
			if (!validationError) {
				getDetailObjectUtil().newObject(request, response);
			} else {
				getDetailObjectUtil().restoreObject(null, request, response);
			}
		}
	}

	private boolean hasValidationError(final String message) {
		boolean toReturn = false;
		if (message.equalsIgnoreCase(DelegatedDetailService.DETAIL_UPDATE)
				|| message
						.equalsIgnoreCase(DelegatedDetailService.DETAIL_INSERT)) {
			ValidationCoordinator.validate("PAGE", getValidationPage(), this);

			EMFErrorHandler errorHandler = getErrorHandler();

			Collection errors = errorHandler.getErrors();

			if (errors != null && errors.size() > 0) {
				Iterator iterator = errors.iterator();
				while (iterator.hasNext()) {
					Object error = iterator.next();
					if (error instanceof EMFValidationError) {
						toReturn = true;
					}
				}
			}
		}
		return toReturn;
	}

}
 