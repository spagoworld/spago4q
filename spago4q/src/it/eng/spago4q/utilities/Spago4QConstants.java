/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.utilities;

import it.eng.spago4q.extractors.utilities.ExtractorConstants;

public final class Spago4QConstants {

	private Spago4QConstants() {}
	public static final String LOGGER = "Spago4Q";

	public static final String NAME_MODULE = "Spago4Q";

	public static final String SPAGO4Q_MODE = "SPAGO4Q.SPAGO4Q-MODE.mode";
	public static final String SPAGO4Q_MODE_PORTLET = "PORTLET";
	public static final String SPAGO4Q_MODE_WEB = "WEB";
	public static final String SPAGO4Q_MODE_HTTP = "HTTP";

	public static final String PAGE = "PAGE";
	public static final String MODULE = "MODULE";

	public static final String PAGE_MESSAGE = "MESSAGE";
	public static final String MESSAGE_LIST = "LIST";
	public static final String MESSAGE_DETAIL = "DETAIL";
	public static final String MESSAGE_VIEW = "VIEW";
	public static final String MESSAGE_EDIT = "EDIT";
	public static final String MESSAGE_DELETE = "DELETE";

	public static final String SPAGO4Q_LANGUAGE_SUPPORTED_LANGUAGE = "SPAGO4Q.LANGUAGE_SUPPORTED.LANGUAGE";

	public static final String URL_BUILDER = "URL_BUILDER";
	public static final String MESSAGE_BUILDER = "MESSAGE_BUILDER";

	public static final String[] FIELD_TYPES_ARRAY = new String[] {
			ExtractorConstants.STRING, ExtractorConstants.INTEGER,
			ExtractorConstants.DOUBLE, ExtractorConstants.BOOLEAN,
			ExtractorConstants.DATE };
	
	public static final String[] SCRIPT_TYPES_ARRAY = new String[] {
		ExtractorConstants.GROOVY, ExtractorConstants.DATE_CONVERTER};
	
}
