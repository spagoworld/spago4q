/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.process.service;

import it.eng.spago.base.SourceBean;
import it.eng.spago.base.SourceBeanException;
import it.eng.spago.error.EMFErrorHandler;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EOperationField;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spagobi.kpi.utils.AbstractConfigurableListModule;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

public class ListEOperationFieldModule extends AbstractConfigurableListModule {

	private static final long serialVersionUID = 1094426006002612708L;
	private static transient Logger logger = Logger
			.getLogger(ListEOperationFieldModule.class);

	@Override
	public void service(final SourceBean request, final SourceBean response)
			throws Exception {

		super.service(request, response);

		String operationId = (String) request.getAttribute("OPERATION_ID");

		HashMap parametersMap = (HashMap) response
				.getAttribute("PARAMETERS_MAP");
		if (parametersMap == null) {
			parametersMap = new HashMap();
			parametersMap.put("OPERATION_ID", operationId);
			response.setAttribute("PARAMETERS_MAP", parametersMap);
		} else {
			parametersMap.put("OPERATION_ID", operationId);
			response.updAttribute("PARAMETERS_MAP", parametersMap);
		}
	}

	@Override
	protected List getObjectList(final SourceBean request) {
		String fieldOrder = (String) request.getAttribute("FIELD_ORDER");
		String typeOrder = (String) request.getAttribute("TYPE_ORDER");
		List result = null;
		try {
			String operationId = (String) request.getAttribute("OPERATION_ID");
			if (operationId != null && !operationId.trim().equals("")) {
				IES4QFKDAO iEDataSourceDAO = (IES4QFKDAO) DAOFactory
						.getDAO("EOperationFieldDAO");
				result = iEDataSourceDAO.loadObjectList(Integer
						.parseInt(operationId), fieldOrder, typeOrder);
			}
		} catch (EMFUserError e) {
			logger.error(e);
		}
		return result;
	}

	@Override
	protected void setRowAttribute(final SourceBean rowSB, final Object obj)
			throws SourceBeanException {
		EOperationField aOperationField = (EOperationField) obj;
		rowSB.setAttribute("ID", aOperationField.getId());
		rowSB.setAttribute("NAME", aOperationField.getName());
		if (aOperationField.getInterfaceField() != null) {
			rowSB.setAttribute("NAME_INTERFACE_FIELD", aOperationField
					.getInterfaceField().getName());
		} else {
			rowSB.setAttribute("NAME_INTERFACE_FIELD", "");
		}
	}

	@Override
	public boolean delete(final SourceBean request, final SourceBean response) {
		boolean toReturn = false;
		String id = (String) request.getAttribute("ID");
		try {
			IES4QFKDAO operationFieldDAO = (IES4QFKDAO) DAOFactory
					.getDAO("EOperationFieldDAO");
			toReturn = operationFieldDAO.deleteObject(Integer.parseInt(id));
			toReturn = true;
		} catch (NumberFormatException e) {
			EMFErrorHandler engErrorHandler = getErrorHandler();
			engErrorHandler.addError(new EMFUserError(EMFErrorSeverity.WARNING,
					"10012", "component_spago4q_messages"));
		} catch (EMFUserError e) {
			EMFErrorHandler engErrorHandler = getErrorHandler();
			engErrorHandler.addError(e);
		}

		return toReturn;
	}

}
