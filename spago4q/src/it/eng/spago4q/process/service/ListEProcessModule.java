/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.process.service;

import it.eng.spago.base.SourceBean;
import it.eng.spago.base.SourceBeanException;
import it.eng.spago.error.EMFErrorHandler;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EProcess;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QDAO;
import it.eng.spagobi.kpi.utils.AbstractConfigurableListModule;

import java.util.List;

import org.apache.log4j.Logger;


public class ListEProcessModule extends AbstractConfigurableListModule {

	private static final long serialVersionUID = -8216420013345351040L;
	private static transient Logger logger = Logger.getLogger(ListEProcessModule.class);

	protected List getObjectList(final SourceBean request) {
		List result = null;
		String fieldOrder = (String)request.getAttribute("FIELD_ORDER");
		String typeOrder = (String)request.getAttribute("TYPE_ORDER");
		try {
			IES4QDAO eProcessDao = (IES4QDAO)DAOFactory.getDAO("EProcessDAO");
			result = eProcessDao.loadObjectList(fieldOrder, typeOrder);
		} catch (EMFUserError e) {
			logger.error(e);
		}
		return result;
	}

	protected void setRowAttribute(final SourceBean rowSB, final Object obj)
			throws SourceBeanException {
		EProcess aProcess = (EProcess) obj;
		rowSB.setAttribute("name", aProcess.getName());
		rowSB.setAttribute("id", aProcess.getId());
	}
	
	@Override
	public boolean delete(final SourceBean request, final SourceBean response) {
		boolean toReturn = false;
		String id = (String) request.getAttribute("ID");
		try {
			IES4QDAO  iEProcessDAO = (IES4QDAO) DAOFactory.getDAO("EProcessDAO");
			toReturn = iEProcessDAO.deleteObject(Integer.parseInt(id));
			toReturn = true;
		} catch (NumberFormatException e) {
			EMFErrorHandler engErrorHandler = getErrorHandler();
			engErrorHandler.addError(new EMFUserError(EMFErrorSeverity.WARNING,
					"10012", "component_spago4q_messages"));
		} catch (EMFUserError e) {
			EMFErrorHandler engErrorHandler = getErrorHandler();
			engErrorHandler.addError(e);
		}

		return toReturn;
	}
}
