/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.process.service;

import it.eng.spago4q.process.utils.DetailOperationParameterUtil;
import it.eng.spago4q.utilities.DetailS4Q2Module;
import it.eng.spago4q.utilities.DetailS4Q2ObjectUtil;

public class DetailEOperationParameterModule extends DetailS4Q2Module{

	private static final String VALIDATION_PAGE = "EOperationParameterDetailPage";
	private DetailOperationParameterUtil detailOperationParameterUtil = new DetailOperationParameterUtil();

	@Override
	protected DetailS4Q2ObjectUtil getDetailObjectUtil() {
		return detailOperationParameterUtil;
	}

	@Override
	protected String getValidationPage() {
		return VALIDATION_PAGE;
	}

}
