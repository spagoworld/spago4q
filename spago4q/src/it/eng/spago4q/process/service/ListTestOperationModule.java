/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.process.service;

import it.eng.spago.base.SourceBean;
import it.eng.spago.base.SourceBeanException;
import it.eng.spago.dbaccess.sql.DataRow;
import it.eng.spago.dispatching.module.list.basic.AbstractBasicListModule;
import it.eng.spago.error.EMFErrorHandler;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFInternalError;
import it.eng.spago.error.EMFUserError;
import it.eng.spago.paginator.basic.ListIFace;
import it.eng.spago.paginator.basic.PaginatorIFace;
import it.eng.spago.paginator.basic.impl.GenericList;
import it.eng.spago.paginator.basic.impl.GenericPaginator;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.extractors.bo.ExtractorParameter;
import it.eng.spago4q.extractors.bo.GenericItemInterface;
import it.eng.spago4q.extractors.parameterfiller.DefaultExtractionParameterFiller;
import it.eng.spago4q.extractors.parameterfiller.ExtractionParameterFiller;
import it.eng.spago4q.extractors.utilities.OperationUtil;
import it.eng.spagobi.commons.constants.SpagoBIConstants;
import it.eng.spagobi.commons.services.DelegatedBasicListService;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

public class ListTestOperationModule extends AbstractBasicListModule {

	private static final long serialVersionUID = 7153710212209059634L;
	private static transient Logger logger = Logger
			.getLogger(ListTestOperationModule.class);
	private EMFErrorHandler errorHandler;
	public static final String MESSAGE_BUNDLE = "messages";

	public ListTestOperationModule() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.eng.spago.dispatching.service.list.basic.IFaceBasicListService#getList
	 * (it.eng.spago.base.SourceBean, it.eng.spago.base.SourceBean)
	 */
	public ListIFace getList(final SourceBean request, final SourceBean response)
			throws Exception {
		logger.debug("IN");
		errorHandler = getResponseContainer().getErrorHandler();
		ListIFace listIFace = null;
		try {
			listIFace = getTestResultList(request, response);

		} catch (EMFUserError eex) {
			errorHandler.addError(eex);
			return null;
		} catch (Exception ex) {
			EMFInternalError internalError = new EMFInternalError(
					EMFErrorSeverity.ERROR, ex);
			errorHandler.addError(internalError);
			return null;
		}
		return listIFace;
	}

	public void service(final SourceBean request, final SourceBean response)
			throws Exception {

		super.service(request, response);

		String processId = (String) request.getAttribute("PROCESS_ID");
		String id = (String) request.getAttribute("ID");
		HashMap parametersMap = (HashMap) response
				.getAttribute("PARAMETERS_MAP");
		if (parametersMap == null) {
			parametersMap = new HashMap();
			parametersMap.put("PROCESS_ID", processId);
			parametersMap.put("ID", id);
			response.setAttribute("PARAMETERS_MAP", parametersMap);
		} else {
			parametersMap.put("PROCESS_ID", processId);
			parametersMap.put("ID", id);
			response.updAttribute("PARAMETERS_MAP", parametersMap);
		}
	}

	/**
	 * Gets the test result list.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the test result list
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public ListIFace getTestResultList(final SourceBean request,
			final SourceBean response) throws Exception {
		logger.debug("IN");
		// define the spago paginator and list object
		PaginatorIFace paginator = new GenericPaginator();
		ListIFace list = new GenericList();

		String sOperationId = (String) request.getAttribute("ID");
		Integer operationId = Integer.parseInt(sOperationId);

		List<GenericItemInterface> result = executeOperation(operationId);
		Set<String> colNames = null;

		colNames = getColumnNames(result);

		// Build the list fill paginator
		fillPaginator(result, list, paginator);

		list.setPaginator(paginator);

		// build module configuration for the list
		buildModuleConfiguration(response, colNames);
		// filter the list
		addFilter(request, list);

		logger.debug("OUT");
		return list;
	}

	private void fillPaginator(final List<GenericItemInterface> result,
			final ListIFace list, final PaginatorIFace paginator)
			throws SourceBeanException {
		Set<String> keys = new HashSet<String>();
		for (Iterator<GenericItemInterface> it = result.iterator(); it
				.hasNext();) {
			GenericItemInterface genericItem = it.next();
			Set<String> currentKeys = genericItem.getKeys();
			if (currentKeys != null) {
				keys.addAll(currentKeys);
			}
		}
		for (Iterator<GenericItemInterface> it = result.iterator(); it
				.hasNext();) {
			GenericItemInterface genericItem = it.next();
			SourceBean rowSB = new SourceBean(DataRow.ROW_TAG);
			setRowAttribute(rowSB, genericItem, keys);
			if (rowSB != null) {
				paginator.addRow(rowSB);
			}
		}

	}

	private void setRowAttribute(final SourceBean rowSB,
			final GenericItemInterface genericItem, final Set<String> keys)
			throws SourceBeanException {
		// Set<String> keys = genericItem.getKeys();
		for (Iterator<String> iterator = keys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			String value = genericItem.getValue(key);
			if (value != null) {
				rowSB.setAttribute(key, value);
			} else {
				rowSB.setAttribute(key, "null");
			}
		}
	}

	private void addFilter(final SourceBean request, ListIFace list) {
		String valuefilter = (String) request
				.getAttribute(SpagoBIConstants.VALUE_FILTER);
		if (valuefilter != null) {
			String columnfilter = (String) request
					.getAttribute(SpagoBIConstants.COLUMN_FILTER);
			String typeFilter = (String) request
					.getAttribute(SpagoBIConstants.TYPE_FILTER);
			String typeValueFilter = (String) request
					.getAttribute(SpagoBIConstants.TYPE_VALUE_FILTER);
			list = DelegatedBasicListService.filterList(list, valuefilter,
					typeValueFilter, columnfilter, typeFilter,
					getResponseContainer().getErrorHandler());
		}
	}

	private List<GenericItemInterface> executeOperation(
			final Integer operationId) throws EMFUserError {
		List<GenericItemInterface> toReturn = null;

		IES4QFKDAO operationDAO = (IES4QFKDAO) DAOFactory
				.getDAO("EOperationDAO");
		EOperation operation = (EOperation) operationDAO
				.loadObjectById(operationId);

		ExtractionParameterFiller extractorParameterFiller = new DefaultExtractionParameterFiller();
        // set the operation
        extractorParameterFiller.setOperation(operation);
        // get data source parameters
        List<ExtractorParameter> dataSourceParameters = extractorParameterFiller
                .getDataSourceParameters();
        // get operations parameters
        List<ExtractorParameter> operationParameters = extractorParameterFiller
                .getOperationParameters();

        toReturn = OperationUtil
                .executeOperation(operation, dataSourceParameters,
                        operationParameters);

		return toReturn;
	}

	private Set<String> getColumnNames(final List<GenericItemInterface> values) {
		Set<String> toReturn = null;
		if (values != null && values.size() != 0) {
			GenericItemInterface firstGenericItem = values.get(0);
			toReturn = firstGenericItem.getKeys();
		}

		return toReturn;

	}

	private void buildModuleConfiguration(final SourceBean response,
			final Set<String> colNames) throws SourceBeanException {
		SourceBean config = new SourceBean("CONFIG");
		SourceBean queries = new SourceBean("QUERIES");
		config.setAttribute(queries);
		SourceBean columns = new SourceBean("COLUMNS");
		// if there's no colum name add a fake column to show that there's no
		// data
		if (colNames == null || colNames.size() == 0) {
			SourceBean column = new SourceBean("COLUMN");
			column.setAttribute("name", "No Result Found");
			columns.setAttribute(column);
		} else {
			Iterator iterColNames = colNames.iterator();
			while (iterColNames.hasNext()) {
				String colName = (String) iterColNames.next();
				SourceBean column = new SourceBean("COLUMN");
				column.setAttribute("name", colName);
				columns.setAttribute(column);
			}
		}

		SourceBean captions = new SourceBean("CAPTIONS");
		SourceBean buttons = new SourceBean("BUTTONS");
		config.setAttribute(columns);
		config.setAttribute(captions);
		config.setAttribute(buttons);
		response.setAttribute(config);
	}

}
