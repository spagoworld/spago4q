/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.process.service;

import it.eng.spago.base.SourceBean;
import it.eng.spago.base.SourceBeanException;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.ELog;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spagobi.kpi.utils.AbstractConfigurableListModule;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

public class ListELogModule extends AbstractConfigurableListModule {

	private static final long serialVersionUID = 3098193652321830051L;
	private static transient Logger logger = Logger
			.getLogger(ListELogModule.class);

	@Override
	public void service(final SourceBean request, final SourceBean response)
			throws Exception {

		super.service(request, response);		

		String operationId = (String) request.getAttribute("OPERATION_ID");

		HashMap parametersMap = (HashMap) response
				.getAttribute("PARAMETERS_MAP");
		if (parametersMap == null) {
			parametersMap = new HashMap();
			parametersMap.put("OPERATION_ID", operationId);
			response.setAttribute("PARAMETERS_MAP", parametersMap);
		} else {
			parametersMap.put("OPERATION_ID", operationId);
			response.updAttribute("PARAMETERS_MAP", parametersMap);
		}
	}

	@Override
	protected List getObjectList(final SourceBean request) {
		String fieldOrder = (String) request.getAttribute("FIELD_ORDER");
		String typeOrder = (String) request.getAttribute("TYPE_ORDER");
		List result = null;
		try {
			String operationId = (String) request.getAttribute("OPERATION_ID");
			if (operationId != null && !operationId.trim().equals("")) {
				IES4QFKDAO iELogDAO = (IES4QFKDAO) DAOFactory
						.getDAO("ELogDAO");
				result = iELogDAO.loadObjectList(Integer
						.parseInt(operationId), fieldOrder, typeOrder);
			}
		} catch (EMFUserError e) {
			logger.error(e);
		}
		return result;
	}

	@Override
	protected void setRowAttribute(final SourceBean rowSB, final Object obj)
			throws SourceBeanException {
		ELog aLog = (ELog) obj;
		Integer extracted = aLog.getExtracted();
		Integer inserted = aLog.getInserted();
		Date operationDate = aLog.getOperationDate();
		Date extractionDate = aLog.getExecutionDate();
		String archivePath = aLog.getArchivePath();
		Integer operationId = aLog.getOperationId();
		Boolean completed = aLog.getCompleted();
		Integer rejected = aLog.getRejected();
		
		rowSB.setAttribute("ID", aLog.getIdLog());
		
		if(extracted != null) {
			rowSB.setAttribute("EXTRACTED", extracted);
		} else {
			rowSB.setAttribute("EXTRACTED", "null");
		}
		if(inserted != null) {
			rowSB.setAttribute("INSERTED", inserted);
		} else {
			rowSB.setAttribute("INSERTED", "null");
		}
		if(rejected != null) {
			rowSB.setAttribute("REJECTED", rejected);
		} else{
			rowSB.setAttribute("REJECTED", "null");
		}
		if(operationDate != null){
			rowSB.setAttribute("OPERATION_DATE", operationDate);
		} else {
			rowSB.setAttribute("OPERATION_DATE", "null");
		}
		if(extractionDate != null){
			rowSB.setAttribute("EXTRACTION_DATE", extractionDate);
		} else {
			rowSB.setAttribute("EXTRACTION_DATE", "null");
		}
		
		if(completed != null){
			rowSB.setAttribute("COMPLETED", completed);
		} else {
			rowSB.setAttribute("COMPLETED", "not set");
		}
		
		if(archivePath != null){
			rowSB.setAttribute("ARCHIVE","set");
		} else {
			rowSB.setAttribute("ARCHIVE","not set");
		}
		if(operationId != null){
			rowSB.setAttribute("OPERATION_ID", operationId);	
		}
	}

}
