/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.process.service;

import it.eng.spago.base.SourceBean;
import it.eng.spago.dispatching.action.AbstractHttpAction;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.ELog;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spagobi.commons.utilities.GeneralUtilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class ELogArchiveAction extends AbstractHttpAction {

	private static Logger logger = Logger.getLogger(ELogArchiveAction.class);

	public void service(final SourceBean serviceRequest,
			final SourceBean serviceResponse) throws Exception {
		freezeHttpResponse();
		HttpServletResponse httpResponse = getHttpResponse();
		String logId = (String) serviceRequest.getAttribute("ID");
		Boolean rejected = new Boolean((String) serviceRequest.getAttribute("REJECTED"));
		String archivePath = null;
		if (rejected) {
			archivePath = getRejectedPath(Integer.parseInt(logId));
		} else {
			archivePath = getArchivePath(Integer.parseInt(logId));
		}
		if (archivePath != null) {
			manageDownload(archivePath, httpResponse);
		}
	}

	private String getArchivePath(final Integer logId) {
		String toReturn = null;
		try {
			IES4QFKDAO eLogDAO = (IES4QFKDAO) DAOFactory.getDAO("ELogDAO");
			ELog log = (ELog) eLogDAO.loadObjectById(logId);
			toReturn = log.getArchivePath();
		} catch (EMFUserError e) {
			logger.warn("Error. Impossible to load ELog object with id "
					+ logId + ":" + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return toReturn;
	}

	private String getRejectedPath(final Integer logId) {
		String toReturn = null;
		try {
			IES4QFKDAO eLogDAO = (IES4QFKDAO) DAOFactory.getDAO("ELogDAO");
			ELog log = (ELog) eLogDAO.loadObjectById(logId);
			toReturn = log.getRejectedArchivePath();
		} catch (EMFUserError e) {
			logger.warn("Error. Impossible to load ELog object with id "
					+ logId + ":" + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return toReturn;
	}

	private void manageDownload(final String fileName,
			final HttpServletResponse response) {
		logger.debug("IN");
		try {
			File exportedFile = new File(fileName);
			response.setHeader("Content-Disposition", "attachment; filename=\""
					+ exportedFile.getName() + "\";");
			byte[] exportContent = "".getBytes();
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(exportedFile);
				exportContent = GeneralUtilities
						.getByteArrayFromInputStream(fis);
			} catch (IOException ioe) {
				logger.error("Cannot get bytes of the exported file", ioe);
			}
			response.setContentLength(exportContent.length);
			response.getOutputStream().write(exportContent);
			response.getOutputStream().flush();
			if (fis != null) {
				fis.close();
			}
		} catch (IOException ioe) {
			logger.error("Cannot flush response", ioe);
		} finally {
			logger.debug("OUT");
		}
	}

}
