/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.process.utils;

import it.eng.spago.base.SourceBean;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.bo.EOperationField;
import it.eng.spago4q.bo.InterfaceField;
import it.eng.spago4q.bo.Script;
import it.eng.spago4q.utilities.DetailS4Q2ObjectUtil;

public class DetailOperationFieldUtil extends DetailS4Q2ObjectUtil {

	private static final String DAO_NAME = "EOperationFieldDAO";
	private static final String OBJ_NAME = "EOPERATIONFIELD";

	@Override
	protected String getDAOName() {
		return DAO_NAME;
	}

	@Override
	protected Object getObjectFromRequest(final SourceBean serviceRequest) {
		EOperationField toReturn = new EOperationField();
		String name = (String) serviceRequest.getAttribute("name");

		String operationId = (String) serviceRequest
				.getAttribute("Operation_Id");
		String interfaceFieldId = (String) serviceRequest
				.getAttribute("InterfaceField_Id");
		String scriptId = (String) serviceRequest.getAttribute("Script_Id");

		toReturn.setName(name);

		Script script = null;
		
		if(scriptId != null && (! scriptId.equals("-1"))) {
			script = new Script();
			script.setId(Integer.parseInt(scriptId));
		}
		if (interfaceFieldId != null){
			InterfaceField interfaceField = new InterfaceField();
			interfaceField.setId(Integer.parseInt(interfaceFieldId));
			toReturn.setInterfaceField(interfaceField);}

		EOperation operation = new EOperation();
		operation.setId(Integer.parseInt(operationId));


		toReturn.setEOperation(operation);
		toReturn.setScript(script);

		return toReturn;
	}

	@Override
	protected String getObjectName() {
		return OBJ_NAME;
	}

	@Override
	protected void setObjectId(final Object object, final Integer id) {
		((EOperationField) object).setId(id);
	}
}
