/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.process.utils;

import it.eng.spago.base.SourceBean;
import it.eng.spago4q.bo.DataSource;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.bo.EProcess;
import it.eng.spago4q.bo.InterfaceType;
import it.eng.spago4q.source.utils.SourceTypeUtil;
import it.eng.spago4q.source.utils.TestMessage;
import it.eng.spago4q.utilities.DetailS4Q2ObjectUtil;

public class DetailOperationUtil extends DetailS4Q2ObjectUtil {

	private static final long serialVersionUID = 8100468559976479484L;
	private static final String DAO_NAME = "EOperationDAO";
	private static final String OBJ_NAME = "EOperation";
	private TestMessage testMessage;

	@Override
	protected String getDAOName() {
		return DAO_NAME;
	}

	@Override
	protected Object getObjectFromRequest(final SourceBean serviceRequest) {
		EOperation toReturn = new EOperation();
		String name = (String) serviceRequest.getAttribute("name");
		String description = (String) serviceRequest
				.getAttribute("description");

		String processId = (String) serviceRequest.getAttribute("Process_Id");
		String interfaceTypeId = (String) serviceRequest
				.getAttribute("InterfaceType_Id");
		String dataSourceId = (String) serviceRequest
				.getAttribute("dataSource_Id");
		String archiveString = (String) serviceRequest.getAttribute("archive");
		String rejectedString = (String) serviceRequest
				.getAttribute("rejected");

		boolean archive = false;

		if (archiveString != null) {
			archive = true;
		}

		boolean rejected = false;
		if (rejectedString != null) {
			rejected = true;
		}

		toReturn.setName(name);
		toReturn.setDescription(description);
		toReturn.setArchive(archive);
		toReturn.setRejected(rejected);

		EProcess process = new EProcess();
		process.setId(Integer.parseInt(processId));

		InterfaceType interfaceType = null;
		if (interfaceTypeId != null) {
			interfaceType = new InterfaceType();
			interfaceType.setId(Integer.parseInt(interfaceTypeId));
		}

		DataSource dataSource = null;
		if (dataSourceId != null) {
			dataSource = new DataSource();
			dataSource.setId(Integer.parseInt(dataSourceId));
		}

		toReturn.setInterfaceType(interfaceType);
		toReturn.setDataSource(dataSource);
		toReturn.setEProcess(process);

		return toReturn;
	}

	@Override
	protected String getObjectName() {
		return OBJ_NAME;
	}

	@Override
	protected void setObjectId(final Object object, final Integer id) {
		((EOperation) object).setId(id);
	}

	@Override
	public void updateObjectFromRequest(final SourceBean serviceRequest,
			final Integer id) throws Exception {
		super.updateObjectFromRequest(serviceRequest, id);
		String testValue = (String) serviceRequest
				.getAttribute("TESTOPERATION");
		if (testValue != null && testValue.toUpperCase().equals("TEST")) {
			testMessage = SourceTypeUtil.testOperation(id);
		}
	}

	@Override
	public void selectObject(final Integer id, final SourceBean serviceResponse)
			throws Exception {
		super.selectObject(id, serviceResponse);
		if (testMessage != null) {
			serviceResponse.setAttribute("TESTRESULT", testMessage);
		}
	}

}
