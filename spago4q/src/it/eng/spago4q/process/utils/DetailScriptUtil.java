/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.process.utils;

import it.eng.spago.base.RequestContainer;
import it.eng.spago.base.SourceBean;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.Script;
import it.eng.spago4q.extractors.script.DateConverterScript;
import it.eng.spago4q.extractors.script.IMappingScript;
import it.eng.spago4q.extractors.script.ScriptCreator;
import it.eng.spago4q.extractors.utilities.ExtractorConstants;
import it.eng.spago4q.utilities.DetailS4Q1ObjectUtil;

import javax.script.ScriptException;

public class DetailScriptUtil extends DetailS4Q1ObjectUtil {

	private static final String DAO_NAME = "EScriptDAO";
	private static final String OBJ_NAME = "EScript";
	private static final String OPERATION = "OPERATION";
	public static final String TEST_VALUE = "TESTVALUE";
	public static final String TEST_RESULT = "TESTRESULT";
	private static final String TEST = "TEST";

	private String testValue = null;
	private String testResult = null;

	@Override
	protected String getDAOName() {
		return DAO_NAME;
	}

	@Override
	protected Object getObjectFromRequest(final SourceBean serviceRequest) {
		Script toReturn = new Script();
		String name = (String) serviceRequest.getAttribute("name");
		String description = (String) serviceRequest
				.getAttribute("description");
		String scriptType = (String) serviceRequest.getAttribute("scriptType");
		String script = (String) serviceRequest.getAttribute("script");

		toReturn.setName(name);
		toReturn.setDescription(description);
		toReturn.setScriptType(scriptType);
		toReturn.setScript(script);

		return toReturn;
	}

	@Override
	public void updateObjectFromRequest(
			final RequestContainer requestContainer,
			final SourceBean serviceRequest, final Integer id) throws Exception {
		super.updateObjectFromRequest(requestContainer, serviceRequest, id);

		String extraction = (String) serviceRequest.getAttribute(OPERATION);
		String script = (String) serviceRequest.getAttribute("script");
		String currTestValue = (String) serviceRequest.getAttribute(TEST_VALUE);
		String scriptType = (String) serviceRequest.getAttribute("scriptType");
		String currTestResult = null;
		if (extraction != null && extraction.toUpperCase().equals(TEST)) {
			// run script
			if (scriptType != null
					&& scriptType.equals(ExtractorConstants.GROOVY)) {
				try {
					IMappingScript mappingScript = ScriptCreator
							.createScriptObject(script);
					currTestResult = mappingScript.execute(currTestValue);
				} catch (ClassCastException e) {
					currTestResult = currTestValue;
					EMFUserError error = new EMFUserError(
							EMFErrorSeverity.ERROR, 10034);
					error = new EMFUserError(EMFErrorSeverity.ERROR, error
							.getDescription()
							+ e.getMessage());
					throw error;
				} catch (ScriptException e1) {
					currTestResult = currTestValue;
					EMFUserError error = new EMFUserError(
							EMFErrorSeverity.ERROR, 10034);
					error = new EMFUserError(EMFErrorSeverity.ERROR, error
							.getDescription()
							+ e1.getMessage());
					throw error;
				}
			}
			if (scriptType != null
					&& scriptType.equals(ExtractorConstants.DATE_CONVERTER)) {
				currTestResult = DateConverterScript.execute(currTestValue,
						script);
			}
		}

		// input
		this.testValue = currTestValue;
		// output
		this.testResult = currTestResult;
	}

	@Override
	public void selectObject(final Integer id, final SourceBean serviceResponse)
			throws Exception {
		super.selectObject(id, serviceResponse);
		Script toReturn = (Script) serviceResponse
				.getAttribute(getObjectName());
		toReturn.setTestValue(testValue);
		toReturn.setTestResult(testResult);
		serviceResponse.updAttribute(getObjectName(), toReturn);
	}

	@Override
	protected String getObjectName() {
		return OBJ_NAME;
	}

	@Override
	protected void setObjectId(final Object object, final Integer id) {
		((Script) object).setId(id);
	}

}
