/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.process.utils;

import it.eng.spago.base.RequestContainer;
import it.eng.spago.base.SessionContainer;
import it.eng.spago.base.SourceBean;
import it.eng.spago.security.IEngUserProfile;
import it.eng.spago4q.bo.EPeriodicity;
import it.eng.spago4q.bo.EProcess;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QDAO;
import it.eng.spago4q.extractors.OperationCoordinatorFactory;
import it.eng.spago4q.extractors.OperationCoordinatorInterface;
import it.eng.spago4q.extractors.scheduler.SchedulerSupplier;
import it.eng.spago4q.tools.utils.FileExportUtil;
import it.eng.spago4q.utilities.DetailS4Q1ObjectUtil;

public class DetailProcessUtil extends DetailS4Q1ObjectUtil {

	private static final String DAO_NAME = "EProcessDAO";
	private static final String OBJ_NAME = "EProcess";
	private static final String EXTRACTION = "EXTRACTION";

	@Override
	protected String getDAOName() {
		return DAO_NAME;
	}

	@Override
	protected Object getObjectFromRequest(final SourceBean serviceRequest) {
		EProcess toReturn = new EProcess();
		String name = (String) serviceRequest.getAttribute("name");
		String description = (String) serviceRequest
				.getAttribute("description");
		String coordinatorClass = (String) serviceRequest
				.getAttribute("coordinatorClass");
		String ePeriodicityId = (String) serviceRequest
				.getAttribute("PERIODICITY_ID");

		EPeriodicity ePeriodicity = null;
		if (ePeriodicityId != null && !(ePeriodicityId.equals("-1"))) {
			ePeriodicity = new EPeriodicity();
			ePeriodicity.setIdPeriodicity(Integer.parseInt(ePeriodicityId));
		}
		toReturn.setEPeriodicity(ePeriodicity);
		toReturn.setName(name);
		toReturn.setDescription(description);
		toReturn.setCoordinatorClass(coordinatorClass);
		return toReturn;
	}

	@Override
	protected String getObjectName() {
		return OBJ_NAME;
	}

	@Override
	protected void setObjectId(final Object object, final Integer id) {
		((EProcess) object).setId(id);

	}

	@Override
	public void updateObjectFromRequest(
			final RequestContainer requestContainer,
			final SourceBean serviceRequest, final Integer id) throws Exception {
		super.updateObjectFromRequest(requestContainer, serviceRequest, id);
		SessionContainer aSessionContainer = requestContainer
				.getSessionContainer();
		SessionContainer permanentSession = aSessionContainer
				.getPermanentContainer();
		IEngUserProfile userProfile = (IEngUserProfile) permanentSession
				.getAttribute(IEngUserProfile.ENG_USER_PROFILE);

		String extraction = (String) serviceRequest.getAttribute(EXTRACTION);
		if (extraction != null && extraction.toUpperCase().equals(EXTRACTION)) {
			// run coordinator operations
			OperationCoordinatorInterface operationCoordinator = OperationCoordinatorFactory
					.create(id, FileExportUtil.getExportTempFolderPath());
			operationCoordinator.run(userProfile);
		}

		String ePeriodicityId = (String) serviceRequest
				.getAttribute("PERIODICITY_ID");

		SchedulerSupplier.removeExtractionProcess(id);
		if (ePeriodicityId != null && !(ePeriodicityId.equals("-1"))) {
			// add to scheduler
			Integer periodicityId = Integer.parseInt(ePeriodicityId);
			IES4QDAO ePeriodicityDao = (IES4QDAO) DAOFactory
					.getDAO("EPeriodicityDAO");
			EPeriodicity ePeriodicity = (EPeriodicity) ePeriodicityDao
					.loadObjectById(periodicityId);
			if (ePeriodicity != null) {
				SchedulerSupplier.addExtractionProcess(id, ePeriodicity
						.getCronstring(), FileExportUtil
						.getExportTempFolderPath());
			}
		}

	}
}
