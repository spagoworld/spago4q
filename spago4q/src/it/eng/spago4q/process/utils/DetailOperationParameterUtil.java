/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.process.utils;

import it.eng.spago.base.SourceBean;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.bo.EOperationParameter;
import it.eng.spago4q.utilities.DetailS4Q2ObjectUtil;

public class DetailOperationParameterUtil extends DetailS4Q2ObjectUtil {

	private static final String DAO_NAME = "EOperationParameterDAO";
	private static final String OBJ_NAME = "OPERATIONPARAMETER";

	@Override
	protected String getDAOName() {
		return DAO_NAME;
	}

	@Override
	protected Object getObjectFromRequest(final SourceBean serviceRequest) {
		EOperationParameter toReturn = new EOperationParameter();
		String name = (String) serviceRequest.getAttribute("name");
		String value = (String) serviceRequest.getAttribute("value");
		String criptStr = (String) serviceRequest.getAttribute("cript");
		
		String operationId = (String) serviceRequest
				.getAttribute("Operation_Id");

		boolean cript = false;

		if (criptStr != null) {
			cript = true;
		}

		toReturn.setName(name);
		toReturn.setValue(value);
		toReturn.setCript(cript);

		EOperation operation = new EOperation();

		operation.setId(Integer.parseInt(operationId));

		toReturn.setEOperation(operation);

		return toReturn;

	}

	@Override
	protected String getObjectName() {
		return OBJ_NAME;
	}

	@Override
	protected void setObjectId(final Object object, final Integer id) {
		((EOperationParameter) object).setId(id);
	}
}