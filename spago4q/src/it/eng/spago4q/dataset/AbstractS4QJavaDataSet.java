/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dataset;

import it.eng.spago.base.SourceBean;
import it.eng.spago.base.SourceBeanException;
import it.eng.spago.configuration.ConfigSingleton;
import it.eng.spago.error.EMFUserError;
import it.eng.spagobi.commons.SingletonConfig;
import it.eng.spagobi.commons.dao.DAOFactory;
import it.eng.spagobi.kpi.config.bo.Kpi;
import it.eng.spagobi.kpi.config.bo.KpiInstance;
import it.eng.spagobi.kpi.config.bo.KpiValue;
import it.eng.spagobi.kpi.config.dao.IKpiDAO;
import it.eng.spagobi.kpi.model.bo.ModelInstance;
import it.eng.spagobi.kpi.model.bo.Resource;
import it.eng.spagobi.kpi.model.dao.IModelInstanceDAO;
import it.eng.spagobi.kpi.model.dao.IResourceDAO;
import it.eng.spagobi.tools.dataset.bo.IJavaClassDataSet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

public abstract class AbstractS4QJavaDataSet implements IJavaClassDataSet {

	private static transient Logger logger = Logger
			.getLogger(AbstractS4QJavaDataSet.class);

	public abstract List getNamesOfProfileAttributeRequired();

	public String setDataSetResult(final Object value) {
		SourceBean resultSB = null;
		String toReturn = "";
		try {
			resultSB = new SourceBean("ROWS");
			SourceBean rowSB = new SourceBean("ROW");
			rowSB.setAttribute("value", value);
			resultSB.setAttribute(rowSB);
			toReturn = resultSB.toString();
		} catch (SourceBeanException e) {
			logger.error("Impossible to create ROWS SourceBean:"
					+ e.getLocalizedMessage());
		}
		return toReturn;
	}

	public String getKpiCode(final Map map) {
		String toReturn = null;
		Integer idKpiInstance = Integer.valueOf((String) map
				.get("ParKpiInstance"));
		if (idKpiInstance != null) {
			try {
				KpiInstance kpiInstance = DAOFactory.getKpiInstanceDAO()
						.loadKpiInstanceById(idKpiInstance);
				Integer idKpi = kpiInstance.getKpi();
				Kpi kpi = DAOFactory.getKpiDAO().loadKpiById(idKpi);
				toReturn = kpi.getCode();
			} catch (EMFUserError e) {
				logger.debug("Impossible to load kpiCode "
						+ e.getLocalizedMessage());
				logger.debug(e.getStackTrace());
			}
		}
		return toReturn;
	}

	public List<KpiValue> getKpiValueOfModelInstanceChildren(final Map map) {
		List<KpiValue> toReturn = new ArrayList<KpiValue>();
		Integer IdModelInstance = Integer.valueOf((String) map
				.get("ParModelInstance"));
		String parKpiDateString = (String) map.get("ParKpiDate");
		if (parKpiDateString != null) {
			parKpiDateString = parKpiDateString.replace("'", "");
		}
		
		String formatServer = SingletonConfig.getInstance().getConfigValue("SPAGOBI.TIMESTAMP-FORMAT.format");
		//SourceBean formatServerSB = ((SourceBean)ConfigSingleton.getInstance().getAttribute("SPAGOBI.TIMESTAMP-FORMAT"));
		//String formatServer = (String) formatServerSB.getAttribute("format");
		SimpleDateFormat fServ = new SimpleDateFormat();
		fServ.applyPattern(formatServer);
		
		String resourceName = (String) map.get("ParKpiResource");
		Date parKpiDate = null;
		try {
			if (parKpiDateString != null) {
				parKpiDate = fServ.parse(parKpiDateString);
			}
		} catch (ParseException e1) {
			logger.debug(e1.getLocalizedMessage());
		}
		try {
			Resource resource = null;
			if (resourceName != null) {
				IResourceDAO resourceDAO = DAOFactory.getResourceDAO();
				resource = resourceDAO
						.loadResourcesByNameAndModelInst(resourceName);
			}
			IKpiDAO kpiDao = DAOFactory.getKpiDAO();

			IModelInstanceDAO modelInstanceDAO = DAOFactory
					.getModelInstanceDAO();
			ModelInstance modelInstance = modelInstanceDAO
					.loadModelInstanceWithChildrenById(IdModelInstance);
			List children = modelInstance.getChildrenNodes();
			for (Iterator iterator = children.iterator(); iterator.hasNext();) {
				ModelInstance child = (ModelInstance) iterator.next();
				KpiInstance kpiInstanceChild = child.getKpiInstance();
				if (kpiInstanceChild != null) {
					KpiValue kpiValue = kpiDao.getKpiValue(kpiInstanceChild
							.getKpiInstanceId(), parKpiDate, resource,null);
					if (kpiValue != null) {
						toReturn.add(kpiValue);
					}
				}
			}
		} catch (EMFUserError e) {
			logger.error("Impossible to create the list of KpiValue:"
					+ e.getLocalizedMessage());
			e.printStackTrace();
		}
		return toReturn;
	}

	public List<KpiValue> getKpiValuesByModelCode(final Map map,
			final List<String> codes) throws EMFUserError {
		String resourceName = (String) map.get("ParKpiResource");
		Resource resource = null;
		if (resourceName != null) {
			IResourceDAO resourceDAO = DAOFactory.getResourceDAO();
			resource = resourceDAO
					.loadResourcesByNameAndModelInst(resourceName);
		}
		Integer IdModelInstance = Integer.valueOf((String) map
				.get("ParModelInstance"));
		IModelInstanceDAO modelInstanceDAO = DAOFactory.getModelInstanceDAO();
		ModelInstance modelInstance = modelInstanceDAO
				.loadModelInstanceWithChildrenById(IdModelInstance);
		String parKpiDateString = (String) map.get("ParKpiDate");
		if (parKpiDateString != null) {
			parKpiDateString = parKpiDateString.replace("'", "");
		}
		SourceBean formatServerSB = ((SourceBean)ConfigSingleton.getInstance().getAttribute("SPAGOBI.DATE-FORMAT-SERVER"));
		String formatServer = (String) formatServerSB.getAttribute("format");
		SimpleDateFormat fServ = new SimpleDateFormat();
		fServ.applyPattern(formatServer);
		Date parKpiDate = null;
		try {
			if (parKpiDateString != null) {
				parKpiDate = fServ.parse(parKpiDateString);
			}
		} catch (ParseException e1) {
			logger.debug(e1.getLocalizedMessage());
		}


		List<KpiValue> toReturn = new ArrayList<KpiValue>();
		for (String modelCode : codes) {
			KpiValue value = findKpiValue(modelInstance, modelCode, resource,parKpiDate);
			if (value != null) {
				toReturn.add(value);
			}
		}
		return toReturn;
	}

	private KpiValue findKpiValue(ModelInstance modelInstance, String code, Resource resource,Date date) throws EMFUserError {
		KpiValue toReturn = null;
		if (modelInstance.getModel().getCode().equals(code)) {
			if(modelInstance.getKpiInstance() != null){
			IKpiDAO kpiDao = DAOFactory.getKpiDAO();
			toReturn = kpiDao.getKpiValue(modelInstance.getKpiInstance()
					.getKpiInstanceId(), date, resource, null);
			}
		} else {
			List children = modelInstance.getChildrenNodes();
			for (Object child : children) {
				toReturn = findKpiValue(((ModelInstance) child), code, resource,date);
				if (toReturn != null) {
					break;
				}
			}
		}
		return toReturn;
	}
}
