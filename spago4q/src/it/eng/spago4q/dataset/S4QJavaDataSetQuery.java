/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dataset;

import it.eng.spago.error.EMFUserError;
import it.eng.spagobi.commons.dao.DAOFactory;
import it.eng.spagobi.tools.dataset.common.dataproxy.JDBCDataProxy;
import it.eng.spagobi.tools.dataset.common.datareader.JDBCDataReader;
import it.eng.spagobi.tools.dataset.common.datastore.IDataStore;
import it.eng.spagobi.tools.datasource.bo.IDataSource;
import it.eng.spagobi.tools.datasource.dao.IDataSourceDAO;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

public class S4QJavaDataSetQuery extends AbstractS4QJavaDataSet {

	private static transient Logger logger = Logger.getLogger(S4QJavaDataSetQuery.class);

	public String getValues(final Map profile, final Map parameters) {
	
		String DSLabel = "SpagoBI";
		String statement = "SELECT * FROM sbi_domains s;";
		String value = "";
		
		try {
			IDataSourceDAO dataSourceDAO = DAOFactory.getDataSourceDAO();
			IDataSource dataSource = dataSourceDAO.loadDataSourceByLabel(DSLabel);
			
			JDBCDataProxy dataProxy = new JDBCDataProxy(dataSource, statement);
			JDBCDataReader dataReader = new JDBCDataReader();
			
			IDataStore dataStore = null;
			dataStore = dataProxy.load(dataReader);
			value = Integer.toString(dataStore.getFieldDistinctValues(0).size());
			logger.debug("value: " + value);
			
		} catch (EMFUserError e) {
			logger.debug(e.getLocalizedMessage());
		}
		return setDataSetResult(value);
		
	}

	@Override
	public List getNamesOfProfileAttributeRequired() {
		return null;
	}	
	
}
