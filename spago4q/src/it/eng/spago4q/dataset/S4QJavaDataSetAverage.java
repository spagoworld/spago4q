/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dataset;

import it.eng.spagobi.kpi.config.bo.KpiValue;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * SpagoBI Java Data Set that computes the average of the children of
 * the current modelInstance that is analyzed from the KPI Engine.
 * SpagoBIKpiInternalEngine
 */
public class S4QJavaDataSetAverage extends AbstractS4QJavaDataSet {

	private static transient Logger logger = Logger
			.getLogger(S4QJavaDataSetAverage.class);

	public String getValues(Map profile,Map parameters) {
		logger.info("IN");
		Integer numberOfChildren = 0;
		Double totalKpiChildrenValue = 0.0;
		Double toReturn = 0.0;
		List<KpiValue> values = getKpiValueOfModelInstanceChildren(parameters);
		
		for (KpiValue kpiValue : values) {
			if (kpiValue.getValue() != null){
				totalKpiChildrenValue += new Double(kpiValue.getValue());
				numberOfChildren ++;
			}	
		}
		if (numberOfChildren != 0){
			toReturn = totalKpiChildrenValue / numberOfChildren;
			BigDecimal bd = new BigDecimal(toReturn);
			bd = bd.setScale(2,BigDecimal.ROUND_UP);
			toReturn = bd.doubleValue();
		}
		logger.info("OUT");
		return setDataSetResult(toReturn);
	}

	public List getNamesOfProfileAttributeRequired() {
		return null;
	}

}