         ********************************************************
         ***    Spago4Q - Open Source for Software Quality    ***
         ********************************************************
                                                  
This Spago4Q 2.4 release provide the following archives and documents:

Spago4Q-2.4-core.zip
----------------------
This package contains the main application for extending an already installed 
SpagoBI platform.

Spago4Q-2.4-sql.zip
---------------------
This package contains all the sql scripts for extending an already installed 
SpagoBI meta-model.

Spago4Q-2.4-quick-install.zip
-------------------------------
This package contains the binaries, database dump and the configuration files 
used for the quick installation steps.

Spago4Q-2.4-src.zip
---------------------
This package contains the source code of the Spago4Q platform projects:
  - spago4q-common-lib
  - spago4q-data-access
  - spago4q-extractors
  - spago4q-web

Spago4Q Documentation
---------------------
https://wiki.spago4q.org/xwiki
