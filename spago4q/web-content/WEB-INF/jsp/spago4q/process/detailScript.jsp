<%--

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

--%>

<%@ include file="/WEB-INF/jsp/commons/portlet_base.jsp"%>
<%@ page
	import="java.util.Map,java.util.HashMap,java.util.List,java.util.ArrayList"%>
<%@page import="it.eng.spago.dispatching.service.detail.impl.DelegatedDetailService"%>
<%@page import="it.eng.spagobi.commons.dao.DAOFactory"%>
<%@page import="it.eng.spago4q.bo.Script"%>
<%@page import="it.eng.spago.navigation.LightNavigationManager"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.eng.spagobi.commons.bo.Domain"%>
<%@page import="it.eng.spago4q.process.utils.DetailScriptUtil" %>
<%@page import="it.eng.spago4q.utilities.Spago4QConstants" %>

<%
	String messageIn = (String) aServiceRequest.getAttribute("MESSAGE");
	String id = (String) aServiceRequest.getAttribute("ID");
	String name = "";
	String description = "";
	String scriptType = "";
	String script = "";

	String title = "";
	String moduleName = "DetailEScriptModule";
	String pageName = "EScriptPage";
	
	String testValue = null;
	String testResult = null;
	
	SourceBean moduleResponse = (SourceBean) aServiceResponse
	.getAttribute(moduleName);
	
    ConfigSingleton configure = ConfigSingleton.getInstance();
	SourceBean moduleBean = (SourceBean) configure
			.getFilteredSourceBeanAttribute("MODULES.MODULE", "NAME",
					moduleName);
	
	if (moduleBean.getAttribute("CONFIG.TITLE") != null)
		title = (String) moduleBean.getAttribute("CONFIG.TITLE");
	
	String messageSave = "";

	// DETAIL_SELECT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	// DETAIL_UPDATE
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_UPDATE)) {

		messageIn = (String) moduleResponse.getAttribute("MESSAGE");
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	
	//DETAIL_NEW
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_NEW)) {
		messageSave = DelegatedDetailService.DETAIL_INSERT;
	}
	//DETAIL_INSERT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_INSERT)) {

		Script aScript = (Script) moduleResponse.getAttribute("ESCRIPT");
		
		if(aScript.getId()!= null){
			id = aScript.getId().toString();
			messageIn = (String) moduleResponse.getAttribute("MESSAGE");
			messageSave = DelegatedDetailService.DETAIL_UPDATE;
		} else { // if it has a validation error
			messageIn = DelegatedDetailService.DETAIL_SELECT;
			messageSave = DelegatedDetailService.DETAIL_INSERT;
		}
		
	}

	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {

		Script aScript = (Script) moduleResponse.getAttribute("ESCRIPT");
		if (aScript != null) {
			name = aScript.getName();
			description = aScript.getDescription();
			scriptType = aScript.getScriptType();
			script = aScript.getScript();
			testValue = aScript.getTestValue();
			testResult = aScript.getTestResult();
		}
	}

	Map formUrlPars = new HashMap();
	formUrlPars.put("PAGE", pageName);
	formUrlPars.put("MODULE", moduleName);
	formUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_DISABLED, "true");
	formUrlPars.put("MESSAGE", messageSave);
	String formUrl = urlBuilder.getUrl(request, formUrlPars);

	Map backUrlPars = new HashMap();
	backUrlPars.put("PAGE", pageName);
	backUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_BACK_TO, "1");
	String backUrl = urlBuilder.getUrl(request, backUrlPars);

	String messageBundle = "component_spago4q_messages";
%>


<table
	class='header-table-portlet-section'>
	<tr class='header-row-portlet-section'>
		<td class='header-title-column-portlet-section'
			style='vertical-align: middle; padding-left: 5px;'><spagobi:message
			key="<%=title%>" bundle="<%=messageBundle%>" /></td>
		<td class='header-empty-column-portlet-section'>&nbsp;</td>
		<td class='header-button-column-portlet-section'><a
			href="javascript:saveDS('TEST')">
		<img class='header-button-image-portlet-section'
			title='<spagobi:message key = "s4q.button.save.and.test.title" bundle="<%=messageBundle%>" />'
			src='<%=urlBuilder.getResourceLinkByTheme(request,
									"/img/test.png", currTheme)%>'
			alt='<spagobi:message key = "s4q.button.save.and.test.title" bundle="<%=messageBundle%>"/>' />
		</a></td>
		<td class='header-button-column-portlet-section'><a
			href="javascript:document.getElementById('EScriptForm').submit()">
		<img class='header-button-image-portlet-section'
			title='<spagobi:message key = "s4q.button.save.title" bundle="<%=messageBundle%>" />'
			src='<%=urlBuilder.getResourceLinkByTheme(request,
									"/img/save.png", currTheme)%>'
			alt='<spagobi:message key = "s4q.button.save.title" bundle="<%=messageBundle%>"/>' />
		</a></td>
		<td class='header-button-column-portlet-section'><a
			href='<%=backUrl%>'> <img
			class='header-button-image-portlet-section'
			title='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>" />'
			src='<%=urlBuilder.getResourceLinkByTheme(request,
									"/img/back.png", currTheme)%>'
			alt='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>"/>' />
		</a></td>
	</tr>
</table>

<form method='post' action='<%=formUrl%>' id='EScriptForm' name='EScriptForm'>
	
	<input type="hidden" name="ID" value="<%=id%>">
	
	<input type='hidden' value='' name='OPERATION' />
	<input type='hidden' value='' name='TESTVALUE' />
	
<div class="div_detail_area_forms">

<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.name" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form'><input
	class='portlet-form-input-field' type="text" name="name" size="50"
	value="<%=name%>" maxlength="200"> &nbsp;*</div>

<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.description" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form' style='height: 150px;'>
	<textarea name="description" cols="40" style='height: 110px;' class='portlet-text-area-field'><%=description%></textarea>
</div>
<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.eScript.scriptType" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form'>

	<select class='portlet-form-field' name="scriptType">
<%

for(int i = 0 ; i < Spago4QConstants.SCRIPT_TYPES_ARRAY.length; i ++){
	String selected = "";
	if (scriptType.equals(Spago4QConstants.SCRIPT_TYPES_ARRAY[i]))
		selected = "selected"; %>
		<option value='<%=Spago4QConstants.SCRIPT_TYPES_ARRAY[i]%>'
		label='<%= Spago4QConstants.SCRIPT_TYPES_ARRAY[i]%>'
		<%=selected %> ><%= Spago4QConstants.SCRIPT_TYPES_ARRAY[i]%></option>
<%	
}

%>
</select>
</div>
<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.eScript.script" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form' style='height: 150px;'>
    <textarea name="script" cols="40" style='height: 110px;' class='portlet-text-area-field'><%=script%></textarea>
</div>

</div>

</form>

<% if(testValue != null ){ %>
	
	<div class="div_detail_area_forms">
	<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.eScript.input.test.value" bundle="<%=messageBundle%>" /> </span></div>
	<div class='div_detail_form'>
	<%= testValue %>
	</div>

	<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.eScript.output.test.value" bundle="<%=messageBundle%>" /> </span></div>
	<div class='div_detail_form'>
	<%= testResult %>
	</div>

	</div>	
<%} %>



<spagobi:error />

<script>

function saveDS(type) {
	  var value = prompt("Please enter a input value","");
	  document.EScriptForm.TESTVALUE.value = value;
  	  document.EScriptForm.OPERATION.value = type;
	  document.getElementById('EScriptForm').submit();
}

</script>

<%@ include file="/WEB-INF/jsp/commons/footer.jsp"%>