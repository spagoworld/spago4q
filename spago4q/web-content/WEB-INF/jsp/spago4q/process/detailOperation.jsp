<%--

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

--%>

<%@ include file="/WEB-INF/jsp/commons/portlet_base.jsp"%>

<%@page import="java.util.Map,java.util.HashMap"%>
<%@page import="it.eng.spago.navigation.LightNavigationManager"%>
<%@page import="it.eng.spagobi.commons.utilities.ChannelUtilities"%>
<%@page	import="it.eng.spago.dispatching.service.detail.impl.DelegatedDetailService"%>
<%@page import="it.eng.spago4q.bo.EOperation"%>
<%@page import="it.eng.spago4q.bo.DataSource"%>
<%@page import="it.eng.spago4q.bo.InterfaceType"%>
<%@page import="it.eng.spago4q.dao.DAOFactory"%>
<%@page import="it.eng.spago4q.dao.IES4QFKDAO"%>
<%@page import="it.eng.spago4q.dao.IES4QDAO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="it.eng.spago4q.source.utils.SourceTypeUtil"%>
<%@page import="it.eng.spago4q.source.utils.TestMessage"%>

<script>

function testOperationParameter(messageValue, okValue) {
	if (messageValue != 'null'){
			testOperationPar(messageValue, okValue)
	}
}

function testOperationPar(messageValue, okValue) {
	var message = messageValue;
    var ok = okValue;
	if (ok){
	Ext.MessageBox.show({
		title: 'Status',
		msg: message,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.INFO,
		width:350
	});
	} else {
		Ext.MessageBox.show({
			title: 'Status',
			msg: message,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width:350
		});
		}
	return;
 	 
}

function saveDS(type) {	
	document.operationForm.TESTOPERATION.value=type;
	document.getElementById('operationForm').submit();
}

</script>

<%
	String id = "";
	String title = "";	
	String name = "";
	String description = "";
	String process_id = "";
	Integer idInterfaceType = null;
	Integer idDataSource = null;
	Boolean archive = false;
	String archiveChecked = "";
	Boolean rejected = false;
	String rejectedChecked = "";

    String messageBundle = "component_spago4q_messages"; 
    String moduleName = "DetailEOperationModule";
    String pageName = "EOperationPage";
    String objectName = "EOPERATION";
    String FKId = "PROCESS_ID";
    String testMessageString = null;
    Boolean testStatus = false;

    ConfigSingleton configure = ConfigSingleton.getInstance();

    SourceBean moduleBean = (SourceBean) configure
			.getFilteredSourceBeanAttribute("MODULES.MODULE", "NAME",
					moduleName);
	

	if (moduleBean.getAttribute("CONFIG.TITLE") != null)
		title = (String) moduleBean.getAttribute("CONFIG.TITLE");
	
	if (aServiceRequest.getAttribute(FKId) != null)
		process_id = (String)aServiceRequest.getAttribute(FKId);
	
	
	String messageIn = (String) aServiceRequest.getAttribute("MESSAGE");
	String messageSave = "";
	
	// DETAIL_SELECT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	// DETAIL_UPDATE
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_UPDATE)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
		.getAttribute(moduleName);
		
		
		messageIn = (String) moduleResponse.getAttribute("MESSAGE");
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	
	//DETAIL_NEW
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_NEW)) {
		messageSave = DelegatedDetailService.DETAIL_INSERT;
	}
	//DETAIL_INSERT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_INSERT)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
				.getAttribute(moduleName);
		EOperation operation = (EOperation) moduleResponse.getAttribute(objectName);
		if(operation.getId()!= null){
			id = operation.getId().toString();
			messageIn = (String) moduleResponse.getAttribute("MESSAGE");
			messageSave = DelegatedDetailService.DETAIL_UPDATE;
		} else {
			messageIn = DelegatedDetailService.DETAIL_SELECT;
			messageSave = DelegatedDetailService.DETAIL_INSERT;
		}
	}

	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
				.getAttribute(moduleName);
		EOperation operation = (EOperation) moduleResponse.getAttribute(objectName);
		TestMessage testMessage = (TestMessage)moduleResponse.getAttribute("TESTRESULT");
		if (testMessage != null){
			testMessageString = testMessage.getMessage();
			testStatus = testMessage.isOk();
		}
		if (operation != null) {
			if(operation.getId()!= null)
				id = operation.getId().toString();
			if(operation.getName()!=null)
				name = operation.getName();
			if(operation.getDescription()!=null)
				description = operation.getDescription();
			if(operation.getArchive()!=null)
				archive = operation.getArchive();
			if(operation.getRejected()!=null)
				rejected = operation.getRejected();
			if(operation.getInterfaceType()!=null && operation.getInterfaceType().getId()!=null)
				idInterfaceType = operation.getInterfaceType().getId();
			if(operation.getDataSource()!=null && operation.getDataSource().getId()!=null)
				idDataSource = operation.getDataSource().getId();
		}
	}

	
	Map formUrlPars = new HashMap();
	formUrlPars.put("PAGE", pageName);
	formUrlPars.put("MODULE", moduleName);
	formUrlPars.put("MESSAGE", messageSave);
	formUrlPars.put(FKId, process_id);
	formUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_DISABLED, "true");
	
	String formUrl = urlBuilder.getUrl(request, formUrlPars);
	
	Map backUrlPars = new HashMap();
	backUrlPars.put("PAGE", pageName);
	backUrlPars.put(FKId, process_id);
	backUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_BACK_TO, "1");
	
	String backUrl = urlBuilder.getUrl(request, backUrlPars);
	
	Map testUrlPars = new HashMap();

	testUrlPars.put("PAGE", "EOperationTestPage");
	testUrlPars.put(FKId, process_id);
	testUrlPars.put("ID", id);
	
	String testUrl = urlBuilder.getUrl(request, testUrlPars);
%>

<script type="text/javascript" language="JavaScript">
	testOperationParameter('<%= testMessageString%>', <%= testStatus%>)    	
</script>

<table class='header-table-portlet-section'>
  <tr class='header-row-portlet-section'>
    <td class='header-title-column-portlet-section'
      style='vertical-align: middle; padding-left: 5px;'><spagobi:message
      key="<%=title%>"  bundle="<%=messageBundle%>" /></td>
    <td class='header-empty-column-portlet-section'>&nbsp;</td>
    <td class='header-button-column-portlet-section'><a
			href="javascript:saveDS('TEST')">
		<img class='header-button-image-portlet-section'
			title='<spagobi:message key = "s4q.button.test.operation.parameter.title" bundle="<%=messageBundle%>" />'
			src='<%=urlBuilder.getResourceLinkByTheme(request,
									"/img/test-op.png", currTheme)%>'
			alt='<spagobi:message key = "s4q.button.test.operation.parameter.title" bundle="<%=messageBundle%>"/>' />
		</a>
	</td>
	<td class='header-button-column-portlet-section'><a
			href='javascript:testOperation()'>
		<img class='header-button-image-portlet-section'
			title='<spagobi:message key = "s4q.button.test.operation.title" bundle="<%=messageBundle%>" />'
			src='<%=urlBuilder.getResourceLinkByTheme(request,
									"/img/test-ex.png", currTheme)%>'
			alt='<spagobi:message key = "s4q.button.test.operation.title" bundle="<%=messageBundle%>"/>' />
		</a>
	</td>
    <td class='header-button-column-portlet-section'><a
      href="javascript:document.getElementById('operationForm').submit()"> <img
      class='header-button-image-portlet-section'
      title='<spagobi:message key = "s4q.button.save.title"  bundle="<%=messageBundle%>" />'
      src='<%=urlBuilder.getResourceLinkByTheme(request, "/img/save.png", currTheme)%>'
      alt='<spagobi:message key = "s4q.button.save.title"  bundle="<%=messageBundle%>" />' /> </a></td>
    <td class='header-button-column-portlet-section'><a
      href='<%=backUrl%>'> <img
      class='header-button-image-portlet-section'
      title='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>" />'
      src='<%=urlBuilder.getResourceLinkByTheme(request, "/img/back.png", currTheme)%>'
      alt='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>" />' /> </a></td>
  </tr>
</table>

<form method='POST' action='<%=formUrl%>' id='operationForm' name='operationForm'>
<input type='hidden' value='<%=id%>' name='id' />
	<input type='hidden' value='NO_VALUE' name='TEST' />
	<input type='hidden' value='' name='TESTOPERATION' />

<div class="div_detail_area_forms">
<div class='div_detail_label'><span
  class='portlet-form-field-label'> <spagobi:message
  key="s4q.label.name" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form'><input
  class='portlet-form-input-field' type="text" name="name" size="50"
  value="<%=name%>" maxlength="200"> &nbsp;*</div>

<BR>

<div class='div_detail_label'><span
  class='portlet-form-field-label'> <spagobi:message
  key="s4q.label.description" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form' style='height: 150px;'>
<textarea name="description" cols="40" style='height: 110px;' class='portlet-text-area-field'><%=description%></textarea>
</div>
  
  <div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.dataSource" bundle="<%=messageBundle%>"/> </span></div>

<div class='div_detail_form'>
<select class='portlet-form-field' name="dataSource_id" >
<%
	List dataSources = ((IES4QFKDAO)DAOFactory.getDAO("EDataSourceDAO")).loadObjectList(null,null,null);
	Iterator itt = dataSources.iterator();
	while (itt.hasNext()){
		DataSource dataSource = (DataSource)itt.next();
		String selected = "";
	if (idDataSource!=null && dataSource.getId().equals(idDataSource)){
			selected = "selected='selected'";
		}
			%>    			 		
			<option value="<%= dataSource.getId() %>" label="<%= dataSource.getName() %>" <%= selected %>>
				<%= dataSource.getName() %>	
			</option>
			<%

	}
%>
</select >
</div>


<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="sbi.label.interfaceType" bundle="<%=messageBundle%>"/> </span></div>

<div class='div_detail_form'>
<select class='portlet-form-field' name="interfaceType_id" >
<%
	List interfaceTypes = ((IES4QDAO)DAOFactory.getDAO("EInterfaceTypeDAO")).loadObjectList(null,null);
	Iterator itt2 = interfaceTypes.iterator();
	while (itt2.hasNext()){
		InterfaceType interfaceType = (InterfaceType)itt2.next();
		String selected = "";
		if (idInterfaceType != null && interfaceType.getId().equals(idInterfaceType)){
			selected = "selected='selected'";
		}
			%>    			 		
			<option value="<%= interfaceType.getId() %>" label="<%= interfaceType.getName() %>" <%= selected %>>
				<%= interfaceType.getName() %>	
			</option>
			<%

	}
%>
</select >
</div>

<%if(archive != null && archive.booleanValue()) 
	archiveChecked = "checked='checked'";
%>

<div class='div_detail_label'><span
  class='portlet-form-field-label'> <spagobi:message
  key="s4q.label.operation.archive" bundle="<%=messageBundle%>" /> </span>
  </div>
<div class='div_detail_form'>
<input class='portlet-form-input-field' type="checkbox" name="archive" 
  <%=archiveChecked%>" maxlength="200"> &nbsp;
  </div>
<%if(rejected != null && rejected.booleanValue()) 
	rejectedChecked = "checked='checked'";
%>  
  
  <div class='div_detail_label'><span
  class='portlet-form-field-label'> <spagobi:message
  key="s4q.label.operation.rejected" bundle="<%=messageBundle%>" /> </span>
  </div>
<div class='div_detail_form'>
<input class='portlet-form-input-field' type="checkbox" name="rejected" 
  <%=rejectedChecked%>" maxlength="200"> &nbsp;
  </div>
<br>

</div>
</form>

<spagobi:error />

<script>


function testOperation() {
	Ext.MessageBox.confirm('<spagobi:message key = "s4q.confirm.title" bundle="<%=messageBundle%>" />', '<spagobi:message key = "s4q.operation.test.confirm.message" bundle="<%=messageBundle%>" />', showResult);
}

function showResult(btn){
	   if (btn == 'yes'){
	   window.location='<%=testUrl%>'
	   }
};


</script>

<%@ include file="/WEB-INF/jsp/commons/footer.jsp"%>