<%--

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

--%>

<%@ include file="/WEB-INF/jsp/commons/portlet_base.jsp"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="it.eng.spago.navigation.LightNavigationManager"%>
<%@page import="it.eng.spago.configuration.ConfigSingleton"%>

<%
	String messageBundle = "component_spago4q_messages"; 
	String operationPageName = "EOperationPage";
	String moduleName = "ListTestOperationModule";
	String title = "s4q.detailOperationTest.title";

	ConfigSingleton configure = ConfigSingleton.getInstance();

	SourceBean moduleBean = (SourceBean) configure
	.getFilteredSourceBeanAttribute("MODULES.MODULE", "NAME",moduleName);

	Map backUrlPars = new HashMap();
		backUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_BACK_TO, "1");

	String backUrl = urlBuilder.getUrl(request, backUrlPars);
%>
<table class='header-table-portlet-section'>		
	<tr class='header-row-portlet-section'>
		<td class='header-title-column-portlet-section'
		    style='vertical-align:middle;padding-left:5px;'>
		<spagobi:message
      key="<%=title%>"  bundle="<%=messageBundle%>" />	
		</td>
		<td class='header-empty-column-portlet-section'>&nbsp;</td>
		<td class='header-button-column-portlet-section'>
				<a href='<%=backUrl%>'> 
	      			<img class='header-button-image-portlet-section' 
	      				 title='<spagobi:message key = "SBISet.ListDataSet.backButton"  />' 
	      				 src='<%=urlBuilder.getResourceLinkByTheme(request, "/img/back.png", currTheme)%>' 
	      				 alt='<spagobi:message key = "SBISet.ListDataSet.backButton" />' 
	      			/>
				</a>
			</td>		
	</tr>
</table>
<div class=''>
   <!-- ERROR TAG --> 
	<spagobi:error/>
	<div width="100%">
			<spagobi:list moduleName="ListTestOperationModule"/>
	</div>
</div>