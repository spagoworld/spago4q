<%--

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

--%>

<%@ include file="/WEB-INF/jsp/commons/portlet_base.jsp"%>

<%@page import="java.util.Map,java.util.HashMap"%>
<%@page import="it.eng.spago.navigation.LightNavigationManager"%>
<%@page import="it.eng.spagobi.commons.utilities.ChannelUtilities"%>
<%@page	import="it.eng.spago.dispatching.service.detail.impl.DelegatedDetailService"%>
<%@page import="it.eng.spago4q.bo.EOperationField"%>
<%@page import="it.eng.spago4q.bo.EOperation"%>
<%@page import="it.eng.spago4q.bo.InterfaceField"%>
<%@page import="it.eng.spago4q.bo.Script"%>
<%@page import="it.eng.spago4q.dao.DAOFactory"%>
<%@page import="it.eng.spago4q.dao.IES4QDAO"%>
<%@page import="it.eng.spago4q.dao.IEOperationFieldDAO"%>


<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>

<%
	String id = "";
	String title = "";
	
	String name = "";
	String operation_id = "";
	Integer idInterfaceField = null;
	Integer idScript = null;
	
	
    String messageBundle = "component_spago4q_messages"; 
    String moduleName = "DetailEOperationFieldModule";
    String pageName = "EOperationFieldPage";
    String objectName = "EOPERATIONFIELD";
    String FKId = "OPERATION_ID";

    ConfigSingleton configure = ConfigSingleton.getInstance();

    SourceBean moduleBean = (SourceBean) configure
			.getFilteredSourceBeanAttribute("MODULES.MODULE", "NAME",
					moduleName);
	

	if (moduleBean.getAttribute("CONFIG.TITLE") != null)
		title = (String) moduleBean.getAttribute("CONFIG.TITLE");
	
	if (aServiceRequest.getAttribute(FKId) != null)
		operation_id = (String)aServiceRequest.getAttribute(FKId);
	
	
	String messageIn = (String) aServiceRequest.getAttribute("MESSAGE");
	String messageSave = "";
	
	// DETAIL_SELECT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	// DETAIL_UPDATE
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_UPDATE)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
		.getAttribute(moduleName);
		
		
		messageIn = (String) moduleResponse.getAttribute("MESSAGE");
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	
	//DETAIL_NEW
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_NEW)) {
		messageSave = DelegatedDetailService.DETAIL_INSERT;
	}
	//DETAIL_INSERT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_INSERT)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
				.getAttribute(moduleName);
		EOperationField operationField = (EOperationField) moduleResponse.getAttribute(objectName);
		if(operationField.getId()!= null){
			id = operationField.getId().toString();
			messageIn = (String) moduleResponse.getAttribute("MESSAGE");
			messageSave = DelegatedDetailService.DETAIL_UPDATE;
		} else {
			messageIn = DelegatedDetailService.DETAIL_SELECT;
			messageSave = DelegatedDetailService.DETAIL_INSERT;
		}
	}

	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
				.getAttribute(moduleName);
		EOperationField operationField = (EOperationField) moduleResponse.getAttribute(objectName);
		if (operationField != null) {
			if(operationField.getId()!= null)
				id = operationField.getId().toString();
			if(operationField.getName()!=null)
				name = operationField.getName();
			if(operationField.getInterfaceField()!=null && operationField.getInterfaceField().getId()!=null)
				idInterfaceField = operationField.getInterfaceField().getId();
			if(operationField.getScript()!=null && operationField.getScript().getId()!=null)
				idScript = operationField.getScript().getId();
		}
	}

	
	Map formUrlPars = new HashMap();
//	if(ChannelUtilities.isPortletRunning()) {
		formUrlPars.put("PAGE", pageName);
		formUrlPars.put("MODULE", moduleName);
		formUrlPars.put("MESSAGE", messageSave);
		formUrlPars.put(FKId, operation_id);
		formUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_DISABLED, "true");
//	}
	
	String formUrl = urlBuilder.getUrl(request, formUrlPars);
	
	Map backUrlPars = new HashMap();
	backUrlPars.put("PAGE", pageName);
	backUrlPars.put(FKId, operation_id);
	backUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_BACK_TO, "1");
	
	String backUrl = urlBuilder.getUrl(request, backUrlPars);
%>


<table class='header-table-portlet-section'>
  <tr class='header-row-portlet-section'>
    <td class='header-title-column-portlet-section'
      style='vertical-align: middle; padding-left: 5px;'><spagobi:message
      key="<%=title%>"  bundle="<%=messageBundle%>" /></td>
    <td class='header-empty-column-portlet-section'>&nbsp;</td>
    <td class='header-button-column-portlet-section'><a
      href="javascript:document.getElementById('operationFieldForm').submit()"> <img
      class='header-button-image-portlet-section'
      title='<spagobi:message key = "s4q.button.save.title"  bundle="<%=messageBundle%>" />'
      src='<%=urlBuilder.getResourceLinkByTheme(request, "/img/save.png", currTheme)%>'
      alt='<spagobi:message key = "s4q.button.save.title"  bundle="<%=messageBundle%>" />' /> </a></td>
    <td class='header-button-column-portlet-section'><a
      href='<%=backUrl%>'> <img
      class='header-button-image-portlet-section'
      title='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>" />'
      src='<%=urlBuilder.getResourceLinkByTheme(request, "/img/back.png", currTheme)%>'
      alt='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>" />' /> </a></td>
  </tr>
</table>

<form method='POST' action='<%=formUrl%>' id='operationFieldForm' name='operationFieldForm'>
<input type='hidden' value='<%=id%>' name='id' />

<div class="div_detail_area_forms">
<div class='div_detail_label'><span
  class='portlet-form-field-label'> <spagobi:message
  key="s4q.label.name" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form'><input
  class='portlet-form-input-field' type="text" name="name" size="50"
  value="<%=name%>" maxlength="200"> &nbsp;*</div>

<BR>
  
  <div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.wz6.Script" bundle="<%=messageBundle%>"/> </span></div>

<div class='div_detail_form'>
<select class='portlet-form-field' name="script_id" >
	<option value="-1" label="" ></option>

<%
	List scripts = ((IES4QDAO)DAOFactory.getDAO("EScriptDAO")).loadObjectList(null,null);
	Iterator itt = scripts.iterator();
	while (itt.hasNext()){
		Script script = (Script)itt.next();
		String selected = "";
	if (idScript!=null && script.getId().equals(idScript)){
			selected = "selected='selected'";
		}
			%>    			 		
			<option value="<%= script.getId() %>" label="<%= script.getName() %>" <%= selected %>>
				<%= script.getName() %>	
			</option>
			<%

	}
%>
</select >
</div>


<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="sbi.label.interfaceField" bundle="<%=messageBundle%>"/> </span></div>

<div class='div_detail_form'>
<select class='portlet-form-field' name="interfaceField_id" >
<%
	List interfaceFields = null;
	if (id.equals("")){
		interfaceFields = ((IEOperationFieldDAO)DAOFactory.getDAO("EOperationFieldDAO")).getInterfaceFieldList(Integer.parseInt(operation_id), null);
	} else {
		interfaceFields = ((IEOperationFieldDAO)DAOFactory.getDAO("EOperationFieldDAO")).getInterfaceFieldList(Integer.parseInt(operation_id), Integer.parseInt(id));	
	}

	Iterator itt2 = interfaceFields.iterator();
	while (itt2.hasNext()){
		InterfaceField interfaceField = (InterfaceField)itt2.next();
		String selected = "";
		if (idInterfaceField != null && interfaceField.getId().equals(idInterfaceField)){
			selected = "selected='selected'";
		}
			%>    			 		
			<option value="<%= interfaceField.getId() %>" label="<%= interfaceField.getName() %>" <%= selected %>>
				<%= interfaceField.getName() %>	
			</option>
			<%

	}
%>
</select >
</div>

  
</div>
</form>

<spagobi:error />



<%@ include file="/WEB-INF/jsp/commons/footer.jsp"%>