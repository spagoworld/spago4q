<%--

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

--%>

<%@ include file="/WEB-INF/jsp/commons/portlet_base.jsp"%>
<%@page import="java.util.Map,java.util.HashMap,java.util.List,java.util.ArrayList"%>
<%@page import="it.eng.spago.dispatching.service.detail.impl.DelegatedDetailService"%>
<%@page import="it.eng.spago4q.dao.DAOFactory"%>
<%@page import="it.eng.spago4q.dao.IES4QDAO" %>
<%@page import="it.eng.spago4q.bo.EProcess"%>
<%@page import="it.eng.spago4q.bo.EPeriodicity"%>
<%@page import="it.eng.spago.navigation.LightNavigationManager"%>
<%@page import="java.util.ArrayList"%>
<%@page import="it.eng.spagobi.commons.bo.Domain"%>
<%@page import="it.eng.spagobi.commons.constants.ObjectsTreeConstants" %>
<%@page import="it.eng.spagobi.commons.constants.SpagoBIConstants" %>

<%
	String messageIn = (String) aServiceRequest.getAttribute("MESSAGE");
	String id = (String) aServiceRequest.getAttribute("ID");
	String name = "";
	String description = "";
	String coordinatorClass = "";
	Integer periodicityId = null;

	String title = "";
	String moduleName = "DetailEProcessModule";
	String pageName = "EProcessPage";
	
	
    ConfigSingleton configure = ConfigSingleton.getInstance();
	SourceBean moduleBean = (SourceBean) configure
			.getFilteredSourceBeanAttribute("MODULES.MODULE", "NAME",
					moduleName);
	
	if (moduleBean.getAttribute("CONFIG.TITLE") != null)
		title = (String) moduleBean.getAttribute("CONFIG.TITLE");
	
	String messageSave = "";

	// DETAIL_SELECT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	// DETAIL_UPDATE
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_UPDATE)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
		.getAttribute(moduleName);
		messageIn = (String) moduleResponse.getAttribute("MESSAGE");
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	
	//DETAIL_NEW
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_NEW)) {
		messageSave = DelegatedDetailService.DETAIL_INSERT;
	}
	//DETAIL_INSERT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_INSERT)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
				.getAttribute(moduleName);
		EProcess eProcess = (EProcess) moduleResponse.getAttribute("EPROCESS");
		
		if(eProcess.getId()!= null){
			id = eProcess.getId().toString();
			messageIn = (String) moduleResponse.getAttribute("MESSAGE");
			messageSave = DelegatedDetailService.DETAIL_UPDATE;
		} else { // if it has a validation error
			messageIn = DelegatedDetailService.DETAIL_SELECT;
			messageSave = DelegatedDetailService.DETAIL_INSERT;
		}
		
	}

	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
				.getAttribute(moduleName);
		EProcess eProcess = (EProcess) moduleResponse.getAttribute("EPROCESS");
		if (eProcess != null) {
			name = eProcess.getName();
			description = eProcess.getDescription();
			coordinatorClass = eProcess.getCoordinatorClass();
			if(eProcess.getEPeriodicity()!= null)
				periodicityId = eProcess.getEPeriodicity().getIdPeriodicity();
		}
	}

	Map formUrlPars = new HashMap();
	formUrlPars.put("PAGE", pageName);
	formUrlPars.put("MODULE", moduleName);
	formUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_DISABLED, "true");
	formUrlPars.put("MESSAGE", messageSave);
	String formUrl = urlBuilder.getUrl(request, formUrlPars);

	Map backUrlPars = new HashMap();
	backUrlPars.put("PAGE", pageName);
	backUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_BACK_TO, "1");
	String backUrl = urlBuilder.getUrl(request, backUrlPars);

	String messageBundle = "component_spago4q_messages";
	%>


<table
	class='header-table-portlet-section'>
	<tr class='header-row-portlet-section'>
		<td class='header-title-column-portlet-section'
			style='vertical-align: middle; padding-left: 5px;'><spagobi:message
			key="<%=title%>" bundle="<%=messageBundle%>" /></td>
		<td class='header-empty-column-portlet-section'>&nbsp;</td>
		
		<td class='header-button-column-portlet-section'><a
			href="javascript:testExtraction()">
		<img class='header-button-image-portlet-section'
			title='<spagobi:message key = "s4q.button.execute.title" bundle="<%=messageBundle%>" />'
			src='<%=urlBuilder.getResourceLinkByTheme(request,
									"/img/exec22.png", currTheme)%>'
			alt='<spagobi:message key = "s4q.button.execute.title" bundle="<%=messageBundle%>"/>' />
		</a></td>
		<td class='header-button-column-portlet-section'><a
			href="javascript:document.getElementById('EProcessForm').submit()">
		<img class='header-button-image-portlet-section'
			title='<spagobi:message key = "s4q.button.save.title" bundle="<%=messageBundle%>" />'
			src='<%=urlBuilder.getResourceLinkByTheme(request,
									"/img/save.png", currTheme)%>'
			alt='<spagobi:message key = "s4q.button.save.title" bundle="<%=messageBundle%>"/>' />
		</a></td>
		<!-- 
		<td class='header-button-column-portlet-section'>
		<a href='javascript:saveDocument("true");'> 
			<img name='isaveAndGoBack' id='isaveAndGoBack' class='header-button-image-portlet-section'
				   src='<%=urlBuilder.getResourceLinkByTheme(request, "/img/saveAndGoBack.png", currTheme) %>'
      		 title='<spagobi:message key = "SBIDev.docConf.docDet.saveAndGoBackButt" />' 
      		 alt='<spagobi:message key = "SBIDev.docConf.docDet.saveAndGoBackButt" />' />
			</a>
		</td>	-->	
		<td class='header-button-column-portlet-section'><a
			href='<%=backUrl%>'> <img
			class='header-button-image-portlet-section'
			title='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>" />'
			src='<%=urlBuilder.getResourceLinkByTheme(request,
									"/img/back.png", currTheme)%>'
			alt='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>"/>' />
		</a></td>
	</tr>
</table>

<form method='post' action='<%=formUrl%>' id='EProcessForm'
	name='EProcessForm'>
	
	<input type="hidden" name="ID" value="<%=id%>">
	
	<input type='hidden' value='' name='EXTRACTION' />
	
	<input type="hidden" name="" value="" id="saveAndGoBack" />
	
<div class="div_detail_area_forms">

<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.name" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form'><input
	class='portlet-form-input-field' type="text" name="name" size="50"
	value="<%=name%>" maxlength="200"> &nbsp;*</div>

<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.description" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form' style='height: 150px;'>
	<textarea name="description" cols="40" style='height: 110px;' class='portlet-text-area-field'><%=description%></textarea>
</div>

<%if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_NEW)) {
	coordinatorClass = "it.eng.spago4q.extractors.DefaultCoordinator";
					} %>
<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.eProcess.coordinatorclass" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form'><input
	class='portlet-form-input-field' type="text" name="coordinatorClass" size="50"
	value="<%=coordinatorClass%>" maxlength="200"></div>


<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.eProcess.periodicity" bundle="<%=messageBundle%>" /> </span></div>	
<div class='div_detail_form'>
<select class='portlet-form-field' name="PERIODICITY_ID">
<%
 
	if(periodicityId == null) { %>
		<option value="-1"
			label="" selected>
		</option>
<%	} else {%>
	<option value="-1"
			label="" >
		</option>
		<%} %>
	<%
	IES4QDAO periodicityDAO = (IES4QDAO)DAOFactory.getDAO("EPeriodicityDAO");
	List periodicityList = periodicityDAO.loadObjectList(null,null);
	for (java.util.Iterator iterator = periodicityList.iterator(); iterator
			.hasNext();) {
		String selected = "";
		EPeriodicity ePeriodicity = (EPeriodicity) iterator.next();
		if(ePeriodicity.getIdPeriodicity().equals(periodicityId)){
			selected = "selected";
		}
		else {
			selected = "";
		}
	%>
	<option value="<%=ePeriodicity.getIdPeriodicity()%>"
		label="<%=ePeriodicity.getName()%>" <%=selected%>><%=ePeriodicity.getName()%>
	</option>
	<%
	}
	%>

</select>
</div>

</div>


</form>


<spagobi:error />

<script>

function saveDS(type) {	
  	  document.EProcessForm.EXTRACTION.value=type;
	  document.getElementById('EProcessForm').submit();
}


function testExtraction() {
	Ext.MessageBox.confirm('<spagobi:message key = "s4q.confirm.title" bundle="<%=messageBundle%>" />', '<spagobi:message key = "s4q.extraction.execution.confirm.message" bundle="<%=messageBundle%>" />', showResult);	
}

function showResult(btn){
	   if (btn == 'yes'){
		   Ext.MessageBox.show({
	           msg: 'Extracting data, please wait...',
	           progressText: 'Extracting...',
	           width:300,
	           wait:true,
	           waitConfig: {interval:200},
	           animEl: 'mb7'
	       });
		   saveDS('EXTRACTION');
	   }
};

function saveDocument(goBack) {
	if (goBack == 'true'){
	    document.getElementById('saveAndGoBack').name = 'saveAndGoBack';
		document.getElementById('saveAndGoBack').value = 'saveAndGoBack';		
	}
	document.getElementById('EProcessForm').submit();
}

</script>
<%@ include file="/WEB-INF/jsp/commons/footer.jsp"%>