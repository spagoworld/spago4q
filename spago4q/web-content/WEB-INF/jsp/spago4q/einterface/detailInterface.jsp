<%--

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

--%>

<%@ include file="/WEB-INF/jsp/commons/portlet_base.jsp"%>
<%@ page
	import="java.util.Map,java.util.HashMap,java.util.List,java.util.ArrayList"%>
<%@page import="it.eng.spago.dispatching.service.detail.impl.DelegatedDetailService"%>
<%@page import="it.eng.spagobi.commons.dao.DAOFactory"%>
<%@page import="it.eng.spago4q.bo.InterfaceType"%>
<%@page import="it.eng.spago.navigation.LightNavigationManager"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="it.eng.spagobi.commons.bo.Domain"%>
<%@page import="it.eng.spagobi.tools.dataset.bo.JDBCDataSet"%>
<%@page import="it.eng.spagobi.services.dataset.bo.SpagoBiDataSet"%>
<%@page import="it.eng.spagobi.tools.datasource.bo.DataSource"%>
<%@page import="it.eng.spago4q.source.utils.TestMessage"%>

<script>
function showMessage(messageValue, operationStatus) {
	if (messageValue != 'null'){
			displayMessage(messageValue, operationStatus)
	}
}

function displayMessage(messageValue, operationStatus) {
	var message = messageValue;
    var ok = operationStatus;
    
	if (ok){
		Ext.MessageBox.show({
			title: 'Status',
			msg: message,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width:350
		});
		} else {
			Ext.MessageBox.show({
				title: 'Status',
				msg: message,
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.ERROR,
				width:350
			});
			}
		return;
}

function saveDS(type) {	
	  document.EInterfaceTypeForm.TABLEOPERATION.value=type;
	  document.getElementById('EInterfaceTypeForm').submit();
}
</script>

<%
	String messageIn = (String) aServiceRequest.getAttribute("MESSAGE");
	String id = (String) aServiceRequest.getAttribute("ID");
	String name = "";
	String description = "";
	String tableName = "";
	String filterClass = "";
	Integer idDataSource = null;
	String messageString = null;
	boolean operationStatus = false;
	
	String title = "";
	String moduleName = "DetailEInterfaceTypeModule";
	String pageName = "EInterfaceTypePage";
	
	
    ConfigSingleton configure = ConfigSingleton.getInstance();
	SourceBean moduleBean = (SourceBean) configure
			.getFilteredSourceBeanAttribute("MODULES.MODULE", "NAME",
					moduleName);
	
	if (moduleBean.getAttribute("CONFIG.TITLE") != null)
		title = (String) moduleBean.getAttribute("CONFIG.TITLE");
	
	String messageSave = "";

	// DETAIL_SELECT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	// DETAIL_UPDATE
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_UPDATE)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
		.getAttribute(moduleName);
		messageIn = (String) moduleResponse.getAttribute("MESSAGE");
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	
	//DETAIL_NEW
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_NEW)) {
		messageSave = DelegatedDetailService.DETAIL_INSERT;
	}
	//DETAIL_INSERT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_INSERT)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
				.getAttribute(moduleName);
		InterfaceType eInterfaceType = (InterfaceType) moduleResponse.getAttribute("EINTERFACETYPE");
		
		if(eInterfaceType.getId()!= null){
			id = eInterfaceType.getId().toString();
			messageIn = (String) moduleResponse.getAttribute("MESSAGE");
			messageSave = DelegatedDetailService.DETAIL_UPDATE;
		} else { // if it has a validation error
			messageIn = DelegatedDetailService.DETAIL_SELECT;
			messageSave = DelegatedDetailService.DETAIL_INSERT;
		}
		
	}

	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
				.getAttribute(moduleName);
		InterfaceType eInterfaceType = (InterfaceType) moduleResponse.getAttribute("EINTERFACETYPE");
		TestMessage operationMessage = (TestMessage) moduleResponse.getAttribute("MESSAGETOSHOW");
		if (operationMessage != null){
			messageString = operationMessage.getMessage();
			operationStatus = operationMessage.isOk();
		}
		if (eInterfaceType != null) {
			name = eInterfaceType.getName();
			description = eInterfaceType.getDescription();
			tableName = eInterfaceType.getTableName();
			if (eInterfaceType.getFilterClass() != null){
				filterClass = eInterfaceType.getFilterClass();
			} else {
				filterClass = "";
			}
			idDataSource = eInterfaceType.getDataSourceId();
		}
	}

	Map formUrlPars = new HashMap();
	formUrlPars.put("PAGE", pageName);
	formUrlPars.put("MODULE", moduleName);
	formUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_DISABLED, "true");
	formUrlPars.put("MESSAGE", messageSave);
	String formUrl = urlBuilder.getUrl(request, formUrlPars);

	Map backUrlPars = new HashMap();
	backUrlPars.put("PAGE", pageName);
	backUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_BACK_TO, "1");
	String backUrl = urlBuilder.getUrl(request, backUrlPars);

	String messageBundle = "component_spago4q_messages";
%>

<script type="text/javascript" language="JavaScript">
	showMessage('<spagobi:message key = "<%=messageString %>" bundle="<%=messageBundle%>" />',<%= operationStatus%>)    	
</script>

<table
	class='header-table-portlet-section'>
	<tr class='header-row-portlet-section'>
		<td class='header-title-column-portlet-section'
			style='vertical-align: middle; padding-left: 5px;'><spagobi:message
			key="<%=title%>" bundle="<%=messageBundle%>" /></td>
		<td class='header-empty-column-portlet-section'>&nbsp;</td>
		
		<td class='header-button-column-portlet-section'><a href="javascript:saveDS('CREATE')">
		<img class='header-button-image-portlet-section'
			title='<spagobi:message key = "s4q.button.create.table.title" bundle="<%=messageBundle%>" />'
			src='<%=urlBuilder.getResourceLinkByTheme(request,
									"/img/webapp/table_add.gif",currTheme)%>'
			alt='<spagobi:message key = "s4q.button.create.table.title" bundle="<%=messageBundle%>"/>' />
		</a></td>
		
		<td class='header-button-column-portlet-section'><a href="javascript:dropFactTable()">
		<img class='header-button-image-portlet-section'
			title='<spagobi:message key = "s4q.button.drop.table.title" bundle="<%=messageBundle%>" />'
			src='<%=urlBuilder.getResourceLinkByTheme(request,
									"/img/webapp/table_delete.gif",currTheme)%>'
			alt='<spagobi:message key = "s4q.button.drop.table.title" bundle="<%=messageBundle%>"/>' />
		</a></td>
		
		<td class='header-button-column-portlet-section'><a
			href="javascript:document.getElementById('EInterfaceTypeForm').submit()">
		<img class='header-button-image-portlet-section'
			title='<spagobi:message key = "s4q.button.save.title" bundle="<%=messageBundle%>" />'
			src='<%=urlBuilder.getResourceLinkByTheme(request,
									"/img/save.png",currTheme)%>'
			alt='<spagobi:message key = "s4q.button.save.title" bundle="<%=messageBundle%>"/>' />
		</a></td>
		<td class='header-button-column-portlet-section'><a
			href='<%=backUrl%>'> <img
			class='header-button-image-portlet-section'
			title='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>" />'
			src='<%=urlBuilder.getResourceLinkByTheme(request,
									"/img/back.png",currTheme)%>'
			alt='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>"/>' />
		</a></td>
	</tr>
</table>

<form method='post' action='<%=formUrl%>' id='EInterfaceTypeForm'
	name='EInterfaceTypeForm'><input type="hidden" name="ID" value="<%=id%>">
	
	<input type='hidden' value='' name='TABLEOPERATION' />
	
<div class="div_detail_area_forms">

<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.name" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form'><input
	class='portlet-form-input-field' type="text" name="name" size="50"
	value="<%=name%>" maxlength="200"> &nbsp;*</div>

<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.description" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form' style='height: 150px;'>
	<textarea name="description" cols="40" style='height: 110px;' class='portlet-text-area-field'><%=description%></textarea>
</div>

<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.eInterfaceType.tableName" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form'><input
	class='portlet-form-input-field' type="text" name="tablename" size="50"
	value="<%=tableName%>" maxlength="200"></div>
	
   	<div class='div_detail_label' id="DATASOURCELABEL">
			<span class='portlet-form-field-label'>
				<spagobi:message key = "SBISet.eng.dataSource" />
			</span>
		</div>	
	<div class='div_detail_form'>
		<select class='portlet-form-field' name="DATASOURCE" id="DATASOURCE" >			
			<!--   option value="-1"></option> -->
			<%

			List dataSources = DAOFactory.getDataSourceDAO().loadAllDataSources();
			Iterator dataSourceIt = dataSources.iterator();
	
			
			while (dataSourceIt.hasNext()) {
				DataSource dataSourceD = (DataSource) dataSourceIt.next();
					
				String selected = "";
				if (idDataSource!= null && dataSourceD.getDsId() == idDataSource.intValue()) {
					selected = "selected='selected'";										
					}				
			 	%>    			 		
    				<option value="<%= dataSourceD.getDsId()  %>" label="<%= dataSourceD.getLabel() %>" <%= selected %>>
    					<%=dataSourceD.getLabel() %>	
    				</option>
    				<%				
			  	}
			
			%>
		</select>
	</div>
	<div class='div_detail_label'><span
	class='portlet-form-field-label'> <spagobi:message
	key="s4q.label.eInterfaceType.filterClass" bundle="<%=messageBundle%>" /> </span></div>
	<div class='div_detail_form'><input
	class='portlet-form-input-field' type="text" name="filterClass" size="50"
	value="<%=filterClass%>" maxlength="255"></div>
	

</div>


</form>


<spagobi:error />

<script>

function dropFactTable() {
	Ext.MessageBox.confirm('<spagobi:message key = "s4q.button.drop.table.title" bundle="<%=messageBundle%>" />', '<spagobi:message key = "s4q.einterface.drop.confirm.message" bundle="<%=messageBundle%>" />', showResult).setIcon(Ext.MessageBox.WARNING);
}

function showResult(btn){
	   if (btn == 'yes'){
		   saveDS('DROP');
	   }
}
</script>

<%@ include file="/WEB-INF/jsp/commons/footer.jsp"%>