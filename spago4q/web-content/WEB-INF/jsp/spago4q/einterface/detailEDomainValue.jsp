<%--

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

--%>

<%@ include file="/WEB-INF/jsp/commons/portlet_base.jsp"%>

<%@page import="java.util.Map,java.util.HashMap"%>
<%@page import="it.eng.spago.navigation.LightNavigationManager"%>
<%@page import="it.eng.spagobi.commons.utilities.ChannelUtilities"%>
<%@page	import="it.eng.spago.dispatching.service.detail.impl.DelegatedDetailService"%>
<%@page import="it.eng.spago4q.bo.DomainValue"%>
<%@page import="it.eng.spago.configuration.ConfigSingleton"%>

<%
	String id = "";
	String title = "";
	
	String value = "";

	String interfaceField_id = "";
	
    String messageBundle = "component_spago4q_messages"; 
    String moduleName = "DetailEDomainValueModule";
    String pageName = "EDomainValuePage";
    String objectName = "EDomainValue";

    ConfigSingleton configure = ConfigSingleton.getInstance();

    SourceBean moduleBean = (SourceBean) configure
			.getFilteredSourceBeanAttribute("MODULES.MODULE", "NAME",
					moduleName);
	

	if (moduleBean.getAttribute("CONFIG.TITLE") != null)
		title = (String) moduleBean.getAttribute("CONFIG.TITLE");
	
	if (aServiceRequest.getAttribute("FIELD_ID") != null)
		interfaceField_id = (String)aServiceRequest.getAttribute("FIELD_ID");
	
	
	String messageIn = (String) aServiceRequest.getAttribute("MESSAGE");
	String messageSave = "";
	
	// DETAIL_SELECT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	// DETAIL_UPDATE
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_UPDATE)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
		.getAttribute(moduleName);
		
		
		messageIn = (String) moduleResponse.getAttribute("MESSAGE");
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	
	//DETAIL_NEW
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_NEW)) {
		messageSave = DelegatedDetailService.DETAIL_INSERT;
	}
	//DETAIL_INSERT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_INSERT)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
				.getAttribute(moduleName);
		DomainValue domainValue = (DomainValue) moduleResponse.getAttribute(objectName);
		if(domainValue.getId()!= null){
			id = domainValue.getId().toString();
			messageIn = (String) moduleResponse.getAttribute("MESSAGE");
			messageSave = DelegatedDetailService.DETAIL_UPDATE;
		} else {
			messageIn = DelegatedDetailService.DETAIL_SELECT;
			messageSave = DelegatedDetailService.DETAIL_INSERT;
		}
	}

	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
				.getAttribute(moduleName);
		DomainValue domainValue = (DomainValue) moduleResponse.getAttribute(objectName);
		if (domainValue != null) {
			if(domainValue.getId()!= null)
				id = domainValue.getId().toString();
			if(domainValue.getValue()!=null)
				value = domainValue.getValue();

		}
	}

	
	Map formUrlPars = new HashMap();
//	if(ChannelUtilities.isPortletRunning()) {
		formUrlPars.put("PAGE", pageName);
		formUrlPars.put("MODULE", moduleName);
		formUrlPars.put("MESSAGE", messageSave);
		formUrlPars.put("FIELD_ID", interfaceField_id);
		formUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_DISABLED, "true");
//	}
	
	String formUrl = urlBuilder.getUrl(request, formUrlPars);
	
	Map backUrlPars = new HashMap();
	backUrlPars.put("PAGE", pageName);
	backUrlPars.put("FIELD_ID", interfaceField_id);
	backUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_BACK_TO, "1");
	
	String backUrl = urlBuilder.getUrl(request, backUrlPars);
%>


<table class='header-table-portlet-section'>
  <tr class='header-row-portlet-section'>
    <td class='header-title-column-portlet-section'
      style='vertical-align: middle; padding-left: 5px;'><spagobi:message
      key="<%=title%>"  bundle="<%=messageBundle%>" /></td>
    <td class='header-empty-column-portlet-section'>&nbsp;</td>
    <td class='header-button-column-portlet-section'><a
      href="javascript:document.getElementById('interfaceFieldForm').submit()"> <img
      class='header-button-image-portlet-section'
      title='<spagobi:message key = "s4q.button.save.title"  bundle="<%=messageBundle%>" />'
      src='<%=urlBuilder.getResourceLinkByTheme(request, "/img/save.png", currTheme)%>'
      alt='<spagobi:message key = "s4q.button.save.title"  bundle="<%=messageBundle%>" />' /> </a></td>
    <td class='header-button-column-portlet-section'><a
      href='<%=backUrl%>'> <img
      class='header-button-image-portlet-section'
      title='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>" />'
      src='<%=urlBuilder.getResourceLinkByTheme(request, "/img/back.png", currTheme)%>'
      alt='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>" />' /> </a></td>
  </tr>
</table>

<form method='POST' action='<%=formUrl%>' id='interfaceFieldForm' name='interfaceFieldForm'>
<input type='hidden' value='<%=id%>' name='id' />

<div class="div_detail_area_forms">
<div class='div_detail_label'><span
  class='portlet-form-field-label'> <spagobi:message
  key="s4q.label.edomainvalue.value" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form'><input
  class='portlet-form-input-field' type="text" name="value" size="50"
  value="<%=value%>" maxlength="200"> &nbsp;*</div>

<BR>
  
  
</div>
</form>

<spagobi:error />



<%@ include file="/WEB-INF/jsp/commons/footer.jsp"%>