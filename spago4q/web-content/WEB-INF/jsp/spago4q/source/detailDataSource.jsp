<%--

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

--%>

<%@ include file="/WEB-INF/jsp/commons/portlet_base.jsp"%>

<%@page import="java.util.Map,java.util.HashMap"%>
<%@page import="it.eng.spago.navigation.LightNavigationManager"%>
<%@page import="it.eng.spagobi.commons.utilities.ChannelUtilities"%>
<%@page	import="it.eng.spago.dispatching.service.detail.impl.DelegatedDetailService"%>
<%@page import="it.eng.spago4q.bo.DataSource"%>
<%@page import="it.eng.spago4q.source.utils.SourceTypeUtil"%>
<%@page import="it.eng.spago4q.source.utils.TestMessage"%>

<script>

function testDataSource(messageValue, okValue) {
	if (messageValue != 'null'){
			testDataSourceParameter(messageValue, okValue)
	}
}

function testDataSourceParameter(messageValue, okValue) {
	var message = messageValue;
    var ok = okValue;
	if (ok){
	Ext.MessageBox.show({
		title: 'Status',
		msg: message,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.INFO,
		width:350
	});
	} else {
		Ext.MessageBox.show({
			title: 'Status',
			msg: message,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width:350
		});
		}
	return;
 	 
}

function saveDS(type) {	
	document.dataSourceForm.TESTDATASOURCE.value=type;
	document.getElementById('dataSourceForm').submit();
}

</script>

<%
	String id = "";
	String title = "";
	String name = "";
	String description = "";
	String sourceType_id = "";
	
    String messageBundle = "component_spago4q_messages"; 
    String moduleName = "DetailEDataSourceModule";
    String testMessageString = null;
    boolean testStatus = false;
    
    ConfigSingleton configure = ConfigSingleton.getInstance();

    SourceBean moduleBean = (SourceBean) configure
			.getFilteredSourceBeanAttribute("MODULES.MODULE", "NAME",
					moduleName);
	

	if (moduleBean.getAttribute("CONFIG.TITLE") != null)
		title = (String) moduleBean.getAttribute("CONFIG.TITLE");
	
	if (aServiceRequest.getAttribute("TYPE_ID") != null)
		sourceType_id = (String)aServiceRequest.getAttribute("TYPE_ID");
	
	
	String messageIn = (String) aServiceRequest.getAttribute("MESSAGE");
	String messageSave = "";
	
	// DETAIL_SELECT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	// DETAIL_UPDATE
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_UPDATE)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
		.getAttribute(moduleName);
		
		
		messageIn = (String) moduleResponse.getAttribute("MESSAGE");
		messageSave = DelegatedDetailService.DETAIL_UPDATE;
	}
	
	//DETAIL_NEW
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_NEW)) {
		messageSave = DelegatedDetailService.DETAIL_INSERT;
	}
	//DETAIL_INSERT
	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_INSERT)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
				.getAttribute(moduleName);
		DataSource dataSource = (DataSource) moduleResponse.getAttribute("DATASOURCE");
		if(dataSource.getId()!= null){
			id = dataSource.getId().toString();
			messageIn = (String) moduleResponse.getAttribute("MESSAGE");
			messageSave = DelegatedDetailService.DETAIL_UPDATE;
		} else {
			messageIn = DelegatedDetailService.DETAIL_SELECT;
			messageSave = DelegatedDetailService.DETAIL_INSERT;
		}
	}

	if (messageIn != null
			&& messageIn
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_SELECT)) {
		SourceBean moduleResponse = (SourceBean) aServiceResponse
				.getAttribute(moduleName);
		DataSource dataSource = (DataSource) moduleResponse.getAttribute("DATASOURCE");
		TestMessage testMessage = (TestMessage)moduleResponse.getAttribute("TESTRESULT");
		if (testMessage != null){
			testMessageString = testMessage.getMessage();
			testStatus = testMessage.isOk();
		}
		if (dataSource != null) {
			if(dataSource.getId()!= null)
				id = dataSource.getId().toString();
			if(dataSource.getName()!=null)
				name = dataSource.getName();
			if(dataSource.getDescription()!=null)
				description = dataSource.getDescription();
		}
	}
	
	Map formUrlPars = new HashMap();
//	if(ChannelUtilities.isPortletRunning()) {
		formUrlPars.put("PAGE", "EDataSourcePage");
		formUrlPars.put("MODULE", moduleName);
		formUrlPars.put("MESSAGE", messageSave);
		formUrlPars.put("TYPE_ID", sourceType_id);
		formUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_DISABLED, "true");
//	}
	
	String formUrl = urlBuilder.getUrl(request, formUrlPars);
	
	Map backUrlPars = new HashMap();
	backUrlPars.put("PAGE", "EDataSourcePage");
	backUrlPars.put("TYPE_ID", sourceType_id);
	backUrlPars.put(LightNavigationManager.LIGHT_NAVIGATOR_BACK_TO, "1");
	
	String backUrl = urlBuilder.getUrl(request, backUrlPars);
%>

<script type="text/javascript" language="JavaScript">
	testDataSource('<%= testMessageString%>', <%= testStatus%>)    	
</script>

<table class='header-table-portlet-section'>
  <tr class='header-row-portlet-section'>
    <td class='header-title-column-portlet-section'
      style='vertical-align: middle; padding-left: 5px;'><spagobi:message
      key="<%=title%>"  bundle="<%=messageBundle%>" /></td>
    <td class='header-empty-column-portlet-section'>&nbsp;</td>
<%if (messageSave != null
			&& messageSave
					.equalsIgnoreCase(DelegatedDetailService.DETAIL_UPDATE)) {
	 %>
	<td class='header-button-column-portlet-section' id='testButton'>
		<a href="javascript:saveDS('TEST')"> 
			<img class='header-button-image-portlet-section' 
				title='<spagobi:message key = "s4q.button.test.datasource.title" bundle="<%=messageBundle%>" />' 
				src='<%=urlBuilder.getResourceLinkByTheme(request, "/img/test.png", currTheme)%>' 
				alt='<spagobi:message key = "s4q.button.test.datasource.title" bundle="<%=messageBundle%>" />' 
			/> 
		</a>
	</td>
	<%} %>
    <td class='header-button-column-portlet-section'><a
      href="javascript:document.getElementById('dataSourceForm').submit()"> <img
      class='header-button-image-portlet-section'
      title='<spagobi:message key = "s4q.button.save.title"  bundle="<%=messageBundle%>" />'
      src='<%=urlBuilder.getResourceLinkByTheme(request, "/img/save.png", currTheme)%>'
      alt='<spagobi:message key = "s4q.button.save.title"  bundle="<%=messageBundle%>" />' /> </a></td>
    <td class='header-button-column-portlet-section'><a
      href='<%=backUrl%>'> <img
      class='header-button-image-portlet-section'
      title='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>" />'
      src='<%=urlBuilder.getResourceLinkByTheme(request, "/img/back.png", currTheme)%>'
      alt='<spagobi:message key = "s4q.button.back.title" bundle="<%=messageBundle%>" />' /> </a></td>
  </tr>
</table>

<form method='POST' action='<%=formUrl%>' id='dataSourceForm' name='dataSourceForm'>
<input type='hidden' value='<%=id%>' name='id' />
<input type='hidden' value='' name='TESTDATASOURCE' />

<div class="div_detail_area_forms">
<div class='div_detail_label'><span
  class='portlet-form-field-label'> <spagobi:message
  key="s4q.label.name" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form'><input
  class='portlet-form-input-field' type="text" name="name" size="50"
  value="<%=name%>" maxlength="200"> &nbsp;*</div>

<BR>

<div class='div_detail_label'><span
  class='portlet-form-field-label'> <spagobi:message
  key="s4q.label.description" bundle="<%=messageBundle%>" /> </span></div>
<div class='div_detail_form' style='height: 150px;'>
<textarea name="description" cols="40" style='height: 110px;' class='portlet-text-area-field'><%=description%></textarea>
</div>
</div>
</form>

<spagobi:error />


<%@ include file="/WEB-INF/jsp/commons/footer.jsp"%>