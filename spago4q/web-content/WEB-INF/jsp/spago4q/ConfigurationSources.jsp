<%--

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

--%>

<%@ taglib uri='http://java.sun.com/portlet' prefix='portlet'%>

<%@ include file="/WEB-INF/jsp/commons/portlet_base.jsp"%>

<% String messageBunle = "component_spago4q_messages"; %>

<portlet:defineObjects/>

<table class='header-table-portlet-section'>
  <tr class='header-row-portlet-section'>
    <td class='header-title-column-portlet-section-no-buttons' 
        style='vertical-align:middle;padding-left:5px;'>
    <spagobi:message key = "s4q.configurationSources.title" bundle="<%=messageBunle%>"/>
    </td>
  </tr>
</table>


<div class="div_background">
    <br/> 
  <table>
    <tr class="portlet-font">
      <td width="100" align="center">
        <img src='<%=renderResponse.encodeURL(renderRequest.getContextPath() + "/img/dataSourceIcon.png")%>'/>
      </td>
      <td width="20">
        &nbsp;
      </td>
      <td vAlign="middle">
          <br/> 
        <a href='<portlet:actionURL><portlet:param name="PAGE" value="ESourceTypePage"/></portlet:actionURL>' 
          class="link_main_menu" >
          <spagobi:message key="s4q.wz5.datasources" bundle="<%=messageBunle%>"/>
        </a>
      </td>
    </tr>
        <tr class="portlet-font">
      <td width="100" align="center">
        <img src='<%=renderResponse.encodeURL(renderRequest.getContextPath() + "/img/extractionProcessIcon.png")%>'/>
      </td>
      <td width="20">
        &nbsp;
      </td>
      <td vAlign="middle">
          <br/> 
        <a href='<portlet:actionURL><portlet:param name="PAGE" value="EProcessPage"/></portlet:actionURL>' 
          class="link_main_menu" >
          <spagobi:message key="s4q.wz6.extractionProcess" bundle="<%=messageBunle%>"/>
        </a>
      </td>
    </tr>
    <tr class="portlet-font">
      <td width="100" align="center">
        <img src='<%=renderResponse.encodeURL(renderRequest.getContextPath() + "/img/extractionProcessIcon.png")%>'/>
      </td>
      <td width="20">
        &nbsp;
      </td>
      <td vAlign="middle">
          <br/> 
        <a href='<portlet:actionURL><portlet:param name="PAGE" value="EInterfaceTypePage"/></portlet:actionURL>' 
          class="link_main_menu" >
          <spagobi:message key="s4q.wz6.Interface" bundle="<%=messageBunle%>"/>
        </a>
      </td>
    </tr>
    <tr class="portlet-font">
      <td width="100" align="center">
        <img src='<%=renderResponse.encodeURL(renderRequest.getContextPath() + "/img/extractionProcessIcon.png")%>'/>
      </td>
      <td width="20">
        &nbsp;
      </td>
      <td vAlign="middle">
          <br/> 
        <a href='<portlet:actionURL><portlet:param name="PAGE" value="EScriptPage"/></portlet:actionURL>' 
          class="link_main_menu" >
          <spagobi:message key="s4q.wz6.Script" bundle="<%=messageBunle%>"/>
        </a>
      </td>
    </tr>
    
  </table>
  <br/>
</div>  

<%@ include file="/WEB-INF/jsp/commons/footer.jsp"%>