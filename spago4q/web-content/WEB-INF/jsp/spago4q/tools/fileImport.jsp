<%--

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

--%>

<%@ include file="/WEB-INF/jsp/commons/portlet_base.jsp"%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="it.eng.spago4q.source.utils.SourceTypeUtil"%>
<%@page import="it.eng.spago4q.source.utils.TestMessage"%>
<%@page import="it.eng.spago4q.dao.DAOFactory"%>
<%@page import="it.eng.spago4q.dao.IES4QFKDAO"%>
<%@page import="it.eng.spago4q.dao.IES4QDAO"%>
<%@page import="it.eng.spago4q.bo.EOperation"%>
<%@page import="it.eng.spago4q.tools.service.FileImportModule"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>

<%
String dojoUrl = urlBuilder.getResourceLink(request,"/js/dojo/dojo.js");
%>
 
<script type='text/javascript' src='<%= dojoUrl %>'></script>


<script type='text/javascript'>
	dojo.require('dojo.widget.DropdownDatePicker');
	dojo.require('dojo.widget.DropdownTimePicker');
</script>


<%  
	String dojoFormat ="MM/dd/yyyy";
	String timeFormat ="HH:mm";
	String dateFormat = GeneralUtilities.getLocaleDateFormat(aSessionContainer);
	DateFormat dateFormatter = new SimpleDateFormat(dojoFormat);
	DateFormat timeFormatter = new SimpleDateFormat(timeFormat);
	
	String messageBundle = "component_spago4q_messages"; 
	
	String date= dateFormatter.format(new Date());
	String time= timeFormatter.format(new Date());
	String xPath = "/GENERICITEMS/GENERICITEM";
	Integer operationId = null;
	
	// restore value
	SourceBean moduleResponse = (SourceBean) aServiceResponse
	.getAttribute("FileImportModule");
	
	Date  oldExportDate = (Date) moduleResponse.getAttribute("exportDate");
	if (oldExportDate != null){
		date =  dateFormatter.format(oldExportDate);
	}
	String oldXPath = (String) moduleResponse.getAttribute("xPath");
	if (oldXPath != null){
		xPath = oldXPath;
	}
	String oldOperationID = (String) moduleResponse.getAttribute("operation_id");
	if (oldOperationID != null && !oldOperationID.equals("")){
		operationId = Integer.parseInt(oldOperationID);
	}
	
	

	Map formImportUrlPars = new HashMap();
	String formImportUrl = urlBuilder.getUrl(request, formImportUrlPars);
	
%>

<spagobi:error />

<form method='POST' action='<%=formImportUrl%>' id='importForm' name='importForm' enctype="multipart/form-data">
<input type="hidden" name="PAGE" value="FileImportPage" />
<input type="hidden" name="MESSAGE" value="EXECUTE" />
 	<div style="float:left;width:45%" class="div_detail_area_forms">
		<div class='portlet-section-header' style="float:left;width:78%;">
				<spagobi:message key = "SBISet.import" bundle="component_impexp_messages"/>
		</div>
		<div style="float:left;width:20%;">
		  <center>
			<a class='link_without_dec' style="text-decoration:none;" href="javascript:testExtraction()">
					<img src= '<%= urlBuilder.getResourceLinkByTheme(request, "/img/tools/importexport/importexport32.gif", currTheme) %>'
						title='<spagobi:message key = "SBISet.import" bundle="component_impexp_messages"/>' 
						alt='<spagobi:message key = "SBISet.import" bundle="component_impexp_messages"/>' />
				</a>
			</center>
		</div>
		<div class='div_detail_label'>
		   <span class='portlet-form-field-label'>
			<spagobi:message key = "s4q.file.import.label" bundle="<%=messageBundle%>"/>:
		  </span>
		</div>		
		<div class='div_detail_form' style="float: left;">
			<input type="file"  name="genericItemXml" alt="FileName.zip"/>
		</div>
		<div class='div_detail_label'>
			<span class='portlet-form-field-label'>
				<spagobi:message key="s4q.operation.label"  bundle="<%=messageBundle%>" />
			</span>
		</div>
		<div class='div_detail_form' style="float: left;">
			<select class='portlet-form-field' name="operation_id" >
			<option value="" label="" selected="selected"></option>
			<%
				List operations = ((IES4QFKDAO)DAOFactory.getDAO("EOperationDAO")).loadObjectList(null,null,null);
				Iterator itt = operations.iterator();
				while (itt.hasNext()){
					EOperation operation = (EOperation)itt.next();
					String selected = "";
					if(operation.getId().equals(operationId)){
						selected = "selected = 'selected'";
					}
					%> 		
					<option value="<%= operation.getId() %>" label="<%= operation.getName() %>" <%= selected %>>
						<%= operation.getName() %>	
					</option>
					<%			
				}
			%>
			</select >
		</div>
		<div class='div_detail_label'>
		<span class='portlet-form-field-label'>
			<spagobi:message key="s4q.label.date"  bundle="<%=messageBundle%>" />
		</span>
		</div>
		<div class='div_detail_form' style="float: left;">
			<input type='text' name='exportDate' dojoType='dropdowndatepicker'
			saveFormat='<%=FileImportModule.DATE_FORMAT %>'
			displayFormat='<%= dateFormat %>'
			widgetId='searchDate'
			value='<%= date%>'/>
		</div>
		<div class='div_detail_form' style="float: left;">
			<input type='text' name='exportTime' dojoType='DropdownTimePicker'
			value='<%=time%>'/>
		</div>
		
		<div class='div_detail_label'>
			<span class='portlet-form-field-label'>
			<spagobi:message key="s4q.xpath.label"  bundle="<%=messageBundle%>" />
			</span>
		</div>
		<div class='div_detail_form' style="float: left;">
			<input class='portlet-form-input-field' 
			type="text" name="xPath" size="50"value='<%= xPath%>' maxlength="200">
		</div>
	</div>

	</form>
	<script>
	function executeFileImport() {	
		  document.getElementById('importForm').submit();
	}


	function testExtraction() {
		Ext.MessageBox.confirm('<spagobi:message key = "s4q.confirm.title" bundle="<%=messageBundle%>" />', '<spagobi:message key = "s4q.extraction.execution.confirm.message" bundle="<%=messageBundle%>" />', showResult);	
	}

	function showResult(btn){
		   if (btn == 'yes'){
			   Ext.MessageBox.show({
		           msg: 'Extracting data, please wait...',
		           progressText: 'Extracting...',
		           width:300,
		           wait:true,
		           waitConfig: {interval:200},
		           animEl: 'mb7'
		       });
			   executeFileImport();
		   }
	};
	</script>


<%@ include file="/WEB-INF/jsp/commons/footer.jsp"%>