<%--

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

--%>

<%@ include file="/WEB-INF/jsp/commons/portlet_base.jsp"%>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="it.eng.spago4q.dao.DAOFactory"%>
<%@page import="it.eng.spago4q.dao.IES4QFKDAO"%>
<%@page import="it.eng.spago4q.dao.IES4QDAO"%>
<%@page import="it.eng.spago4q.bo.EOperation"%>
<%@page import="it.eng.spago4q.bo.EProcess"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="it.eng.spagobi.commons.utilities.GeneralUtilities"%>
<%@page import="it.eng.spagobi.tools.datasource.bo.DataSource"%>
<script>
function get_check_value()
{
var c_value = "";
if(document.exportForm.operations.checked ){
	c_value = document.exportForm.operations.value
}	
for (var i=0; i < document.exportForm.operations.length; i++)
   {
   if (document.exportForm.operations[i].checked)
      {
      c_value = c_value + document.exportForm.operations[i].value + ",";
      }
   }
document.exportForm.selectedoperations.value = c_value;
}
function submitExport(){
	get_check_value();
	var divprog = document.getElementById('divProgress');
	divprog.style.display='inline';
	document.getElementById('exportForm').submit();
	var divprog = document.getElementById('divDownload');
	divprog.style.display='none';
	
}

function submitDownloadForm(actionurl) {
	downform = document.getElementById('downForm');
	var divdown = document.getElementById('divDownload');
	divdown.style.display='none';
	downform.submit();
}


</script>



<%
String messageBundle = "component_spago4q_messages";

SourceBean moduleResponse = (SourceBean) aServiceResponse
.getAttribute("S4qImportExportModule");
String exportFile = (String)moduleResponse.getAttribute("EXPORT_FILE");
String exportDir = (String)moduleResponse.getAttribute("EXPORT_DIR");

Map formExportUrlPars = new HashMap();
String formExportUrl = urlBuilder.getUrl(request, formExportUrlPars);

Map formImportUrlPars = new HashMap();
String formImportUrl = urlBuilder.getUrl(request, formImportUrlPars);

String downloadUrl = GeneralUtilities.getSpagoBIProfileBaseUrl(userId);
downloadUrl += "&ACTION_NAME=S4QDOWNLOAD_FILE_ACTION";
if((exportFile!=null) && !exportFile.trim().equalsIgnoreCase("") 
		&& (exportDir!=null) && !exportDir.trim().equalsIgnoreCase("")) {
	downloadUrl += "&OPERATION=downloadExportFile&FILE_NAME="+  exportFile + "&FILE_DIR="+ exportDir +"&FILE_EXTENSION=.zip";
}

%>

<spagobi:error />

<form method='POST' action='<%=formExportUrl%>' id='exportForm' name='exportForm'>
<input type="hidden" name="PAGE" value="S4qImportExportPage" />
<input type="hidden" name="MESSAGE" value="Export" />
<input type="hidden" name="selectedoperations" value="" />
 	<div style="float:left;width:45%" class="div_detail_area_forms">
		<div class='portlet-section-header' style="float:left;width:78%;">
				<spagobi:message key = "SBISet.export" bundle="component_impexp_messages"/>
		</div>
		<div style="float:left;width:20%;">
		  <center>
			<a class='link_without_dec' style="text-decoration:none;" href="javascript:submitExport()">
					<img src= '<%= urlBuilder.getResourceLinkByTheme(request, "/img/tools/importexport/importexport32.gif", currTheme) %>'
						title='<spagobi:message key = "SBISet.export" bundle="component_impexp_messages"/>' 
						alt='<spagobi:message key = "SBISet.export" bundle="component_impexp_messages"/>' />
				</a>
			</center>
		</div>
		<div id="divProgress" style="clear:left;margin-left:15px;;display:none;padding-top:15px;color:#074B88;">
				<spagobi:message key = "SBISet.importexport.opProg" bundle="component_impexp_messages"/>
			</div>
			<div id="divDownload" style="clear:left;display:none;">
				 <div style="padding-left:15px;padding-right:15px;color:#074B88;">
					<spagobi:message key = "SBISet.importexport.opComplete"  bundle="component_impexp_messages"/>
					<a style='text-decoration:none;color:#CC0000;' href="javascript:submitDownloadForm()">
						<spagobi:message key = "Sbi.download" bundle="component_impexp_messages"/>
					</a>
				</div>
				<div style="padding-left:15px;padding-right:15px;color:#074B88;">
					<spagobi:message key = "s4q.importexport.exportCompleteResourcesWarning" bundle="<%=messageBundle%>"/>
				</div>
			</div>
		<div style="clear:left;margin-left:15px;padding-top:10px;">
		<span class='portlet-form-field-label'>
				<spagobi:message key = "SBISet.importexport.nameExp" bundle="component_impexp_messages"/>
				: 
		</span>
				<input type="text" name="exportFileName" size="30" />
		</div>
		
		<div style="clear:left;margin-left:15px;marin-bottom:10px;padding-top:10px;">
			<dl>
<%
	IES4QFKDAO operationDAO = (IES4QFKDAO)DAOFactory.getDAO("EOperationDAO");
	List processes = ((IES4QDAO)DAOFactory.getDAO("EProcessDAO")).loadObjectList(null,null);
	Iterator itt = processes.iterator();
	while (itt.hasNext()){
		EProcess process = (EProcess)itt.next();
%>
		<dt><%=process.getName()%></dt>
<% List operations = operationDAO.loadObjectList(process.getId(),null,null); 
   Iterator itOperation = operations.iterator();
   while (itOperation.hasNext()){
		EOperation operation = (EOperation)itOperation.next();
%>
		<dd><input type="checkbox" name="operations" value='<%=operation.getId()%>' /> - <%=operation.getName()%></dd>
<%
   }
%>			
<%
	}
%>
  			</dl>
			<br>
		</div>
			
	</div>
</form>

<form method='POST' action='<%=downloadUrl%>' id='downForm' name='downForm'>
</form>

<%
if((exportFile!=null) && !exportFile.trim().equalsIgnoreCase("") 
		&& (exportDir!=null) && !exportDir.trim().equalsIgnoreCase("")) {
	%>
		<script>
		var divprog = document.getElementById('divProgress');
		divprog.style.display='none';
		var divprog = document.getElementById('divDownload');
		divprog.style.display='inline';
	</script>
<%
}

%>

<form method='POST' action='<%=formImportUrl%>' id='importForm' name='importForm' enctype="multipart/form-data">
<input type="hidden" name="PAGE" value="S4qImportExportPage" />
<input type="hidden" name="MESSAGE" value="Import" />
 	<div style="float:left;width:45%;padding-bottom:10px;" class="div_detail_area_forms">
		<div class='portlet-section-header' style="float:left;width:78%;">
				<spagobi:message key = "SBISet.import" bundle="component_impexp_messages"/>
		</div>
		<div style="float:left;width:20%;">
		  <center>
			<a class='link_without_dec' style="text-decoration:none;" href="javascript:document.getElementById('importForm').submit()">
					<img src= '<%= urlBuilder.getResourceLinkByTheme(request, "/img/tools/importexport/importexport32.gif", currTheme) %>'
						title='<spagobi:message key = "SBISet.import" bundle="component_impexp_messages"/>' 
						alt='<spagobi:message key = "SBISet.import" bundle="component_impexp_messages"/>' />
				</a>
			</center>
		</div>
		<br>
		<div class='div_detail_label'>
		   <span class='portlet-form-field-label'>
			<spagobi:message key = "s4q.file.import.label" bundle="<%=messageBundle%>"/>:
		  </span>
		</div>		
		<div class='div_detail_form' style="float: left;">
			<input type="file"  name="operationsArchive" alt="FileName.zip"/>
		</div><br>
		<div class='div_detail_label' style="float: left;" id="DATASOURCELABEL">
			<span class='portlet-form-field-label'>
				<spagobi:message key = "SBISet.eng.dataSource" />
			</span>
		</div>	
		<div class='div_detail_form' style="float: left;">
		<select class='portlet-form-field' name="DATASOURCE" id="DATASOURCE" >			
			<!--   option value="-1"></option> -->
			<%

			List dataSources = it.eng.spagobi.commons.dao.DAOFactory.getDataSourceDAO().loadAllDataSources();
			Iterator dataSourceIt = dataSources.iterator();
	
			
			while (dataSourceIt.hasNext()) {
				DataSource dataSourceD = (DataSource) dataSourceIt.next();
					
			 	%>    			 		
    				<option value="<%= dataSourceD.getDsId()  %>" label="<%= dataSourceD.getLabel() %>" >
    					<%=dataSourceD.getLabel() %>	
    				</option>
    				<%				
			  	}
			
			%>
		</select>
		</div>
		<br>
		<div class='div_detail_label'>
			<span class='portlet-form-field-label'>
				<spagobi:message key="s4q.importexport.overwrite"  bundle="<%=messageBundle%>" />
			</span>
		</div>
		<div class='div_detail_form' style="float: left;">
		<select class='portlet-form-field' name="overwrite" >
			<option value="true" label="overwriteValue">true</option>
			<option value="false" label="overwriteValue" selected="selected">false</option>
		</select>
		</div>
	</div>
</form>
 
<%@ include file="/WEB-INF/jsp/commons/footer.jsp"%>