/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.cctq.jtrc;

/**
 * Specifies possible states of issue within a workflow defined in and managed by the tracking system
 * @author Daniele Gagliardi
 *
 */
public class IssueState {
	private String id;
	private String name;
	private String description;
	
	/**
	 * @param id the id of the issue type
	 * @param name the name of the issue type
	 * @param description the description  of the issue type
	 */
	public IssueState(String id, String name, String description){
		this.id=id;
		this.name=name;
		this.description=description;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
