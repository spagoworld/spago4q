/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.cctq.jtrc;

import java.io.Serializable;
import java.util.Date;

/**
 * Represents the main object managed by a tracking systems. An issue can be a
 * task, a bug, a change request, an improvements, and so on. It is identified
 * in a tracking system with a unique identifier, has a state defined by a
 * workflow, and can be related to an action point, which describes a macro
 * activity, build upon a set of atomic tasks that this class represents. Each
 * issue has a subject and a description of the things to do. Workflows are
 * defined in the tracking systems, each system has its own. Each issue has a
 * field to store state labels.
 * 
 * @author Daniele Gagliardi
 * 
 */
public class Issue implements Serializable {

	/**
	 * Class serial version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Unique numeric identifier in the tracking system
	 */
	private String trackerId;
	
	/**
	 * Unique key in the tracking system
	 */
	private String trackerKey;
	/**
	 * The priority of the current issue
	 */
	private String priority;
	/**
	 * Define issue type
	 */
	private IssueType type;

	/**
	 * Textual identification of the current issue
	 */
	private String subject;

	/**
	 * Textual description to explain which activities current issue requires
	 * should be performed
	 */
	private String description;

	/**
	 * Define the issue state in the workflow
	 */
	private String state;

	/**
	 * Describe resolution performed for current issue
	 */
	private IssueResolution resolution;
	
	/**
	 * Identify the person who report the current issue
	 */
	private String reporter;
	
	/**
	 * Identify the person is working on (or should work on) current issue
	 */
	private String assignee;

	/**
	 * Name of the custom field that links to the tracker
	 */
	private String customFieldName;
	/**
	 * Value of the custom field that links to the tracker
	 */
	private String customFieldValue;

	/**
	 * Identifier of the project current issue is related to
	 */
	private String projectReference;

	/**
	 * Issue creation date
	 */
	private Date creationDate;

	/**
	 * Issue due date;
	 */
	private Date dueDate;
	
	/**
	 * URL to the page on remote tracking system sowing current issue
	 */
	private String url;

	/**
	 * Create a new issue with given type and subject. Set
	 * {@link it.eng.cctq.jtrc.IssueState#NEW} as default state and bind to the given project. Description is
	 * left empty. Creation date is set to current system time.
	 * 
	 * @param t
	 *            Issue type
	 * @param s
	 *            Issue subject
	 * @param project
	 *            Project current issue is related to
	 */
	public Issue(IssueType t, String s, String project) {
		if (t == null) {
			throw new NullPointerException("Issue type cannot be null");
		}
		if (s == null) {
			throw new NullPointerException("Issue subject cannot be null");
		}
		if (s == null || s.trim().isEmpty()) {
			throw new IllegalArgumentException("Issue subject cannot be empty");
		}
		if (project == null) {
			throw new NullPointerException("Issue project cannot be null");
		}
		if (project == null || s.trim().isEmpty()) {
			throw new IllegalArgumentException("Issue project cannot be empty");
		}
		this.type = t;
		this.subject = s.trim();
		this.projectReference = project;
//		this.state = IssueState.NEW;
		this.creationDate = new Date();
	}

	/**
	 * Create a new issue with given type and subject, and set a reference to an
	 * action point. Set {@link it.eng.cctq.jtrc.IssueState#NEW} as default
	 * state and bind to the given project. Description is left empty. Creation date is set to current system time.
	 * 
	 * @param t
	 *            Issue type
	 * @param s
	 *            Issue subject Issue subject
	 * @param project
	 *            Project current issue is related to
	 * @param ap
	 *            Action point reference
	 */
	public Issue(IssueType t, String s, String project, String reporter, String cusFieldName, String cusFieldVal) {
		this(t, s, project);
		this.reporter=reporter;
		this.customFieldName=cusFieldName;
		this.customFieldValue=cusFieldVal;
	}

	/**
	 * Set the unique identifier assigned by tracking system
	 * 
	 * @param id
	 *            String representing unique identifier within the tracking
	 *            system
	 */
	public void setTrackerId(String id) {
		if (id != null && !id.trim().isEmpty()) {
			this.trackerId = id.trim();
		}
	}

	/**
	 * Return identifier assigned by tracking system
	 * 
	 * @return String representing unique identifier within the tracking system
	 */
	public String getTrackerId() {
		return this.trackerId;
	}

	/**
	 * Return issue type
	 * 
	 * @return Type describing issue nature
	 */
	public IssueType getType() {
		return this.type;
	}

	/**
	 * Return issue subject
	 * 
	 * @return A string containing a brief description of issue
	 */
	public String getSubject() {
		return this.subject;
	}

	/**
	 * Set a description for current issue
	 * 
	 * @param text
	 *            String containing a description for things to do with current
	 *            issue
	 */
	public void setDescription(String text) {
		this.description = text;
	}

	/**
	 * Return current issue description
	 * 
	 * @return String containing a description for things to do with current
	 *         issue
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Return state within issue workflow
	 * 
	 * @return Issue state
	 */
	public String getState() {
		return this.state;
	}

	/**
	 * Set a resolution for current issue. Workflows can state that if a
	 * resolution is approved, issue can be closed.
	 * 
	 * @param res
	 *            Resolution for current issue
	 */
	public void resolve(IssueResolution res) {
		this.resolution = res;
	}

	/**
	 * Return resolution set for current issue
	 * 
	 * @return resolution decided for current issue
	 */
	public IssueResolution getResolution() {
		return this.resolution;
	}

	/**
	 * Assign current issue to a person which has to work on it
	 * 
	 * @param person
	 *            String identifying person working on current issue
	 */
	public void assignTo(String person) {
		if (person != null && !person.trim().isEmpty()) {
			this.assignee = person.trim();
//			this.state = IssueState.ASSIGNED;
		}
	}

	/**
	 * Return current issue assignee
	 * 
	 * @return String identifying person working on current issue
	 */
	public String getAssignee() {
		return this.assignee;
	}

//	/**
//	 * Set the issue as closed. No further things to do with it
//	 */
//	public void close() {
//		this.state = IssueState.CLOSED;
//	}
	
	/**
	 * Modify issue state, accordingly to the current workflow step
	 * @param newState New state current issue should be in
	 */
	public void modifyState(String newState) {
		this.state = newState;
	}

	/**
	 * Return reference used in tracker to refer to the project
	 * current issue belongs to
	 * @return String containing project reference
	 */
	public String getProject() {
		return this.projectReference;
	}

	/**	
	 * Return creation date for current issue
	 * @return Date and time when current issue was created
	 */
	public Date getCreationDate() {
		return this.creationDate;
	}

	/**
	 * Set the date when the current issue should be completed
	 * @param date Date and time of completion date
	 */
	public void setDueDate(Date date) {
		this.dueDate = date;
	}

	/**
	 * Return the date when the current issue should be completed
	 * @return Date and time of completion date
	 */
	public Date getDueDate() {
		return this.dueDate;
	}
	
	/**
	 * Set URL reference to the page on remote issue tracking system
	 * showing current issue details
	 * @param issueUrl String containing issue URL
	 */
	public void setUrl(String issueUrl) {
		this.url = issueUrl;
	}
	
	/**
	 * Return URL reference to the page on remote issue tracking system
	 * showing current issue details
	 * @return String containing issue URL
	 */
	public String getUrl() {
		return this.url;
	}

	/**
	 * Return the key of the current issue
	 * @return String containing the issue key
	 */
	public String getTrackerKey() {
		return this.trackerKey;
	}
	
	/**
	 * Set the key of the current issue
	 * @param key String containing the key of the issue
	 */
	public void setTrackerKey(String key) {
		this.trackerKey = key;
	}

	/**
	 * @return the priority of the current issue
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority of the current issue to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}

	/**
	 * @return Return the reporter of the current issue
	 */
	public String getReporter() {
		return reporter;
	}

	/**
	 * @param reporter the reporter to set
	 */
	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	/**
	 * @return the customFieldName
	 */
	public String getCustomFieldName() {
		return customFieldName;
	}

	/**
	 * @param customFieldName the customFieldName to set
	 */
	public void setCustomFieldName(String customFieldName) {
		this.customFieldName = customFieldName;
	}

	/**
	 * @return the customFieldValue
	 */
	public String getCustomFieldValue() {
		return customFieldValue;
	}

	/**
	 * @param customFieldValue the customFieldValue to set
	 */
	public void setCustomFieldValue(String customFieldValue) {
		this.customFieldValue = customFieldValue;
	}

}