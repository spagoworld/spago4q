/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.cctq.jtrc;

/**
 * An exception that provide informations on tracker access error or other
 * errors during communication with tracking system.
 * 
 * @author Daniele Gagliardi
 * 
 */
public class TrackerException extends RuntimeException {

	/**
	 * Exception serial version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create a new empty exception with no informations on errors
	 */
	public TrackerException() {
		super();
	}

	/**
	 * Create a new exception with an error message
	 * 
	 * @param msg
	 *            String containing error message
	 */
	public TrackerException(String msg) {
		super(msg);
	}

	/**
	 * Create a new exception with an error message and the cause of error
	 * 
	 * @param msg
	 *            String containing error message
	 * @param ex
	 *            Error cause
	 */
	public TrackerException(String msg, Throwable ex) {
		super(msg, ex);
	}

}
