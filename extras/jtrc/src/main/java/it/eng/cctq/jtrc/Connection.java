/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.cctq.jtrc;

import java.util.Set;

/**
 * A connection with a specific tracker. Tracker remote API are used to create
 * and retrieve tasks. If no remote API is available, implementations should
 * access directly to tracker database (if tracker itself stores tasks on it).
 * 
 * @author Daniele Gagliardi
 *
 *  
 */
public interface Connection {

	/**
	 * Create a new issue on tracking system and return it fully populated with
	 * internal identifier provided by tracking system
	 * 
	 * @param issue
	 *            Issue to create
	 * @return Created issue with tracking system additional info
	 */
	Issue createIssue(Issue issue);

	/**
	 * Update issue informations
	 * 
	 * @param issue
	 *            Issue containing modified info to be stored on remote tracking
	 *            system
	 */
	void updateIssue(Issue issue);

	/**
	 * Delete an issue from remote tracking system
	 * 
	 * @param id
	 *            identifier of the issue to delete
	 */
	void deleteIssue(String id);

	/**
	 * Retrieve issues by action point reference
	 * 
	 * @param actionItemID
	 *            Action item reference
	 * @return all issues related to given action point
	 */
	Set<Issue> findByActionPoint(String actionPointID);

	/**
	 * Retrieve issues assigned to the given person
	 * 
	 * @param assignee
	 *            Issues assignee
	 * @return A set of issues assigned to given person
	 */
	Set<Issue> findByAssignee(String assignee);

	/**
	 * Retrieve all closed issues
	 * 
	 * @return A set of issues with state set to
	 *         {@link it.eng.cctq.jtrc.IssueState#CLOSED}
	 */
	Set<Issue> findClosed();

	/**
	 * Retrieve all assigned issues
	 * 
	 * @return A set of issues that have one assignee
	 */
	Set<Issue> findAssigned();

	/**
	 * Retrieve all new issues
	 * 
	 * @return A set of issues with state set to
	 *         {@link it.eng.cctq.jtrc.IssueState#NEW}
	 */
	Set<Issue> findNew();

	/**
	 * Retrieve all issues
	 * 
	 * @return A set of all issues present in a tracking system
	 */
	Set<Issue> listAllIssues();

	/**
	 * Retrieve all requirements
	 * 
	 * @return A set of issues with type set to
	 *         {@link it.eng.cctq.jtrc.IssueType#REQUIREMENT}
	 */
	Set<Issue> listRequirements();

	/**
	 * Retrieve all bugs
	 * 
	 * @return A set of issues with type set to
	 *         {@link it.eng.cctq.jtrc.IssueType#BUG}
	 */
	Set<Issue> listBugs();

	/**
	 * Retrieve all change requests
	 * 
	 * @return A set of issues with type set to
	 *         {@link it.eng.cctq.jtrc.IssueType#CR}
	 */
	Set<Issue> listChangeRequests();
	
	/**
	 * Return the issue with the given id
	 * @param id The id to search
	 * @return The issue with the given id
	 */
	Issue getIssueByTrackerId(String id);

	IssueType[] getIssueTypesForProject(String projectKey);

	IssueType createIssueType(String typeId, String project);

}