/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.cctq.jtrc;

import java.util.Properties;

/**
 * The interface the every tracker driver must implement.
 * 
 * @author Daniele Gagliardi
 * 
 */
public interface TrackerDriver {

	/**
	 * Connect to tracking system and create a
	 * {@link it.eng.cctq.jtrc.Connection} implementation for connected tracking
	 * system
	 * 
	 * @param trackerConnectionProperties
	 *            Tracking system connection properties. It must contain
	 *            tracking system (endpoint) url, credentials and so on
	 * @return A connection to remote tracking system
	 * @throws TrackerException
	 *             Thrown if a tracking system access error occurs
	 */
	Connection connect(Properties trackerConnectionProperties)
			throws TrackerException;

	/**
	 * Return a string identifying the tracker
	 * 
	 * @return
	 */
	String getTrackerName();

	/**
	 * Return the tracker class name
	 * 
	 * @return String containing tracker class name
	 */
	String getDriverClassName();
}
