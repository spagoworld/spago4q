/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.cctq.jtrc;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class TrackerManager {

	/**
	 * Contains all registered tracker drivers
	 */
	private static Set<TrackerDriver> drivers = new HashSet<TrackerDriver>();

	/**
	 * String identifying JIRA tracker
	 */
	public static final String JIRA = "jira";

	/**
	 * String identifying Trac tracker
	 */
	public static final String TRAC = "trac";

	/**
	 * Create a new connection for a given tracking system type, identfied by
	 * the driver class name. Properties are sued to contain all connection
	 * parameters. Implementations of {@link it.eng.cctq.jtrc.Connection} are
	 * free to use tracking system remote endpoint or direct access to tracking
	 * system database or a combination of two
	 * 
	 * @param trackerDriverClassName
	 *            String containing driver class name
	 * @param properties
	 *            Tracking system connection properties
	 * @return An active connection to tracking system
	 * @throws TrackerException
	 *             Thrown if a tracking system access error occurs
	 */
	public synchronized static Connection getConnection(
			String trackerDriverClassName, Properties properties)
			throws TrackerException {
		if (trackerDriverClassName == null) {
			throw new NullPointerException(
					"Tracker driver class name cannot be null");
		}
		if (trackerDriverClassName.trim().isEmpty()) {
			throw new IllegalArgumentException(
					"Tracker driver class name cannot be empty");
		}
		if (properties == null) {
			throw new NullPointerException(
					"Tracker connection properties cannot be null");
		}
		if (properties.isEmpty()) {
			throw new NullPointerException(
					"Tracker connection properties cannot be empty");
		}
		Connection connection = null;

		for (TrackerDriver currentDriver : drivers) {
			if (trackerDriverClassName.equals(currentDriver
					.getDriverClassName())) {
				connection = currentDriver.connect(properties);
				if (connection != null) {
					break;
				}
			}
		}

		return connection;
	}

	/**
	 * Register a new tracker driver
	 * 
	 * @param driver
	 *            Tracking system driver
	 */
	public static void registerDriver(TrackerDriver driver) {
		if (driver != null) {
			drivers.add(driver);
		}
	}

}