/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.cctq.jtrc;

/**
 * Specifies resolution states for a issue
 * 
 * @author Daniele Gagliardi
 * 
 */
public enum IssueResolution {

	/**
	 * The issue isn't resolved yet
	 */
	UNRESOLVED,

	/**
	 * The issue is resolved
	 */
	FIXED,

	/**
	 * The issue is duplicate of another (that could be already resolved)
	 */
	DUPLICATE,

	/**
	 * This issue won't be fixed (it's not a bug, it's a feature...). Used
	 * primarily for bugs
	 */
	WONTFIX,

	/**
	 * Cannot reproduce issue. Used primarily for bugs
	 */
	WORKSFORME
}
