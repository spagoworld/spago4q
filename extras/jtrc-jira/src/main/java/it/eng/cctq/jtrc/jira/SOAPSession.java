/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.cctq.jtrc.jira;

import java.net.URL;
import java.rmi.RemoteException;
import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;

import com.atlassian.jira.rpc.soap.client.JiraSoapService;
import com.atlassian.jira.rpc.soap.client.JiraSoapServiceService;
import com.atlassian.jira.rpc.soap.client.JiraSoapServiceServiceLocator;
import com.atlassian.jira.rpc.soap.client.RemoteAuthenticationException;

/**
 * This represents a SOAP session with JIRA including that state of being logged in or not
 */
public class SOAPSession {
	
	/**
	 * Class logger
	 */
	private final static Logger LOGGER = Logger.getLogger(SOAPSession.class);
	
    /**
     * SOAP service locator
     */
	private JiraSoapServiceService jiraSoapServiceLocator;
    
	/**
	 * Interface to remote required JIRA SOAP service
	 */
	private JiraSoapService jiraSoapService;
    
	/**
	 * JIRA authentication token, generated after a successful JIRA login
	 */
	private String token;

    /**
     * Create a new session with JIRA remote Web Service Endpoint
     * @param webServicePort Base URL to JIRA remote Web Service Endpoint
     * @throws JiraConnectionException Thrown if something goes wrong retrieving Jira SOAP remote service
     */
	public SOAPSession(URL webServicePort) throws JiraConnectionException {
        jiraSoapServiceLocator = new JiraSoapServiceServiceLocator();
        try {
            if (webServicePort == null) {
            	LOGGER.debug("Base URL null, trying to lookup remote Web Service Endpoint with default...");
                jiraSoapService = jiraSoapServiceLocator.getJirasoapserviceV2();
            }
            else {
            	LOGGER.debug("Trying to lookup remote Web Service Endpoint with base URL " + webServicePort.toExternalForm() + "...");
                jiraSoapService = jiraSoapServiceLocator.getJirasoapserviceV2(webServicePort);
                LOGGER.debug("SOAP Session service endpoint at " + webServicePort.toExternalForm());
            }
        } catch (ServiceException e) {
        	String message = "Error retrieving remote SOAP service at " + (webServicePort.toExternalForm() != null ? webServicePort.toExternalForm() : "default endpoint (provided a null base URL)");
        	LOGGER.error(message, e);
            throw new JiraConnectionException(message, e);
        }
    }

    /**
     * Login to JIRA and store authentication token
     * @param userName username for a JIRA account
     * @param password password for a JIRA account
     * @throws JiraInvalidCredentialsException Thrown if SOAP client provided invalid JIRA credentials
     * @throws JiraConnectionException Thrown if some communication error happens
     * during login
     */
	public void connect(String userName, String password) throws JiraInvalidCredentialsException, JiraConnectionException {
		LOGGER.debug("Connecting via SOAP as : " + userName + "...");
		this.token = null;
        try {
			token = getJiraSoapService().login(userName, password);
		} catch (RemoteAuthenticationException e) {
			String message = "JIRA login error (via SOAP as " + userName + ")";
			LOGGER.error(message, e);
			throw new JiraInvalidCredentialsException(message,e);
		} catch (com.atlassian.jira.rpc.soap.client.RemoteException e) {
			String message = "JIRA SOAP login remote error (via SOAP as " + userName + ")";
			LOGGER.error(message, e);
			throw new JiraConnectionException(message,e);
		} catch (RemoteException e) {
			String message = "Remote error during Jira SOAP login";
			LOGGER.error(message, e);
			throw new JiraConnectionException(message,e);
		}
        LOGGER.debug("Connected");
    }
	
	/**
	 * Logout from JIRA and set to <code>null</code> authentication token 
	 * @param token Authentication token. If <code>null</code> use the current one
     * @throws JiraConnectionException Thrown if some communication error happens
     * during logout
	 */
	public void logout(String token) throws JiraConnectionException {
		LOGGER.debug("Logout from JIRA remote service with token " + token + "...");
		boolean loggedOut = false;
		try {
			if (token != null && !token.isEmpty()) {
				loggedOut = getJiraSoapService().logout(token);
			} else {
				loggedOut = getJiraSoapService().logout(getAuthenticationToken());
			}
		} catch (RemoteException e) {
			String message = "Remote error loggin out from Jira remote service with token " + token;
			LOGGER.error(message, e);
			throw new JiraConnectionException(message,e);
		} finally {
			this.token = null;
		}
		LOGGER.debug(loggedOut ? " Successfully logged out from JIRA remote service" : "Already logget out");
	}

	/**
	 * Logout from JIRA and set to <code>null</code> authentication token 
     * @throws JiraConnectionException Thrown if some communication error happens
     * during logout
	 */
	public void logout() throws JiraConnectionException {
		this.logout(null);
	}

	/**
     * Return JIRA authentication token
     * @return String containing JIRA authentication token or <code>null</code>
     * if no successful login was performed
     */
	public String getAuthenticationToken() {
        return token;
    }

    /**
     * Return an interface connected to JIRA Web Service Endpoint
     * @return Interface to remote required JIRA SOAP service
     */
	public JiraSoapService getJiraSoapService() {
        return jiraSoapService;
    }

    /**
     * Return the current JIRA SOAP service locator
     * @return Locator to retrieve interfaces to JIRA SOAP remote services
     */
	public JiraSoapServiceService getJiraSoapServiceLocator() {
        return jiraSoapServiceLocator;
    }
}
