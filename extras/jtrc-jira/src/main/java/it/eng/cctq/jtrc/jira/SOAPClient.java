/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.cctq.jtrc.jira;

import java.net.URL;
import java.net.MalformedURLException;

import java.rmi.RemoteException;

import org.apache.log4j.Logger;

import com.atlassian.jira.rpc.soap.client.JiraSoapService;

/**
 * Abstract base class for JIRA SOAP clients. It offers methods to configure connection parameters
 * (base URL and JIRA credentials for a principal enabled to perform remote operations)
 *
 */
abstract class SOAPClient {

	/**
	 * Parameter stored in JIRA connection properties file, containing JIRA Web Service Endpoint
	 * for remote users services
	 */
	protected static final String JIRA_BASEURL_PARAM_NAME = "jira.baseurl";

	/**
	 * Parameter stored in JIRA connection properties file, containing JIRA base url used to
	 * compose issue url
	 */
	protected static final String JIRA_HOME_PARAM_NAME = "jira.webhome";

	/**
	 * Parameter stored in JIRA connection properties file, containing username for a JIRA account
	 * with user management permissions 
	 */
	protected static final String JIRA_USER_PARAM_NAME = "jira.username";

	/**
	 * Parameter stored in JIRA connection properties file, containing password for a JIRA account
	 * with user management permissions 
	 */
	protected static final String JIRA_PASSWORD_PARAM_NAME = "jira.password";

	/**
	 * JIRA SOAP Service base URL (default to <code>http://localhost:8080/rpc/soap/jirasoapservice-v2</code>)
	 */
	private String baseUrl = "http://localhost:8080/rpc/soap/jirasoapservice-v2";

	/**
	 * JIRA username for a user enabled to perform remote operations on a JIRA instance.
	 * Default to <code>soapclient</code>
	 */
    private String loginName = "soapclient";

	/**
	 * JIRA password for a user enabled to perform remote operations on a JIRA instance.
	 * Default to <code>soapclient</code>
	 */
    private String loginPassword = "soapclient";
	
    /**
	 * SOAP session
	 */
	private SOAPSession soapSession;
	
	/**
	 * JIRA webhome url
	 */
	private String webHomeUrl;

	/**
	 * Class logger
	 */
	private final static Logger LOGGER = Logger.getLogger(SOAPClient.class);

	/**
	 * Set base URL for JIRA SOAP service
	 * @param jirasoapserviceBaseUrl JIRA SOAP service base url
	 */
	public void setBaseURL(String jirasoapserviceBaseUrl) {
		if (jirasoapserviceBaseUrl == null || jirasoapserviceBaseUrl.isEmpty()) {
			String message = "Invalid JIRA SOAP Service base URL: url empty";
			LOGGER.error(message);
			throw new IllegalArgumentException(message);
		}
		this.baseUrl = jirasoapserviceBaseUrl;
	}
	
	/**
	 * Return base URL for JIRA SOAP service
	 * @return String containing JIRA SOAP service base url
	 */
	public String getBaseURL() {
		return this.baseUrl;
	}
	
	/**
	 * Set JIRA login username
	 * @param jiraUsername String containing JIRA login username
	 */
	public void setUsername(String jiraUsername) {
		if (jiraUsername == null || jiraUsername.isEmpty()) {
			String message = "Invalid JIRA login username: username empty";
			LOGGER.error(message);
			throw new IllegalArgumentException(message);
		}
		this.loginName = jiraUsername.toLowerCase();
	}
	
	/**
	 * Return JIRA login username
	 * @return String containing JIRA login username
	 */
	public String getUsername() {
		return this.loginName;
	}

	/**
	 * Set JIRA login password
	 * @param jiraPassword String containing JIRA login password
	 */
	public void setPassword(String jiraPassword) {
		if (jiraPassword == null || jiraPassword.isEmpty()) {
			String message = "Invalid JIRA login password: password empty";
			LOGGER.error(message);
			throw new IllegalArgumentException(message);
		}
		this.loginPassword = jiraPassword;
	}
	
	/**
	 * Return JIRA login password
	 * @return String containing JIRA login password
	 */
	public String getPassword() {
		return this.loginPassword;
	}
	
	public String getWebHomeUrl() {
		return webHomeUrl;
	}

	public void setWebHomeUrl(String webHomeUrl) {
		this.webHomeUrl = webHomeUrl;
	}

	/**
	 * Connect to JIRA SOAP service using default or previously set base url, username and password
     * @throws JiraInvalidCredentialsException Thrown if default JIRA credentials provided by SOAP client are not valid
     * @throws JiraConnectionException Thrown if some communication error happens
     * during login
	 */
	public void connect() throws JiraConnectionException, JiraInvalidCredentialsException {
		if (this.soapSession == null) {
			if (this.baseUrl == null || this.baseUrl.isEmpty()) {
				String message = "Cannot start a new SOAP session with a null base url. Set a not null base url first";
				LOGGER.error(message);
				throw new JiraConnectionException(message);
			}
			try {
				this.soapSession = new SOAPSession(new URL(this.baseUrl));
			} catch (MalformedURLException e) {
	        	String message = this.baseUrl + ": malformed base URL for JIRA remote SOAP service";
	        	LOGGER.error(message, e);
	            throw new JiraConnectionException(message, e);
			}
		}
		this.soapSession.connect(this.loginName, this.loginPassword);
	}
	
	/**
	 * Connect to JIRA SOAP service using given url, and default or previously set username and password
	 * @param jirasoapserviceBaseUrl JIRA SOAP service base url to connect
     * @throws JiraInvalidCredentialsException Thrown if default JIRA credentials provided by SOAP client are not valid
     * @throws JiraConnectionException Thrown if some communication error happens
     * during login, or if provided base URL has an invalid format
	 */
	public void connect(String jirasoapserviceBaseUrl) throws JiraConnectionException, JiraInvalidCredentialsException {
		if (this.soapSession == null) {
			try {
				this.soapSession = new SOAPSession(new URL(jirasoapserviceBaseUrl));
			} catch (MalformedURLException e) {
	        	String message = jirasoapserviceBaseUrl + ": malformed base URL for JIRA remote SOAP service";
	        	LOGGER.error(message, e);
	            throw new JiraConnectionException(message, e);
			}
		}
		this.soapSession.connect(this.loginName, this.loginPassword);
	}

	/**
	 * Connect to JIRA SOAP service using given username and password, and default or previously set base url
	 * @param jiraUsername String containing JIRA login username
	 * @param jiraPassword String containing JIRA login password
     * @throws JiraInvalidCredentialsException Thrown if SOAP client provided invalid JIRA credentials
     * @throws JiraConnectionException Thrown if some communication error happens
     * during login
	 */
	public void connect(String jiraUsername, String jiraPassword) throws JiraConnectionException, JiraInvalidCredentialsException {
		if (this.soapSession == null) {
			if (this.baseUrl == null || this.baseUrl.isEmpty()) {
				String message = "Cannot start a new SOAP session with a null base url. Set a not null base url first";
				LOGGER.error(message);
				throw new JiraConnectionException(message);
			}
			try {
				this.soapSession = new SOAPSession(new URL(this.baseUrl));
			} catch (MalformedURLException e) {
	        	String message = this.baseUrl + ": malformed base URL for JIRA remote SOAP service";
	        	LOGGER.error(message, e);
	            throw new JiraConnectionException(message, e);
			}
		}
		this.soapSession.connect(jiraUsername.toLowerCase(), jiraPassword);
	}

	/**
	 * Connect to JIRA SOAP service using given url, username and password
	 * @param jirasoapserviceBaseUrl JIRA SOAP service base url to connect
	 * @param jiraUsername String containing JIRA login username
	 * @param jiraPassword String containing JIRA login password
     * @throws JiraInvalidCredentialsException Thrown if SOAP client provided invalid JIRA credentials
     * @throws JiraConnectionException Thrown if some communication error happens
     * during login, or if provided base URL has an invalid format
	 */
	public void connect(String jirasoapserviceBaseUrl, String jiraUsername, String jiraPassword) throws JiraConnectionException, JiraInvalidCredentialsException {
		if (this.soapSession == null) {
			try {
				this.soapSession = new SOAPSession(new URL(jirasoapserviceBaseUrl));
			} catch (MalformedURLException e) {
	        	String message = jirasoapserviceBaseUrl + ": malformed base URL for JIRA remote SOAP service";
	        	LOGGER.error(message, e);
	            throw new JiraConnectionException(message, e);
			}
		}
		this.soapSession.connect(jiraUsername.toLowerCase(), jiraPassword);
	}
	
	/**
	 * Disconnect from JIRA SOAP service
     * @throws JiraConnectionException Thrown if some communication error happens
     * during logout
	 */
	public void disconnect() throws JiraConnectionException, JiraInvalidCredentialsException {
		if (this.soapSession != null) {
			this.soapSession.logout();
		}
	}

	/**
    * Return JIRA SOAP service reference from current SOAP serssion
	* @return JIRA SOAP service reference, offering all remote calls for SOAP clients
	*/
    public JiraSoapService getJiraSoapService() {
        return soapSession.getJiraSoapService();
    }

    /**
	 * Return authentication token from current SOAP session
	 * @return String containing current authentication token (if any)
	 */
	public String getToken() {
        return soapSession.getAuthenticationToken();
    }

    /**
	 * Return authentication token for user corresponding to input credentials
	 * @param jiraUsername String containing JIRA login username
	 * @param jiraPassword String containing JIRA login password
	 * @return String containing authentication token for given user. If input credentials are null or empty, return authentication token from current SOAP session
	 */
	public String getTokenForUser(String jiraUsername, String jiraPassword) throws RemoteException {
        String currentToken;
        if (jiraUsername != null && !jiraUsername.isEmpty() && jiraPassword != null && !jiraPassword.isEmpty())
        {
            currentToken = getJiraSoapService().login(jiraUsername.toLowerCase(), jiraPassword);
        }
        else
        {
            currentToken = getToken();
        }
        return currentToken;
    }
	
	/**
	 * Reset connection parameters to their defaults
	 */
	public void resetConnectionProperties() {
		baseUrl = "http://localhost:8080/rpc/soap/jirasoapservice-v2";
		loginName = "soapclient";
		loginPassword = "soapclient";
	}
	
}