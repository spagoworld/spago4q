/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.cctq.jtrc.jira;

import it.eng.cctq.jtrc.Connection;
import it.eng.cctq.jtrc.Issue;
import it.eng.cctq.jtrc.IssueType;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

import com.atlassian.jira.rpc.soap.client.AbstractRemoteConstant;
import com.atlassian.jira.rpc.soap.client.RemoteAuthenticationException;
import com.atlassian.jira.rpc.soap.client.RemoteException;
import com.atlassian.jira.rpc.soap.client.RemoteField;
import com.atlassian.jira.rpc.soap.client.RemoteFieldValue;
import com.atlassian.jira.rpc.soap.client.RemoteIssue;
import com.atlassian.jira.rpc.soap.client.RemoteIssueType;
import com.atlassian.jira.rpc.soap.client.RemotePermissionException;
import com.atlassian.jira.rpc.soap.client.RemotePriority;
import com.atlassian.jira.rpc.soap.client.RemoteStatus;
import com.atlassian.jira.rpc.soap.client.RemoteValidationException;

/**
 * A connection with JIRA Issue Tracking Systems. Connection methods are base
 * upon JIRA remote XML-RPC
 * 
 * @author Daniele Gagliardi
 * 
 */
public class JIRAConnection extends SOAPClient implements Connection {

	private RemotePriority[] remotePriorityArray;
	private RemoteIssueType[] remoteIssueTypes;
	private RemoteStatus[] remoteStatuses;

	/**
	 * Class logger
	 */
	private final static Logger LOGGER = Logger.getLogger(JIRAConnection.class);

	/**
	 * Create a new connection with a remote instance of JIRA. Requires three
	 * parameters in the in put properties:
	 * <ol>
	 * <li><code>{@link #JIRA_BASEURL_PARAM_NAME}</code>: JIRA base URL, i.e.
	 * JIRA web service endpoint</li>
	 * <li><code>{@link #JIRA_HOME_PARAM_NAME}</code>: JIRA home</li>
	 * <li><code>{@link #JIRA_USER_PARAM_NAME}</code>: JIRA user, a valid JIRA
	 * user enabled to perform remote requests</li>
	 * <li><code>{@link #JIRA_PASSWORD_PARAM_NAME}</code>: JIRA password, the
	 * password to authenticate user</li>
	 * </ol>
	 * 
	 * @param properties
	 */
	public JIRAConnection(Properties properties) {

		LOGGER.debug("Creating new Jira connection with properties "
				+ properties + "...");
		if (properties == null) {
			throw new NullPointerException(
					"JIRA Connection properties cannot be null");
		}
		if (properties.isEmpty()) {
			throw new IllegalArgumentException(
					"JIRA Connection properties cannot be empty");
		}
		if (!properties.containsKey(JIRA_BASEURL_PARAM_NAME)) {
			LOGGER.warn("Missing "
					+ JIRA_BASEURL_PARAM_NAME
					+ " parameter in Jira connection properties, default will be used");
		} else {
			this.setBaseURL(properties.getProperty(JIRA_BASEURL_PARAM_NAME));
		}
		if (!properties.containsKey(JIRA_HOME_PARAM_NAME)) {
			LOGGER.warn("Missing "
					+ JIRA_HOME_PARAM_NAME
					+ " parameter in Jira connection properties, default will be used");
		} else {
			this.setWebHomeUrl(properties.getProperty(JIRA_HOME_PARAM_NAME));
		}
		if (!properties.containsKey(JIRA_USER_PARAM_NAME)) {
			LOGGER.warn("Missing "
					+ JIRA_USER_PARAM_NAME
					+ " parameter in Jira connection properties, default will be used");
		} else {
			this.setUsername(properties.getProperty(JIRA_USER_PARAM_NAME));
		}
		if (!properties.containsKey(JIRA_PASSWORD_PARAM_NAME)) {
			LOGGER.warn("Missing "
					+ JIRA_PASSWORD_PARAM_NAME
					+ " parameter in Jira connection properties, default will be used");
		} else {
			this.setPassword(properties.getProperty(JIRA_PASSWORD_PARAM_NAME));
		}
		LOGGER.info("JIRA connection established.");
		LOGGER.debug("Jira connection properties read from properties "
				+ properties + ".");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.eng.cctq.jtrc.Connection#createIssue(it.eng.cctq.jtrc.Issue)
	 */
	public Issue createIssue(Issue issue) {
		RemoteIssue jiraIssue = new RemoteIssue();
		jiraIssue.setProject(issue.getProject());
		jiraIssue.setType(issue.getType().getId());
		jiraIssue.setSummary(issue.getSubject());
		jiraIssue.setDescription(issue.getDescription());
		// jiraIssue.setPriority(PRIORITY_ID);
		if (issue.getDueDate() != null) {
			Calendar dueDate = Calendar.getInstance();
			dueDate.setTime(issue.getDueDate());
			jiraIssue.setDuedate(dueDate);
		}

		// if (issue.getCustomFieldValue() != null
		// && !issue.getCustomFieldValue().trim().isEmpty()) {
		// String cusFieldId = null;
		// RemoteField[] customFields = null;
		// try {
		// customFields = getJiraSoapService().getCustomFields(getToken());
		// } catch (RemoteException e) {
		// LOGGER.error(
		// "Issue creation: communication error with JIRA remote end point "
		// + getBaseURL()
		// + " while retrieving available custom fields.",
		// e);
		// throw new JiraConnectionException(
		// "Issue creation: communication error with JIRA remote end point "
		// + getBaseURL()
		// + " while retrieving available custom fields.",
		// e);
		// } catch (java.rmi.RemoteException e) {
		// LOGGER.error(
		// "Issue creation: remote method invocation error while retrieving available custom fields.",
		// e);
		// throw new JiraConnectionException(
		// "Issue creation: remote method invocation error while retrieving available custom fields.",
		// e);
		// }
		// for (RemoteField currentField : customFields) {
		// if (issue.getCustomFieldName().equals(currentField.getName())) {
		// cusFieldId = currentField.getId();
		// break;
		// }
		// }
		// if (cusFieldId != null && !cusFieldId.trim().isEmpty()) {
		// // Add action point as a custom field
		// RemoteCustomFieldValue customFieldValue = new RemoteCustomFieldValue(
		// cusFieldId, "", new String[] { issue
		// .getCustomFieldValue().trim() });
		//
		// RemoteCustomFieldValue[] customFieldValues = new
		// RemoteCustomFieldValue[] { customFieldValue };
		// jiraIssue.setCustomFieldValues(customFieldValues);
		// }
		// }

		jiraIssue.setAssignee(issue.getAssignee());
		jiraIssue.setReporter(issue.getReporter());
		RemoteIssue returnedIssue = null;
		try {
			returnedIssue = getJiraSoapService().createIssue(getToken(),
					jiraIssue);
			RemoteField[] issueFields = getJiraSoapService().getFieldsForEdit(
					getToken(), returnedIssue.getKey());
			System.out.println(issueFields);

			String cusFieldId = null;
			for (RemoteField currentField : issueFields) {
				if (issue.getCustomFieldName().equals(currentField.getName())) {
					cusFieldId = currentField.getId();
					break;
				}
			}
			if (cusFieldId != null && !cusFieldId.trim().isEmpty()) {
				// Add action point as a custom field
				RemoteFieldValue customFieldValue = new RemoteFieldValue(
						cusFieldId, new String[] { issue.getCustomFieldValue()
								.trim() });
				getJiraSoapService().updateIssue(getToken(),
						returnedIssue.getKey(),
						new RemoteFieldValue[] { customFieldValue });
			}
		} catch (RemotePermissionException e) {
			LOGGER.error(getUsername()
					+ ": insufficient privileges to create issue on JIRA "
					+ issue.getProject() + " project.", e);
			throw new JiraInsufficientPrivilegesException(getUsername()
					+ ": insufficient privileges to create issue on JIRA "
					+ issue.getProject() + " project.", e);
		} catch (RemoteValidationException e) {
			LOGGER.error("Issue creation: invalid issue " + issue
					+ " or method invocation.", e);
			throw new JIRAOperationException("Issue creation: invalid issue "
					+ issue + " or method invocation.", e);
		} catch (RemoteAuthenticationException e) {
			LOGGER.error(getUsername() + ": invalid credentials for JIRA", e);
			throw new JiraInvalidCredentialsException(getUsername()
					+ ": invalid credentials for JIRA", e);
		} catch (RemoteException e) {
			LOGGER.error(
					"Issue creation: communication error with JIRA remote end point "
							+ getBaseURL() + ".", e);
			throw new JiraConnectionException(
					"Issue creation: communication error with JIRA remote end point "
							+ getBaseURL() + ".", e);
		} catch (java.rmi.RemoteException e) {
			LOGGER.error("Issue creation: remote method invocation error.", e);
			throw new JiraConnectionException(
					"Issue creation: remote method invocation error.", e);
		}
		issue.setTrackerId(returnedIssue.getKey());
		issue.setUrl(JIRA_HOME_PARAM_NAME + "/browse/" + issue.getTrackerId());
		return issue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.eng.cctq.jtrc.Connection#updateIssue(it.eng.cctq.jtrc.Issue)
	 */
	public void updateIssue(Issue issue) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.eng.cctq.jtrc.Connection#deleteIssue(java.lang.String)
	 */
	public void deleteIssue(String id) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.eng.cctq.jtrc.Connection#findByActionPoint(java.lang.String)
	 */
	public Set<Issue> findByActionPoint(String actionPointID) {
		Set<Issue> apIssues = new HashSet<Issue>();
		if (actionPointID != null && !actionPointID.trim().isEmpty()) {
			JiraArraysInit();
			String jiraQuery = actionPointID;
			// "ActionPoint ~ \"" + actionPointID + "\"";
			RemoteIssue[] result = null;
			try {
				result = getJiraSoapService().getIssuesFromJqlSearch(
						getToken(), jiraQuery, Integer.MAX_VALUE);
			} catch (RemoteException e) {
				LOGGER.error("Issue search by action point " + actionPointID
						+ ": communication error with JIRA remote end point "
						+ getBaseURL() + ".", e);
				throw new JiraConnectionException(
						"Issue search by action point "
								+ actionPointID
								+ " communication error with JIRA remote end point "
								+ getBaseURL() + ".", e);
			} catch (java.rmi.RemoteException e) {
				LOGGER.error("Issue search by action point " + actionPointID
						+ ": remote method invocation error.", e);
				throw new JiraConnectionException(
						"Issue search by action point " + actionPointID
								+ ": remote method invocation error..", e);
			}
			if (result != null && result.length > 0) {
				for (RemoteIssue current : result) {
					apIssues.add(convert(current));
				}
			}
		}
		return apIssues;
	}

	public Issue getIssueByTrackerId(String id) {
		Issue issue = null;
		if (id != null && !id.trim().isEmpty()) {
			JiraArraysInit();
			// String query = "id = " + "'" + id + "'";
			RemoteIssue result;
			try {
				result = getJiraSoapService().getIssueById(getToken(), id);
			} catch (RemoteException e) {
				LOGGER.error("Issue search by id " + id
						+ ": communication error with JIRA remote end point "
						+ getBaseURL() + ".", e);
				throw new JiraConnectionException(
						"Issue search by action point "
								+ id
								+ " communication error with JIRA remote end point "
								+ getBaseURL() + ".", e);
			} catch (java.rmi.RemoteException e) {
				LOGGER.error("Issue search by action point " + id
						+ ": remote method invocation error.", e);
				throw new JiraConnectionException("Issue search by id " + id
						+ ": remote method invocation error..", e);
			}
			if (result != null) {
				issue = convert(result);
			}
		}
		return issue;
	}

	private void JiraArraysInit() {
		setRemotePriorityArray();
		setRemoteIssueTypes();
		setRemoteStatuses();

	}

	private void setRemoteIssueTypes() {
		try {
			this.remoteIssueTypes = getJiraSoapService().getIssueTypes(
					getToken());
		} catch (RemotePermissionException e) {
			LOGGER.error(e.getLocalizedMessage());
			throw new JiraConnectionException("Set Remote Issue Types error "
					+ getBaseURL() + ".", e);
		} catch (RemoteAuthenticationException e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw new JiraConnectionException("Set Remote Issue Types error "
					+ getBaseURL() + ".", e);
		} catch (java.rmi.RemoteException e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw new JiraConnectionException("Set Remote Issue Types error "
					+ getBaseURL() + ".", e);
		}

	}

	private void setRemotePriorityArray() {
		try {
			this.remotePriorityArray = getJiraSoapService().getPriorities(
					getToken());
		} catch (RemotePermissionException e) {
			LOGGER.error(e.getLocalizedMessage());
			throw new JiraConnectionException(
					"Set Remote Priority Array error " + getBaseURL() + ".", e);
		} catch (RemoteAuthenticationException e) {
			LOGGER.error(e.getLocalizedMessage());
			throw new JiraConnectionException(
					"Set Remote Priority Array error " + getBaseURL() + ".", e);
		} catch (java.rmi.RemoteException e) {
			LOGGER.error(e.getLocalizedMessage());
			throw new JiraConnectionException(
					"Set Remote Priority Array error " + getBaseURL() + ".", e);
		}
	}

	private void setRemoteStatuses() {
		try {
			this.remoteStatuses = getJiraSoapService().getStatuses(getToken());
		} catch (RemotePermissionException e) {
			LOGGER.error(e.getLocalizedMessage());
			throw new JiraConnectionException(
					"Set Remote Priority Array error " + getBaseURL() + ".", e);
		} catch (RemoteAuthenticationException e) {
			LOGGER.error(e.getLocalizedMessage());
			throw new JiraConnectionException(
					"Set Remote Priority Array error " + getBaseURL() + ".", e);
		} catch (java.rmi.RemoteException e) {
			LOGGER.error(e.getLocalizedMessage());
			throw new JiraConnectionException(
					"Set Remote Priority Array error " + getBaseURL() + ".", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.eng.cctq.jtrc.Connection#findByAssignee(java.lang.String)
	 */
	public Set<Issue> findByAssignee(String assignee) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.eng.cctq.jtrc.Connection#findClosed()
	 */
	public Set<Issue> findClosed() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.eng.cctq.jtrc.Connection#findAssigned()
	 */
	public Set<Issue> findAssigned() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.eng.cctq.jtrc.Connection#findNew()
	 */
	public Set<Issue> findNew() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.eng.cctq.jtrc.Connection#listAllIssues()
	 */
	public Set<Issue> listAllIssues() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.eng.cctq.jtrc.Connection#listRequirements()
	 */
	public Set<Issue> listRequirements() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.eng.cctq.jtrc.Connection#listBugs()
	 */
	public Set<Issue> listBugs() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.eng.cctq.jtrc.Connection#listChangeRequests()
	 */
	public Set<Issue> listChangeRequests() {
		// TODO Auto-generated method stub
		return null;
	}

	private Issue convert(RemoteIssue remoteIssue) {
		Issue issue = null;
		if (remoteIssue != null) {

			issue = new Issue(convertType(remoteIssue),
					remoteIssue.getSummary(), remoteIssue.getProject());
			issue.setTrackerId(remoteIssue.getId());
			issue.setReporter(remoteIssue.getReporter());
			issue.setTrackerKey(remoteIssue.getKey());
			issue.setDescription(remoteIssue.getDescription());
			issue.setPriority(getNameFromId(remoteIssue.getPriority(),
					remotePriorityArray));
			issue.assignTo(remoteIssue.getAssignee());
			issue.modifyState(getNameFromId(remoteIssue.getStatus(),
					remoteStatuses));
			if (remoteIssue.getDuedate() != null) {
				issue.setDueDate(remoteIssue.getDuedate().getTime());
			}
			issue.setUrl(getWebHomeUrl() + "/browse/" + issue.getTrackerKey());
		}
		return issue;
	}

	private IssueType convertType(RemoteIssue remoteIssue) {
		IssueType type = new IssueType(remoteIssue.getType(), getNameFromId(
				remoteIssue.getType(), remoteIssueTypes), getDescriptionFromId(
				remoteIssue.getType(), remoteIssueTypes));
		return type;
	}

	private String getDescriptionFromId(String id,
			AbstractRemoteConstant[] remoteConstantArray) {
		String toReturn = "";
		for (AbstractRemoteConstant remoteConstant : remoteConstantArray) {
			if (remoteConstant.getId().equals(id)) {
				return remoteConstant.getDescription();
			}
		}
		return toReturn;
	}

	// private String getCustomFieldValue(RemoteIssue remoteIssue, String
	// cusFieldName) {
	// String cusFieldValue = null;
	// if (remoteIssue.getCustomFieldValues() != null
	// && remoteIssue.getCustomFieldValues().length > 0) {
	// for (RemoteCustomFieldValue currentCustomField : remoteIssue
	// .getCustomFieldValues()) {
	// if (cusFieldName.equals(currentCustomField
	// .getKey())) {
	// String[] values = currentCustomField.getValues();
	// if (values != null && values.length > 0) {
	// // Take the first value
	// cusFieldValue = values[0];
	// break;
	// }
	// }
	// }
	// }
	// return cusFieldValue;
	// }

	private String getNameFromId(String id,
			AbstractRemoteConstant[] remoteConstantArray) {
		String toReturn = "";
		for (AbstractRemoteConstant remoteConstant : remoteConstantArray) {
			if (remoteConstant.getId().equals(id)) {
				return remoteConstant.getName();
			}
		}
		return toReturn;
	}

	public IssueType[] getIssueTypesForProject(String projectKey) {
		IssueType[] toReturn = null;
		try {
			String projectId = getJiraSoapService().getProjectByKey(getToken(),
					projectKey).getId();
			RemoteIssueType[] remTypes = getJiraSoapService()
					.getIssueTypesForProject(getToken(), projectId);
			int i = 0;
			toReturn = new IssueType[remTypes.length];
			for (RemoteIssueType current : remTypes) {
				toReturn[i++] = new IssueType(current.getId(),
						current.getName(), current.getDescription());
			}
		} catch (RemotePermissionException e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
		} catch (RemoteAuthenticationException e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
		} catch (java.rmi.RemoteException e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
		}
		return toReturn;
	}

	public IssueType createIssueType(String id, String projectKey) {
		IssueType it = null;
		IssueType[] its = this.getIssueTypesForProject(projectKey);
		for (IssueType current : its) {
			if (current.getId().equals(id)) {
				it = new IssueType(id, current.getName(),
						current.getDescription());
				break;
			}
		}
		return it;
	}
}
