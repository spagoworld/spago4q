/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.cctq.jtrc.jira;

import java.util.Properties;

import it.eng.cctq.jtrc.Connection;
import it.eng.cctq.jtrc.TrackerDriver;
import it.eng.cctq.jtrc.TrackerException;
import it.eng.cctq.jtrc.TrackerManager;

/**
 * @author Daniele Gagliardi
 *
 */
public class JIRADriver implements TrackerDriver {
	
	/**
	 * Create a new instance and register itself in Tracker Manager
	 */
	public JIRADriver() {
		TrackerManager.registerDriver(this);
	}

	/* (non-Javadoc)
	 * @see it.eng.cctq.jtrc.TrackerDriver#connect(java.util.Properties)
	 */
	public Connection connect(Properties trackerConnectionProperties)
			throws TrackerException {
		JIRAConnection connection = new JIRAConnection(trackerConnectionProperties);
		connection.connect();
		return connection;
	}

	/* (non-Javadoc)
	 * @see it.eng.cctq.jtrc.TrackerDriver#getTrackerName()
	 */
	public String getTrackerName() {
		return "JIRA Tracker Driver";
	}

	/* (non-Javadoc)
	 * @see it.eng.cctq.jtrc.TrackerDriver#getDriverClassName()
	 */
	public String getDriverClassName() {
		return this.getClass().getName();
	}

}
