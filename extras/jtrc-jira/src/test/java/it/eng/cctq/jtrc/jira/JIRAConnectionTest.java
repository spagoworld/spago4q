/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.cctq.jtrc.jira;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Properties;
import java.util.Set;

import it.eng.cctq.jtrc.Connection;
import it.eng.cctq.jtrc.Issue;
import it.eng.cctq.jtrc.IssueType;
import it.eng.cctq.jtrc.TrackerManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Daniele Gagliardi
 * 
 */
public class JIRAConnectionTest {

	private Connection trackerConnection;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		loadJIRATrackerDriver();
		trackerConnection = TrackerManager.getConnection(
				getJIRATrackerDriverClassName(), getJIRAConnectionProperties());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testAssertTrue() {
		assertTrue(true);
	}

	// /**
	// * Test method for {@link
	// it.eng.cctq.jtrc.jira.JIRAConnection#findByActionPoint(java.lang.String)}.
	// */
	// @Test
	// public final void testFindByActionPoint() {
	// Issue newIssue = buildNewIssue();
	// Issue createdIssue = trackerConnection.createIssue(newIssue);
	// assertNotNull(createdIssue);
	// assertNotNull(createdIssue.getTrackerId());
	// Set<Issue> apIssues = trackerConnection.findByActionPoint(newIssue.);
	// assertNotNull(apIssues);
	// assertTrue("At least one issue for action point " +
	// newIssue.getActionPoint(), apIssues.size() >= 1);
	// assertTrue("At least one issue doesn't contains action point " +
	// newIssue.getActionPoint(), allContainsActionPoint(apIssues,
	// newIssue.getActionPoint()));
	// }

	// /**
	// * Test method for {@link
	// it.eng.cctq.jtrc.jira.JIRAConnection#createIssue(it.eng.cctq.jtrc.Issue)}.
	// */
	// @Test
	// public final void testCreateIssue() {
	// Issue newIssue = buildNewIssue();
	// Issue createdIssue = trackerConnection.createIssue(newIssue);
	// assertNotNull(createdIssue);
	// assertNotNull(createdIssue.getTrackerId());
	// }
	//
	// private Issue buildNewIssue() {
	// Issue issue = new Issue(IssueType.TASK,
	// "Test task for JIRA Connection Unit testing",
	// "JTRC-JIRA Unit tests Action Point", "JCON");
	// issue.assignTo("jcon_devel");
	// issue.setDescription("This is the description");
	// return issue;
	// }

	private String getJIRATrackerDriverClassName() {
		return "it.eng.cctq.jtrc.jira.JIRADriver";
	}

	private Properties getJIRAConnectionProperties() {
		Properties connectionProperties = new Properties();
		connectionProperties.put(JIRAConnection.JIRA_BASEURL_PARAM_NAME,
				"http://127.0.0.1:8080/rpc/soap/jirasoapservice-v2");
		connectionProperties.put(JIRAConnection.JIRA_USER_PARAM_NAME,
				"soap-client");
		connectionProperties.put(JIRAConnection.JIRA_PASSWORD_PARAM_NAME,
				"soap-client");
		return connectionProperties;
	}

	private void loadJIRATrackerDriver() throws ClassNotFoundException,
			InstantiationException, IllegalAccessException {
		Class.forName(getJIRATrackerDriverClassName()).newInstance();
	}

	// private boolean allContainsActionPoint(Collection<Issue> issues, String
	// actionPoint) {
	// boolean contains = false;
	// if (issues != null && !issues.isEmpty()) {
	// contains = true;
	// for (Issue currentIssue : issues) {
	// if (!currentIssue.getActionPoint().equals(actionPoint)) {
	// contains = false;
	// break;
	// }
	// }
	// }
	// return contains;
	// }

}
