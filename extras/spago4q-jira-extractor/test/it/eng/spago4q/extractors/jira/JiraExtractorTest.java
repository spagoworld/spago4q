/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.jira;

import it.eng.spago4q.extractors.AbstractExtractor;
import it.eng.spago4q.extractors.bo.ExtractorParameter;
import it.eng.spago4q.extractors.bo.GenericItemInterface;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class JiraExtractorTest extends TestCase {

	public JiraExtractorTest(String test) {
		super(test);
	}

	public void testExecute() {
		try {
			// Create the parameters
			List<ExtractorParameter> operationParameter = new ArrayList<ExtractorParameter>();
			operationParameter
					.add(new ExtractorParameter("URL",
							"http://www.spagoworld.org/jira/rpc/soap/jirasoapservice-v2"));
			operationParameter.add(new ExtractorParameter("QUERY",
					"PROJECT = 'SPAGO'"));
			List<ExtractorParameter> dataSourceParameter = new ArrayList<ExtractorParameter>();

			// Create the extractor
			AbstractExtractor extractor = new JiraExtractor();
			extractor.init(dataSourceParameter, operationParameter);
			// Check the number of parameters
			assertEquals(2, extractor.getOperationParameter().size());

			List<GenericItemInterface> genericItemExtracted = extractor
					.execute();
			assertTrue(genericItemExtracted != null && genericItemExtracted.size() >0);
//			for (GenericItemInterface genericItemInterface : genericItemExtracted) {
//				System.out.println(genericItemInterface);
//			}

		} catch (Exception e) {
			fail();
		}
	}


}
