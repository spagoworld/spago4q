/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/

package it.eng.spago4q.extractors.jira;

import it.eng.cctq.jtrc.jira.JiraConnectionException;
import it.eng.cctq.jtrc.jira.SOAPSession;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.AbstractExtractor;
import it.eng.spago4q.extractors.bo.GenericItem;
import it.eng.spago4q.extractors.bo.GenericItemInterface;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.atlassian.jira.rpc.soap.client.AbstractRemoteConstant;
import com.atlassian.jira.rpc.soap.client.RemoteAuthenticationException;
import com.atlassian.jira.rpc.soap.client.RemoteIssue;
import com.atlassian.jira.rpc.soap.client.RemoteIssueType;
import com.atlassian.jira.rpc.soap.client.RemotePermissionException;
import com.atlassian.jira.rpc.soap.client.RemotePriority;
import com.atlassian.jira.rpc.soap.client.RemoteResolution;
import com.atlassian.jira.rpc.soap.client.RemoteStatus;

public class JiraExtractor extends AbstractExtractor {
	
	private static Logger logger = Logger
	.getLogger(JiraExtractor.class);

	private static final String URL = "URL";
	private static final String QUERY = "QUERY";

	private String url;
	private String query;
	private SOAPSession soapSession = null;
	private String authToken;

	private RemoteStatus[] remoteStatusArray;
	private RemoteResolution[] remoteResolutionArray;
	private RemoteIssueType[] remoteIssueTypeArray;
	private RemotePriority[] remotePriorityArray;

	@Override
	protected List<GenericItemInterface> extract() throws EMFUserError {
		List<GenericItemInterface> toReturn = new ArrayList<GenericItemInterface>();
		try {
			soapSession = new SOAPSession(new URL(url));
			authToken = soapSession.getAuthenticationToken();

			remoteStatusArray = soapSession.getJiraSoapService().getStatuses(
					authToken);
			remoteResolutionArray = soapSession.getJiraSoapService()
					.getResolutions(authToken);
			remoteIssueTypeArray = soapSession.getJiraSoapService()
					.getIssueTypes(authToken);
			remotePriorityArray = soapSession.getJiraSoapService()
					.getPriorities(authToken);
			RemoteIssue[] remoteIssueArray = {};
			remoteIssueArray = soapSession.getJiraSoapService()
					.getIssuesFromJqlSearch(authToken, query,
							Integer.MAX_VALUE);
			toReturn = getGenericItemsFromRemoteIssueArray(remoteIssueArray);

		} catch (JiraConnectionException e) {
			logger.error(e.getLocalizedMessage());
			throw new EMFUserError("ERROR", e.getLocalizedMessage());
		} catch (MalformedURLException e) {
			logger.error(e.getLocalizedMessage());
			throw new EMFUserError("ERROR", e.getLocalizedMessage());
		} catch (RemotePermissionException e) {
			logger.error(e.getLocalizedMessage());
			throw new EMFUserError("ERROR", e.getLocalizedMessage());
		} catch (RemoteAuthenticationException e) {
			logger.error(e.getLocalizedMessage());
			throw new EMFUserError("ERROR", e.getLocalizedMessage());
		} catch (RemoteException e) {
			logger.error(e.getLocalizedMessage());
			throw new EMFUserError("ERROR", e.getLocalizedMessage());
		}
		return toReturn;
	}

	private List<GenericItemInterface> getGenericItemsFromRemoteIssueArray(
			RemoteIssue[] remoteIssueArray) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<GenericItemInterface> toReturn = new ArrayList<GenericItemInterface>();
		for (int i = 0; i < remoteIssueArray.length; i++) {
			RemoteIssue remoteIssue = remoteIssueArray[i];
			GenericItem gi = new GenericItem();
			gi.setValue("ID", remoteIssue.getId());
			gi.setValue("CREATED", sdf.format(remoteIssue.getCreated()
					.getTime()));
			gi.setValue("UPDATED", sdf.format(remoteIssue.getUpdated()
					.getTime()));
			gi.setValue("STATUS", getNameFromId(remoteIssue.getStatus(),
					remoteStatusArray));
			gi.setValue("TYPE", getNameFromId(remoteIssue.getType(),
					remoteIssueTypeArray));
			gi.setValue("PRIORITY", getNameFromId(remoteIssue.getPriority(),
					remotePriorityArray));
			gi.setValue("RESOLUTION", getNameFromId(
					remoteIssue.getResolution(), remoteResolutionArray));
			gi.setValue("PROJECT",remoteIssue.getProject());
			toReturn.add(gi);
		}
		return toReturn;
	}

	@Override
	protected void setUp() throws EMFUserError {
		url = readOperationParameterValue(URL);
		query = readOperationParameterValue(QUERY);
	}

	@Override
	protected void tearDown() throws EMFUserError {
	}

	private String getNameFromId(String id,
			AbstractRemoteConstant[] remoteConstantArray) {
		String toReturn = "";
		for (AbstractRemoteConstant remoteConstant : remoteConstantArray) {
			if (remoteConstant.getId().equals(id)) {
				toReturn = remoteConstant.getName();
				break;
			}
		}
		return toReturn;
	}

}
