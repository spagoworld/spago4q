package it.eng.spago4q.extractors.example;

import it.eng.spago4q.extractors.AbstractExtractor;
import it.eng.spago4q.extractors.bo.ExtractorParameter;
import it.eng.spago4q.extractors.bo.GenericItemInterface;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class ExtractorTest extends TestCase{

	public ExtractorTest(final String value) {
		super(value);
	}

	public void testExecute() {
		try {
			// Create the parameters
			List<ExtractorParameter> operationParameter = new ArrayList<ExtractorParameter>();
			operationParameter.add(new ExtractorParameter("FIELDS", "PROJECT_NAME:BUGS_NUMBER"));
			List<ExtractorParameter> dataSourceParameter = new ArrayList<ExtractorParameter>();
			dataSourceParameter.add(new ExtractorParameter("DATASOURCE_ID",
					"0"));

			// Create the extractor
			AbstractExtractor extractor = new ExtractorExample();
			extractor.init(dataSourceParameter, operationParameter);
			// Check the number of parameters
			assertEquals(1, extractor.getDataSourceParameter().size());
			assertEquals(1, extractor.getOperationParameter().size());

			List<GenericItemInterface> genericItemExtracted = extractor
					.execute();
			// Check the number of extracted items
			assertEquals(1, genericItemExtracted.size());
			// Check an item value
			assertEquals("PRJ-1", genericItemExtracted.get(0).getValue(
					"PROJECT_NAME"));
		} catch (Exception e) {
			fail();
		}
	}
	
}
