/**

  Spago4Q - The Business Intelligence Free Platform

  Copyright (C) 2009 Engineering Ingegneria Informatica S.p.A.

  Spago4Q is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  Spago4Q is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

**/
package it.eng.spago4q.extractors.example;

import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.extractors.AbstractExtractor;
import it.eng.spago4q.extractors.bo.GenericItemInterface;
import it.eng.spago4q.testUtils.AbstractDBTestCase;

import java.util.List;

public class ExtractorDBTest extends AbstractDBTestCase {

	public ExtractorDBTest(String value) {
		super(value);
	}

	private static final String FLAT_FILE = "test/s4qFlatFile.xml";

	@Override
	protected String getFlatFile() {
		return FLAT_FILE;
	}

	public void testExecute() {
		try {
			// get the operation from the DB
			IES4QFKDAO operationDAO = (IES4QFKDAO) DAOFactory.getDAO("EOperationDAO");
			EOperation operation = (EOperation)operationDAO.loadObjectById(1);

			//Create the extractor
			AbstractExtractor extractor = new ExtractorExample();
			extractor.init(operation);
			//Check the number of parameters
			assertEquals(1, extractor.getDataSourceParameter().size());
			assertEquals(1, extractor.getOperationParameter().size());

			List<GenericItemInterface> genericItemExtracted = extractor.execute();
			
			assertEquals(1, genericItemExtracted.size());
			assertEquals(2, genericItemExtracted.get(0).getKeys().size());

			assertEquals("PRJ-1", genericItemExtracted.get(0).getValue("PROJECT_NAME"));

		} catch (Exception e) {
			fail();
		}
	}

}
