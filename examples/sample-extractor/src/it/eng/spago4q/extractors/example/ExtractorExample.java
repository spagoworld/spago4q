/**

  Spago4Q - The Business Intelligence Free Platform

  Copyright (C) 2009 Engineering Ingegneria Informatica S.p.A.

  Spago4Q is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  Spago4Q is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

**/
package it.eng.spago4q.extractors.example;

import java.util.ArrayList;
import java.util.List;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.datasource.DataSourceHandler;
import it.eng.spago4q.extractors.AbstractExtractor;
import it.eng.spago4q.extractors.bo.GenericItem;
import it.eng.spago4q.extractors.bo.GenericItemInterface;

public class ExtractorExample extends AbstractExtractor {

	public static final String DATASOURCE_ID = "DATASOURCE_ID";
	public static final String FIELDS = "FIELDS";

	private String[] fields;
	private Integer dataSourceId;
	private DataSourceHandler dataSourceHandler = new DataSourceHandler();

	@Override
	protected void setUp() {
		String dataSourceIdParameter = readDataSourceParameterValue(DATASOURCE_ID);
		String fieldsParameter = readOperationParameterValue(FIELDS);
		dataSourceId = transformDataSourceIdParameter(dataSourceIdParameter);
		fields = transformFieldsParameter(fieldsParameter);
	}

	@Override
	protected void tearDown() {
		// Do nothing
	}

	@Override
	protected List<GenericItemInterface> extract() throws EMFUserError {
		List<GenericItemInterface> toReturn = new ArrayList<GenericItemInterface>();
		GenericItem genericItem = new GenericItem();
		for (int i = 0; i < fields.length; i++) {
			String value = dataSourceHandler.getDataSourceValue(dataSourceId,
					fields[i]);
			genericItem.setValue(fields[i], value);
		}
		toReturn.add(genericItem);
		return toReturn;
	}

	private String[] transformFieldsParameter(String fieldsParameter) {
		String[] toReturn = fieldsParameter.split(":");
		return toReturn;
	}

	private Integer transformDataSourceIdParameter(String dataSourceIdParameter) {
		Integer toReturn = null;
		try {
			toReturn = Integer.parseInt(dataSourceIdParameter);
		} catch (NumberFormatException e) {
			toReturn = 0;
		}
		return toReturn;
	}
}
