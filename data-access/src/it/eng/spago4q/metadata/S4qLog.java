/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.metadata;

// Generated 10-feb-2009 10.06.48 by Hibernate Tools 3.2.2.GA

import java.util.Date;

/**
 * S4qLog generated by hbm2java
 */
public class S4qLog implements java.io.Serializable {

	private int idLog;
	private Integer processId;
	private Integer operationId;
	private Integer extracted;
	private Integer inserted;
	private Date operationDate;
	private Date executionDate;
	private String archivePath;
	private Boolean completed;
	private Integer rejected;
	private String rejectedArchivePath;

	public S4qLog() {
	}

	public S4qLog(final int idLog) {
		this.idLog = idLog;
	}

	public S4qLog(final int idLog, final Integer processId,
			final Integer operationId, final Integer extracted,
			final Integer inserted, final Date operationTime,
			final Date extractionTime, final String archivePath,
			final Boolean completed, final Integer rejected,
			final String rejectedArchivePath) {
		this.idLog = idLog;
		this.processId = processId;
		this.operationId = operationId;
		this.extracted = extracted;
		this.inserted = inserted;
		this.operationDate = operationTime;
		this.executionDate = extractionTime;
		this.archivePath = archivePath;
		this.completed = completed;
		this.rejected = rejected;
		this.rejectedArchivePath = rejectedArchivePath;
	}

	public int getIdLog() {
		return this.idLog;
	}

	public void setIdLog(final int idLog) {
		this.idLog = idLog;
	}

	public Integer getProcessId() {
		return this.processId;
	}

	public void setProcessId(final Integer processId) {
		this.processId = processId;
	}

	public Integer getOperationId() {
		return this.operationId;
	}

	public void setOperationId(final Integer operationId) {
		this.operationId = operationId;
	}

	public Integer getExtracted() {
		return this.extracted;
	}

	public void setExtracted(final Integer extracted) {
		this.extracted = extracted;
	}

	public Integer getInserted() {
		return this.inserted;
	}

	public void setInserted(final Integer inserted) {
		this.inserted = inserted;
	}

	public Date getOperationDate() {
		return this.operationDate;
	}

	public void setOperationDate(final Date operationDate) {
		this.operationDate = operationDate;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(final Date extractionDate) {
		this.executionDate = extractionDate;
	}

	public String getArchivePath() {
		return archivePath;
	}

	public void setArchivePath(final String archivePath) {
		this.archivePath = archivePath;
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(final Boolean completed) {
		this.completed = completed;
	}

	public Integer getRejected() {
		return rejected;
	}

	public void setRejected(final Integer rejected) {
		this.rejected = rejected;
	}

	public String getRejectedArchivePath() {
		return rejectedArchivePath;
	}

	public void setRejectedArchivePath(final String rejected_archive_path) {
		this.rejectedArchivePath = rejected_archive_path;
	}
}
