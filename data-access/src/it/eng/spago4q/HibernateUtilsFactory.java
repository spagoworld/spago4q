/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;

import org.apache.log4j.Logger;

public final class HibernateUtilsFactory {

    private HibernateUtilsFactory() {
    }

    private static transient Logger logger = Logger
            .getLogger(HibernateUtilsFactory.class);

    public static HibernateUtilsInterface getHibernateUtils(
            final String hibernateUtils) throws EMFUserError {
        logger.debug("Begin Istantiation of HibernateUtils [" + hibernateUtils
                + "]");

        HibernateUtilsInterface hUtils = null;

        try {
            logger.debug("HibernateUtils Implementation class ["
                    + hibernateUtils + "]");
            
            hUtils = (HibernateUtilsInterface) Class.forName(hibernateUtils)
                    .newInstance();
            logger.debug("HibernateUtils [" + hibernateUtils
                    + "] Instatiate successfully");
            
        } catch (InstantiationException e) {
            throw new EMFUserError(EMFErrorSeverity.ERROR, 1000);
        } catch (IllegalAccessException e) {
            throw new EMFUserError(EMFErrorSeverity.ERROR, 1000);
        } catch (ClassNotFoundException e) {
            throw new EMFUserError(EMFErrorSeverity.ERROR, 1000);
        }

        return hUtils;
    }

}
