/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.InterfaceField;
import it.eng.spago4q.metadata.S4qEInterfaceField;
import it.eng.spago4q.metadata.S4qEInterfaceType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;

public class EInterfaceFieldDAOHibImpl extends AbstractHibernateDAO implements
		IES4QFKDAO {

	private static Logger logger = Logger
			.getLogger(EInterfaceFieldDAOHibImpl.class);

	private static String EINTERFACEFIELD_NAME = "name";
	private static String EINTERFACEFIELD_TYPE = "fieldType";
	private static String EINTERFACEFIELD_KEY = "keyField";
	private static String EINTERFACEFIELD_SENSIBLE = "sensible";

	private String getInterfaceFieldProperty(final String property) {
		String toReturn = null;
		if (property != null && property.toUpperCase().equals("NAME")) {
			toReturn = EINTERFACEFIELD_NAME;
		}
		if (property != null && property.toUpperCase().equals("TYPE")) {
			toReturn = EINTERFACEFIELD_TYPE;
		}
		if (property != null && property.toUpperCase().equals("SENSIBLE")) {
			toReturn = EINTERFACEFIELD_SENSIBLE;
		}
		if (property != null && property.toUpperCase().equals("KEY")) {
			toReturn = EINTERFACEFIELD_KEY;
		}

		return toReturn;
	}

	public boolean deleteObject(final Integer id) throws EMFUserError {
		Session aSession = getCurrentSession();
		Transaction tx = null;
		try {
			tx = aSession.beginTransaction();
			S4qEInterfaceField s4qEInterfaceField = (S4qEInterfaceField) aSession
					.load(S4qEInterfaceField.class, id);
			aSession.delete(s4qEInterfaceField);
			tx.commit();

		} catch (ConstraintViolationException cve) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Impossible to delete a S4qEInterfaceField ", cve);
			throw new EMFUserError(EMFErrorSeverity.WARNING, 10033);
		} catch (HibernateException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Error while delete a S4qEInterfaceField ", e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10012);
		} finally {
			aSession.close();
		}
		return true;

	}

	public Integer insertObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Integer idToReturn;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			InterfaceField interfaceField = (InterfaceField) object;

			S4qEInterfaceField s4qEInterfaceField = new S4qEInterfaceField();

			saveInterfaceFieldValueToS4qEInterfaceField(aSession,
					interfaceField, s4qEInterfaceField);

			idToReturn = (Integer) aSession.save(s4qEInterfaceField);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10030);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}

		return idToReturn;

	}

	private void saveInterfaceFieldValueToS4qEInterfaceField(
			final Session aSession, final InterfaceField interfaceField,
			final S4qEInterfaceField s4qEInterfaceField) {
		String name = interfaceField.getName();
		String fieldType = interfaceField.getFieldType();
		Boolean sensible = interfaceField.getSensible();
		Boolean keyField = interfaceField.getKeyField();

		Integer interfaceTypeId = interfaceField.getInterfaceType().getId();

		S4qEInterfaceType s4qEInterfaceType = (S4qEInterfaceType) aSession
				.load(S4qEInterfaceType.class, interfaceTypeId);

		s4qEInterfaceField.setName(name);
		s4qEInterfaceField.setFieldType(fieldType);
		s4qEInterfaceField.setSensible(sensible);
		s4qEInterfaceField.setKeyField(keyField);

		s4qEInterfaceField.setS4qEInterfaceType(s4qEInterfaceType);
	}

	public InterfaceField loadObjectById(final Integer id) throws EMFUserError {
		logger.debug("IN");
		InterfaceField toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qEInterfaceField hibInterfaceField = (S4qEInterfaceField) aSession
					.load(S4qEInterfaceField.class, id);
			toReturn = toInterfaceField(hibInterfaceField);
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEInterfaceField with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	public List loadObjectList(final Integer interfaceTypeId,
			final String fieldOrder, final String typeOrder)
			throws EMFUserError {
		List<InterfaceField> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<InterfaceField>();
			List<S4qEInterfaceField> toTransform = null;

			Criteria crit = aSession.createCriteria(S4qEInterfaceField.class);

			if (interfaceTypeId != null) {

				S4qEInterfaceType s4qEInterfaceType = (S4qEInterfaceType) aSession
						.load(S4qEInterfaceType.class, interfaceTypeId);

				crit.add(Expression.eq("s4qEInterfaceType", s4qEInterfaceType));
			}

			if (fieldOrder != null && typeOrder != null) {
				if (typeOrder.toUpperCase().trim().equals("ASC")) {
					crit.addOrder(Order
							.asc(getInterfaceFieldProperty(fieldOrder)));
				}
				if (typeOrder.toUpperCase().trim().equals("DESC")) {
					crit.addOrder(Order
							.desc(getInterfaceFieldProperty(fieldOrder)));
				}
			}
			toTransform = crit.list();

			for (Iterator<S4qEInterfaceField> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qEInterfaceField s4qEInterfaceField = iterator.next();

				InterfaceField interfaceField = toInterfaceField(s4qEInterfaceField);

				toReturn.add(interfaceField);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of InterfaceField", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;

	}

	protected static InterfaceField toInterfaceField(
			final S4qEInterfaceField s4qEInterfaceField) {
		InterfaceField toReturn = null;
		if (s4qEInterfaceField != null) {
			toReturn = new InterfaceField();
			toReturn.setId(s4qEInterfaceField.getIdInterfaceField());
			toReturn.setName(s4qEInterfaceField.getName());
			toReturn.setFieldType(s4qEInterfaceField.getFieldType());
			toReturn.setSensible(s4qEInterfaceField.getSensible());
			toReturn.setKeyField(s4qEInterfaceField.getKeyField());

			if (s4qEInterfaceField.getS4qEInterfaceType() != null) {
				toReturn.setInterfaceType(EInterfaceTypeDAOHibImpl
						.toInterfaceType(s4qEInterfaceField
								.getS4qEInterfaceType()));
			}
		}
		return toReturn;
	}

	public void modifyObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();

			InterfaceField interfaceField = (InterfaceField) object;

			S4qEInterfaceField s4qEInterfaceField = (S4qEInterfaceField) aSession
					.load(S4qEInterfaceField.class, interfaceField.getId());

			saveInterfaceFieldValueToS4qEInterfaceField(aSession,
					interfaceField, s4qEInterfaceField);

			aSession.saveOrUpdate(s4qEInterfaceField);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 10032);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}

	}

}
