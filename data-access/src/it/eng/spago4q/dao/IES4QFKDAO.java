/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import java.util.List;

import it.eng.spago.error.EMFUserError;

public interface IES4QFKDAO {

	List loadObjectList(Integer FKId, String fieldOrder, String typeOrder)
			throws EMFUserError;

	Object loadObjectById(Integer id) throws EMFUserError;

	void modifyObject(Object object) throws EMFUserError;

	Integer insertObject(Object object) throws EMFUserError;

	boolean deleteObject(Integer id) throws EMFUserError;

}