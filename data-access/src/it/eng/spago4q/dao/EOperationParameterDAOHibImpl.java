/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago.security.DefaultCipher;
import it.eng.spago4q.bo.EOperationParameter;
import it.eng.spago4q.metadata.S4qEOperation;
import it.eng.spago4q.metadata.S4qEOperationParameter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;

public class EOperationParameterDAOHibImpl extends AbstractHibernateDAO
		implements IES4QFKDAO {

	private static Logger logger = Logger
			.getLogger(EOperationParameterDAOHibImpl.class);

	private static String OPERATIONPARAMETER_NAME = "name";
	private static String OPERATIONPARAMETER_VALUE = "value";

	public boolean deleteObject(final Integer id) throws EMFUserError {
		Session aSession = getCurrentSession();
		Transaction tx = null;
		try {
			tx = aSession.beginTransaction();
			S4qEOperationParameter s4qEOperationParameter = (S4qEOperationParameter) aSession
					.load(S4qEOperationParameter.class, id);
			aSession.delete(s4qEOperationParameter);
			tx.commit();

		} catch (ConstraintViolationException cve) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Impossible to delete a S4qEOperationParameter ", cve);
			throw new EMFUserError(EMFErrorSeverity.WARNING, 10028);
		} catch (HibernateException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Error while delete a S4qEOperationParameter ", e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10012);
		} finally {
			aSession.close();
		}
		return true;
	}

	public Integer insertObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Integer idToReturn;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			EOperationParameter eOperationParameter = (EOperationParameter) object;

			S4qEOperationParameter s4qEOperationParameter = new S4qEOperationParameter();

			saveEOperationParameterValueToS4qEOperationParameter(aSession,
					eOperationParameter, s4qEOperationParameter);

			idToReturn = (Integer) aSession.save(s4qEOperationParameter);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10030);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
		return idToReturn;

	}

	private void saveEOperationParameterValueToS4qEOperationParameter(
			final Session aSession,
			final EOperationParameter eOperationParameter,
			final S4qEOperationParameter s4qEOperationParameter) {
		String name = eOperationParameter.getName();
		String value = eOperationParameter.getValue();
		Boolean cript = eOperationParameter.getCript();

		Integer operationId = eOperationParameter.getEOperation().getId();

		S4qEOperation s4qEOperation = (S4qEOperation) aSession.load(
				S4qEOperation.class, operationId);

		if (cript) {
			DefaultCipher dc = new DefaultCipher();
			value = dc.encrypt(value);
		}

		s4qEOperationParameter.setName(name);
		s4qEOperationParameter.setValue(value);
		s4qEOperationParameter.setCript(cript);
		s4qEOperationParameter.setS4qEOperation(s4qEOperation);
	}

	public Object loadObjectById(final Integer id) throws EMFUserError {
		logger.debug("IN");
		EOperationParameter toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qEOperationParameter hibOperationParameter = (S4qEOperationParameter) aSession
					.load(S4qEOperationParameter.class, id);
			toReturn = toOperationParameter(hibOperationParameter);
		} catch (HibernateException he) {
			logger.error(
					"Error while loading the S4qEOperationParameter with id "
							+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	private EOperationParameter toOperationParameter(
			final S4qEOperationParameter s4qEOperationParameter) {
		EOperationParameter toReturn = null;
		if (s4qEOperationParameter != null) {
			toReturn = new EOperationParameter();
			toReturn.setId(s4qEOperationParameter.getIdOperationParameter());
			toReturn.setName(s4qEOperationParameter.getName());
			toReturn.setCript(s4qEOperationParameter.getCript());

			String value = s4qEOperationParameter.getValue();
			if (s4qEOperationParameter.getCript()!= null && s4qEOperationParameter.getCript()) {
				DefaultCipher dc = new DefaultCipher();
				value = dc.decrypt(value);
			}
			toReturn.setValue(value);

			if (s4qEOperationParameter.getS4qEOperation() != null) {
				toReturn
						.setEOperation(EOperationDAOHibImpl
								.toOperation(s4qEOperationParameter
										.getS4qEOperation()));
			}
		}
		return toReturn;

	}

	public List loadObjectList(final Integer FKId, final String fieldOrder,
			final String typeOrder) throws EMFUserError {
		List<EOperationParameter> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<EOperationParameter>();
			List<S4qEOperationParameter> toTransform = null;

			Criteria crit = aSession
					.createCriteria(S4qEOperationParameter.class);

			S4qEOperation s4qEOperation = (S4qEOperation) aSession.load(
					S4qEOperation.class, FKId);

			crit.add(Expression.eq("s4qEOperation", s4qEOperation));

			if (fieldOrder != null && typeOrder != null) {
				if (typeOrder.toUpperCase().trim().equals("ASC")) {
					crit.addOrder(Order
							.asc(getOperationParameterProperty(fieldOrder)));
				}
				if (typeOrder.toUpperCase().trim().equals("DESC")) {
					crit.addOrder(Order
							.desc(getOperationParameterProperty(fieldOrder)));
				}
			}
			toTransform = crit.list();

			for (Iterator<S4qEOperationParameter> iterator = toTransform
					.iterator(); iterator.hasNext();) {
				S4qEOperationParameter s4qEOperationParameter = iterator.next();

				EOperationParameter operationParameter = toOperationParameter(s4qEOperationParameter);

				toReturn.add(operationParameter);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of OperationParameter",
					he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	private String getOperationParameterProperty(final String property) {
		String toReturn = null;
		if (property != null && property.toUpperCase().equals("NAME")) {
			toReturn = OPERATIONPARAMETER_NAME;
		}
		if (property != null && property.toUpperCase().equals("VALUE")) {
			toReturn = OPERATIONPARAMETER_VALUE;
		}
		return toReturn;
	}

	public void modifyObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			EOperationParameter operationParameter = (EOperationParameter) object;
			S4qEOperationParameter s4qEOperationParameter = (S4qEOperationParameter) aSession
					.load(S4qEOperationParameter.class, operationParameter
							.getId());

			saveEOperationParameterValueToS4qEOperationParameter(aSession,
					operationParameter, s4qEOperationParameter);

			aSession.saveOrUpdate(s4qEOperationParameter);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 10032);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");

	}

}
