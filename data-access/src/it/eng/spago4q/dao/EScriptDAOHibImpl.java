/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.Script;
import it.eng.spago4q.metadata.S4qEScript;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;

public class EScriptDAOHibImpl extends AbstractHibernateDAO implements IES4QDAO {

	private static Logger logger = Logger.getLogger(EScriptDAOHibImpl.class);

	private static String SCRIPT_NAME = "name";

	private String getScriptProperty(final String property) {
		String toReturn = null;
		if (property != null && property.toUpperCase().equals("NAME")) {
			toReturn = SCRIPT_NAME;
		}
		return toReturn;
	}

	public boolean deleteObject(final Integer id) throws EMFUserError {
		Session aSession = getCurrentSession();
		Transaction tx = null;
		try {
			tx = aSession.beginTransaction();
			S4qEScript s4qEScript = (S4qEScript) aSession.load(
					S4qEScript.class, id);
			aSession.delete(s4qEScript);
			tx.commit();

		} catch (ConstraintViolationException cve) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Impossible to delete a Script ", cve);
			throw new EMFUserError(EMFErrorSeverity.WARNING, 10024);
		} catch (HibernateException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Error while delete a Script ", e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10012);
		} finally {
			aSession.close();
		}
		return true;

	}

	public Integer insertObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Integer idToReturn;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();

			Script script = (Script) object;

			S4qEScript s4qEScript = new S4qEScript();

			saveEScriptValueToS4qEScript(script, s4qEScript);

			idToReturn = (Integer) aSession.save(s4qEScript);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10030);

		} finally {
			if (aSession != null) {
				if (aSession.isOpen()) {
					aSession.close();
				}
			}
		}
		logger.debug("OUT");
		return idToReturn;
	}

	private void saveEScriptValueToS4qEScript(final Script ascript,
			final S4qEScript s4qEScript) {
		String name = ascript.getName();
		String description = ascript.getDescription();
		String scriptType = ascript.getScriptType();
		String script = ascript.getScript();

		s4qEScript.setName(name);
		s4qEScript.setDescription(description);
		s4qEScript.setScriptType(scriptType);
		s4qEScript.setScript(script);
	}

	public Object loadObjectById(final Integer id) throws EMFUserError {
		logger.debug("IN");
		Script toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qEScript hibScript = (S4qEScript) aSession.load(S4qEScript.class,
					id);
			toReturn = toScript(hibScript);
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEScript with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;

	}

	public List loadObjectList(final String fieldOrder, final String typeOrder)
			throws EMFUserError {
		logger.debug("IN");
		List<Script> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<Script>();
			List<S4qEScript> toTransform = null;

			Criteria crit = aSession.createCriteria(S4qEScript.class);

			if (fieldOrder != null && typeOrder != null) {
				if (typeOrder.toUpperCase().trim().equals("ASC")) {
					crit.addOrder(Order.asc(getScriptProperty(fieldOrder)));
				}
				if (typeOrder.toUpperCase().trim().equals("DESC")) {
					crit.addOrder(Order.desc(getScriptProperty(fieldOrder)));
				}
			}
			toTransform = crit.list();

			for (Iterator<S4qEScript> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qEScript s4qEScript = iterator.next();

				Script aScript = toScript(s4qEScript);

				toReturn.add(aScript);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of EScript", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	protected static Script toScript(final S4qEScript s4qEScript) {
		Script toReturn = null;
		if (s4qEScript != null) {
			toReturn = new Script();
			toReturn.setId(s4qEScript.getIdScript());
			toReturn.setName(s4qEScript.getName());
			toReturn.setDescription(s4qEScript.getDescription());
			toReturn.setScriptType(s4qEScript.getScriptType());
			toReturn.setScript(s4qEScript.getScript());
		}
		return toReturn;
	}

	public void modifyObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			Script script = (Script) object;

			S4qEScript s4qEScript = (S4qEScript) aSession.load(
					S4qEScript.class, script.getId());

			saveEScriptValueToS4qEScript(script, s4qEScript);

			aSession.saveOrUpdate(s4qEScript);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 10032);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");

	}

	protected static S4qEScript initializeScript(final S4qEScript script){
		S4qEScript toReturn = new S4qEScript();
		toReturn.setIdScript(script.getIdScript());
		toReturn.setName(script.getName());
		toReturn.setDescription(script.getDescription());
		toReturn.setScript(script.getScript());
		toReturn.setScriptType(script.getScriptType());
		return toReturn;
	}
	
	public S4qEScript getObjectById(final Integer id){
		Session aSession = null;
		Transaction tx = null;
		S4qEScript toReturn = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			
			S4qEScript toInitialize = (S4qEScript)aSession.load(S4qEScript.class, id);
			toReturn = initializeScript(toInitialize);
			
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEScript with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

		//	throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
		return toReturn;
	}
	
}
