/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.metadata.S4qEDataSource;
import it.eng.spago4q.metadata.S4qEInterfaceField;
import it.eng.spago4q.metadata.S4qEInterfaceType;
import it.eng.spago4q.metadata.S4qEOperation;
import it.eng.spago4q.metadata.S4qEOperationField;
import it.eng.spago4q.metadata.S4qEOperationParameter;
import it.eng.spago4q.metadata.S4qEProcess;
import it.eng.spago4q.metadata.S4qEScript;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;

public class EOperationDAOHibImpl extends AbstractHibernateDAO implements
		IES4QFKDAO {

	private static Logger logger = Logger.getLogger(EOperationDAOHibImpl.class);

	private static String OPERATION_NAME = "name";

	public boolean deleteObject(final Integer id) throws EMFUserError {
		Session aSession = getCurrentSession();
		Transaction tx = null;
		try {
			tx = aSession.beginTransaction();
			S4qEOperation s4qEOperation = (S4qEOperation) aSession.load(
					S4qEOperation.class, id);
			aSession.delete(s4qEOperation);
			tx.commit();

		} catch (ConstraintViolationException cve) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Impossible to delete a S4qEOperation ", cve);
			throw new EMFUserError(EMFErrorSeverity.WARNING, 10027);
		} catch (HibernateException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Error while delete a S4qEOperation ", e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10012);
		} finally {
			aSession.close();
		}
		return true;
	}

	public Integer insertObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Integer idToReturn;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			EOperation eOperation = (EOperation) object;

			S4qEOperation s4qEOperation = new S4qEOperation();

			saveOperationValueToS4qEOperation(aSession, eOperation,
					s4qEOperation);

			idToReturn = (Integer) aSession.save(s4qEOperation);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10030);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
		return idToReturn;
	}

	private void saveOperationValueToS4qEOperation(final Session aSession,
			final EOperation operation, final S4qEOperation s4qEOperation) {
		String name = operation.getName();
		String description = operation.getDescription();

		Integer processId = operation.getEProcess().getId();
		Integer dataSourceId = operation.getDataSource().getId();
		Integer interfaceTypeId = operation.getInterfaceType().getId();
		Boolean archive = operation.getArchive();
		Boolean rejected = operation.getRejected();

		S4qEDataSource s4qEDataSource = (S4qEDataSource) aSession.load(
				S4qEDataSource.class, dataSourceId);

		S4qEProcess s4qEProcess = (S4qEProcess) aSession.load(
				S4qEProcess.class, processId);

		S4qEInterfaceType s4qEInterfaceType = (S4qEInterfaceType) aSession
				.load(S4qEInterfaceType.class, interfaceTypeId);

		s4qEOperation.setName(name);
		s4qEOperation.setDescription(description);
		s4qEOperation.setArchive(archive);
		s4qEOperation.setRejected(rejected);
		s4qEOperation.setS4qEDataSource(s4qEDataSource);
		s4qEOperation.setS4qEProcess(s4qEProcess);
		s4qEOperation.setS4qEInterfaceType(s4qEInterfaceType);
	}

	public Object loadObjectById(final Integer id) throws EMFUserError {
		logger.debug("IN");
		EOperation toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qEOperation hibEOperation = (S4qEOperation) aSession.load(
					S4qEOperation.class, id);
			toReturn = toOperation(hibEOperation);
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEOperation with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	public List loadObjectList(final Integer FKId, final String fieldOrder,
			final String typeOrder) throws EMFUserError {
		List<EOperation> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<EOperation>();
			List<S4qEOperation> toTransform = null;

			Criteria crit = aSession.createCriteria(S4qEOperation.class);
			if (FKId != null) {
				S4qEProcess s4qEProcess = (S4qEProcess) aSession.load(
						S4qEProcess.class, FKId);

				crit.add(Expression.eq("s4qEProcess", s4qEProcess));

				if (fieldOrder != null && typeOrder != null) {
					if (typeOrder.toUpperCase().trim().equals("ASC")) {
						crit.addOrder(Order
								.asc(getOperationProperty(fieldOrder)));
					}
					if (typeOrder.toUpperCase().trim().equals("DESC")) {
						crit.addOrder(Order
								.desc(getOperationProperty(fieldOrder)));
					}
				}
			}
			toTransform = crit.list();

			for (Iterator<S4qEOperation> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qEOperation s4qEOperation = iterator.next();

				EOperation operation = toOperation(s4qEOperation);

				toReturn.add(operation);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of Operation", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	private String getOperationProperty(final String property) {
		String toReturn = null;
		if (property != null && property.toUpperCase().equals("NAME")) {
			toReturn = OPERATION_NAME;
		}
		return toReturn;
	}

	protected static EOperation toOperation(final S4qEOperation s4qEOperation) {
		EOperation toReturn = null;
		if (s4qEOperation != null) {
			toReturn = new EOperation();
			toReturn.setId(s4qEOperation.getIdOperation());
			toReturn.setName(s4qEOperation.getName());
			toReturn.setDescription(s4qEOperation.getDescription());
			toReturn.setArchive(s4qEOperation.getArchive());
			toReturn.setRejected(s4qEOperation.getRejected());
			if (s4qEOperation.getS4qEProcess() != null) {
				toReturn.setEProcess((EProcessDAOHibImpl
						.toEProcess(s4qEOperation.getS4qEProcess())));
			}
			if (s4qEOperation.getS4qEInterfaceType() != null) {
				toReturn
						.setInterfaceType(EInterfaceTypeDAOHibImpl
								.toInterfaceType((s4qEOperation
										.getS4qEInterfaceType())));
			}
			if (s4qEOperation.getS4qEDataSource() != null) {
				toReturn.setDataSource(EDataSourceDAOHibImpl
						.toDataSource((s4qEOperation.getS4qEDataSource())));
			}
		}
		return toReturn;

	}

	public void modifyObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			EOperation operation = (EOperation) object;
			S4qEOperation s4qEOperation = (S4qEOperation) aSession.load(
					S4qEOperation.class, operation.getId());

			saveOperationValueToS4qEOperation(aSession, operation,
					s4qEOperation);

			aSession.saveOrUpdate(s4qEOperation);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 10032);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
	}

	public S4qEOperation getObjectById(final Integer id) {
		Session aSession = null;
		Transaction tx = null;
		S4qEOperation toReturn = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();

			S4qEOperation toInitialize = (S4qEOperation) aSession.load(
					S4qEOperation.class, id);
			toReturn = initializeOperation(toInitialize);

		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEOperation with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			// throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
		return toReturn;
	}

	protected S4qEOperation initializeOperation(final S4qEOperation toInitialize) {
		S4qEOperation operation = new S4qEOperation();
		operation.setIdOperation(toInitialize.getIdOperation());
		operation.setName(toInitialize.getName());
		operation.setDescription(toInitialize.getDescription());
		operation.setArchive(toInitialize.getArchive());

		S4qEProcess process = new S4qEProcess();

		process.setIdProcess(toInitialize.getS4qEProcess().getIdProcess());
		process.setName(toInitialize.getS4qEProcess().getName());
		process.setDescription(toInitialize.getS4qEProcess().getDescription());
		process.setCoordinatorClass(toInitialize.getS4qEProcess()
				.getCoordinatorClass());

		operation.setS4qEProcess(process);
		operation.setS4qEDataSource(EDataSourceDAOHibImpl
				.initializeDataSource(toInitialize.getS4qEDataSource()));
		operation.setS4qEInterfaceType(EInterfaceTypeDAOHibImpl
				.initializeInterfaceType(toInitialize.getS4qEInterfaceType()));

		Set operationParameters = new HashSet();

		for (Object operationParameterObj : toInitialize
				.getS4qEOperationParameters()) {
			S4qEOperationParameter opeartionParameterToInitialize =
				(S4qEOperationParameter) operationParameterObj;
			S4qEOperationParameter operationParameter = new S4qEOperationParameter();
			operationParameter
					.setIdOperationParameter(opeartionParameterToInitialize
							.getIdOperationParameter());
			operationParameter
					.setName(opeartionParameterToInitialize.getName());
			operationParameter.setValue(opeartionParameterToInitialize
					.getValue());
			operationParameters.add(operationParameter);
		}
		operation.setS4qEOperationParameters(operationParameters);

		Set operationFields = new HashSet();

		for (Object operationFieldObj : toInitialize.getS4qEOperationFields()) {
			S4qEOperationField operationFieldToInitialize = (S4qEOperationField) operationFieldObj;
			S4qEOperationField operationField = new S4qEOperationField();
			operationField.setIdOperationField(operationFieldToInitialize
					.getIdOperationField());
			operationField.setName(operationFieldToInitialize.getName());
			if (operationFieldToInitialize.getS4qEScript() != null) {
				S4qEScript script = new S4qEScript();
				script.setIdScript(operationFieldToInitialize.getS4qEScript()
						.getIdScript());
				script.setName(operationFieldToInitialize.getS4qEScript()
						.getName());
				operationField.setS4qEScript(script);
			}

			if (operationFieldToInitialize.getS4qEInterfaceField() != null) {
				S4qEInterfaceField interfaceField = new S4qEInterfaceField();
				interfaceField.setIdInterfaceField(operationFieldToInitialize
						.getS4qEInterfaceField().getIdInterfaceField());
				interfaceField.setName(operationFieldToInitialize
						.getS4qEInterfaceField().getName());
				operationField.setS4qEInterfaceField(interfaceField);
			}
			operationFields.add(operationField);
		}

		operation.setS4qEOperationFields(operationFields);
		return operation;
	}

}
