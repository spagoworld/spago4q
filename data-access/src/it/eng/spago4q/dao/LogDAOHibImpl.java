/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.ELog;
import it.eng.spago4q.metadata.S4qLog;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;

public class LogDAOHibImpl extends AbstractHibernateDAO implements IES4QFKDAO,
		ILogDAO {

	private static Logger logger = Logger.getLogger(LogDAOHibImpl.class);

	private static String ELOG_INSERTED = "inserted";
	private static String ELOG_EXTRACTED = "extracted";
	private static String ELOG_OPERATIONDATE = "operationDate";
	private static String ELOG_EXECUTIONDATE = "executionDate";
	private static String ELOG_ARCHIVEPATH = "archivePath";
	private static String ELOG_COMPLETED = "completed";
	private static String ELOG_REJECTED = "rejected";

	private String getELogProperty(final String property) {
		String toReturn = null;
		if (property != null && property.toUpperCase().equals("EXTRACTED")) {
			toReturn = ELOG_EXTRACTED;
		}
		if (property != null && property.toUpperCase().equals("INSERTED")) {
			toReturn = ELOG_INSERTED;
		}
		if (property != null && property.toUpperCase().equals("OPERATION_DATE")) {
			toReturn = ELOG_OPERATIONDATE;
		}
		if (property != null && property.toUpperCase().equals("EXECUTION_DATE")) {
			toReturn = ELOG_EXECUTIONDATE;
		}
		if (property != null && property.toUpperCase().equals("ARCHIVE")) {
			toReturn = ELOG_ARCHIVEPATH;
		}
		if (property != null && property.toUpperCase().equals("COMPLETED")){
			toReturn = ELOG_COMPLETED;
		}
		if (property != null && property.toUpperCase().equals("REJECTED")){
			toReturn = ELOG_REJECTED;
		}
		return toReturn;
	}

	public boolean deleteObject(final Integer id) throws EMFUserError {
		Session aSession = getCurrentSession();
		Transaction tx = null;
		try {
			tx = aSession.beginTransaction();
			S4qLog s4qLog = (S4qLog) aSession.load(S4qLog.class, id);
			aSession.delete(s4qLog);
			tx.commit();

		} catch (ConstraintViolationException cve) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Impossible to delete a S4QLog ", cve);
			throw new EMFUserError(EMFErrorSeverity.WARNING, 10024);
		} catch (HibernateException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Error while delete a Script ", e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
		} finally {
			aSession.close();
		}
		return true;

	}

	public Integer insertObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Integer idToReturn;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();

			ELog log = (ELog) object;

			S4qLog s4qLog = new S4qLog();

			saveELogValueToS4qLog(log, s4qLog);

			idToReturn = (Integer) aSession.save(s4qLog);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
		return idToReturn;
	}

	private void saveELogValueToS4qLog(final ELog log, final S4qLog s4qlog) {
		Integer processId = log.getProcessId();
		Integer operationId = log.getOperationId();
		Integer extracted = log.getExtracted();
		Integer inserted = log.getInserted();
		Date operationTime = log.getOperationDate();
		Date extractionTime = log.getExecutionDate();
		String archivePath = log.getArchivePath();
		Boolean completed = log.getCompleted();
		Integer rejected = log.getRejected();
		String rejectedArchivePath = log.getRejectedArchivePath();

		s4qlog.setProcessId(processId);
		s4qlog.setOperationId(operationId);
		s4qlog.setExtracted(extracted);
		s4qlog.setInserted(inserted);
		s4qlog.setOperationDate(operationTime);
		s4qlog.setExecutionDate(extractionTime);
		s4qlog.setArchivePath(archivePath);
		s4qlog.setCompleted(completed);
		s4qlog.setRejected(rejected);
		s4qlog.setRejectedArchivePath(rejectedArchivePath);
	}

	public Object loadObjectById(final Integer id) throws EMFUserError {
		logger.debug("IN");
		ELog toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qLog hibLog = (S4qLog) aSession.load(S4qLog.class, id);
			toReturn = toELog(hibLog);
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qLog with id "
					+ ((id == null) ? "" : id.toString()), he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	private ELog toELog(final S4qLog hibLog) {
		ELog toReturn = null;
		if (hibLog != null) {
			toReturn = new ELog();
			toReturn.setIdLog(hibLog.getIdLog());
			toReturn.setProcessId(hibLog.getProcessId());
			toReturn.setOperationId(hibLog.getOperationId());
			toReturn.setExtracted(hibLog.getExtracted());
			toReturn.setInserted(hibLog.getInserted());
			toReturn.setOperationDate(hibLog.getOperationDate());
			toReturn.setExecutionDate(hibLog.getExecutionDate());
			toReturn.setArchivePath(hibLog.getArchivePath());
			toReturn.setCompleted(hibLog.getCompleted());
			toReturn.setRejected(hibLog.getRejected());
			toReturn.setRejectedArchivePath(hibLog.getRejectedArchivePath());
		}
		return toReturn;
	}

	public List loadObjectList(final Integer FKId, final String fieldOrder,
			final String typeOrder) throws EMFUserError {
		List<ELog> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<ELog>();
			List<S4qLog> toTransform = null;

			Criteria crit = aSession.createCriteria(S4qLog.class);

			crit.add(Expression.eq("operationId", FKId));

			if (fieldOrder != null && typeOrder != null) {
				if (typeOrder.toUpperCase().trim().equals("ASC")) {
					crit.addOrder(Order.asc(getELogProperty(fieldOrder)));
				}
				if (typeOrder.toUpperCase().trim().equals("DESC")) {
					crit.addOrder(Order.desc(getELogProperty(fieldOrder)));
				}
			} else {
				crit.addOrder(Order.desc(getELogProperty("OPERATION_DATE")));
			}

			toTransform = crit.list();

			for (Iterator<S4qLog> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qLog s4qLog = iterator.next();

				ELog eLog = toELog(s4qLog);

				toReturn.add(eLog);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of Extraction Log", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
				logger.debug("OUT");
			}
		}
		return toReturn;
	}

	public List loadObjectList(final String fieldOrder, final String typeOrder)
			throws EMFUserError {
		logger.debug("IN");
		List<ELog> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<ELog>();
			List<S4qLog> toTransform = null;

			Criteria crit = aSession.createCriteria(S4qLog.class);

			toTransform = crit.list();

			for (Iterator<S4qLog> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qLog s4qLog = iterator.next();

				ELog aELog = toELog(s4qLog);

				toReturn.add(aELog);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of S4Q ELog", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 9104);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
				logger.debug("OUT");
			}
		}
		return toReturn;
	}

	public void modifyObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			ELog eLog = (ELog) object;

			S4qLog s4qLog = (S4qLog) aSession.load(S4qLog.class, eLog
					.getIdLog());

			saveELogValueToS4qLog(eLog, s4qLog);

			aSession.saveOrUpdate(s4qLog);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
	}

	public Date getLastOperationExecutionDate(final Integer operationId) {
		logger.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		Date toReturn = null;
		if (operationId != null) {
			try {
				aSession = getCurrentSession();
				tx = aSession.beginTransaction();

				Criteria crit = aSession.createCriteria(S4qLog.class);

				crit.add(Expression.eq("operationId", operationId));
				crit.add(Expression.eq("completed",true));
				crit.addOrder(Order.desc(getELogProperty("EXECUTION_DATE")));
				crit.addOrder(Order.desc(getELogProperty("OPERATION_DATE")));

				List queryResult = crit.list();
				if (queryResult != null && queryResult.size() > 0) {
					toReturn = ((S4qLog) queryResult.get(0)).getOperationDate();
				}
			} catch (HibernateException he) {
				logException(he);
				if (tx != null) {
					tx.rollback();
				}
			} finally {
				if (aSession != null && aSession.isOpen()) {
					aSession.close();
				}
			}
		}
		logger.debug("OUT");
		return toReturn;
	}

}
