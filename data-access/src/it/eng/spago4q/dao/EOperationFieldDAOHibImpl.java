/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EOperationField;
import it.eng.spago4q.bo.InterfaceField;
import it.eng.spago4q.metadata.S4qEInterfaceField;
import it.eng.spago4q.metadata.S4qEOperation;
import it.eng.spago4q.metadata.S4qEOperationField;
import it.eng.spago4q.metadata.S4qEScript;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;

public class EOperationFieldDAOHibImpl extends AbstractHibernateDAO implements
		IES4QFKDAO, IEOperationFieldDAO {

	private static Logger logger = Logger
			.getLogger(EOperationFieldDAOHibImpl.class);

	private static String OPERATIONFIELD_NAME = "name";

	public boolean deleteObject(final Integer id) throws EMFUserError {
		Session aSession = getCurrentSession();
		Transaction tx = null;
		try {
			tx = aSession.beginTransaction();
			S4qEOperationField s4qEOperationField = (S4qEOperationField) aSession
					.load(S4qEOperationField.class, id);
			aSession.delete(s4qEOperationField);
			tx.commit();

		} catch (ConstraintViolationException cve) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Impossible to delete a S4qEOperationField ", cve);
			throw new EMFUserError(EMFErrorSeverity.WARNING, 10029);
		} catch (HibernateException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Error while delete a S4qEOperationField ", e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10012);
		} finally {
			aSession.close();
		}
		return true;
	}

	public Integer insertObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Integer idToReturn;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			EOperationField eOperationField = (EOperationField) object;

			S4qEOperationField s4qEOperationField = new S4qEOperationField();

			saveOperationFieldValueToS4qEOperationField(aSession,
					eOperationField, s4qEOperationField);

			idToReturn = (Integer) aSession.save(s4qEOperationField);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10030);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
		return idToReturn;
	}

	private void saveOperationFieldValueToS4qEOperationField(
			final Session aSession, final EOperationField operationField,
			final S4qEOperationField s4qEOperationField) {
		String name = operationField.getName();

		Integer operationId = operationField.getEOperation().getId();
		S4qEScript s4qEScript = null;
		if (operationField.getScript() != null) {
			Integer scriptId = operationField.getScript().getId();
			s4qEScript = (S4qEScript) aSession.load(S4qEScript.class, scriptId);

		}
		Integer interfaceFieldId = operationField.getInterfaceField().getId();

		S4qEOperation s4qEOperation = (S4qEOperation) aSession.load(
				S4qEOperation.class, operationId);

		S4qEInterfaceField s4qEInterfaceField = (S4qEInterfaceField) aSession
				.load(S4qEInterfaceField.class, interfaceFieldId);

		s4qEOperationField.setName(name);
		s4qEOperationField.setS4qEInterfaceField(s4qEInterfaceField);
		s4qEOperationField.setS4qEOperation(s4qEOperation);
		s4qEOperationField.setS4qEScript(s4qEScript);

	}

	public Object loadObjectById(final Integer id) throws EMFUserError {
		logger.debug("IN");
		EOperationField toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qEOperationField hibEOperationField = (S4qEOperationField) aSession
					.load(S4qEOperationField.class, id);
			toReturn = toOperationField(hibEOperationField);
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEOperationField with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	public List loadObjectList(final Integer FKId, final String fieldOrder,
			final String typeOrder) throws EMFUserError {
		List<EOperationField> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<EOperationField>();
			List<S4qEOperationField> toTransform = null;

			Criteria crit = aSession.createCriteria(S4qEOperationField.class);

			S4qEOperation s4qEOperation = (S4qEOperation) aSession.load(
					S4qEOperation.class, FKId);

			crit.add(Expression.eq("s4qEOperation", s4qEOperation));

			if (fieldOrder != null && typeOrder != null) {
				if (typeOrder.toUpperCase().trim().equals("ASC")) {
					crit.addOrder(Order
							.asc(getOperationFieldProperty(fieldOrder)));
				}
				if (typeOrder.toUpperCase().trim().equals("DESC")) {
					crit.addOrder(Order
							.desc(getOperationFieldProperty(fieldOrder)));
				}
			}
			toTransform = crit.list();

			for (Iterator<S4qEOperationField> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qEOperationField s4qEOperationField = iterator.next();

				EOperationField operationField = toOperationField(s4qEOperationField);

				toReturn.add(operationField);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of OperationField", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;

	}

	private EOperationField toOperationField(
			final S4qEOperationField s4qEOperationField) {
		EOperationField toReturn = null;
		if (s4qEOperationField != null) {
			toReturn = new EOperationField();
			toReturn.setId(s4qEOperationField.getIdOperationField());
			toReturn.setName(s4qEOperationField.getName());
			if (s4qEOperationField.getS4qEOperation() != null) {
				toReturn.setEOperation(EOperationDAOHibImpl
						.toOperation(s4qEOperationField.getS4qEOperation()));
			}

			if (s4qEOperationField.getS4qEInterfaceField() != null) {
				toReturn.setInterfaceField(EInterfaceFieldDAOHibImpl
						.toInterfaceField(s4qEOperationField
								.getS4qEInterfaceField()));
			}

			if (s4qEOperationField.getS4qEScript() != null) {
				toReturn.setScript(EScriptDAOHibImpl
						.toScript(s4qEOperationField.getS4qEScript()));
			}

		}
		return toReturn;
	}

	private String getOperationFieldProperty(final String property) {
		String toReturn = null;
		if (property != null && property.toUpperCase().equals("NAME")) {
			toReturn = OPERATIONFIELD_NAME;
		} if (property != null && property.toUpperCase().equals("NAME_INTERFACE_FIELD")) {
			toReturn = "s4qEInterfaceField";
		}
		

		return toReturn;
	}

	public void modifyObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			EOperationField operationField = (EOperationField) object;
			S4qEOperationField s4qEOperationField = (S4qEOperationField) aSession
					.load(S4qEOperationField.class, operationField.getId());

			saveOperationFieldValueToS4qEOperationField(aSession,
					operationField, s4qEOperationField);

			aSession.saveOrUpdate(s4qEOperationField);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 10032);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}

	}

	public List getInterfaceFieldList(final Integer OperationId,
			final Integer OperationFieldId) throws EMFUserError {
		List<InterfaceField> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<InterfaceField>();
			List<S4qEInterfaceField> toTransform = null;
			List<S4qEOperationField> toExclude = null;

			S4qEOperation s4qEOperation = (S4qEOperation) aSession.load(
					S4qEOperation.class, OperationId);

			S4qEOperationField s4qEOperationField = null;

			if (OperationFieldId != null) {
				s4qEOperationField = (S4qEOperationField) aSession.load(
						S4qEOperationField.class, OperationFieldId);
			}

			// to exclude
			Criteria critToExclude = aSession
					.createCriteria(S4qEOperationField.class);
			critToExclude.add(Expression.eq("s4qEOperation", s4qEOperation));

			toExclude = critToExclude.list();

			Criteria crit = aSession.createCriteria(S4qEInterfaceField.class);

			crit.add(Expression.eq("s4qEInterfaceType", s4qEOperation
					.getS4qEInterfaceType()));

			toTransform = crit.list();

			for (Iterator<S4qEInterfaceField> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qEInterfaceField s4qEInterfaceField = iterator.next();

				if (((s4qEOperationField != null) && s4qEInterfaceField
						.getIdInterfaceField().equals(
								s4qEOperationField.getS4qEInterfaceField()
										.getIdInterfaceField()))
						|| interfaceFieldIsNotUsed(s4qEInterfaceField,
								toExclude)) {

					InterfaceField interfaceField = EInterfaceFieldDAOHibImpl
							.toInterfaceField(s4qEInterfaceField);

					toReturn.add(interfaceField);
				}
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of InterfaceField", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;

	}

	private boolean interfaceFieldIsNotUsed(final S4qEInterfaceField interfaceField,
			final List<S4qEOperationField> toExclude) {
		for (Iterator iterator = toExclude.iterator(); iterator.hasNext();) {
			S4qEOperationField operationField = (S4qEOperationField) iterator
					.next();
			if (operationField.getS4qEInterfaceField().equals(interfaceField)){
				return false;
			}
		}
		return true;
	}
}
