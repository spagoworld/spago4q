/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.InterfaceType;
import it.eng.spago4q.metadata.S4qEDomainValue;
import it.eng.spago4q.metadata.S4qEInterfaceField;
import it.eng.spago4q.metadata.S4qEInterfaceType;
import it.eng.spagobi.tools.datasource.metadata.SbiDataSource;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;

public class EInterfaceTypeDAOHibImpl extends AbstractHibernateDAO implements
		IES4QDAO {

	private static Logger logger = Logger
			.getLogger(EInterfaceTypeDAOHibImpl.class);

	private static String EINTERFACETYPE_NAME = "name";

	private String getInterfaceTypeProperty(final String property) {
		String toReturn = null;
		if (property != null && property.toUpperCase().equals("NAME")) {
			toReturn = EINTERFACETYPE_NAME;
		}
		return toReturn;
	}

	public boolean deleteObject(final Integer id) throws EMFUserError {
		Session aSession = getCurrentSession();
		Transaction tx = null;
		try {
			tx = aSession.beginTransaction();
			S4qEInterfaceType s4qEInterfaceType = (S4qEInterfaceType) aSession
					.load(S4qEInterfaceType.class, id);
			aSession.delete(s4qEInterfaceType);
			tx.commit();

		} catch (ConstraintViolationException cve) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Impossible to delete a Interface Type ", cve);
			throw new EMFUserError(EMFErrorSeverity.WARNING, 10023);
		} catch (HibernateException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Error while delete a Interface Type ", e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10012);
		} finally {
			aSession.close();
		}
		return true;

	}

	public Integer insertObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Integer idToReturn;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			InterfaceType interfaceType = (InterfaceType) object;

			S4qEInterfaceType s4qEInterfaceType = new S4qEInterfaceType();

			saveEInterfaceTypeValueToS4qEInterfaceType(aSession, interfaceType,
					s4qEInterfaceType);

			idToReturn = (Integer) aSession.save(s4qEInterfaceType);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10030);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}

		return idToReturn;
	}

	private void saveEInterfaceTypeValueToS4qEInterfaceType(
			final Session aSession, final InterfaceType interfaceType,
			final S4qEInterfaceType s4qEInterfaceType) {

		String name = interfaceType.getName();
		String description = interfaceType.getDescription();
		String tableName = interfaceType.getTableName();
		Integer dataSourceId = interfaceType.getDataSourceId();
		String filterClass = interfaceType.getFilterClass();

		s4qEInterfaceType.setName(name);
		s4qEInterfaceType.setDescription(description);
		s4qEInterfaceType.setTablename(tableName);
		s4qEInterfaceType.setFilterClass(filterClass);
		if (dataSourceId != null) {
			SbiDataSource sbiDataSource = (SbiDataSource) aSession.load(
					SbiDataSource.class, dataSourceId);
			s4qEInterfaceType.setSbiDataSource(sbiDataSource);
		} else {
			s4qEInterfaceType.setSbiDataSource(null);
		}

	}

	public InterfaceType loadObjectById(final Integer id) throws EMFUserError {
		logger.debug("IN");
		InterfaceType toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qEInterfaceType hibInterfaceType = (S4qEInterfaceType) aSession
					.load(S4qEInterfaceType.class, id);
			toReturn = toInterfaceType(hibInterfaceType);
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEInterfaceType with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	public List loadObjectList(final String fieldOrder, final String typeOrder)
			throws EMFUserError {
		logger.debug("IN");
		List<InterfaceType> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<InterfaceType>();
			List<S4qEInterfaceType> toTransform = null;

			Criteria crit = aSession.createCriteria(S4qEInterfaceType.class);

			if (fieldOrder != null && typeOrder != null) {
				if (typeOrder.toUpperCase().trim().equals("ASC")) {
					crit.addOrder(Order
							.asc(getInterfaceTypeProperty(fieldOrder)));
				}
				if (typeOrder.toUpperCase().trim().equals("DESC")) {
					crit.addOrder(Order
							.desc(getInterfaceTypeProperty(fieldOrder)));
				}
			}
			toTransform = crit.list();

			for (Iterator<S4qEInterfaceType> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qEInterfaceType s4qEInterfaceType = iterator.next();

				InterfaceType aInterfaceType = toInterfaceType(s4qEInterfaceType);

				toReturn.add(aInterfaceType);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of EInterfaceType", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	protected static InterfaceType toInterfaceType(
			final S4qEInterfaceType interfaceType) {
		InterfaceType toReturn = null;
		if (interfaceType != null) {
			toReturn = new InterfaceType();
			toReturn.setId(interfaceType.getIdInterfaceType());
			toReturn.setName(interfaceType.getName());
			toReturn.setDescription(interfaceType.getDescription());
			toReturn.setTableName(interfaceType.getTablename());
			toReturn.setFilterClass(interfaceType.getFilterClass());
			if (interfaceType.getSbiDataSource() != null) {
				toReturn.setDataSourceId(interfaceType.getSbiDataSource()
						.getDsId());
			} else {
				toReturn.setDataSourceId(null);
			}
		}
		return toReturn;
	}

	public void modifyObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();

			InterfaceType interfaceType = (InterfaceType) object;
			S4qEInterfaceType s4qEInterfaceType = (S4qEInterfaceType) aSession
					.load(S4qEInterfaceType.class, interfaceType.getId());

			saveEInterfaceTypeValueToS4qEInterfaceType(aSession, interfaceType,
					s4qEInterfaceType);

			aSession.saveOrUpdate(s4qEInterfaceType);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 10032);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
	}

	
	protected static S4qEInterfaceType initializeInterfaceType (final S4qEInterfaceType interfaceType){
		S4qEInterfaceType toReturn = new S4qEInterfaceType();
		toReturn.setIdInterfaceType(interfaceType.getIdInterfaceType());
		toReturn.setName(interfaceType.getName());
		toReturn.setDescription(interfaceType.getDescription());
		toReturn.setTablename(interfaceType.getTablename());
		
		Set interfaceFields = new HashSet();
		
		for (Object par : interfaceType.getS4qEInterfaceFields()){
			S4qEInterfaceField curPar = (S4qEInterfaceField) par;
			S4qEInterfaceField interfaceField = new S4qEInterfaceField();
			interfaceField.setIdInterfaceField(curPar.getIdInterfaceField());
			interfaceField.setName(curPar.getName());
			interfaceField.setKeyField(curPar.getKeyField());
			interfaceField.setSensible(curPar.getSensible());
			interfaceField.setFieldType(curPar.getFieldType());
			Set domainValues = new HashSet();
			
			for (Object domainValueObj : curPar.getS4qEDomainValues()){
				S4qEDomainValue currDomainValue = (S4qEDomainValue) domainValueObj;
				S4qEDomainValue domainValue = new S4qEDomainValue();
				domainValue.setValue(currDomainValue.getValue());
				domainValues.add(domainValue);
			}
			interfaceField.setS4qEDomainValues(domainValues);
			interfaceFields.add(interfaceField);
		}
		toReturn.setS4qEInterfaceFields(interfaceFields);
		return toReturn;
	}
	
	public S4qEInterfaceType getObjectById(final Integer id) {
		Session aSession = null;
		Transaction tx = null;
		S4qEInterfaceType toReturn = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			
			S4qEInterfaceType toInitialize = (S4qEInterfaceType)aSession.load(S4qEInterfaceType.class, id);
			toReturn = initializeInterfaceType(toInitialize);
			
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEInterfaceType with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

		//	throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
		return toReturn;
	}
}
