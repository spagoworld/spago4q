/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.SourceType;
import it.eng.spago4q.metadata.S4qESourceType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;

public class ESourceTypeDAOHibImpl extends AbstractHibernateDAO implements
		IES4QDAO {

	private static final Logger LOGGER = Logger
			.getLogger(ESourceTypeDAOHibImpl.class);

	private static String SOURCETYPE_NAME = "name";
	private static String SOURCETYPE_DESCRIPTION = "description";

	private String getSourceTypeProperty(final String property) {
		String toReturn = null;
		if (property != null && property.toUpperCase().equals("NAME")) {
			toReturn = SOURCETYPE_NAME;
		}
		if (property != null && property.toUpperCase().equals("DESCRIPTION")) {
			toReturn = SOURCETYPE_DESCRIPTION;
		}
		return toReturn;
	}

	public boolean deleteObject(final Integer id) throws EMFUserError {
		Session aSession = getCurrentSession();
		Transaction tx = null;
		try {
			tx = aSession.beginTransaction();
			S4qESourceType s4qESourceType = (S4qESourceType) aSession.load(
					S4qESourceType.class, id);
			aSession.delete(s4qESourceType);
			tx.commit();

		} catch (ConstraintViolationException cve) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			LOGGER.error("Impossible to delete a S4qESourceType ", cve);
			throw new EMFUserError(EMFErrorSeverity.WARNING, 10021);
		} catch (HibernateException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			LOGGER.error("Error while delete a S4qESourceType ", e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10012);
		} finally {
			aSession.close();
		}
		return true;
	}

	public Integer insertObject(final Object sourceType) throws EMFUserError {
		LOGGER.debug("IN");
		Integer idToReturn;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();

			S4qESourceType s4qESourceType = new S4qESourceType();

			saveSourceTypeValueToS4qESourceType((SourceType) sourceType,
					s4qESourceType);

			idToReturn = (Integer) aSession.save(s4qESourceType);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10030);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		LOGGER.debug("OUT");
		return idToReturn;
	}

	public Object loadObjectById(final Integer id) throws EMFUserError {
		LOGGER.debug("IN");
		SourceType toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qESourceType hibSourceType = (S4qESourceType) aSession.load(
					S4qESourceType.class, id);
			toReturn = toSourceType(hibSourceType);
		} catch (HibernateException he) {
			LOGGER.error("Error while loading the S4qESourceType with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			LOGGER.debug("OUT");
		}
		return toReturn;
	}

	public List loadObjectList(final String fieldOrder, final String typeOrder)
			throws EMFUserError {
		LOGGER.debug("IN");
		List<SourceType> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<SourceType>();
			List<S4qESourceType> toTransform = null;

			Criteria crit = aSession.createCriteria(S4qESourceType.class);

			if (fieldOrder != null && typeOrder != null) {
				if (typeOrder.toUpperCase().trim().equals("ASC")) {
					crit.addOrder(Order.asc(getSourceTypeProperty(fieldOrder)));
				}
				if (typeOrder.toUpperCase().trim().equals("DESC")) {
					crit
							.addOrder(Order
									.desc(getSourceTypeProperty(fieldOrder)));
				}
			}
			toTransform = crit.list();

			for (Iterator<S4qESourceType> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qESourceType s4qESourceType = iterator.next();

				SourceType sourceType = toSourceType(s4qESourceType);

				toReturn.add(sourceType);
			}

		} catch (HibernateException he) {
			LOGGER.error("Error while loading the list of SourceType", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			LOGGER.debug("OUT");
		}
		return toReturn;
	}

	protected static SourceType toSourceType(final S4qESourceType s4qESourceType) {
		SourceType toReturn = null;
		if (s4qESourceType != null) {
			toReturn = new SourceType();
			toReturn.setId(s4qESourceType.getIdSourceType());
			toReturn.setName(s4qESourceType.getName());
			toReturn.setDescription(s4qESourceType.getDescription());
			toReturn.setExtractorClass(s4qESourceType.getExtractorClass());
		}
		return toReturn;
	}

	private void saveSourceTypeValueToS4qESourceType(
			final SourceType sourceType, final S4qESourceType s4qESourceType) {
		String name = sourceType.getName();
		String description = sourceType.getDescription();
		String extractorClass = sourceType.getExtractorClass();

		s4qESourceType.setName(name);
		s4qESourceType.setDescription(description);
		s4qESourceType.setExtractorClass(extractorClass);
	}

	public void modifyObject(final Object object) throws EMFUserError {
		LOGGER.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			SourceType sourceType = (SourceType) object;

			S4qESourceType s4qESourceType = (S4qESourceType) aSession.load(
					S4qESourceType.class, sourceType.getId());

			saveSourceTypeValueToS4qESourceType(sourceType, s4qESourceType);

			aSession.saveOrUpdate(s4qESourceType);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10032);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		LOGGER.debug("OUT");
	}

}
