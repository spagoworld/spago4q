/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EPeriodicity;
import it.eng.spago4q.bo.EProcess;
import it.eng.spago4q.metadata.S4qEPeriodicity;
import it.eng.spago4q.metadata.S4qEProcess;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;

public class EProcessDAOHibImpl extends AbstractHibernateDAO implements
		IES4QDAO {

	private static Logger logger = Logger.getLogger(EProcessDAOHibImpl.class);

	private static String PROCESS_NAME = "name";
	private static String PROCESS_DESCRIPTION = "description";

	private String getProcessProperty(final String property) {
		String toReturn = null;
		if (property != null && property.toUpperCase().equals("NAME")) {
			toReturn = PROCESS_NAME;
		}
		if (property != null && property.toUpperCase().equals("DESCRIPTION")) {
			toReturn = PROCESS_DESCRIPTION;
		}

		return toReturn;
	}

	public boolean deleteObject(final Integer id) throws EMFUserError {
		Session aSession = getCurrentSession();
		Transaction tx = null;
		try {
			tx = aSession.beginTransaction();
			S4qEProcess s4qEProcess = (S4qEProcess) aSession.load(
					S4qEProcess.class, id);
			aSession.delete(s4qEProcess);
			tx.commit();

		} catch (ConstraintViolationException cve) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Impossible to delete a Process ", cve);
			throw new EMFUserError(EMFErrorSeverity.WARNING, 10021);
		} catch (HibernateException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Error while delete a Process ", e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10012);
		} finally {
			aSession.close();
		}
		return true;
	}

	public Integer insertObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Integer idToReturn;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();

			EProcess process = (EProcess) object;

			S4qEProcess s4qEProcess = new S4qEProcess();

			saveEProcessValueToS4qEProcess(aSession, process, s4qEProcess);

			idToReturn = (Integer) aSession.save(s4qEProcess);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10030);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
		return idToReturn;
	}

	private void saveEProcessValueToS4qEProcess(final Session aSession,
			final EProcess eProcess, final S4qEProcess s4qEProcess) {

		String name = eProcess.getName();
		String description = eProcess.getDescription();
		String coordinatorClass = eProcess.getCoordinatorClass();

		s4qEProcess.setName(name);
		s4qEProcess.setDescription(description);
		s4qEProcess.setCoordinatorClass(coordinatorClass);

		S4qEPeriodicity s4qEPeriodicity = null;

		if (eProcess.getEPeriodicity() != null) {
			s4qEPeriodicity = (S4qEPeriodicity) aSession.load(
					S4qEPeriodicity.class, eProcess.getEPeriodicity()
							.getIdPeriodicity());
		}
		s4qEProcess.setS4qEPeriodicity(s4qEPeriodicity);
	}

	public EProcess loadObjectById(final Integer id) throws EMFUserError {
		logger.debug("IN");
		EProcess toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qEProcess hibProcess = (S4qEProcess) aSession.load(
					S4qEProcess.class, id);
			toReturn = toEProcess(hibProcess);
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEProcess with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;

	}

	public List loadObjectList(final String fieldOrder, final String typeOrder)
			throws EMFUserError {
		logger.debug("IN");
		List<EProcess> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<EProcess>();
			List<S4qEProcess> toTransform = null;

			Criteria crit = aSession.createCriteria(S4qEProcess.class);

			if (fieldOrder != null && typeOrder != null) {
				if (typeOrder.toUpperCase().trim().equals("ASC")) {
					crit.addOrder(Order.asc(getProcessProperty(fieldOrder)));
				}
				if (typeOrder.toUpperCase().trim().equals("DESC")) {
					crit.addOrder(Order.desc(getProcessProperty(fieldOrder)));
				}
			}
			toTransform = crit.list();

			for (Iterator<S4qEProcess> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qEProcess s4qEProcess = iterator.next();

				EProcess aprocess = toEProcess(s4qEProcess);

				toReturn.add(aprocess);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of EProcess", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	protected static EProcess toEProcess(final S4qEProcess s4qEProcess) {
		EProcess toReturn = null;
		if (s4qEProcess != null) {
			toReturn = new EProcess();
			toReturn.setId(s4qEProcess.getIdProcess());
			toReturn.setName(s4qEProcess.getName());
			toReturn.setDescription(s4qEProcess.getDescription());
			toReturn.setCoordinatorClass(s4qEProcess.getCoordinatorClass());
			if (s4qEProcess.getS4qEPeriodicity() != null) {
				EPeriodicity ePeriodicity = EPeriodicityDAOHibImpl
						.toEPeriodicity(s4qEProcess.getS4qEPeriodicity());
				toReturn.setEPeriodicity(ePeriodicity);
			}
		}
		return toReturn;
	}

	public void modifyObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			EProcess process = (EProcess) object;

			S4qEProcess s4qEProcess = (S4qEProcess) aSession.load(
					S4qEProcess.class, process.getId());

			saveEProcessValueToS4qEProcess(aSession, process, s4qEProcess);

			aSession.saveOrUpdate(s4qEProcess);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 10032);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
	}

}
