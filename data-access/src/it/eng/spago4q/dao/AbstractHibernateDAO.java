/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.base.SourceBean;
import it.eng.spago.configuration.ConfigSingleton;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.HibernateUtilsFactory;
import it.eng.spago4q.HibernateUtilsInterface;

import org.apache.log4j.Logger;
import org.hibernate.Session;

public class AbstractHibernateDAO {

	private static transient Logger logger = Logger
			.getLogger(AbstractHibernateDAO.class);

	public Session getCurrentSession() {
		Session mySession = null;
		ConfigSingleton configSingleton = ConfigSingleton.getInstance();
		SourceBean hibernateUtilsClassSB = (SourceBean) configSingleton
				.getAttribute("SPAGO4Q.HIBERNATE-UTILS-CONF");
		if (hibernateUtilsClassSB != null) {
			String hibernateUtilsClass = hibernateUtilsClassSB.getCharacters();
			hibernateUtilsClass = hibernateUtilsClass.trim();
			try {
				HibernateUtilsInterface hibernateUtils = null;
				hibernateUtils = HibernateUtilsFactory
						.getHibernateUtils(hibernateUtilsClass);
				mySession = hibernateUtils.getCurrentSession();
			} catch (EMFUserError e) {
				logException(e);
			}
		}
		return mySession;
	}

	/**
	 * Traces the exception information of a throwable input object.
	 * 
	 * @param t
	 *            The input throwable object
	 */
	public void logException(final Throwable t) {
		logger.error(t.getClass().getName() + " " + t.getMessage(), t);
	}
}
