/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.DataSource;
import it.eng.spago4q.metadata.S4qEDataSource;
import it.eng.spago4q.metadata.S4qEDataSourceParameter;
import it.eng.spago4q.metadata.S4qESourceType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;

public class EDataSourceDAOHibImpl extends AbstractHibernateDAO implements
		IES4QFKDAO {

	private static Logger logger = Logger
			.getLogger(EDataSourceDAOHibImpl.class);

	private static String DATASOURCE_NAME = "name";
	private static String DATASOURCE_SOURCETYPE = "name";

	public boolean deleteObject(final Integer id) throws EMFUserError {
		Session aSession = getCurrentSession();
		Transaction tx = null;
		try {
			tx = aSession.beginTransaction();
			S4qEDataSource s4qEDataSource = (S4qEDataSource) aSession.load(
					S4qEDataSource.class, id);
			aSession.delete(s4qEDataSource);
			tx.commit();

		} catch (ConstraintViolationException cve) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Impossible to delete a S4qEDataSource ", cve);
			throw new EMFUserError(EMFErrorSeverity.WARNING, 10022);
		} catch (HibernateException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Error while delete a S4qEDataSource ", e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10012);
		} finally {
			aSession.close();
		}
		return true;

	}

	public Integer insertObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Integer idToReturn;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			DataSource dataSource = (DataSource) object;

			S4qEDataSource s4qEDataSource = new S4qEDataSource();

			saveDataSourceValueToS4qEDataSource(aSession, dataSource,
					s4qEDataSource);

			idToReturn = (Integer) aSession.save(s4qEDataSource);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10030);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
		return idToReturn;
	}

	public DataSource loadObjectById(final Integer id) throws EMFUserError {
		logger.debug("IN");
		DataSource toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qEDataSource hibDataSource = (S4qEDataSource) aSession.load(
					S4qEDataSource.class, id);
			toReturn = toDataSource(hibDataSource);
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEDataSource with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
		return toReturn;

	}

	public List loadObjectList(final Integer sourceTypeId,
			final String fieldOrder, final String typeOrder)
			throws EMFUserError {
		List<DataSource> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<DataSource>();
			List<S4qEDataSource> toTransform = null;

			Criteria crit = aSession.createCriteria(S4qEDataSource.class);

			if (sourceTypeId != null) {
				S4qESourceType s4qESourceType = (S4qESourceType) aSession.load(
						S4qESourceType.class, sourceTypeId);

				crit.add(Expression.eq("s4qESourceType", s4qESourceType));
			}

			if (fieldOrder != null && typeOrder != null) {
				if (typeOrder.toUpperCase().trim().equals("ASC")) {
					crit.addOrder(Order.asc(getDataSourceProperty(fieldOrder)));
				}
				if (typeOrder.toUpperCase().trim().equals("DESC")) {
					crit
							.addOrder(Order
									.desc(getDataSourceProperty(fieldOrder)));
				}
			}
			toTransform = crit.list();

			for (Iterator<S4qEDataSource> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qEDataSource s4qEDataSource = iterator.next();

				DataSource dataSource = toDataSource(s4qEDataSource);

				toReturn.add(dataSource);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of DataSource", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	protected static DataSource toDataSource(final S4qEDataSource s4qEDataSource) {
		DataSource toReturn = null;
		if (s4qEDataSource != null) {
			toReturn = new DataSource();
			toReturn.setId(s4qEDataSource.getIdDataSource());
			toReturn.setName(s4qEDataSource.getName());
			toReturn.setDescription(s4qEDataSource.getDescription());
			if (s4qEDataSource.getS4qESourceType() != null) {
				toReturn.setSourceType(ESourceTypeDAOHibImpl
						.toSourceType(s4qEDataSource.getS4qESourceType()));
			}
		}
		return toReturn;

	}

	private String getDataSourceProperty(final String property) {
		String toReturn = null;
		if (property != null && property.toUpperCase().equals("NAME")) {
			toReturn = DATASOURCE_NAME;
		}
		if (property != null && property.toUpperCase().equals("NAME_TYPE")) {
			toReturn = DATASOURCE_SOURCETYPE;
		}

		return toReturn;
	}

	public void modifyObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			DataSource dataSource = (DataSource) object;
			S4qEDataSource s4qEDataSource = (S4qEDataSource) aSession.load(
					S4qEDataSource.class, dataSource.getId());

			saveDataSourceValueToS4qEDataSource(aSession, dataSource,
					s4qEDataSource);

			aSession.saveOrUpdate(s4qEDataSource);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 10032);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
	}

	private void saveDataSourceValueToS4qEDataSource(final Session aSession,
			final DataSource dataSource, final S4qEDataSource s4qEDataSource) {
		String name = dataSource.getName();
		String description = dataSource.getDescription();
		Integer sourceTypeId = dataSource.getSourceType().getId();

		S4qESourceType s4qESourceType = (S4qESourceType) aSession.load(
				S4qESourceType.class, sourceTypeId);

		s4qEDataSource.setName(name);
		s4qEDataSource.setDescription(description);
		s4qEDataSource.setS4qESourceType(s4qESourceType);
	}
	
	public S4qEDataSource getObjectById(final Integer id){
		Session aSession = null;
		Transaction tx = null;
		S4qEDataSource toReturn = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			
			S4qEDataSource toInitialize = (S4qEDataSource)aSession.load(S4qEDataSource.class, id);
			toReturn = initializeDataSource(toInitialize);
			
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEDataSource with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

		//	throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
		return toReturn;
	}
	
	protected static S4qEDataSource initializeDataSource (final S4qEDataSource dataSource){
		S4qEDataSource toReturn = new S4qEDataSource();
		toReturn.setIdDataSource(dataSource.getIdDataSource());
		toReturn.setName(dataSource.getName());
		toReturn.setDescription(dataSource.getDescription());
		
		S4qESourceType sourceType = new S4qESourceType();
		if(dataSource.getS4qESourceType()!= null){
			sourceType.setIdSourceType(dataSource.getS4qESourceType().getIdSourceType());
			sourceType.setName(dataSource.getS4qESourceType().getName());
			sourceType.setDescription(dataSource.getS4qESourceType().getDescription());
			sourceType.setExtractorClass(dataSource.getS4qESourceType().getExtractorClass());
		}
		toReturn.setS4qESourceType(sourceType);
		
		Set dataSourceParameters = new HashSet();
		
		for (Object par : dataSource.getS4qEDataSourceParameters()){
			S4qEDataSourceParameter curPar = (S4qEDataSourceParameter) par;
			S4qEDataSourceParameter dataSourceParameter = new S4qEDataSourceParameter();
			dataSourceParameter.setIdDataSourceParameter(curPar.getIdDataSourceParameter());
			dataSourceParameter.setName(curPar.getName());
			dataSourceParameter.setValue(curPar.getValue());
			dataSourceParameter.setCript(curPar.getCript());
			dataSourceParameters.add(dataSourceParameter);
		}
		toReturn.setS4qEDataSourceParameters(dataSourceParameters);
		return toReturn;
	}

}
