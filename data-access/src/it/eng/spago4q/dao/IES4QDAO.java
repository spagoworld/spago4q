/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFUserError;

import java.util.List;

public interface IES4QDAO {
	
	List loadObjectList(String fieldOrder, String typeOrder) throws EMFUserError;

	Object loadObjectById(Integer id) throws EMFUserError;

	void modifyObject(Object bo) throws EMFUserError;

	Integer insertObject(Object bo) throws EMFUserError;

	boolean deleteObject(Integer id) throws EMFUserError;

}