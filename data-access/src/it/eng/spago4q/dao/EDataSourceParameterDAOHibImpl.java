/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.DataSourceParameter;
import it.eng.spago4q.metadata.S4qEDataSource;
import it.eng.spago4q.metadata.S4qEDataSourceParameter;
import it.eng.spago.security.DefaultCipher;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;

public class EDataSourceParameterDAOHibImpl extends AbstractHibernateDAO
		implements IES4QFKDAO {

	private static Logger logger = Logger
			.getLogger(EDataSourceParameterDAOHibImpl.class);

	private static String DATASOURCEPARAMETER_NAME = "name";
	private static String DATASOURCEPARAMETER_VALUE = "value";

	public boolean deleteObject(final Integer id) throws EMFUserError {
		Session aSession = getCurrentSession();
		Transaction tx = null;
		try {
			tx = aSession.beginTransaction();
			S4qEDataSourceParameter s4qEDataSourceParameter = (S4qEDataSourceParameter) aSession
					.load(S4qEDataSourceParameter.class, id);
			aSession.delete(s4qEDataSourceParameter);
			tx.commit();

		} catch (ConstraintViolationException cve) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger
					.error("Impossible to delete a S4qEDataSourceParameter ",
							cve);
			throw new EMFUserError(EMFErrorSeverity.WARNING, 10026);
		} catch (HibernateException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Error while delete a S4qEDataSourceParameter ", e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10012);
		} finally {
			aSession.close();
		}
		return true;
	}

	public Integer insertObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Integer idToReturn;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			DataSourceParameter dataSourceParameter = (DataSourceParameter) object;

			S4qEDataSourceParameter s4qEDataSourceParameter = new S4qEDataSourceParameter();

			saveDataSourceParameterValueToS4qEDataSourceParameter(aSession,
					dataSourceParameter, s4qEDataSourceParameter);

			idToReturn = (Integer) aSession.save(s4qEDataSourceParameter);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10030);

		} finally {
			if (aSession != null) {
				if (aSession.isOpen()) {
					aSession.close();
				}
			}
		}
		logger.debug("OUT");
		return idToReturn;

	}

	private void saveDataSourceParameterValueToS4qEDataSourceParameter(
			final Session aSession,
			final DataSourceParameter dataSourceParameter,
			final S4qEDataSourceParameter s4qEDataSourceParameter) {
		String name = dataSourceParameter.getName();
		String value = dataSourceParameter.getValue();
		Boolean cript = dataSourceParameter.getCript();

		Integer dataSourceId = dataSourceParameter.getDataSource().getId();

		S4qEDataSource s4qEDataSource = (S4qEDataSource) aSession.load(
				S4qEDataSource.class, dataSourceId);

		if (cript) {
			DefaultCipher dc = new DefaultCipher();
			value = dc.encrypt(value);
		}

		s4qEDataSourceParameter.setName(name);
		s4qEDataSourceParameter.setValue(value);
		s4qEDataSourceParameter.setCript(cript);
		s4qEDataSourceParameter.setS4qEDataSource(s4qEDataSource);

	}

	public Object loadObjectById(final Integer id) throws EMFUserError {
		logger.debug("IN");
		DataSourceParameter toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qEDataSourceParameter hibDataSourceParameter = (S4qEDataSourceParameter) aSession
					.load(S4qEDataSourceParameter.class, id);
			toReturn = toDataSourceParameter(hibDataSourceParameter);
		} catch (HibernateException he) {
			logger.error(
					"Error while loading the S4qEDataSourceParameter with id "
							+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	public List loadObjectList(final Integer FKId, final String fieldOrder,
			final String typeOrder) throws EMFUserError {
		List<DataSourceParameter> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<DataSourceParameter>();
			List<S4qEDataSourceParameter> toTransform = null;

			Criteria crit = aSession
					.createCriteria(S4qEDataSourceParameter.class);

			S4qEDataSource s4qEDataSource = (S4qEDataSource) aSession.load(
					S4qEDataSource.class, FKId);

			crit.add(Expression.eq("s4qEDataSource", s4qEDataSource));

			if (fieldOrder != null && typeOrder != null) {
				if (typeOrder.toUpperCase().trim().equals("ASC")) {
					crit.addOrder(Order
							.asc(getDataSourceParameterProperty(fieldOrder)));
				}
				if (typeOrder.toUpperCase().trim().equals("DESC")) {
					crit.addOrder(Order
							.desc(getDataSourceParameterProperty(fieldOrder)));
				}
			}
			toTransform = crit.list();

			for (Iterator<S4qEDataSourceParameter> iterator = toTransform
					.iterator(); iterator.hasNext();) {
				S4qEDataSourceParameter s4qEDataSourceParameter = iterator
						.next();

				DataSourceParameter dataSourceParameter = toDataSourceParameter(s4qEDataSourceParameter);

				toReturn.add(dataSourceParameter);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of DataSourceParameter",
					he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	private DataSourceParameter toDataSourceParameter(
			final S4qEDataSourceParameter s4qEDataSourceParameter) {
		DataSourceParameter toReturn = null;
		if (s4qEDataSourceParameter != null) {
			toReturn = new DataSourceParameter();
			toReturn.setId(s4qEDataSourceParameter.getIdDataSourceParameter());
			toReturn.setName(s4qEDataSourceParameter.getName());
			toReturn.setCript(s4qEDataSourceParameter.getCript());
			String value = s4qEDataSourceParameter.getValue();

			if (s4qEDataSourceParameter.getCript()) {
				DefaultCipher dc = new DefaultCipher();
				value = dc.decrypt(value);
			}

			toReturn.setValue(value);
			if (s4qEDataSourceParameter.getS4qEDataSource() != null) {
				toReturn.setDataSource(EDataSourceDAOHibImpl
						.toDataSource(s4qEDataSourceParameter
								.getS4qEDataSource()));
			}
		}
		return toReturn;
	}

	private String getDataSourceParameterProperty(final String property) {
		String toReturn = null;
		if (property != null && property.toUpperCase().equals("NAME")) {
			toReturn = DATASOURCEPARAMETER_NAME;
		}
		if (property != null && property.toUpperCase().equals("VALUE")) {
			toReturn = DATASOURCEPARAMETER_VALUE;
		}

		return toReturn;
	}

	public void modifyObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			DataSourceParameter dataSourceParameter = (DataSourceParameter) object;
			S4qEDataSourceParameter s4qEDataSourceParameter = (S4qEDataSourceParameter) aSession
					.load(S4qEDataSourceParameter.class, dataSourceParameter
							.getId());

			saveDataSourceParameterValueToS4qEDataSourceParameter(aSession,
					dataSourceParameter, s4qEDataSourceParameter);

			aSession.saveOrUpdate(s4qEDataSourceParameter);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 10032);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
		}
		logger.debug("OUT");
	}

}
