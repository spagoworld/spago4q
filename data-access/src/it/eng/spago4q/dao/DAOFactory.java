/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.base.SourceBean;
import it.eng.spago.configuration.ConfigSingleton;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;

import org.apache.log4j.Logger;

public final class DAOFactory {

	private DAOFactory() {
	}

	private static transient Logger logger = Logger.getLogger(DAOFactory.class);

	public static Object getDAO(final String daoEnvEntry) throws EMFUserError {
		logger.debug("Begin Istantiation of DAO [" + daoEnvEntry + "]");
		Object daoObject = null;
		try {
			ConfigSingleton configSingleton = ConfigSingleton.getInstance();
			SourceBean daoConfigSourceBean = (SourceBean) configSingleton
					.getFilteredSourceBeanAttribute("SPAGO4Q.DAO-CONF.DAO",
							"name", daoEnvEntry);
			String daoClassName = (String) daoConfigSourceBean
					.getAttribute("implementation");
			logger.debug("DAO [" + daoEnvEntry + "] Implementation class ["
					+ daoClassName + "]");

			daoObject = Class.forName(daoClassName).newInstance();
			logger.debug("createDAOInstance DAO [" + daoEnvEntry
					+ "] Instatiate successfully");
		} catch (InstantiationException e) {
			throw new EMFUserError(EMFErrorSeverity.ERROR, 1000);
		} catch (IllegalAccessException e) {
			throw new EMFUserError(EMFErrorSeverity.ERROR, 1000);
		} catch (ClassNotFoundException e) {
			throw new EMFUserError(EMFErrorSeverity.ERROR, 1000);
		}
		return daoObject;
	}

}
