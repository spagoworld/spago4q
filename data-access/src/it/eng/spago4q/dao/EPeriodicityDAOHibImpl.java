/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EPeriodicity;
import it.eng.spago4q.metadata.S4qEPeriodicity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EPeriodicityDAOHibImpl extends AbstractHibernateDAO implements
		IES4QDAO {

	private static Logger logger = Logger
			.getLogger(EPeriodicityDAOHibImpl.class);

	public boolean deleteObject(final Integer id) throws EMFUserError {
		return false;
	}

	public Integer insertObject(final Object bo) throws EMFUserError {
		return null;
	}

	public Object loadObjectById(final Integer id) throws EMFUserError {
		logger.debug("IN");
		EPeriodicity toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qEPeriodicity hibPeriodicity = (S4qEPeriodicity) aSession.load(
					S4qEPeriodicity.class, id);
			toReturn = toEPeriodicity(hibPeriodicity);
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEPeriodicity with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	public List loadObjectList(final String fieldOrder, final String typeOrder)
			throws EMFUserError {
		logger.debug("IN");
		List<EPeriodicity> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<EPeriodicity>();
			List<S4qEPeriodicity> toTransform = null;

			Criteria crit = aSession.createCriteria(S4qEPeriodicity.class);

			toTransform = crit.list();

			for (Iterator<S4qEPeriodicity> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qEPeriodicity s4qEPeriodicity = iterator.next();

				EPeriodicity aPeriodicity = toEPeriodicity(s4qEPeriodicity);

				toReturn.add(aPeriodicity);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of EPeriodicity", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	protected static EPeriodicity toEPeriodicity(final S4qEPeriodicity s4qEPeriodicity) {
		EPeriodicity toReturn = null;
		if (s4qEPeriodicity != null) {
			toReturn = new EPeriodicity();
			toReturn.setIdPeriodicity(s4qEPeriodicity.getIdPeriodicity());
			toReturn.setName(s4qEPeriodicity.getName());
			toReturn.setDescription(s4qEPeriodicity.getDescription());
			toReturn.setCronstring(s4qEPeriodicity.getCronstring());
		}
		return toReturn;
	}

	public void modifyObject(final Object bo) throws EMFUserError {
	}

}
