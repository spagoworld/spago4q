/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.dao;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.DomainValue;
import it.eng.spago4q.metadata.S4qEDomainValue;
import it.eng.spago4q.metadata.S4qEInterfaceField;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;

public class EDomainValueDAOHibImpl extends AbstractHibernateDAO implements
		IES4QFKDAO {

	private static Logger logger = Logger
			.getLogger(EDomainValueDAOHibImpl.class);

	private static String EDOMAINVALUE_VALUE = "value";

	private String getEDomainValueProperty(final String property) {
		String toReturn = null;
		if (property != null && property.toUpperCase().equals("VALUE")) {
			toReturn = EDOMAINVALUE_VALUE;
		}

		return toReturn;
	}

	public boolean deleteObject(final Integer id) throws EMFUserError {
		Session aSession = getCurrentSession();
		Transaction tx = null;
		try {
			tx = aSession.beginTransaction();
			S4qEDomainValue s4qEdomainValue = (S4qEDomainValue) aSession.load(
					S4qEDomainValue.class, id);
			aSession.delete(s4qEdomainValue);
			tx.commit();

		} catch (ConstraintViolationException cve) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Impossible to delete a S4qEdomainValue ", cve);
			throw new EMFUserError(EMFErrorSeverity.WARNING, 10025);
		} catch (HibernateException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			logger.error("Error while delete a S4qEdomainValue ", e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
		} finally {
			aSession.close();
		}
		return true;
	}

	public Integer insertObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Integer idToReturn;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			DomainValue domainValue = (DomainValue) object;

			S4qEDomainValue s4qEDomainValue = new S4qEDomainValue();

			saveDomainValueToS4qEDomainValue(aSession, domainValue,
					s4qEDomainValue);

			idToReturn = (Integer) aSession.save(s4qEDomainValue);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}

		return idToReturn;
	}

	private void saveDomainValueToS4qEDomainValue(final Session aSession,
			final DomainValue domainValue, final S4qEDomainValue s4qEDomainValue) {
		String value = domainValue.getValue();

		Integer interfaceFieldId = domainValue.getInterfaceField().getId();

		S4qEInterfaceField s4qEInterfaceField = (S4qEInterfaceField) aSession
				.load(S4qEInterfaceField.class, interfaceFieldId);

		s4qEDomainValue.setValue(value);

		s4qEDomainValue.setS4qEInterfaceField(s4qEInterfaceField);

	}

	public Object loadObjectById(final Integer id) throws EMFUserError {
		logger.debug("IN");
		DomainValue toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			S4qEDomainValue hibDomainValue = (S4qEDomainValue) aSession.load(
					S4qEDomainValue.class, id);
			toReturn = toDomainValue(hibDomainValue);
		} catch (HibernateException he) {
			logger.error("Error while loading the S4qEDomainValue with id "
					+ ((id == null) ? "" : id.toString()), he);

			if (tx != null) {
				tx.rollback();
			}

			throw new EMFUserError(EMFErrorSeverity.ERROR, 10101);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;
	}

	private DomainValue toDomainValue(final S4qEDomainValue hibDomainValue) {
		DomainValue toReturn = null;
		if (hibDomainValue != null) {
			toReturn = new DomainValue();
			toReturn.setId(hibDomainValue.getIdFieldValue());
			toReturn.setValue(hibDomainValue.getValue());

			if (hibDomainValue.getS4qEInterfaceField() != null) {
				toReturn.setInterfaceField(EInterfaceFieldDAOHibImpl
						.toInterfaceField(hibDomainValue
								.getS4qEInterfaceField()));
			}
		}
		return toReturn;
	}

	public List loadObjectList(final Integer FKId, final String fieldOrder,
			final String typeOrder) throws EMFUserError {
		List<DomainValue> toReturn = null;
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();
			toReturn = new ArrayList<DomainValue>();
			List<S4qEDomainValue> toTransform = null;

			Criteria crit = aSession.createCriteria(S4qEDomainValue.class);

			S4qEInterfaceField s4qEInterfaceField = (S4qEInterfaceField) aSession
					.load(S4qEInterfaceField.class, FKId);

			crit.add(Expression.eq("s4qEInterfaceField", s4qEInterfaceField));

			if (fieldOrder != null && typeOrder != null) {
				if (typeOrder.toUpperCase().trim().equals("ASC")) {
					crit.addOrder(Order
							.asc(getEDomainValueProperty(fieldOrder)));
				}
				if (typeOrder.toUpperCase().trim().equals("DESC")) {
					crit.addOrder(Order
							.desc(getEDomainValueProperty(fieldOrder)));
				}
			}
			toTransform = crit.list();

			for (Iterator<S4qEDomainValue> iterator = toTransform.iterator(); iterator
					.hasNext();) {
				S4qEDomainValue s4qEDomainValue = iterator.next();

				DomainValue domainValue = toDomainValue(s4qEDomainValue);

				toReturn.add(domainValue);
			}

		} catch (HibernateException he) {
			logger.error("Error while loading the list of DomainValue", he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10031);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}
		return toReturn;

	}

	public void modifyObject(final Object object) throws EMFUserError {
		logger.debug("IN");
		Session aSession = null;
		Transaction tx = null;
		try {
			aSession = getCurrentSession();
			tx = aSession.beginTransaction();

			DomainValue domainValue = (DomainValue) object;

			S4qEDomainValue s4qEDomainValue = (S4qEDomainValue) aSession.load(
					S4qEDomainValue.class, domainValue.getId());

			saveDomainValueToS4qEDomainValue(aSession, domainValue,
					s4qEDomainValue);

			aSession.saveOrUpdate(s4qEDomainValue);

			tx.commit();

		} catch (HibernateException he) {
			logException(he);

			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 10032);

		} finally {
			if (aSession != null && aSession.isOpen()) {
				aSession.close();
			}
			logger.debug("OUT");
		}

	}

}
