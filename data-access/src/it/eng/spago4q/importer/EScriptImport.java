/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.importer;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.metadata.S4qEScript;

public interface EScriptImport {
	
	/** 
	 * Import a script in the metadata model.
	 * @param script object to import
	 * @param overwrite if true overwrite existing data. if false create new object.
	 */
	public void importData(S4qEScript script, boolean overwrite) throws EMFUserError;


}
