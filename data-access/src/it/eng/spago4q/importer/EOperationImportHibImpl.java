/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.importer;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.dao.AbstractHibernateDAO;
import it.eng.spago4q.metadata.S4qEDataSource;
import it.eng.spago4q.metadata.S4qEInterfaceField;
import it.eng.spago4q.metadata.S4qEInterfaceType;
import it.eng.spago4q.metadata.S4qEOperation;
import it.eng.spago4q.metadata.S4qEOperationField;
import it.eng.spago4q.metadata.S4qEOperationParameter;
import it.eng.spago4q.metadata.S4qEProcess;
import it.eng.spago4q.metadata.S4qEScript;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class EOperationImportHibImpl extends AbstractHibernateDAO implements
		EOperationImport {

	private static Logger logger = Logger
			.getLogger(EOperationImportHibImpl.class);

	public void importData(final S4qEOperation operation,
			final boolean overwrite) throws EMFUserError {
		Session session = null;
		Transaction tx = null;
		try {
			session = getCurrentSession();
			tx = session.beginTransaction();
			S4qEProcess currProcess = importProcess(session, operation
					.getS4qEProcess(), overwrite);

			operation.setS4qEProcess(currProcess);

			S4qEOperation currOperation = importOperation(session, operation,
					overwrite);

			if (overwrite) {
				// // get all parameters name
				Set<String> keys = new HashSet<String>();
				for (Object iterable_element : operation
						.getS4qEOperationParameters()) {
					((S4qEOperationParameter) iterable_element)
							.setS4qEOperation(currOperation);
					keys.add(((S4qEOperationParameter) iterable_element)
							.getName());
				}
				deleteOperationParameters(session, keys, currOperation);
			}

			for (Object iterable_element : operation
					.getS4qEOperationParameters()) {
				((S4qEOperationParameter) iterable_element)
						.setS4qEOperation(currOperation);
				importOperationParameter(session,
						((S4qEOperationParameter) iterable_element), overwrite);
			}

			if (overwrite) {
				// get all parameters name
				Set<String> keys = new HashSet<String>();
				for (Object iterable_element : operation
						.getS4qEOperationFields()) {
					((S4qEOperationField) iterable_element)
							.setS4qEOperation(currOperation);
					keys.add(((S4qEOperationField) iterable_element).getName());
				}
				deleteOperationFields(session, keys, currOperation);
			}

			for (Object iterable_element : operation.getS4qEOperationFields()) {
				S4qEOperationField operationField = ((S4qEOperationField) iterable_element);
				operationField.setS4qEOperation(currOperation);
				operationField.setS4qEScript(findScript(session, operationField
						.getS4qEScript()));
				operationField.setS4qEInterfaceField(findInterfaceField(
						session, operationField.getS4qEInterfaceField(),
						operation.getS4qEInterfaceType()));
				importOperationField(session, operationField, overwrite);
			}
			session.flush();
			tx.commit();
		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
			logger.debug("OUT");
		}
	}

	private S4qEInterfaceField findInterfaceField(final Session session,
			final S4qEInterfaceField interfaceField,
			final S4qEInterfaceType interfaceType) {
		S4qEInterfaceField toReturn = null;
		Criteria crit = session.createCriteria(S4qEInterfaceField.class);
		crit.add(Restrictions.eq("name", interfaceField.getName()));
		if (interfaceType != null) {
			Criteria dataSourceCrit = crit.createCriteria("s4qEInterfaceType");
			dataSourceCrit
					.add(Restrictions.eq("name", interfaceType.getName()));
		}
		toReturn = (S4qEInterfaceField) crit.uniqueResult();
		return toReturn;

	}

	private S4qEScript findScript(final Session session, final S4qEScript script) {
		S4qEScript toReturn = null;
		if (script != null) {
			Criteria crit = session.createCriteria(S4qEScript.class);
			crit.add(Restrictions.eq("name", script.getName()));

			toReturn = (S4qEScript) crit.uniqueResult();
		}
		return toReturn;
	}

	private void importOperationField(final Session session,
			final S4qEOperationField operationField, final boolean overwrite) {
		if (overwrite) {
			// overite == true..
			// insert object
			session.save(operationField);
		} else {
			// overwrite = false
			List result = findOperationField(session, operationField);
			if (result == null || result.size() == 0) {
				session.save(operationField);
			}
		}
	}

	private void deleteOperationFields(final Session session,
			final Set<String> keys, final S4qEOperation currOperation) {
		for (String key : keys) {
			S4qEOperationField operationField = new S4qEOperationField();
			operationField.setName(key);
			operationField.setS4qEOperation(currOperation);
			List listToDelete = findOperationField(session, operationField);
			for (Object toDelete : listToDelete) {
				session.delete(toDelete);
			}
		}
	}

	private List findOperationField(final Session session,
			final S4qEOperationField operationField) {
		List toReturn = null;
		Criteria crit = session.createCriteria(S4qEOperationField.class);
		crit.add(Restrictions.eq("name", operationField.getName()));

		Criteria dataSourceCrit = crit.createCriteria("s4qEOperation");
		dataSourceCrit.add(Restrictions.eq("name", operationField
				.getS4qEOperation().getName()));

		Criteria sourceTypeCrit = dataSourceCrit.createCriteria("s4qEProcess");
		sourceTypeCrit.add(Restrictions.eq("name", operationField
				.getS4qEOperation().getS4qEProcess().getName()));

		if (operationField.getS4qEInterfaceField() != null) {
			Criteria interfaceFieldCrit = crit
					.createCriteria("s4qEInterfaceField");
			interfaceFieldCrit.add(Restrictions.eq("name", operationField
					.getS4qEInterfaceField().getName()));
		}
		toReturn = crit.list();
		return toReturn;
	}

	private void importOperationParameter(final Session session,
			final S4qEOperationParameter operationParameter,
			final boolean overwrite) {
		if (overwrite) {
			// overite == true..
			// insert object
			session.save(operationParameter);
		} else {
			// overwrite = false
			List result = findOperationParameter(session, operationParameter);
			if (result == null || result.size() == 0) {
				session.save(operationParameter);
			}
		}

	}

	private void deleteOperationParameters(final Session session,
			final Set<String> keys, final S4qEOperation currOperation) {
		for (String key : keys) {
			S4qEOperationParameter operationParameter = new S4qEOperationParameter();
			operationParameter.setName(key);
			operationParameter.setS4qEOperation(currOperation);
			List listToDelete = findOperationParameter(session,
					operationParameter);
			for (Object toDelete : listToDelete) {
				session.delete(toDelete);
			}
		}
	}

	private List findOperationParameter(final Session session,
			final S4qEOperationParameter operationParameter) {
		List toReturn = null;
		Criteria crit = session.createCriteria(S4qEOperationParameter.class);
		crit.add(Restrictions.eq("name", operationParameter.getName()));

		Criteria dataSourceCrit = crit.createCriteria("s4qEOperation");
		dataSourceCrit.add(Restrictions.eq("name", operationParameter
				.getS4qEOperation().getName()));

		Criteria sourceTypeCrit = dataSourceCrit.createCriteria("s4qEProcess");
		sourceTypeCrit.add(Restrictions.eq("name", operationParameter
				.getS4qEOperation().getS4qEProcess().getName()));

		toReturn = crit.list();
		return toReturn;
	}

	private S4qEOperation importOperation(final Session session,
			final S4qEOperation operation, final boolean overwrite) {
		S4qEOperation toReturn = null;
		S4qEOperation oldOperation = findOperation(session, operation);
		// find object into the DB
		if (oldOperation != null) {
			toReturn = oldOperation;
			if (overwrite) {
				// fill the data of the DB object with the data of the
				// object imported
				toReturn.setName(operation.getName());
				toReturn.setDescription(operation.getDescription());
				toReturn.setArchive(operation.getArchive());

				setInterfaceType(session, toReturn, operation
						.getS4qEInterfaceType());
				setDataSource(session, toReturn, operation.getS4qEDataSource());

				toReturn.setS4qEProcess(operation.getS4qEProcess());
			}
		} else {
			setInterfaceType(session, operation, operation
					.getS4qEInterfaceType());
			setDataSource(session, operation, operation.getS4qEDataSource());

			Integer idOperation = (Integer) session.save(operation);
			toReturn = (S4qEOperation) session.get(S4qEOperation.class,
					idOperation);

		}
		return toReturn;
	}

	private void setDataSource(final Session session,
			final S4qEOperation operation, final S4qEDataSource dataSource) {
		Criteria crit = session.createCriteria(S4qEDataSource.class);
		crit.add(Restrictions.eq("name", dataSource.getName()));
		Criteria sourceTypeCrit = crit.createCriteria("s4qESourceType");
		sourceTypeCrit.add(Restrictions.eq("name", dataSource
				.getS4qESourceType().getName()));
		S4qEDataSource dataSourceToSet = (S4qEDataSource) crit.uniqueResult();
		operation.setS4qEDataSource(dataSourceToSet);
	}

	private void setInterfaceType(final Session session,
			final S4qEOperation toReturn, final S4qEInterfaceType interfaceType) {
		Criteria crit = session.createCriteria(S4qEInterfaceType.class);
		crit.add(Restrictions.eq("name", interfaceType.getName()));
		S4qEInterfaceType interfaceTypeToSet = (S4qEInterfaceType) crit
				.uniqueResult();
		toReturn.setS4qEInterfaceType(interfaceTypeToSet);
	}

	private S4qEOperation findOperation(final Session session,
			final S4qEOperation operation) {
		S4qEOperation toReturn = null;
		Criteria crit = session.createCriteria(S4qEOperation.class);
		crit.add(Restrictions.eq("name", operation.getName()));

		Criteria sourceTypeCrit = crit.createCriteria("s4qEProcess");
		sourceTypeCrit.add(Restrictions.eq("name", operation.getS4qEProcess()
				.getName()));

		toReturn = (S4qEOperation) crit.uniqueResult();
		return toReturn;

	}

	private S4qEProcess importProcess(final Session session,
			final S4qEProcess process, final boolean overwrite) {
		S4qEProcess toReturn = null;
		S4qEProcess oldProcess = findProcess(session, process);
		// find sourceType into the DB
		if (oldProcess != null) {
			toReturn = oldProcess;
			if (overwrite) {
				// fill the data of the DB process with the data of the
				// process imported
				toReturn.setName(process.getName());
				toReturn.setDescription(process.getDescription());
				toReturn.setCoordinatorClass(process.getCoordinatorClass());
			}
		} else {
			// the process isn't in the DB
			Integer idProcess = (Integer) session.save(process);
			toReturn = (S4qEProcess) session.get(S4qEProcess.class, idProcess);
		}
		return toReturn;
	}

	private S4qEProcess findProcess(final Session session,
			final S4qEProcess process) {
		S4qEProcess toReturn = null;
		Criteria crit = session.createCriteria(S4qEProcess.class);
		crit.add(Restrictions.eq("name", process.getName()));
		toReturn = (S4qEProcess) crit.uniqueResult();
		return toReturn;
	}

}
