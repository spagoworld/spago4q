/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.importer;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.dao.AbstractHibernateDAO;
import it.eng.spago4q.metadata.S4qEDataSource;
import it.eng.spago4q.metadata.S4qEDataSourceParameter;
import it.eng.spago4q.metadata.S4qESourceType;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class EDataSourceImportHibImpl extends AbstractHibernateDAO implements
		EDataSourceImport {

	private static Logger logger = Logger
			.getLogger(EDataSourceImportHibImpl.class);

	public void importData(final S4qEDataSource dataSource,
			final boolean overwrite) throws EMFUserError {
		Session session = null;
		Transaction tx = null;
		try {
			session = getCurrentSession();
			tx = session.beginTransaction();

			S4qESourceType currSourceType = importSourceType(session,
					dataSource.getS4qESourceType(), overwrite);

			dataSource.setS4qESourceType(currSourceType);

			S4qEDataSource currDataSource = importDataSource(session,
					dataSource, overwrite);

			if (overwrite) {
				// get all parameters name
				Set<String> keys = new HashSet<String>();
				for (Object iterable_element : dataSource
						.getS4qEDataSourceParameters()) {
					((S4qEDataSourceParameter) iterable_element)
							.setS4qEDataSource(currDataSource);
					keys.add(((S4qEDataSourceParameter) iterable_element)
							.getName());
				}
				deleteDataSourceParameters(session, keys, currDataSource);
			}

			for (Object iterable_element : dataSource
					.getS4qEDataSourceParameters()) {
				((S4qEDataSourceParameter) iterable_element)
						.setS4qEDataSource(currDataSource);
				importDataSourceParameter(session,
						((S4qEDataSourceParameter) iterable_element), overwrite);
			}
			session.flush();

			tx.commit();

		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
			logger.debug("OUT");
		}
	}

	private void deleteDataSourceParameters(final Session session,
			final Set<String> keys, final S4qEDataSource currDataSource) {
		// select dataSourceParameters and delete it
		for (String key : keys) {
			S4qEDataSourceParameter dataSourceParameter = new S4qEDataSourceParameter();
			dataSourceParameter.setName(key);
			dataSourceParameter.setS4qEDataSource(currDataSource);
			List listToDelete = findDataSourceParameter(session,
					dataSourceParameter);
			for (Object toDelete : listToDelete) {
				session.delete(toDelete);
			}
		}
	}

	private S4qESourceType importSourceType(final Session session,
			final S4qESourceType sourceType, final boolean overwrite) {
		S4qESourceType toReturn = null;
		S4qESourceType oldSourceType = findSourceType(session, sourceType);
		// find sourceType into the DB
		if (oldSourceType != null) {
			toReturn = oldSourceType;
			if (overwrite) {
				// fill the data of the DB source type with the data of the
				// source type imported
				toReturn.setName(sourceType.getName());
				toReturn.setDescription(sourceType.getDescription());
				toReturn.setExtractorClass(sourceType.getExtractorClass());
			}
		} else {
			// the source type isn't in the DB
			Integer idSourceType = (Integer) session.save(sourceType);
			toReturn = (S4qESourceType) session.get(S4qESourceType.class,
					idSourceType);
		}
		return toReturn;
	}

	private S4qESourceType findSourceType(final Session session,
			final S4qESourceType sourceType) {
		S4qESourceType toReturn = null;
		Criteria crit = session.createCriteria(S4qESourceType.class);
		crit.add(Restrictions.eq("name", sourceType.getName()));
		toReturn = (S4qESourceType) crit.uniqueResult();
		return toReturn;
	}

	private S4qEDataSource importDataSource(final Session session,
			final S4qEDataSource dataSource, final boolean overwrite) {
		S4qEDataSource toReturn = null;
		S4qEDataSource oldDataSource = findDataSource(session, dataSource);
		// find object into the DB
		if (oldDataSource != null) {
			toReturn = oldDataSource;
			if (overwrite) {
				// fill the data of the DB object with the data of the
				// object imported
				toReturn.setName(dataSource.getName());
				toReturn.setDescription(dataSource.getDescription());
				toReturn.setS4qESourceType(dataSource.getS4qESourceType());
			}
		} else {
			// the object isn't in the DB
			Integer idDataSource = (Integer) session.save(dataSource);
			toReturn = (S4qEDataSource) session.get(S4qEDataSource.class,
					idDataSource);
		}
		return toReturn;
	}

	private S4qEDataSource findDataSource(final Session session,
			final S4qEDataSource datasource) {
		S4qEDataSource toReturn = null;
		Criteria crit = session.createCriteria(S4qEDataSource.class);
		crit.add(Restrictions.eq("name", datasource.getName()));

		Criteria sourceTypeCrit = crit.createCriteria("s4qESourceType");
		sourceTypeCrit.add(Restrictions.eq("name", datasource
				.getS4qESourceType().getName()));

		toReturn = (S4qEDataSource) crit.uniqueResult();
		return toReturn;
	}

	private void importDataSourceParameter(final Session session,
			final S4qEDataSourceParameter dataSourceParameter,
			final boolean overwrite) {
		if (overwrite) {
			// overite == true..
			// insert object
			session.save(dataSourceParameter);
		} else {
			// overwrite = false
			List result = findDataSourceParameter(session, dataSourceParameter);
			if (result == null || result.size() == 0) {
				session.save(dataSourceParameter);
			}
		}
	}

	private List findDataSourceParameter(final Session session,
			final S4qEDataSourceParameter dataSourceParameter) {
		List toReturn = null;
		Criteria crit = session.createCriteria(S4qEDataSourceParameter.class);
		crit.add(Restrictions.eq("name", dataSourceParameter.getName()));

		Criteria dataSourceCrit = crit.createCriteria("s4qEDataSource");
		dataSourceCrit.add(Restrictions.eq("name", dataSourceParameter
				.getS4qEDataSource().getName()));

		Criteria sourceTypeCrit = dataSourceCrit
				.createCriteria("s4qESourceType");
		sourceTypeCrit.add(Restrictions.eq("name", dataSourceParameter
				.getS4qEDataSource().getS4qESourceType().getName()));

		toReturn = crit.list();
		return toReturn;
	}
}
