/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.importer;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.dao.AbstractHibernateDAO;
import it.eng.spago4q.metadata.S4qEScript;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class EScriptImportHibImpl extends AbstractHibernateDAO implements
		EScriptImport {

	private static Logger logger = Logger.getLogger(EScriptImportHibImpl.class);

	public void importData(final S4qEScript script, final boolean overwrite)
			throws EMFUserError {
		Session session = null;
		Transaction tx = null;
		try {
			session = getCurrentSession();
			tx = session.beginTransaction();
			importScript(session, script, overwrite);

			session.flush();
			tx.commit();
		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
			logger.debug("OUT");
		}
	}

	private S4qEScript importScript(final Session session, final S4qEScript script,
			final boolean overwrite) {
		S4qEScript toReturn = null;
		S4qEScript oldScript = findScript(session, script);
		// find script into the DB
		if (oldScript != null) {
			toReturn = oldScript;
			if (overwrite) {
				// fill the data of the DB script with the data of the
				// script imported
				toReturn.setName(script.getName());
				toReturn.setDescription(script.getDescription());
				toReturn.setScript(script.getScript());
				toReturn.setScriptType(script.getScriptType());
			}
		} else {
			// the source type isn't in the DB
			Integer idScript = (Integer) session.save(script);
			toReturn = (S4qEScript) session.get(S4qEScript.class, idScript);
		}
		return toReturn;
	}

	private S4qEScript findScript(final Session session, final S4qEScript script) {
		S4qEScript toReturn = null;
		Criteria crit = session.createCriteria(S4qEScript.class);
		crit.add(Restrictions.eq("name", script.getName()));
		toReturn = (S4qEScript) crit.uniqueResult();
		return toReturn;
	}
}
