/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.importer;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.dao.AbstractHibernateDAO;
import it.eng.spago4q.metadata.S4qEDomainValue;
import it.eng.spago4q.metadata.S4qEInterfaceField;
import it.eng.spago4q.metadata.S4qEInterfaceType;
import it.eng.spagobi.tools.datasource.metadata.SbiDataSource;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class EInterfaceTypeImportHibImpl extends AbstractHibernateDAO implements
		EInterfaceTypeImport {

	private static Logger logger = Logger
			.getLogger(EInterfaceTypeImportHibImpl.class);

	public void importData(final S4qEInterfaceType interfaceType,
			final Integer dataSourceId, final boolean overwrite)
			throws EMFUserError {
		Session session = null;
		Transaction tx = null;
		try {
			session = getCurrentSession();
			tx = session.beginTransaction();
			// INTERFACE TYPE
			S4qEInterfaceType currInterfaceType = importInterfaceType(session,
					interfaceType, dataSourceId, overwrite);

			Set interfaceFields = interfaceType.getS4qEInterfaceFields();

			for (Object object : interfaceFields) {
				S4qEInterfaceField interfaceField = (S4qEInterfaceField) object;
				interfaceField.setS4qEInterfaceType(currInterfaceType);
				// INTERFACE FIELD
				S4qEInterfaceField currInterfacefield = importInterfaceField(
						session, interfaceField, overwrite);
				// DOMAIN VALUES
				if (overwrite
						&& (currInterfacefield.getS4qEDomainValues() == null || currInterfacefield
								.getS4qEDomainValues().size() != 0)) {
					deleteDomainValues(session, currInterfacefield);
				}
				Set domainValues = interfaceField.getS4qEDomainValues();

				if (domainValues != null) {
					for (Object domainValueObj : domainValues) {
						S4qEDomainValue domainValue = (S4qEDomainValue) domainValueObj;
						domainValue.setS4qEInterfaceField(currInterfacefield);
						importDomainValue(session, domainValue, overwrite);
					}
				}
			}

			session.flush();

			tx.commit();
		} catch (HibernateException he) {
			logException(he);
			if (tx != null) {
				tx.rollback();
			}
			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);

		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
			logger.debug("OUT");
		}
	}

	private void importDomainValue(final Session session,
			final S4qEDomainValue domainValue, final boolean overwrite) {
		if (overwrite) {
			session.save(domainValue);
		} else {
			List result = findDomainValue(session, domainValue);
			if (result != null && result.size() != 0) {
				session.save(domainValue);
			}
		}
	}

	private List findDomainValue(final Session session,
			final S4qEDomainValue domainValue) {
		List toReturn = null;
		Criteria crit = session.createCriteria(S4qEDomainValue.class);
		crit.add(Restrictions.eq("value", domainValue.getValue()));

		Criteria interfaceFieldCrit = crit.createCriteria("s4qEInterfaceField");
		interfaceFieldCrit.add(Restrictions.eq("name", domainValue
				.getS4qEInterfaceField().getName()));

		Criteria interfaceTypeCrit = interfaceFieldCrit
				.createCriteria("s4qEInterfaceType");
		interfaceTypeCrit.add(Restrictions.eq("name", domainValue
				.getS4qEInterfaceField().getS4qEInterfaceType().getName()));

		toReturn = crit.list();
		return toReturn;

	}

	private void deleteDomainValues(final Session session,
			final S4qEInterfaceField interfaceField) {
		Criteria crit = session.createCriteria(S4qEDomainValue.class);

		Criteria interfaceFieldCrit = crit.createCriteria("s4qEInterfaceField");
		interfaceFieldCrit.add(Restrictions
				.eq("name", interfaceField.getName()));

		Criteria interfaceTypeCrit = interfaceFieldCrit
				.createCriteria("s4qEInterfaceType");
		interfaceTypeCrit.add(Restrictions.eq("name", interfaceField
				.getS4qEInterfaceType().getName()));

		List domainsValuesToDelete = crit.list();

		for (Object domainValueToDelete : domainsValuesToDelete) {
			session.delete(domainValueToDelete);
		}

	}

	private S4qEInterfaceField importInterfaceField(final Session session,
			final S4qEInterfaceField interfaceField, final boolean overwrite) {
		S4qEInterfaceField toReturn = null;
		S4qEInterfaceField oldInterfaceField = findInterfaceField(session,
				interfaceField);
		// find object into the DB
		if (oldInterfaceField != null) {
			toReturn = oldInterfaceField;
			if (overwrite) {
				// fill the data of the DB object with the data of the
				// object imported
				toReturn.setName(interfaceField.getName());
				toReturn.setFieldType(interfaceField.getFieldType());
				toReturn.setKeyField(interfaceField.getKeyField());
				toReturn.setSensible(interfaceField.getSensible());
			}
		} else {
			// the object isn't in the DB
			Integer idInterfaceField = (Integer) session.save(interfaceField);
			toReturn = (S4qEInterfaceField) session.get(
					S4qEInterfaceField.class, idInterfaceField);
		}

		return toReturn;
	}

	private S4qEInterfaceField findInterfaceField(final Session session,
			final S4qEInterfaceField interfaceField) {
		S4qEInterfaceField toReturn = null;
		Criteria crit = session.createCriteria(S4qEInterfaceField.class);
		crit.add(Restrictions.eq("name", interfaceField.getName()));

		Criteria sourceTypeCrit = crit.createCriteria("s4qEInterfaceType");
		sourceTypeCrit.add(Restrictions.eq("name", interfaceField
				.getS4qEInterfaceType().getName()));

		toReturn = (S4qEInterfaceField) crit.uniqueResult();
		return toReturn;
	}

	private S4qEInterfaceType importInterfaceType(final Session session,
			final S4qEInterfaceType interfaceType, final Integer dataSourceId,
			final boolean overwrite) {
		S4qEInterfaceType toReturn = null;
		S4qEInterfaceType oldInterfaceType = findInterfaceType(session,
				interfaceType);
		if (oldInterfaceType != null) {
			toReturn = oldInterfaceType;
			if (overwrite) {
				toReturn.setName(interfaceType.getName());
				toReturn.setDescription(interfaceType.getDescription());
				toReturn.setTablename(interfaceType.getTablename());
				SbiDataSource sbiDataSource = (SbiDataSource) session.load(
						SbiDataSource.class, dataSourceId);
				toReturn.setSbiDataSource(sbiDataSource);
			}
		} else {
			SbiDataSource sbiDataSource = null;
			if (sbiDataSource != null){
				sbiDataSource = (SbiDataSource) session.load(
					SbiDataSource.class, dataSourceId);
			}
			interfaceType.setSbiDataSource(sbiDataSource);
			Integer idInterfaceType = (Integer) session.save(interfaceType);
			toReturn = (S4qEInterfaceType) session.get(S4qEInterfaceType.class,
					idInterfaceType);
		}
		return toReturn;
	}

	private S4qEInterfaceType findInterfaceType(final Session session,
			final S4qEInterfaceType interfaceType) {
		S4qEInterfaceType toReturn = null;
		Criteria crit = session.createCriteria(S4qEInterfaceType.class);
		crit.add(Restrictions.eq("name", interfaceType.getName()));
		toReturn = (S4qEInterfaceType) crit.uniqueResult();
		return toReturn;
	}
}