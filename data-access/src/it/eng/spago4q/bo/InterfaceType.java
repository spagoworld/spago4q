/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.bo;

import it.eng.spago.dbaccess.sql.DataConnection;
import it.eng.spago.error.EMFInternalError;
import it.eng.spago.error.EMFUserError;
import it.eng.spago.security.IEngUserProfile;
import it.eng.spagobi.commons.dao.DAOFactory;
import it.eng.spagobi.commons.utilities.DataSourceUtilities;

import java.sql.Connection;

import org.apache.log4j.Logger;

public class InterfaceType {
    
    private static Logger logger = Logger
    .getLogger(InterfaceType.class);

	private Integer id;
	private String name;
	private String description;
	private String tableName;
	private Integer dataSourceId;
	private String dataSourceLabel;
	private String tableHandlerEntry;
	private String filterClass;

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(final String tableName) {
		this.tableName = tableName;
	}

	public Integer getDataSourceId() {
		return dataSourceId;
	}

	public void setDataSourceId(final Integer dataSourceId) {
		this.dataSourceId = dataSourceId;
		if (dataSourceId != null){
			try {
				this.dataSourceLabel = DAOFactory.getDataSourceDAO()
						.loadDataSourceByID(dataSourceId).getLabel();
				Integer dialectId = DAOFactory.getDataSourceDAO()
				.loadDataSourceByID(dataSourceId).getDialectId();
				this.tableHandlerEntry = DAOFactory.getDomainDAO().loadDomainById(dialectId).getValueCd();
			} catch (EMFUserError e) {
			    logger.error(e.getLocalizedMessage());
			}
		}
	}

	public Connection getConnection(IEngUserProfile userProfile) {
		return new DataSourceUtilities().getConnection(userProfile , dataSourceLabel);
	}

	public DataConnection getDataConnection(IEngUserProfile userProfile) {
		DataConnection toReturn = null;
			try {
				toReturn = new DataSourceUtilities()
						.getDataConnection(getConnection(userProfile));
			} catch (EMFInternalError e) {
			    logger.error(e.getLocalizedMessage());
			}
		return toReturn;
	}

	public String getTableHandlerEntry() {
		return tableHandlerEntry;
	}

	/**
	 * @param filterClass the filterClass to set
	 */
	public void setFilterClass(String filterClass) {
		this.filterClass = filterClass;
	}

	/**
	 * @return the filterClass
	 */
	public String getFilterClass() {
		return filterClass;
	}

}
