/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.bo;

public class EOperation {

	private Integer id;
	private String name;
	private String description;
	private EProcess eProcess;
	private InterfaceType interfaceType;
	private DataSource dataSource;
	private Boolean archive;
	private Boolean rejected;

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public EProcess getEProcess() {
		return eProcess;
	}

	public void setEProcess(final EProcess process) {
		eProcess = process;
	}

	public InterfaceType getInterfaceType() {
		return interfaceType;
	}

	public void setInterfaceType(final InterfaceType interfaceType) {
		this.interfaceType = interfaceType;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(final DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Boolean getArchive() {
		return archive;
	}

	public void setArchive(final Boolean archive) {
		this.archive = archive;
	}

	/**
	 * @param rejected the rejected to set
	 */
	public void setRejected(Boolean rejected) {
		this.rejected = rejected;
	}

	/**
	 * @return the rejected
	 */
	public Boolean getRejected() {
		return rejected;
	}

}
