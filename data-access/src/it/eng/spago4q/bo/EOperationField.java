/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.bo;

public class EOperationField {

	private String name;
	private Integer id;
	private EOperation eOperation;
	private InterfaceField interfaceField;
	private Script script;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public EOperation getEOperation() {
		return eOperation;
	}

	public void setEOperation(final EOperation operation) {
		eOperation = operation;
	}

	public InterfaceField getInterfaceField() {
		return interfaceField;
	}

	public void setInterfaceField(final InterfaceField interfaceField) {
		this.interfaceField = interfaceField;
	}

	public Script getScript() {
		return script;
	}

	public void setScript(final Script script) {
		this.script = script;
	}

}
