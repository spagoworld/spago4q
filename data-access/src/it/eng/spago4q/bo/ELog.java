/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.bo;

import java.util.Date;

public class ELog {

	private int idLog;
	private Integer processId;
	private Integer operationId;
	private Integer extracted;
	private Integer inserted;
	private Date operationDate;
	private Date executionDate;
	private String archivePath;
	private Boolean completed;
	private Integer rejected;
	private String rejectedArchivePath;

	public Integer getRejected() {
		return rejected;
	}
	public String getRejectedArchivePath() {
		return rejectedArchivePath;
	}
	public int getIdLog() {
		return idLog;
	}
	public void setIdLog(final int idLog) {
		this.idLog = idLog;
	}
	public Integer getProcessId() {
		return processId;
	}
	public void setProcessId(final Integer processId) {
		this.processId = processId;
	}
	public Integer getOperationId() {
		return operationId;
	}
	public void setOperationId(final Integer operationId) {
		this.operationId = operationId;
	}
	public Integer getExtracted() {
		return extracted;
	}
	public void setExtracted(final Integer extracted) {
		this.extracted = extracted;
	}
	public Integer getInserted() {
		return inserted;
	}
	public void setInserted(final Integer inserted) {
		this.inserted = inserted;
	}
	public Date getOperationDate() {
		return operationDate;
	}
	public void setOperationDate(final Date operationTime) {
		this.operationDate = operationTime;
	}
	public Date getExecutionDate() {
		return executionDate;
	}
	public void setExecutionDate(final Date extractionTime) {
		this.executionDate = extractionTime;
	}
	public String getArchivePath() {
		return archivePath;
	}
	public void setArchivePath(final String archivePath) {
		this.archivePath = archivePath;
	}
	public Boolean getCompleted() {
		return completed;
	}
	public void setCompleted(final Boolean completed) {
		this.completed = completed;
	}
	public void setRejected(final Integer rejected) {
		this.rejected = rejected;
	}
	public void setRejectedArchivePath(final String rejected_archive_path) {
		this.rejectedArchivePath = rejected_archive_path;
	}
}
