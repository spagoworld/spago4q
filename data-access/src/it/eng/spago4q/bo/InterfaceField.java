/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.bo;

public class InterfaceField {
	
	private Integer id;
	private String name;
	private String fieldType;
	private Boolean sensible;
	private Boolean keyField;
	private InterfaceType interfaceType;
	
	public Integer getId() {
		return id;
	}
	public void setId(final Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(final String name) {
		this.name = name;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(final String fieldType) {
		this.fieldType = fieldType;
	}
	public Boolean getSensible() {
		return sensible;
	}
	public void setSensible(final Boolean sensible) {
		this.sensible = sensible;
	}
	public InterfaceType getInterfaceType() {
		return interfaceType;
	}
	public void setInterfaceType(final InterfaceType interfaceType) {
		this.interfaceType = interfaceType;
	}
	public Boolean getKeyField() {
		return keyField;
	}
	public void setKeyField(final Boolean keyField) {
		this.keyField = keyField;
	}

}
