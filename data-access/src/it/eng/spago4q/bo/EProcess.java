/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.bo;

public class EProcess {

	private Integer id;
	private String name;
	private String description;
	private String coordinatorClass;
	private EPeriodicity ePeriodicity;
	
	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getCoordinatorClass() {
		return coordinatorClass;
	}

	public void setCoordinatorClass(final String coordinatorClass) {
		this.coordinatorClass = coordinatorClass;
	}

	public EPeriodicity getEPeriodicity() {
		return ePeriodicity;
	}

	public void setEPeriodicity(final EPeriodicity periodicity) {
		ePeriodicity = periodicity;
	}

}
