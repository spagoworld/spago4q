/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.bo;

public class EOperationParameter {

	private Integer id;
	private String name;
	private String value;
	private Boolean cript;
	private EOperation eOperation;

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	public Boolean getCript() {
		return cript;
	}

	public void setCript(final Boolean cript) {
		this.cript = cript;
	}

	public EOperation getEOperation() {
		return eOperation;
	}

	public void setEOperation(final EOperation operation) {
		this.eOperation = operation;
	}

}
