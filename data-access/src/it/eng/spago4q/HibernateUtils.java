/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/

package it.eng.spago4q;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * 
 * Use in hibernate.cfg.xml <property
 * name="hibernate.current_session_context_class">thread</property>
 * 
 */
public final class HibernateUtils {

    private HibernateUtils() {
    }

    private static final SessionFactory SESSIONFACTORY;

    private static Logger log_ = Logger.getLogger(HibernateUtils.class);

    static {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            SESSIONFACTORY = new Configuration().configure()
                    .buildSessionFactory();
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return SESSIONFACTORY;
    }

    /**
     * Get the current session and begin a transaction.
     * 
     * @return
     */
    public static Session getCurrentSession() {
        Session session = null;
        if (SESSIONFACTORY != null) {
            session = SESSIONFACTORY.getCurrentSession();
            session.beginTransaction();
        }
        return session;
    }

    public static Session openSession() {
        if (SESSIONFACTORY != null) {
            SESSIONFACTORY.getCurrentSession();
        }
        return null;
    }

    public static Object genericSave(final Object object) throws Exception {
        try {
            Session session = getCurrentSession();
            session.save(object);
            session.getTransaction().commit();
        } catch (Exception e) {
            log_.info("Unable to save the object: " + object.getClass(), e);
            throw e;
        }
        return object;
    }

    public static Object genericUpdate(final Object object) throws Exception {
        try {
            Session session = getCurrentSession();
            session.update(object);
            session.getTransaction().commit();
        } catch (Exception e) {
            log_.info("Unable to save the object: " + object.getClass(), e);
            throw e;
        }
        return object;
    }

    public static Object genericDelete(final Object object) throws Exception {
        try {
            Session session = getCurrentSession();
            session.delete(object);
            session.getTransaction().commit();
        } catch (Exception e) {
            log_.info("Unable to save the object: " + object.getClass(), e);
            throw e;
        }
        return object;
    }

}
