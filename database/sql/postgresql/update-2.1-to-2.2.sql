ALTER TABLE s4q_e_script ALTER "name" TYPE character varying(50);

ALTER TABLE s4q_e_data_source 
  ADD CONSTRAINT unique_name_id_source_type UNIQUE (name, id_source_type);
ALTER TABLE s4q_e_interface_field 
  ADD CONSTRAINT unique_name_id_interface_type UNIQUE (name, id_interface_type);
ALTER TABLE s4q_e_operation 
  ADD CONSTRAINT unique_name_id_e_process UNIQUE (name, id_e_process);
ALTER TABLE s4q_e_script 
  ADD CONSTRAINT unique_name UNIQUE (name);
