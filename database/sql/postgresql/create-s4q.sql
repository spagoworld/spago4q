/*
Created		13/10/2008
Modified		28/04/2009
Project		
Model			
Company		
Author		
Version		
Database		PostgreSQL 8.1 
*/


/* Create Tables */


Create table S4Q_E_PROCESS
(
	id_process Integer NOT NULL,
	id_periodicity Integer,
	name Varchar(50) NOT NULL UNIQUE,
	description Varchar(255),
	coordinator_class Varchar(255),
 primary key (id_process)
) Without Oids;


Create table S4Q_E_OPERATION
(
	id_operation Integer NOT NULL,
	id_process Integer NOT NULL,
	id_data_source Integer,
	id_interface_type Integer,
	name Varchar(255) NOT NULL,
	description Varchar(255),
	archive Boolean Default false,
	rejected Boolean Default false,
 primary key (id_operation)
) Without Oids;


Create table S4Q_E_DATA_SOURCE
(
	id_data_source Integer NOT NULL,
	id_source_type Integer NOT NULL,
	name Varchar(50) NOT NULL,
	description Varchar(255),
 primary key (id_data_source)
) Without Oids;


Create table S4Q_E_DATA_SOURCE_PARAMETER
(
	id_data_source_parameter Integer NOT NULL,
	id_data_source Integer NOT NULL,
	name Varchar(255) NOT NULL,
	value Varchar(1000),
	cript Boolean,
 primary key (id_data_source_parameter)
) Without Oids;


Create table S4Q_E_SOURCE_TYPE
(
	id_source_type Integer NOT NULL,
	name Varchar(50) NOT NULL UNIQUE,
	description Varchar(255),
	extractor_class Varchar(255),
 primary key (id_source_type)
) Without Oids;


Create table S4Q_E_OPERATION_PARAMETER
(
	id_operation_parameter Integer NOT NULL,
	id_operation Integer NOT NULL,
	name Varchar(255) NOT NULL,
	value Varchar(1000),
	cript Boolean,
 primary key (id_operation_parameter)
) Without Oids;


Create table S4Q_E_OPERATION_FIELD
(
	id_operation_field Integer NOT NULL,
	id_operation Integer NOT NULL,
	id_interface_field Integer NOT NULL,
	id_script Integer,
	name Varchar(255) NOT NULL,
 primary key (id_operation_field)
) Without Oids;


Create table S4Q_E_INTERFACE_FIELD
(
	id_interface_field Integer NOT NULL,
	id_interface_type Integer NOT NULL,
	name Varchar(50),
	field_type Varchar(50),
	sensible Boolean,
	key_field Boolean,
 primary key (id_interface_field)
) Without Oids;


Create table S4Q_E_INTERFACE_TYPE
(
	id_interface_type Integer NOT NULL,
	id_data_source Integer,
	name Varchar(50) NOT NULL,
	description Varchar(255),
	tablename Varchar(255),
 primary key (id_interface_type)
) Without Oids;


Create table S4Q_E_DOMAIN_VALUE
(
	id_field_value Integer NOT NULL,
	id_interface_field Integer NOT NULL,
	value Varchar(400),
 primary key (id_field_value)
) Without Oids;


Create table S4Q_E_SCRIPT
(
	id_script Integer NOT NULL,
	name Varchar(400) NOT NULL,
	description Varchar(1000),
	script_type Varchar(20),
	script Text,
 primary key (id_script)
) Without Oids;


Create table S4Q_LOG
(
	id_log Integer NOT NULL,
	process_id Integer,
	operation_id Integer,
	extracted Integer,
	inserted Integer,
	operation_date Timestamp with time zone,
	exectution_date Timestamp with time zone,
	archive_path Varchar(1000),
	completed Boolean Default false,
 primary key (id_log)
) Without Oids;


Create table S4Q_E_PERIODICITY
(
	id_periodicity Integer NOT NULL,
	name Varchar(50) NOT NULL,
	description Varchar(1000),
	cronstring Varchar(50) NOT NULL,
 primary key (id_periodicity)
) Without Oids;



/* Create Indexes */
Create unique index e_process_name_uk on S4Q_E_PROCESS using btree (name);
Create unique index e_source_type_name_uk on S4Q_E_SOURCE_TYPE using btree (name);
Create unique index e_interface_name_uk on S4Q_E_INTERFACE_TYPE using btree (name);


/* Create Foreign Keys */

Alter table S4Q_E_OPERATION add  foreign key (id_process) references S4Q_E_PROCESS (id_process);

Alter table S4Q_E_OPERATION_FIELD add  foreign key (id_operation) references S4Q_E_OPERATION (id_operation) on update restrict on delete restrict;

Alter table S4Q_E_OPERATION_PARAMETER add  foreign key (id_operation) references S4Q_E_OPERATION (id_operation) on update restrict on delete restrict;

Alter table S4Q_E_DATA_SOURCE_PARAMETER add  foreign key (id_data_source) references S4Q_E_DATA_SOURCE (id_data_source);

Alter table S4Q_E_OPERATION add  foreign key (id_data_source) references S4Q_E_DATA_SOURCE (id_data_source);

Alter table S4Q_E_DATA_SOURCE add  foreign key (id_source_type) references S4Q_E_SOURCE_TYPE (id_source_type);

Alter table S4Q_E_OPERATION_FIELD add  foreign key (id_interface_field) references S4Q_E_INTERFACE_FIELD (id_interface_field);

Alter table S4Q_E_DOMAIN_VALUE add  foreign key (id_interface_field) references S4Q_E_INTERFACE_FIELD (id_interface_field) on update restrict on delete restrict;

Alter table S4Q_E_INTERFACE_FIELD add  foreign key (id_interface_type) references S4Q_E_INTERFACE_TYPE (id_interface_type);

Alter table S4Q_E_OPERATION add  foreign key (id_interface_type) references S4Q_E_INTERFACE_TYPE (id_interface_type);

Alter table S4Q_E_OPERATION_FIELD add  foreign key (id_script) references S4Q_E_SCRIPT (id_script) on update restrict on delete restrict;

Alter table S4Q_E_PROCESS add  foreign key (id_periodicity) references S4Q_E_PERIODICITY (id_periodicity) on update restrict on delete restrict;

Alter table S4Q_E_INTERFACE_TYPE add  foreign key (id_data_source) references SBI_DATA_SOURCE (DS_ID) on update restrict on delete restrict;


