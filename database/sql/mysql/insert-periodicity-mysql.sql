INSERT INTO S4Q_E_PERIODICITY (name, description, cronstring) VALUES 
('every day', 'Fire at midnight every day ','0 0 0 * * ?'),
('every monday', 'Fire every monday at midnight','0 0 0 ? * MON'),
('every month', 'Fire the 1st day of every month at midnight','0 0 0 1 * ?'); 