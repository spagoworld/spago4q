ALTER TABLE S4Q_LOG ADD COLUMN `archive_path` VARCHAR(1000) AFTER `execution_date`;

ALTER TABLE S4Q_E_OPERATION ADD COLUMN `archive` BOOLEAN DEFAULT 0 AFTER `description`;

ALTER TABLE S4Q_LOG ADD COLUMN `completed` BOOLEAN DEFAULT 0 AFTER `archive_path`;

ALTER TABLE S4Q_E_DATA_SOURCE ADD UNIQUE INDEX `unique_name_source_type`(`id_source_type`, `name`);
ALTER TABLE S4Q_E_INTERFACE_FIELD ADD INDEX `unipque_name_interface_type`(`id_interface_type`, `name`);
ALTER TABLE S4Q_E_OPERATION ADD UNIQUE INDEX `unipque_name_process`(`id_process`, `name`);
ALTER TABLE S4Q_E_SCRIPT MODIFY COLUMN `name` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE S4Q_E_SCRIPT ADD UNIQUE INDEX `unique_name`(`name`);
