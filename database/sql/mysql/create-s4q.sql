/*
Created		13/10/2008
Modified		20/07/2009
Project		
Model		
Company		
Author		
Version		
Database		mySQL 5 
*/


Create table `S4Q_E_PROCESS` (
	`id_process` Int NOT NULL AUTO_INCREMENT,
	`id_periodicity` Int,
	`name` Varchar(50) NOT NULL,
	`description` Varchar(255),
	`coordinator_class` Varchar(255),
	UNIQUE (`name`),
 Primary Key (`id_process`)) ENGINE = InnoDB;

Create table `S4Q_E_OPERATION` (
	`id_operation` Int NOT NULL AUTO_INCREMENT,
	`id_process` Int NOT NULL,
	`id_data_source` Int,
	`id_interface_type` Int,
	`name` Varchar(255) NOT NULL,
	`description` Varchar(255),
	`archive` Bool DEFAULT false,
	`rejected` Bool DEFAULT false,
 Primary Key (`id_operation`)) ENGINE = InnoDB;

Create table `S4Q_E_DATA_SOURCE` (
	`id_data_source` Int NOT NULL AUTO_INCREMENT,
	`id_source_type` Int NOT NULL,
	`name` Varchar(50) NOT NULL,
	`description` Varchar(255),
 Primary Key (`id_data_source`)) ENGINE = InnoDB;

Create table `S4Q_E_DATA_SOURCE_PARAMETER` (
	`id_data_source_parameter` Int NOT NULL AUTO_INCREMENT,
	`id_data_source` Int NOT NULL,
	`name` Varchar(255) NOT NULL,
	`value` Varchar(1000),
	`cript` Bool,
 Primary Key (`id_data_source_parameter`)) ENGINE = InnoDB;

Create table `S4Q_E_SOURCE_TYPE` (
	`id_source_type` Int NOT NULL AUTO_INCREMENT,
	`name` Varchar(50) NOT NULL,
	`description` Varchar(255),
	`extractor_class` Varchar(255),
	UNIQUE (`name`),
 Primary Key (`id_source_type`)) ENGINE = InnoDB;

Create table `S4Q_E_OPERATION_PARAMETER` (
	`id_operation_parameter` Int NOT NULL AUTO_INCREMENT,
	`id_operation` Int NOT NULL,
	`name` Varchar(255) NOT NULL,
	`value` Varchar(1000),
	`cript` Bool,
 Primary Key (`id_operation_parameter`)) ENGINE = InnoDB;

Create table `S4Q_E_OPERATION_FIELD` (
	`id_operation_field` Int NOT NULL AUTO_INCREMENT,
	`id_operation` Int NOT NULL,
	`id_interface_field` Int NOT NULL,
	`id_script` Int,
	`name` Varchar(255) NOT NULL,
 Primary Key (`id_operation_field`)) ENGINE = InnoDB;

Create table `S4Q_E_INTERFACE_FIELD` (
	`id_interface_field` Int NOT NULL AUTO_INCREMENT,
	`id_interface_type` Int NOT NULL,
	`name` Varchar(50),
	`field_type` Varchar(50),
	`sensible` Bool,
	`key_field` Bool,
 Primary Key (`id_interface_field`)) ENGINE = InnoDB;

Create table `S4Q_E_INTERFACE_TYPE` (
	`id_interface_type` Int NOT NULL AUTO_INCREMENT,
	`id_data_source` Int,
	`name` Varchar(50) NOT NULL,
	`description` Varchar(255),
	`tablename` Varchar(255),
	`filter_class` Varchar(255),
 Primary Key (`id_interface_type`)) ENGINE = InnoDB;

Create table `S4Q_E_DOMAIN_VALUE` (
	`id_field_value` Int NOT NULL AUTO_INCREMENT,
	`id_interface_field` Int NOT NULL,
	`value` Varchar(400),
 Primary Key (`id_field_value`)) ENGINE = InnoDB;

Create table `S4Q_E_SCRIPT` (
	`id_script` Int NOT NULL AUTO_INCREMENT,
	`name` Varchar(50) NOT NULL,
	`description` Varchar(1000),
	`script_type` Varchar(20),
	`script` Text,
 Primary Key (`id_script`)) ENGINE = InnoDB;

Create table `S4Q_LOG` (
	`id_log` Int NOT NULL AUTO_INCREMENT,
	`process_id` Int,
	`operation_id` Int,
	`extracted` Int,
	`inserted` Int,
	`operation_date` Datetime,
	`execution_date` Datetime,
	`archive_path` Varchar(1000),
	`completed` Bool DEFAULT false,
	`rejected` Int,
	`rejected_archive_path` Varchar(1000),
 Primary Key (`id_log`)) ENGINE = InnoDB;

Create table `S4Q_E_PERIODICITY` (
	`id_periodicity` Int NOT NULL AUTO_INCREMENT,
	`name` Varchar(50) NOT NULL,
	`description` Varchar(1000),
	`cronstring` Varchar(50) NOT NULL,
 Primary Key (`id_periodicity`)) ENGINE = InnoDB;


Alter table `S4Q_E_OPERATION` add unique `unipque_name_process` (`id_process`,`name`);
Alter table `S4Q_E_DATA_SOURCE` add unique `unique_name_source_type` (`id_data_source`,`name`);
Alter table `S4Q_E_INTERFACE_FIELD` add unique `unipque_name_interface_type` (`id_interface_type`,`name`);
Alter table `S4Q_E_SCRIPT` add unique `unique_name` (`name`);

Create UNIQUE Index `e_process_name_uk` ON `S4Q_E_PROCESS` (`name`);
Create UNIQUE Index `e_source_type_name_uk` ON `S4Q_E_SOURCE_TYPE` (`name`);
Create UNIQUE Index `e_interface_name_uk` ON `S4Q_E_INTERFACE_TYPE` (`name`);


Alter table `S4Q_E_OPERATION` add Foreign Key (`id_process`) references `S4Q_E_PROCESS` (`id_process`) on delete no action on update no action;
Alter table `S4Q_E_OPERATION_FIELD` add Foreign Key (`id_operation`) references `S4Q_E_OPERATION` (`id_operation`) on delete  restrict on update  restrict;
Alter table `S4Q_E_OPERATION_PARAMETER` add Foreign Key (`id_operation`) references `S4Q_E_OPERATION` (`id_operation`) on delete  restrict on update  restrict;
Alter table `S4Q_E_DATA_SOURCE_PARAMETER` add Foreign Key (`id_data_source`) references `S4Q_E_DATA_SOURCE` (`id_data_source`) on delete no action on update no action;
Alter table `S4Q_E_OPERATION` add Foreign Key (`id_data_source`) references `S4Q_E_DATA_SOURCE` (`id_data_source`) on delete no action on update no action;
Alter table `S4Q_E_DATA_SOURCE` add Foreign Key (`id_source_type`) references `S4Q_E_SOURCE_TYPE` (`id_source_type`) on delete no action on update no action;
Alter table `S4Q_E_OPERATION_FIELD` add Foreign Key (`id_interface_field`) references `S4Q_E_INTERFACE_FIELD` (`id_interface_field`) on delete no action on update no action;
Alter table `S4Q_E_DOMAIN_VALUE` add Foreign Key (`id_interface_field`) references `S4Q_E_INTERFACE_FIELD` (`id_interface_field`) on delete  restrict on update  restrict;
Alter table `S4Q_E_INTERFACE_FIELD` add Foreign Key (`id_interface_type`) references `S4Q_E_INTERFACE_TYPE` (`id_interface_type`) on delete no action on update no action;
Alter table `S4Q_E_OPERATION` add Foreign Key (`id_interface_type`) references `S4Q_E_INTERFACE_TYPE` (`id_interface_type`) on delete no action on update no action;
Alter table `S4Q_E_OPERATION_FIELD` add Foreign Key (`id_script`) references `S4Q_E_SCRIPT` (`id_script`) on delete  restrict on update  restrict;
Alter table `S4Q_E_PROCESS` add Foreign Key (`id_periodicity`) references `S4Q_E_PERIODICITY` (`id_periodicity`) on delete  restrict on update  restrict;
Alter table `S4Q_E_INTERFACE_TYPE` add Foreign Key (`id_data_source`) references `SBI_DATA_SOURCE` (`DS_ID`) on delete  restrict on update  restrict;


