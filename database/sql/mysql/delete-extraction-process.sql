﻿-- Set here the process name to delete
SET @PROCESS_NAME = 'REST_EXTRACTION_PROCESS';

SET @PROCESS_ID = (SELECT id_process FROM S4Q_E_PROCESS WHERE name = @PROCESS_NAME);

DELETE S4Q_E_OPERATION_PARAMETER FROM S4Q_E_OPERATION_PARAMETER, S4Q_E_OPERATION
  WHERE S4Q_E_OPERATION.id_process = @PROCESS_ID
  AND S4Q_E_OPERATION.id_operation = S4Q_E_OPERATION_PARAMETER.id_operation;

DELETE S4Q_E_OPERATION_FIELD FROM S4Q_E_OPERATION_FIELD, S4Q_E_OPERATION
  WHERE S4Q_E_OPERATION.id_process = @PROCESS_ID
  AND S4Q_E_OPERATION.id_operation = S4Q_E_OPERATION_FIELD.id_operation;

DELETE S4Q_E_OPERATION FROM S4Q_E_OPERATION
  WHERE S4Q_E_OPERATION.id_process = @PROCESS_ID;

DELETE FROM S4Q_E_OPERATION
WHERE id_process = @PROCESS_ID;

DELETE FROM S4Q_E_PROCESS
WHERE id_process = @PROCESS_ID;