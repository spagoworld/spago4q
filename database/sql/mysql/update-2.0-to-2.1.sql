-- add s4q_e_interface_type.id_data_source (FK) -> sbi_data_source.ds_id

ALTER TABLE `S4Q_E_INTERFACE_TYPE` 
  ADD COLUMN `id_data_source` INTEGER 
  AFTER `tablename`;

ALTER TABLE `S4Q_E_INTERFACE_TYPE` 
  ADD CONSTRAINT `FK_s4q_e_interface_type_1` 
  FOREIGN KEY `FK_s4q_e_interface_type_1` (`id_data_source`)
    REFERENCES `sbi_data_source` (`DS_ID`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- update LOG Table
ALTER TABLE `S4Q_LOG` 
  CHANGE COLUMN `operation_time` `operation_date` DATETIME DEFAULT NULL,
  ADD COLUMN `execution_date` DATETIME AFTER `operation_date`;
  
