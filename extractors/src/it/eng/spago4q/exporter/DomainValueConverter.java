/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import it.eng.spago4q.metadata.S4qEDomainValue;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

public class DomainValueConverter extends AbstractSingleValueConverter{

	@Override
	public boolean canConvert(final Class clazz) {
		return clazz.equals(S4qEDomainValue.class);
	}

	@Override
	public Object fromString(final String str) {
		S4qEDomainValue domainValue = new S4qEDomainValue();
		domainValue.setValue(str);
		return domainValue;
	}

}
