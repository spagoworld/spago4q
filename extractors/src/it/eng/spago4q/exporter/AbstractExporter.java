/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import com.thoughtworks.xstream.XStream;

public abstract class AbstractExporter {
	
	private XStream xstream;
	
	public AbstractExporter() {
		xstream = new XStream();
		setUp(xstream);
	}
	
	protected XStream getXStream(){
		return xstream;
	}
	
	public abstract void setUp(XStream par);

}
