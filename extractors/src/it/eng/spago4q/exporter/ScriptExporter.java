/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import java.util.Set;

import it.eng.spago4q.metadata.S4qEScript;

import com.thoughtworks.xstream.XStream;

public class ScriptExporter extends AbstractExporter{
	
	public String exportScript(final S4qEScript script){
		return getXStream().toXML(script);
	}

	public String exportScriptSet(final Set<S4qEScript> scriptSet){
		return getXStream().toXML(scriptSet);
	}

	public S4qEScript importScript(final String xml){
		return (S4qEScript)getXStream().fromXML(xml);
	}

	public Set<S4qEScript> importScriptSet(final String xml){
		return (Set<S4qEScript>)getXStream().fromXML(xml);
	}

	@Override
	public void setUp(final XStream xstreamPar) {
		xstreamPar.alias("script", S4qEScript.class);
		
		xstreamPar.omitField(S4qEScript.class, "idScript");
		xstreamPar.omitField(S4qEScript.class, "s4qEOperationFields");		

	}

}
