/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.bo.GenericItemUtil;
import it.eng.spago4q.importer.EDataSourceImport;
import it.eng.spago4q.importer.EDataSourceImportHibImpl;
import it.eng.spago4q.importer.EInterfaceTypeImport;
import it.eng.spago4q.importer.EInterfaceTypeImportHibImpl;
import it.eng.spago4q.importer.EOperationImport;
import it.eng.spago4q.importer.EOperationImportHibImpl;
import it.eng.spago4q.importer.EScriptImport;
import it.eng.spago4q.importer.EScriptImportHibImpl;
import it.eng.spago4q.metadata.S4qEDataSource;
import it.eng.spago4q.metadata.S4qEInterfaceType;
import it.eng.spago4q.metadata.S4qEOperation;
import it.eng.spago4q.metadata.S4qEScript;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;

import org.apache.log4j.Logger;

public class ImporterCoordinatorImpl implements ImporterCoordinator {

	private static final Logger LOGGER = Logger.getLogger(GenericItemUtil.class);

	public void dataSourceImporter(final String url, final boolean overwrite)
			throws EMFUserError {
		DataSourceExporter dataSourceExporter = new DataSourceExporter();
		// from file get the string
		String xml = readFile(url);
		Set<S4qEDataSource> dataSources = dataSourceExporter
				.importDataSourceSet(xml);
		EDataSourceImport dataSourceImport = new EDataSourceImportHibImpl();
		for (S4qEDataSource dataSource : dataSources) {
			dataSourceImport.importData(dataSource, overwrite);
		}
	}

	public void interfaceTypeImporter(final String url,
			final Integer dataSourceId, final boolean overwrite)
			throws EMFUserError {
		InterfaceTypeExporter interfaceTypeExporter = new InterfaceTypeExporter();
		// from file get the string
		String xml = readFile(url);
		Set<S4qEInterfaceType> interfaceTypes = interfaceTypeExporter
				.importInterfaceTypeSet(xml);
		EInterfaceTypeImport interfaceTypeImport = new EInterfaceTypeImportHibImpl();
		for (S4qEInterfaceType interfaceType : interfaceTypes) {
			interfaceTypeImport.importData(interfaceType, dataSourceId,
					overwrite);
		}
	}

	public void scriptImporter(final String url, final boolean overwrite)
			throws EMFUserError {
		ScriptExporter scriptExporter = new ScriptExporter();
		// from file get the string
		String xml = readFile(url);
		Set<S4qEScript> scripts = scriptExporter.importScriptSet(xml);
		EScriptImport scriptImport = new EScriptImportHibImpl();
		for (S4qEScript script : scripts) {
			scriptImport.importData(script, overwrite);
		}
	}

	public void operationImporter(final String operationUrl,
			final String dataSourceUrl, final String interfaceTypeUrl,
			final String scriptUrl, final Integer dataSourceId,
			final boolean overwrite) throws EMFUserError {
		dataSourceImporter(dataSourceUrl, overwrite);
		interfaceTypeImporter(interfaceTypeUrl, dataSourceId, overwrite);
		scriptImporter(scriptUrl, overwrite);

		OperationExporter operationExporter = new OperationExporter();
		// from file get the string
		String xml = readFile(operationUrl);
		Set<S4qEOperation> operations = operationExporter
				.importOperationSet(xml);
		EOperationImport operationImport = new EOperationImportHibImpl();
		for (S4qEOperation operation : operations) {
			operationImport.importData(operation, overwrite);
		}
	}

	private String readFile(final String url) {
		String toReturn = "";
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(url));
			String str;
			while ((str = in.readLine()) != null) {
				toReturn += str;
			}
			
		} catch (IOException e) {
			LOGGER.error(e.getLocalizedMessage());
		}finally{
			if (in != null){
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return toReturn;
	}
}