/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import java.util.Set;

public interface ExporterCoordinator {
	
	public void exportDataSource(Set<Integer> ids, String url);
	
	public void exportInterfaceType(Set<Integer> ids, String url);
	
	public void exportScript(Set<Integer> ids, String url);
	
	public void exportOperation(Set<Integer> ids, String operationUrl,
			String dataSourceUrl, String interfaceTypeUrl, String scriptUrl);

}
