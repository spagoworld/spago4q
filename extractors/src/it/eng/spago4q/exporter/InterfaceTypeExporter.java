/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import java.util.Set;

import it.eng.spago4q.metadata.S4qEDomainValue;
import it.eng.spago4q.metadata.S4qEInterfaceField;
import it.eng.spago4q.metadata.S4qEInterfaceType;

import com.thoughtworks.xstream.XStream;

public class InterfaceTypeExporter extends AbstractExporter {
	
	public String exportInterfaceType(final S4qEInterfaceType interfaceType){
		return getXStream().toXML(interfaceType);
	}

	public String exportInterfaceTypeSet(final Set<S4qEInterfaceType> interfaceTypeSet){
		return getXStream().toXML(interfaceTypeSet);
	}

	public S4qEInterfaceType importInterfaceType(final String xml){
		return (S4qEInterfaceType)getXStream().fromXML(xml);
	}

	public Set<S4qEInterfaceType> importInterfaceTypeSet(final String xml){
		return (Set<S4qEInterfaceType>)getXStream().fromXML(xml);
	}

	public void setUp(final XStream xstreamPar){
		xstreamPar.alias("interfaceType", S4qEInterfaceType.class);
		xstreamPar.alias("interfaceField", S4qEInterfaceField.class);
		xstreamPar.alias("value", S4qEDomainValue.class);

		xstreamPar.aliasField("interfaceFields", S4qEInterfaceType.class, "s4qEInterfaceFields");
		xstreamPar.aliasField("domainValues", S4qEInterfaceField.class, "s4qEDomainValues");

		xstreamPar.omitField(S4qEInterfaceType.class, "idInterfaceType");
		xstreamPar.omitField(S4qEInterfaceType.class, "s4qEOperations");
		xstreamPar.omitField(S4qEDomainValue.class, "idFieldValue");
		xstreamPar.omitField(S4qEInterfaceField.class, "s4qEOperationFields");
		
		xstreamPar.registerConverter(new DomainValueConverter());
		
	}
}
