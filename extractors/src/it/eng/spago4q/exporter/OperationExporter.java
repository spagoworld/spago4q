/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import java.util.Set;

import it.eng.spago4q.metadata.S4qEOperation;
import it.eng.spago4q.metadata.S4qEOperationField;
import it.eng.spago4q.metadata.S4qEOperationParameter;
import it.eng.spago4q.metadata.S4qEProcess;

import com.thoughtworks.xstream.XStream;

public class OperationExporter extends AbstractExporter{
	

	public String exportOperation(final S4qEOperation operation){
		return getXStream().toXML(operation);
	}

	public String exportOperationSet(final Set<S4qEOperation> operationSet){
		return getXStream().toXML(operationSet);
	}

	public S4qEOperation importOperation(final String xml){
		return (S4qEOperation)getXStream().fromXML(xml);
	}
	
	public Set<S4qEOperation> importOperationSet(final String xml){
		return (Set<S4qEOperation>)getXStream().fromXML(xml);
	}

	@Override
	public void setUp(final XStream xstreamPar) {
		xstreamPar.alias("operation", S4qEOperation.class);
		xstreamPar.alias("operationParameter" , S4qEOperationParameter.class);
		xstreamPar.alias("operationField" , S4qEOperationField.class);

		xstreamPar.aliasField("process", S4qEOperation.class, "s4qEProcess");
		xstreamPar.aliasField("operationParameters", S4qEOperation.class,"s4qEOperationParameters");
		xstreamPar.aliasField("dataSource", S4qEOperation.class, "s4qEDataSource");
		xstreamPar.aliasField("interfaceType", S4qEOperation.class, "s4qEInterfaceType");
		xstreamPar.aliasField("operationFields", S4qEOperation.class,"s4qEOperationFields");
		
		xstreamPar.aliasField("script", S4qEOperationField.class, "s4qEScript");
		xstreamPar.aliasField("interfaceField", S4qEOperationField.class, "s4qEInterfaceField");

		xstreamPar.omitField(S4qEOperation.class, "idOperation");

		xstreamPar.omitField(S4qEProcess.class, "idProcess");
		xstreamPar.omitField(S4qEProcess.class, "s4qEOperations");
		xstreamPar.omitField(S4qEProcess.class, "s4qEPeriodicity");
		
		xstreamPar.omitField(S4qEOperationField.class, "s4qEOperation");
		xstreamPar.omitField(S4qEOperationField.class, "idOperationField");
		
		xstreamPar.omitField(S4qEOperationParameter.class, "s4qEOperation");
		xstreamPar.omitField(S4qEOperationParameter.class, "idOperationParameter");
		
		xstreamPar.registerConverter(new DataSourceConverter());
		xstreamPar.registerConverter(new InterfaceTypeConverter());
		xstreamPar.registerConverter(new InterfaceFieldConverter());
		xstreamPar.registerConverter(new ScriptConverter());

	}

}
