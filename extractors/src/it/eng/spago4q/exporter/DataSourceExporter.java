/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import java.util.Set;

import it.eng.spago4q.metadata.S4qEDataSource;
import it.eng.spago4q.metadata.S4qEDataSourceParameter;
import it.eng.spago4q.metadata.S4qESourceType;

import com.thoughtworks.xstream.XStream;

public class DataSourceExporter extends AbstractExporter{
	
	public String exportDataSource(final S4qEDataSource dataSource){
		return getXStream().toXML(dataSource);
	}
	
	public String exportDataSourceSet(final Set<S4qEDataSource> dataSourceSet){
		return getXStream().toXML(dataSourceSet);
	}
	
	public S4qEDataSource importDataSource(final String xml){
		return (S4qEDataSource)getXStream().fromXML(xml);
	}

	public Set<S4qEDataSource> importDataSourceSet(final String xml){
		return (Set<S4qEDataSource>)getXStream().fromXML(xml);
	}
	
	public void setUp(final XStream xstreamPar){
		xstreamPar.alias("dataSource", S4qEDataSource.class);
		xstreamPar.alias("dataSourceParameter", S4qEDataSourceParameter.class);
		
		xstreamPar.aliasField("sourceType", S4qEDataSource.class, "s4qESourceType");
		xstreamPar.aliasField("dataSourceParameters", S4qEDataSource.class, "s4qEDataSourceParameters");

		xstreamPar.omitField(S4qEDataSource.class, "s4qEOperations");
		xstreamPar.omitField(S4qEDataSource.class, "idDataSource");
		
		xstreamPar.omitField(S4qESourceType.class, "idSourceType");
		xstreamPar.omitField(S4qESourceType.class, "s4qEDataSources");
		xstreamPar.omitField(S4qEDataSourceParameter.class, "idDataSourceParameter");
	}

}
