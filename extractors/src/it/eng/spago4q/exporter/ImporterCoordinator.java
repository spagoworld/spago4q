/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import it.eng.spago.error.EMFUserError;

public interface ImporterCoordinator {
	
	public void operationImporter(String operationUrl, String dataSourceUrl,
			String interfaceTypeUrl, String scriptUrl, Integer dataSourceId,
			boolean overwrite) throws EMFUserError;
	
	public void dataSourceImporter(String url, boolean overwrite) throws EMFUserError;
	
	public void interfaceTypeImporter(String url, Integer dataSourceId, boolean overwrite) throws EMFUserError;
	
	public void scriptImporter(String url, boolean overwrite) throws EMFUserError;

}
