/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import it.eng.spago4q.dao.EDataSourceDAOHibImpl;
import it.eng.spago4q.dao.EInterfaceTypeDAOHibImpl;
import it.eng.spago4q.dao.EOperationDAOHibImpl;
import it.eng.spago4q.dao.EScriptDAOHibImpl;
import it.eng.spago4q.metadata.S4qEDataSource;
import it.eng.spago4q.metadata.S4qEInterfaceType;
import it.eng.spago4q.metadata.S4qEOperation;
import it.eng.spago4q.metadata.S4qEOperationField;
import it.eng.spago4q.metadata.S4qEScript;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ExporterCoordinatorImpl implements ExporterCoordinator {

	public void exportDataSource(final Set<Integer> ids, final String url) {
		// get dataSource and insert into a set
		Set<S4qEDataSource> dataSources = new HashSet<S4qEDataSource>();
		EDataSourceDAOHibImpl daoDataSource = new EDataSourceDAOHibImpl();
		for (Integer id : ids) {
			dataSources.add(daoDataSource.getObjectById(id));
		}
		// export the set in a file
		DataSourceExporter dataSourceExporter = new DataSourceExporter();
		String dataSourceXml = dataSourceExporter
				.exportDataSourceSet(dataSources);
		// create the file and insert the xml
		writeInAFile(dataSourceXml, url);
	}

	public void exportInterfaceType(final Set<Integer> ids, final String url) {
		// get interfaceType and insert into a set
		Set<S4qEInterfaceType> interfaceTypes = new HashSet<S4qEInterfaceType>();
		EInterfaceTypeDAOHibImpl daoInterfaceType = new EInterfaceTypeDAOHibImpl();
		for (Integer id : ids) {
			interfaceTypes.add(daoInterfaceType.getObjectById(id));
		}
		// export the set in a file
		InterfaceTypeExporter interfaceTypeExporter = new InterfaceTypeExporter();
		String interfaceTypeXml = interfaceTypeExporter
				.exportInterfaceTypeSet(interfaceTypes);
		// create the file and insert the xml
		writeInAFile(interfaceTypeXml, url);
	}

	public void exportOperation(final Set<Integer> ids,
			final String operationUrl, final String dataSourceUrl,
			final String interfaceTypeUrl, final String scriptUrl) {
		// get operation and insert into a set
		Set<S4qEOperation> operations = new HashSet<S4qEOperation>();
		Set<Integer> dataSourceIds = new HashSet<Integer>();
		Set<Integer> interfaceTypeIds = new HashSet<Integer>();
		Set<Integer> scriptIds = new HashSet<Integer>();

		EOperationDAOHibImpl operationDao = new EOperationDAOHibImpl();
		for (Integer id : ids) {
			S4qEOperation operation = operationDao.getObjectById(id);
			operations.add(operation);
			// get dataSource ids and merge
			if (operation.getS4qEDataSource() != null){
				dataSourceIds.add(operation.getS4qEDataSource()
						.getIdDataSource());
			}
			// get interfaceTypes ids
			if (operation.getS4qEInterfaceType() != null){
				interfaceTypeIds.add(operation.getS4qEInterfaceType()
						.getIdInterfaceType());
			}
			// get script ids
			for (Object operationFieldObj : operation.getS4qEOperationFields()) {
				if (((S4qEOperationField) operationFieldObj).getS4qEScript() != null){
					scriptIds.add(((S4qEOperationField) operationFieldObj)
							.getS4qEScript().getIdScript());
				}
			}
		}

		exportDataSource(dataSourceIds, dataSourceUrl);
		exportInterfaceType(interfaceTypeIds, interfaceTypeUrl);
		exportScript(scriptIds, scriptUrl);

		// export the set in a file
		OperationExporter operationExporter = new OperationExporter();
		String operationXml = operationExporter.exportOperationSet(operations);
		// create the file and insert the xml
		writeInAFile(operationXml, operationUrl);
	}

	public void exportScript(final Set<Integer> ids, final String url) {
		// get script and insert into a set
		Set<S4qEScript> scripts = new HashSet<S4qEScript>();
		EScriptDAOHibImpl daoScript = new EScriptDAOHibImpl();
		for (Integer id : ids) {
			scripts.add(daoScript.getObjectById(id));
		}
		// export the set in a file
		ScriptExporter scriptExporter = new ScriptExporter();
		String scriptXml = scriptExporter.exportScriptSet(scripts);
		writeInAFile(scriptXml, url);

	}

	private void writeInAFile(final String string, final String fileName) {
		BufferedWriter out = null;
		try {
			File aFile = new File(fileName);
			out = new BufferedWriter(new FileWriter(aFile));
			out.write(string);
		} catch (IOException e) {
			System.out.println(e.getLocalizedMessage());
		}finally{
			if (out != null){
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
