/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import it.eng.spago4q.metadata.S4qEInterfaceField;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

public class InterfaceFieldConverter extends AbstractSingleValueConverter{
	
	@Override
	public boolean canConvert(final Class clazz) {
		return clazz.equals(S4qEInterfaceField.class);
	}

	@Override
	public Object fromString(final String str) {
		S4qEInterfaceField toReturn = new S4qEInterfaceField();
		toReturn.setName(str);
		return toReturn;
	}

}
