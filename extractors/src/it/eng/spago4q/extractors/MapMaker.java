/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.bo.EOperationField;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.extractors.bo.GenericItemInterface;
import it.eng.spago4q.extractors.bo.InterfaceFieldInstance;
import it.eng.spago4q.extractors.bo.InterfaceItem;
import it.eng.spago4q.extractors.filter.FilterFactory;
import it.eng.spago4q.extractors.filter.FilterInterface;
import it.eng.spago4q.extractors.script.ScriptExecutor;
import it.eng.spago4q.extractors.utilities.ExtractorConstants;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

public final class MapMaker {

	private MappingErrorManager mappingErrorManager;

	public MapMaker() {
		mappingErrorManager = new MappingErrorManager();
	}

	public List<InterfaceItem> mapping(final EOperation operation,
			final List<GenericItemInterface> genericItemList,
			final ScriptExecutor scriptExecutor,
			final DomainValueCacheCreator domainValueCache) throws EMFUserError {

		FilterInterface filter = FilterFactory.create(operation);

		List<InterfaceItem> toReturn = new ArrayList<InterfaceItem>();

		IES4QFKDAO eOperationFieldDao = (IES4QFKDAO) DAOFactory
				.getDAO("EOperationFieldDAO");

		List operationFieldList = eOperationFieldDao.loadObjectList(operation
				.getId(), null, null);

		for (Iterator<GenericItemInterface> iterator = genericItemList
				.iterator(); iterator.hasNext();) {
			String operationFieldName = null;
			String interfaceFieldName = null;
			InterfaceItem interfaceItem = new InterfaceItem();
			GenericItemInterface genericItem = iterator.next();
			if (genericItem == null || genericItem.getKeys().size() == 0) {
				continue;
			}
			try {
				for (Iterator iterator2 = operationFieldList.iterator(); iterator2
						.hasNext();) {
					EOperationField operationField = (EOperationField) iterator2
							.next();
					operationFieldName = operationField.getName();
					interfaceFieldName = operationField.getInterfaceField()
							.getName();
					// get value from generic Item
					String value = genericItem.getValue(operationFieldName);

					// tansform value with script
					if (operationField.getScript() != null
							&& operationField.getScript().getId() != null) {
						value = scriptExecutor.execute(operationField
								.getScript().getId(), operationField
								.getScript().getScriptType(), value);
					}

					Object objectValue = transform(value, operationField
							.getInterfaceField().getFieldType(),
							domainValueCache, operationField
									.getInterfaceField().getName());

					InterfaceFieldInstance interfaceFieldInstance = new InterfaceFieldInstance(
							objectValue, operationField.getInterfaceField()
									.getSensible(), operationField
									.getInterfaceField().getKeyField(),
							operationField.getInterfaceField().getFieldType());
					interfaceItem.setValue(operationField.getInterfaceField()
							.getName(), interfaceFieldInstance);
					operationFieldName = null;
					interfaceFieldName = null;
				}
				if (operationFieldList.size() != 0) {
					if (filter != null && (!filter.validate(interfaceItem))) {
						throw new MapMakerException(filter.getMessage());
					}
					toReturn.add(interfaceItem);
				}
			} catch (MapMakerException e) {
				mappingErrorManager.addError(operationFieldName,
						interfaceFieldName, e.getMessage(), genericItem);
			}
		}
		return toReturn;
	}

	protected MappingErrorManager getMappingErrorManager() {
		return mappingErrorManager;
	}

	private static Object transform(final String value, final String type,
			final DomainValueCacheCreator domainValueCache, final String name)
			throws MapMakerException {
		Object toReturn = null;
		if (!domainValueCache.hasDomainValue(name)) {

			if (type != null
					&& type.equalsIgnoreCase(ExtractorConstants.STRING)
					&& value != null) {
				toReturn = StringEscapeUtils.escapeSql(value);
			}
			if (type != null
					&& type.equalsIgnoreCase(ExtractorConstants.INTEGER)
					&& value != null) {
				try {
					toReturn = Integer.parseInt(value);
				} catch (NumberFormatException e) {
					throw new MapMakerException(
							"Not Integer value");
				}
			}
			if (type != null
					&& type.equalsIgnoreCase(ExtractorConstants.DOUBLE)
					&& value != null) {
				try {
					toReturn = new Double(value);
				} catch (NumberFormatException e) {
					throw new MapMakerException(
							"Not Double value");
				}
			}
			if (type != null
					&& type.equalsIgnoreCase(ExtractorConstants.BOOLEAN)) {
				if (value == null
						|| value.equalsIgnoreCase(Boolean
								.toString(Boolean.TRUE))
						|| value.equalsIgnoreCase(Boolean
								.toString(Boolean.FALSE))
						|| value.equalsIgnoreCase("0")
						|| value.equalsIgnoreCase("1")) {
					toReturn = value;
				} else {
					throw new MapMakerException(
							"Not Boolean value");
				}
			}
			if (type != null && type.equalsIgnoreCase(ExtractorConstants.DATE)) {
				toReturn = value;
			}
		} else {
			toReturn = domainValueCache.getDomainValueId(name, value);
		}
		return toReturn;
	}
}
