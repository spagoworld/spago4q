/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import it.eng.spago.base.SourceBean;
import it.eng.spago.dbaccess.sql.DataConnection;
import it.eng.spago.error.EMFUserError;
import it.eng.spago.security.IEngUserProfile;
import it.eng.spago4q.bo.InterfaceType;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QDAO;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.extractors.bo.InterfaceItem;
import it.eng.spago4q.extractors.tablehandlers.TableHandlerFactory;
import it.eng.spago4q.extractors.tablehandlers.TableHandlerInterface;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

public class FactTableManager {
    private TableHandlerInterface tableHandler;
    private InterfaceType interfaceType;

    private static Logger logger = Logger.getLogger(FactTableManager.class);

    public FactTableManager(final Integer interfaceTypeId) {
        IES4QDAO interfaceTypeDAO;
        try {
            interfaceTypeDAO = (IES4QDAO) DAOFactory
                    .getDAO("EInterfaceTypeDAO");
            interfaceType = (InterfaceType) interfaceTypeDAO
                    .loadObjectById(interfaceTypeId);
        } catch (EMFUserError e) {
            logger.error(e.getLocalizedMessage());
        }
        String tableHandlerEntry = interfaceType.getTableHandlerEntry();
        tableHandler = TableHandlerFactory.getTableHandler(tableHandlerEntry);
    }

    private Connection getConnection(final IEngUserProfile userProfile) {
        return interfaceType.getConnection(userProfile);
    }

    private DataConnection getDataConnection(final IEngUserProfile userProfile) {
        return interfaceType.getDataConnection(userProfile);
    }

    public void saveFactTable(final Integer interfaceTypeId,
            final IEngUserProfile userProfile) throws EMFUserError {
        IES4QDAO interfaceTypeDAO = (IES4QDAO) DAOFactory
                .getDAO("EInterfaceTypeDAO");
        InterfaceType currentInterfaceType = (InterfaceType) interfaceTypeDAO
                .loadObjectById(interfaceTypeId);
        String query = null;
        String tableName = tableHandler.transformTableName(currentInterfaceType
                .getTableName());
        if (tableHandler.hasThisTable(getConnection(userProfile), tableName)) {
            // alterTable

        } else {
            IES4QFKDAO interfaceFieldDAO = (IES4QFKDAO) DAOFactory
                    .getDAO("EInterfaceFieldDAO");
            List interfaceFieldList = interfaceFieldDAO.loadObjectList(
                    currentInterfaceType.getId(), null, null);
            tableHandler.createFactTable(getDataConnection(userProfile),
                    tableName, interfaceFieldList);
        }
    }

    public void dropFactTable(final Integer interfaceTypeId,
            final IEngUserProfile userProfile) throws EMFUserError {
        IES4QDAO interfaceTypeDAO = (IES4QDAO) DAOFactory
                .getDAO("EInterfaceTypeDAO");
        InterfaceType currentInterfaceType = (InterfaceType) interfaceTypeDAO
                .loadObjectById(interfaceTypeId);
        String tableName = tableHandler.transformTableName(currentInterfaceType
                .getTableName());
        if (tableHandler.hasThisTable(getConnection(userProfile), tableName)) {
            tableHandler.dropFactTable(getDataConnection(userProfile),
                    tableName);
        }
    }

    public boolean loadData(final Date operationDate,
            final Integer interfaceTypeId, final InterfaceItem interfaceItem,
            final Integer logId, final IEngUserProfile userProfile)
            throws EMFUserError {
        boolean toReturn = false;
        IES4QDAO interfaceTypeDAO = (IES4QDAO) DAOFactory
                .getDAO("EInterfaceTypeDAO");
        InterfaceType currentInterfaceType = (InterfaceType) interfaceTypeDAO
                .loadObjectById(interfaceTypeId);
        String tableName = tableHandler.transformTableName(currentInterfaceType
                .getTableName());
        SourceBean selectResult = tableHandler.selectFromFactTable(
                getDataConnection(userProfile), tableName, interfaceItem,
                operationDate);
        if (selectResult == null
                || selectResult.getContainedAttributes().size() == 0) {

            tableHandler.updateFactTable(operationDate,
                    getDataConnection(userProfile), tableName, interfaceItem);
            tableHandler.insertIntoFactTable(operationDate,
                    getDataConnection(userProfile), tableName, interfaceItem,
                    logId);
            toReturn = true;
        }
        // if resultSet > 1 the row is in the DB -> Do Noting
        // if (selectResult != null
        // && (selectResult.getContainedAttributes().size() != 0)) {
        // // Noting
        // }
        return toReturn;
    }
}
