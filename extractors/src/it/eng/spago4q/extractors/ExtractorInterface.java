/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import java.util.List;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.extractors.bo.ExtractorParameter;
import it.eng.spago4q.extractors.bo.GenericItemInterface;

public interface ExtractorInterface {
		
	/**
	 * 
	 * @return
	 * @throws EMFUserError
	 */
	public List<GenericItemInterface> execute() throws EMFUserError;
	
	public void init(EOperation eOperation) throws EMFUserError;
	
	
	
	/**
	 * 
	 * @param dataSourceId
	 * @return
	 * @throws EMFUserError
	 */
	public String dataSourceTest(Integer dataSourceId) throws EMFUserError;
	
	public String operationTest(Integer operationId)throws EMFUserError;

    public void init(List<ExtractorParameter> dataSourceParameters,
            List<ExtractorParameter> operationParameters);
}
