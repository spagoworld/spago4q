/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.scheduler;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.scheduler.job.ProcessExtractionJob;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public final class SchedulerSupplier {
    
    private static Logger logger = Logger
    .getLogger(SchedulerSupplier.class);
	
	private SchedulerSupplier() {}

	private static JobDetail getExtractionJob(final Integer processId, final String archiveDir) {
		JobDetail jobDetail = new JobDetail("PROCESS_" + processId,
				"EXTRACTION_PROCESS_GROUP", ProcessExtractionJob.class);

		jobDetail.getJobDataMap().put("E_PROCESS_ID", processId);
		jobDetail.getJobDataMap().put("ARCHIVE_DIR", archiveDir);
		jobDetail.setDurability(true);
		jobDetail.setVolatility(false);
		/*
		 * RequestsRecovery - if a job "requests recovery", and it is executing
		 * during the time of a 'hard shutdown' of the scheduler (i.e. the
		 * process it is running within crashes, or the machine is shut off),
		 * then it is re-executed when the scheduler is started again. In this
		 * case, the JobExecutionContext.isRecovering() method will return true.
		 */
		jobDetail.setRequestsRecovery(true);

		return jobDetail;
	}

	private static Trigger getExtractionCronTrigger(final String cronString,
			final Integer processId) throws ParseException {
		CronTrigger trigger = new CronTrigger();
		trigger.setName("TRIGGER_" + processId);
		trigger.setGroup("TRIGGER_PROCESS_GROUP");
		trigger.setCronExpression(cronString);
		// trigger.setMisfireInstruction(CronTrigger.MISFIRE_INSTRUCTION_SMART_POLICY);
		Date startTime = Calendar.getInstance().getTime();
		trigger.setStartTime(startTime);
		return trigger;

	}

	public static void addExtractionProcess(final Integer processId, final String cronString, final String archiveDir)
			throws EMFUserError {
		try {

			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			// set Job
			JobDetail jobDetail = getExtractionJob(processId, archiveDir);

			// set Trigger
			Trigger trigger = getExtractionCronTrigger(cronString, processId);
			scheduler.scheduleJob(jobDetail, trigger);

		} catch (SchedulerException se) {
		    logger.error(se.getLocalizedMessage());
		    throw new EMFUserError("ERROR" , se.getLocalizedMessage());
		} catch (ParseException e) {
            logger.error(e.getLocalizedMessage());
            throw new EMFUserError("ERROR" , e.getLocalizedMessage());
		}

	}

	public static void removeExtractionProcess(final Integer processId) {
		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.deleteJob("PROCESS_" + processId,
					"EXTRACTION_PROCESS_GROUP");
		} catch (SchedulerException e) {
		    logger.error(e.getLocalizedMessage());
		}
	}

}
