/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.scheduler.job;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.OperationCoordinatorFactory;
import it.eng.spago4q.extractors.OperationCoordinatorInterface;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
Non-stateful jobs only have their JobDataMap stored at the time they are added to the scheduler.
This means that any changes made to the contents of the job data map during execution of the job will be lost,
and will not seen by the job the next time it executes
*/

public class ProcessExtractionJob implements Job {
	
	private static transient Logger logger = Logger
			.getLogger(ProcessExtractionJob.class);

	public void execute(final JobExecutionContext context)
			throws JobExecutionException {
		logger.debug(ProcessExtractionJob.class.getName() + "::execute::"
				+ "Begin");
		System.out.println("Begin execution of ProcessExtractionJob");
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		int eProcessId = dataMap.getInt("E_PROCESS_ID");
		String archiveDir = dataMap.getString("ARCHIVE_DIR");

		logger.debug(ProcessExtractionJob.class.getName() + "::execute::"
				+ "Begin Istantiation of Operation Coordinetor"
				+ "of extraction process [" + eProcessId + "]");
		try {
			OperationCoordinatorInterface operationCoordinator =
				OperationCoordinatorFactory.create(eProcessId, archiveDir);
			//createSchedulerUserProfile (UserProfile)
			operationCoordinator.run(null);

			logger.debug(ProcessExtractionJob.class.getName() + "::execute::"
					+ "End execution of extraction process [" + eProcessId
					+ "]");
			System.out.println("End execution of ProcessExtractionJob");

		} catch (EMFUserError e) {
			logger.warn(ProcessExtractionJob.class.getName()
					+ "::execute::An error Occurs durring the execution"
					+ "of extraction process [" + eProcessId + "]", e);
		}

	}

}
