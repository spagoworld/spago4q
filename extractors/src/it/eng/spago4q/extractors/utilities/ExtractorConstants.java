/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.utilities;

public final class ExtractorConstants {
	
	private ExtractorConstants() {}
	
	public static final String STRING = "String";
	public static final String INTEGER = "Integer";
	public static final String DOUBLE = "Double";
	public static final String BOOLEAN = "Boolean";
	public static final String DATE = "Date";
	public static final String GROOVY = "Groovy";
	public static final String DATE_CONVERTER = "Date Converter";
}
