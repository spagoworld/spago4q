/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.utilities;

import it.eng.spago.base.SourceBean;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.extractors.ExtractorInterface;
import it.eng.spago4q.extractors.bo.ExtractorParameter;
import it.eng.spago4q.extractors.bo.GenericItemInterface;
import it.eng.spago4q.extractors.bo.GenericItemUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

public final class OperationUtil {

    private static Logger logger = Logger.getLogger(OperationUtil.class);
    private static final String REJECTED = "rejected";

    private OperationUtil() {
    }

    public static List<GenericItemInterface> executeOperation(
            final EOperation operation,
            final List<ExtractorParameter> dataSourceParameters,
            final List<ExtractorParameter> operationParameters)
            throws EMFUserError {
        List<GenericItemInterface> toReturn = null;

        String extractorClassName = operation.getDataSource().getSourceType()
                .getExtractorClass();

        ExtractorInterface extractor = null;

        try {
            extractor = (ExtractorInterface) Class.forName(extractorClassName)
                    .newInstance();
            // extractor.init(operation);
            extractor.init(dataSourceParameters, operationParameters);
            // extracted
            toReturn = extractor.execute();

        } catch (InstantiationException e) {
            logger.error("Error while Instantiate class " + extractorClassName,
                    e);
            throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
        } catch (IllegalAccessException e) {
            logger.error("Error while Instantiate class " + extractorClassName,
                    e);
            throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
        } catch (ClassNotFoundException e) {
            logger.error("Error while Instantiate class " + extractorClassName,
                    e);
            throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
        }

        return toReturn;
    }

    public static String createBeckUpArchive(
            final List<GenericItemInterface> genericItemList,
            final Date executionDate, final EOperation operation,
            final String extractorDir) {
        String fileName = operation.getName().replace(' ', '_');
        return createBeckupArchive(genericItemList, fileName, executionDate,
                extractorDir, null);
    }

    public static String createRejectedArchive(
            final List<GenericItemInterface> genericItemList,
            final List<String> messageErrors, final Date executionDate,
            final EOperation operation, final String extractorDir) {
        String fileName = operation.getName().replace(' ', '_');
        return createBeckupArchive(genericItemList, fileName, executionDate,
                extractorDir, messageErrors);
    }

    private static String createBeckupArchive(
            final List<GenericItemInterface> genericItemList,
            final String fileName, final Date executionDate,
            final String extractorDir, final List<String> messageErrors) {
        String toReturn = null;
        SourceBean genericItemSB = null;
        DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String archiveName = fileName + "-" + formatter.format(executionDate);

        if (messageErrors != null) {
            genericItemSB = GenericItemUtil
                    .genericItemListWithErrorsToSourceBean(genericItemList,
                            messageErrors);
            archiveName += "-" + REJECTED;
        } else {
            genericItemSB = GenericItemUtil
                    .genericItemListToSourceBean(genericItemList);
        }
        archiveName += ".zip";
        toReturn = GenericItemUtil.sourceBeanToZipFile(extractorDir,
                archiveName, genericItemSB);
        return toReturn;
    }

}
