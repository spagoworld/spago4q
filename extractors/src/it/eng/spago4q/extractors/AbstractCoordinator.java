/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import it.eng.spago.error.EMFUserError;
import it.eng.spago.security.IEngUserProfile;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.extractors.bo.ExtractorParameter;
import it.eng.spago4q.extractors.bo.GenericItemInterface;
import it.eng.spago4q.extractors.parameterfiller.ExtractionParameterFiller;
import it.eng.spago4q.extractors.script.ScriptExecutor;
import it.eng.spago4q.extractors.utilities.OperationUtil;

import java.util.Date;
import java.util.List;

public abstract class AbstractCoordinator extends TransformAndInsertUtil
        implements OperationCoordinatorInterface {

    protected abstract ExtractionParameterFiller getExtractionParameterFiller();

    protected void executeOperation(final Integer processId,
            final EOperation operation, final Date executionDate,
            final ScriptExecutor scriptExecutor,
            final IEngUserProfile userProfile) throws EMFUserError {
        String archivePath = null;

        ExtractionParameterFiller extractorParameterFiller = getExtractionParameterFiller();
        // set the operation
        extractorParameterFiller.setOperation(operation);
        // get data source parameters
        List<ExtractorParameter> dataSourceParameters = extractorParameterFiller
                .getDataSourceParameters();
        // get operations parameters
        List<ExtractorParameter> operationParameters = extractorParameterFiller
                .getOperationParameters();

        List<GenericItemInterface> genericItemList = OperationUtil
                .executeOperation(operation, dataSourceParameters,
                        operationParameters);
        transformAndInsertData(processId, operation, executionDate,
                executionDate, scriptExecutor, userProfile, archivePath,
                genericItemList);
    }

}
