/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import it.eng.spago.error.EMFUserError;
import it.eng.spago.security.IEngUserProfile;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.extractors.bo.GenericItemInterface;
import it.eng.spago4q.extractors.script.ScriptExecutor;
import it.eng.spago4q.extractors.xml.FileImportExtractor;

import java.util.Date;
import java.util.List;

public class FileImportCoordinator extends TransformAndInsertUtil {

	private FileImportExtractor fileImportExtractor;
	private EOperation operation;
	private Date operationDate;

	public void init(final Integer operationId, final String fileName,
			final String xPath, final Date opDate, final String archDir)
			throws EMFUserError {
		init(archDir);
		fileImportExtractor = new FileImportExtractor();
		fileImportExtractor.init(fileName, xPath);
		IES4QFKDAO eOperationDAO = (IES4QFKDAO) DAOFactory
				.getDAO("EOperationDAO");
		operation = (EOperation) eOperationDAO.loadObjectById(operationId);
		this.operationDate = opDate;
	}

	public void run(final IEngUserProfile userProfile) throws EMFUserError {
		List<GenericItemInterface> genericItemList = fileImportExtractor
				.execute();

		String archivePath = null;
		ScriptExecutor scriptExecutor = new ScriptExecutor();
		transformAndInsertData(null, operation, operationDate, new Date(), scriptExecutor,
				userProfile, archivePath, genericItemList);
	}
}
