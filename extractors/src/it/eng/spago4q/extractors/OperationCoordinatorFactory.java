/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import org.apache.log4j.Logger;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EProcess;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QDAO;

public final class OperationCoordinatorFactory {
	
	private OperationCoordinatorFactory() {}

	private static final String EPROCESS_DAO = "EProcessDAO";

	private static Logger logger = Logger
			.getLogger(OperationCoordinatorFactory.class);

	public static OperationCoordinatorInterface create(final Integer processId, final String extractionDir)
			throws EMFUserError {
		IES4QDAO eProcessDAO = (IES4QDAO) DAOFactory.getDAO(EPROCESS_DAO);

		EProcess eProcess = (EProcess) eProcessDAO.loadObjectById(processId);

		String coordinatorClassName = eProcess.getCoordinatorClass();

		OperationCoordinatorInterface coordinator = null;

		try {
			coordinator = (OperationCoordinatorInterface) Class.forName(
					coordinatorClassName).newInstance();
			coordinator.init(eProcess, extractionDir);

		} catch (InstantiationException e) {
			logger.error("Error while Instantiate class "
					+ coordinatorClassName, e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
		} catch (IllegalAccessException e) {
			logger.error("Error while Instantiate class "
					+ coordinatorClassName, e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
		} catch (ClassNotFoundException e) {
			logger.error("Error while Instantiate class "
					+ coordinatorClassName, e);
			throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
		}

		return coordinator;
	}

}
