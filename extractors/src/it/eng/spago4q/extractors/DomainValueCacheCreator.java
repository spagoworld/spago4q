/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.DomainValue;
import it.eng.spago4q.bo.InterfaceField;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;

import java.util.HashMap;
import java.util.List;

public class DomainValueCacheCreator {

	private HashMap<String, HashMap<String, Integer>> cache;

	public DomainValueCacheCreator(final Integer interfaceTypeId)
			throws EMFUserError {
		// get interfaceField
		IES4QFKDAO eInterfaceFieldDAO = (IES4QFKDAO) DAOFactory
				.getDAO("EInterfaceFieldDAO");

		List<InterfaceField> interfaceFieldList = (List<InterfaceField>) eInterfaceFieldDAO
				.loadObjectList(interfaceTypeId, null, null);

		if (interfaceFieldList != null && !(interfaceFieldList.isEmpty())) {
			// get DomainValueDAO
			cache = new HashMap<String, HashMap<String, Integer>>();
			IES4QFKDAO eDomainValueDAO = (IES4QFKDAO) DAOFactory
					.getDAO("EDomainValueDAO");

			for (InterfaceField interfaceField : interfaceFieldList) {
				List domainValueList = eDomainValueDAO.loadObjectList(
						interfaceField.getId(), null, null);
				if (domainValueList != null && !(domainValueList.isEmpty())) {
					HashMap<String, Integer> domainValueHashMap = 
						createDomainValueHashMap((List<DomainValue>) domainValueList);
					cache.put(interfaceField.getName(), domainValueHashMap);
				}
			}
		}
	}

	public Integer getDomainValueId(final String columnName,
			final String valueName) {
		Integer toReturn = null;
		HashMap<String, Integer> columnHashMap = cache.get(columnName);
		if (columnHashMap != null) {
			toReturn = columnHashMap.get(valueName);
		}
		return toReturn;
	}

	public boolean hasDomainValue(final String columnName) {
		boolean toReturn = false;
		if (cache.get(columnName) != null && !(cache.isEmpty())) {
			toReturn = true;
		}
		return toReturn;
	}

	private HashMap<String, Integer> createDomainValueHashMap(
			final List<DomainValue> domainValueList) {
		HashMap<String, Integer> toReturn = new HashMap<String, Integer>();
		for (DomainValue domainValue : domainValueList) {
			toReturn.put(domainValue.getValue(), domainValue.getId());
		}
		return toReturn;
	}
}
