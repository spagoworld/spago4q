/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.xml;

import it.eng.spago4q.extractors.GenericItemSetterInterface;
import it.eng.spago4q.extractors.bo.GenericItem;

import java.util.Iterator;

import org.dom4j.Element;
import org.dom4j.ElementHandler;
import org.dom4j.ElementPath;

public class SaxElementHandler implements ElementHandler{
	
	private GenericItemSetterInterface extractor;

	public SaxElementHandler(final GenericItemSetterInterface extractor) {
		this.extractor = extractor;
	}
	
	public void onEnd(final ElementPath path) {
		// process a ROW element
		Element row = path.getCurrent();
		GenericItem gi = new GenericItem();
		for (Iterator<Element> iterator = row.elementIterator(); iterator
				.hasNext();) {
			Element object = (Element) iterator.next();
			if (!object.getTextTrim().equals("")){
				gi.setValue(object.getName(), object.getTextTrim());
			}
		}
		extractor.setGenericItem(gi);
		// prune the tree
		row.detach();
	}

	public void onStart(final ElementPath path) {
	
	}
}