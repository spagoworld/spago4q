/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.xml;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EOperation;

public class FileImportExtractor extends SaxXmlExtractor {

	
	@Override
	public void init(final EOperation operation) throws EMFUserError {
	}
	
	public void init(final String filePath, final String xPath){
		setDocument(filePath);
		setXPathExpression(xPath);
	}

	@Override
	protected void setUp() {
	}

	@Override
	protected void tearDown() {
	}
}