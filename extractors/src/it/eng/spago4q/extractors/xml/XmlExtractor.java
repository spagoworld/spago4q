/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.xml;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.AbstractExtractor;
import it.eng.spago4q.extractors.bo.GenericItem;
import it.eng.spago4q.extractors.bo.GenericItemInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlExtractor extends AbstractExtractor {

	private static final String FILE = "FILE";
	private static final String XPATH = "XPATH";

	private XPathExpression xPathExpression;
	private Document document;
	
	private Document getDocument() throws EMFUserError{
		if (document == null){
			throw new EMFUserError(EMFErrorSeverity.ERROR, "Impossible to create the document.");
		}
		return document;
	}
	
	private XPathExpression getXPathExpression() throws EMFUserError{
		if (xPathExpression == null){
			throw new EMFUserError(EMFErrorSeverity.ERROR, "Impossible to create the xPath expression.");
		}
		return xPathExpression;
	}

	private void setXPathExpression(final String expression) {
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = (XPath) factory.newXPath();
		try {
			xPathExpression = ((javax.xml.xpath.XPath) xpath)
					.compile(expression);
		} catch (XPathExpressionException e) {
			xPathExpression = null;
		}
	}

	private void setDocument(final String path) {
		DocumentBuilderFactory domFactory = DocumentBuilderFactory
				.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder;
		try {
			builder = domFactory.newDocumentBuilder();
			document = (Document) builder.parse(path);
		} catch (ParserConfigurationException e) {
			document = null;
		} catch (SAXException e) {
			document = null;
		} catch (IOException e) {
			document = null;
		}
	}
	
	protected List<GenericItemInterface> getElementValue() throws EMFUserError {
		Object result;
		List<GenericItemInterface> toReturn = new ArrayList<GenericItemInterface>();
		try {
			result = getXPathExpression().evaluate(getDocument(), XPathConstants.NODESET);
		NodeList nodes = (NodeList) result;
		for (int i = 0; i < nodes.getLength(); i++) {
			GenericItem genericItem = new GenericItem();
			Node currentNode = nodes.item(i);
			NodeList children = currentNode.getChildNodes();
			for (int j = 0; j < children.getLength(); j++) {
				Node currentChild = children.item(j);
				if (currentChild.hasChildNodes()){
					genericItem.setValue(currentChild.getNodeName(),
								currentChild.getFirstChild().getNodeValue());
				}
			}
			toReturn.add(genericItem);
		}
		} catch (XPathExpressionException e) {
			throw new EMFUserError(EMFErrorSeverity.ERROR ,e.getMessage());
		}
		return toReturn;
		
	}

	@Override
	protected List<GenericItemInterface> extract() throws EMFUserError {
		return getElementValue();
	}

	@Override
	protected void setUp() {
		setDocument(readDataSourceParameterValue(FILE));
		setXPathExpression(readOperationParameterValue(XPATH));
	}

	@Override
	protected void tearDown() {
	}

	public String dataSourceTest(final Integer dataSourceId) throws EMFUserError {
		String toReturn = "";
		setDataSourceParameter(dataSourceId);
		String documentPath = readDataSourceParameterValue(FILE);
		setDocument(documentPath);
		toReturn = "Document URI: " + getDocument().getDocumentURI();
		
		return toReturn;
	}

	public String operationTest(final Integer operationId) throws EMFUserError {
		String toReturn = "";
		setOperationParameter(operationId);
		String xPathQuery = readOperationParameterValue(XPATH);
		setXPathExpression(xPathQuery);
		getXPathExpression();
		toReturn = "XPathExpression: " + xPathQuery;
		return toReturn;
	}
}
