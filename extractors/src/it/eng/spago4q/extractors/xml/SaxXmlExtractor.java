/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.xml;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.AbstractExtractor;
import it.eng.spago4q.extractors.GenericItemSetterInterface;
import it.eng.spago4q.extractors.bo.GenericItemInterface;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

public class SaxXmlExtractor extends AbstractExtractor implements
        GenericItemSetterInterface {

    private List<GenericItemInterface> result = new ArrayList<GenericItemInterface>();
    private static final String FILE = "FILE";
    private static final String XPATH = "XPATH";

    private static final String PROXY_URL = "PROXY_URL";
    private static final String PROXY_PORT = "PROXY_PORT";
    private static final String PROXY_USER = "PROXY_USER";
    private static final String PROXY_PASSWORD = "PROXY_PASSWORD";

    private static final String HTTP_PROXY_HOST = "http.proxyHost";
    private static final String HTTP_PROXY_PORT = "http.proxyPort";
    private static final String PROXY_AUTHORIZATION = "Proxy-Authorization";
    private static final String BASIC_ = "Basic ";

    private String xPathExpression = null;
    private String document = null;
    private String proxyUserPasswordEncoded = null;

    public void setGenericItem(final GenericItemInterface genericItem) {
        result.add(genericItem);
    }

    protected void generateResult() throws EMFUserError {
        SAXReader reader = new SAXReader();
        reader.addHandler(getXPathExpression(), new SaxElementHandler(this));
        try {
            if (proxyUserPasswordEncoded != null) {
                reader.read(getConnectionInputStream(getDocument()));
            } else {
                reader.read(getDocument());
            }
        } catch (DocumentException e) {
            throw new EMFUserError(EMFErrorSeverity.ERROR, e.getMessage());
        }
    }

    private String getDocument() throws EMFUserError {
        if (document == null) {
            throw new EMFUserError(EMFErrorSeverity.ERROR,
                    "Impossible to create the document.");
        }
        return document;
    }

    private String getXPathExpression() throws EMFUserError {
        if (xPathExpression == null) {
            throw new EMFUserError(EMFErrorSeverity.ERROR,
                    "Impossible to create the XPath expression.");
        }
        return xPathExpression;
    }

    @Override
    protected void setUp() {
        setDocument(readDataSourceParameterValue(FILE));
        setXPathExpression(readOperationParameterValue(XPATH));
        // new datasource parameter
        setProxyConfiguration(readDataSourceParameterValue(PROXY_URL),
                readDataSourceParameterValue(PROXY_PORT));
        setUserPasswordEncoded(readDataSourceParameterValue(PROXY_USER),
                readDataSourceParameterValue(PROXY_PASSWORD));
    }

    protected void setXPathExpression(final String xPathExp) {
        xPathExpression = xPathExp;
    }

    protected void setDocument(final String doc) {
        document = doc;
    }

    private void setProxyConfiguration(final String proxyUrl, final String proxyPort) {
        if (proxyPort != null && proxyUrl != null) {
            System.setProperty(HTTP_PROXY_HOST, proxyUrl);
            System.setProperty(HTTP_PROXY_PORT, proxyPort);
        }
    }

    private void setUserPasswordEncoded(final String proxyUser, final String proxyPassword) {
        if (proxyUser != null && proxyPassword != null) {
            proxyUserPasswordEncoded = new String(Base64.encodeBase64(((proxyUser
                    + ":" + proxyPassword).getBytes())).toString());
        }
    }

    private InputStream getConnectionInputStream(final String url) throws EMFUserError {
        InputStream toReturn = null;
        URL myUrl;
        try {
            myUrl = new URL(url);
            URLConnection uc = myUrl.openConnection();
            if (proxyUserPasswordEncoded != null) {
                uc.setRequestProperty(PROXY_AUTHORIZATION, BASIC_
                        + proxyUserPasswordEncoded);
            }
            toReturn = uc.getInputStream();
        } catch (MalformedURLException e) {
            throw new EMFUserError(EMFErrorSeverity.ERROR,
            e.getLocalizedMessage());
        } catch (IOException e) {
            throw new EMFUserError(EMFErrorSeverity.ERROR,
                    e.getLocalizedMessage());
        }
        return toReturn;
    }

    @Override
    protected void tearDown() {
    }

    @Override
    protected List<GenericItemInterface> extract() throws EMFUserError {
        generateResult();
        return result;
    }
}
