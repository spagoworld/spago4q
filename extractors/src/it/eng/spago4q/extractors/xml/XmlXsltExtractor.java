/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.xml;

import it.eng.spago.error.EMFUserError;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;
import org.xml.sax.helpers.XMLReaderFactory;

public class XmlXsltExtractor extends Dom4JXmlExtractor {
    private TransformerFactory myTransformerFactory = null;
    private Transformer myTransformer = null;

    private static final String XSLT = "XSLT";
    private static final String FILE = "FILE";
    private String xsltUrl;

    private String outputFileName;
    private String inputFileName;
    
    private static transient Logger logger = Logger
    .getLogger(XmlXsltExtractor.class);

    @Override
    protected void setUp() throws EMFUserError {
        super.setUp();
        setXSLPath(readDataSourceParameterValue(XSLT));
        inputFileName = readDataSourceParameterValue(FILE);
        outputFileName = createTempOutputFile();
        if (xsltUrl != null && !(xsltUrl.trim().isEmpty())) {
            doTransform(inputFileName, getXSLPath(), outputFileName);
            setDocument(outputFileName);
        }
    }

    @Override
    protected void tearDown() {
        super.tearDown();
        if (outputFileName != null) {
            new File(outputFileName).delete();
        }
    }

    private String createTempOutputFile() {
        Date date = new Date();
        String name = "XSLXML" + date.getTime();
        File temp = null;
        try {
            temp = File.createTempFile(name, ".xml");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return temp.getAbsolutePath();
    }

    private void setXSLPath(final String value) {
        xsltUrl = value;
    }

    private String getXSLPath() {
        return xsltUrl;
    }

    // 
    private void initTransformer(final String _templateName) throws EMFUserError,
            TransformerConfigurationException {
        Source myTransformTemplate = getSourceObject(_templateName);
        myTransformer = myTransformerFactory
                .newTransformer(myTransformTemplate);
    }

    public void doTransform(final String _inputFileName,
            final String _transformerFileName,
            final String _outputFileName) throws EMFUserError {
        myTransformerFactory = TransformerFactory.newInstance();
        try {
            initTransformer(_transformerFileName);
        } catch (TransformerConfigurationException tce) {
            logger.error("Exception in transformer initialization.");
            throw new EMFUserError("ERROR", tce.getLocalizedMessage());
        }
        Source myXMLSource = getInputSourceObject(_inputFileName);
        Result myResult;
        try {
            myResult = getResultObject(_outputFileName);
        } catch (FileNotFoundException e) {
            logger.error(e.getLocalizedMessage());
            throw new EMFUserError("ERROR", e.getLocalizedMessage());
        }
        try {
            myTransformer.transform(myXMLSource, myResult);
        } catch (TransformerException te) {
            logger.error("Exception in transformation.");
            logger.error(te.getMessage());
            throw new EMFUserError("ERROR", te.getLocalizedMessage());
        }
    }

    protected Source getInputSourceObject(final String _pathName) throws EMFUserError {
        InputSource inputSource = getInputSourceFromFile(_pathName);
        XMLReader xmlFilter = getFilteringReader();
        // SAXSource saxSource = new SAXSource(inputSource);
        SAXSource saxSource = new SAXSource(xmlFilter, inputSource);
        return saxSource;
    }

    private InputSource getInputSourceFromFile(final String _fileName)
            throws EMFUserError {
        File file = new File(_fileName);
        if (!fileExists(file)) {
            throw getFileNotFoundException(file);
        }
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException fnfe) {
            throw new EMFUserError("ERROR", "Input source file not found.");
        }
        InputSource inputSource = new InputSource(fis);
        return inputSource;
    };

    private XMLReader getFilteringReader() {
        XMLReader myReader = getReader();
        XMLFilterImpl xmlFilter = new XMLFilterImpl(myReader);
        return xmlFilter;
    }

    protected XMLReader getReader() {
        XMLReader myReader = null;
        try {
            myReader = XMLReaderFactory.createXMLReader();
        } catch (SAXException e) {
            logger.error("Error in creating XMLReader instance");
        }
        return myReader;
    }

    private Source getSourceObject(final String _pathName) throws EMFUserError {
        File file = new File(_pathName);
        if (!fileExists(file)) {
            throw getFileNotFoundException(file);
        }
        StreamSource streamSource = new StreamSource(file);
        return streamSource;
    }

    protected boolean fileExists(final File _file) {
        boolean toReturn = false;
        if (_file.isFile() || _file.canRead()){
            toReturn =  true;
        }
        return toReturn;
    }

    protected EMFUserError getFileNotFoundException(final File _file) {
        String errorMessage = "File " + _file.getAbsolutePath()
                + " is not found, or is not Readable.";
        return new EMFUserError("ERROR", errorMessage);
    }

    private Result getResultObject(final String _fileName)
            throws FileNotFoundException {
        File file = new File(_fileName);
        StreamResult streamResult = new StreamResult(file);
        return streamResult;
    }

}
