/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.xml;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.AbstractExtractor;
import it.eng.spago4q.extractors.bo.GenericItem;
import it.eng.spago4q.extractors.bo.GenericItemInterface;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class Dom4JXmlExtractor extends AbstractExtractor {

    private static final String FILE = "FILE";
    private static final String XPATH = "XPATH";

    private String xPathExpression;
    private Document document;

    public Document parse(final String uri) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document toReturn = reader.read(uri);
        return toReturn;
    }

    protected List<GenericItemInterface> getElementsValue() throws EMFUserError {
        List<GenericItemInterface> toReturn = new ArrayList<GenericItemInterface>();
        List list = getDocument().selectNodes(getXPathExpression());

        for (Iterator iter = list.iterator(); iter.hasNext();) {
            Element node = (Element) iter.next();
            GenericItem item = new GenericItem();
            for (Iterator<Element> iterator = node.elementIterator(); iterator
                    .hasNext();) {
                Element object = (Element) iterator.next();
                item.setValue(object.getName(), object.getTextTrim());
            }
            toReturn.add(item);
        }
        return toReturn;
    }

    @Override
    protected List<GenericItemInterface> extract() throws EMFUserError {
        return getElementsValue();
    }

    @Override
    protected void setUp() throws EMFUserError {
        setDocument(readDataSourceParameterValue(FILE));
        setXPathExpression(readOperationParameterValue(XPATH));
    }

    private void setXPathExpression(final String xPathExp) {
        xPathExpression = xPathExp;
    }

    protected void setDocument(final String uri) {
        try {
            document = parse(uri);
        } catch (DocumentException e) {
            document = null;
        }
    }

    private Document getDocument() throws EMFUserError {
        if (document == null) {
            throw new EMFUserError(EMFErrorSeverity.ERROR,
                    "Impossible to create the document.");
        }
        return document;
    }

    private String getXPathExpression() throws EMFUserError {
        if (xPathExpression == null) {
            throw new EMFUserError(EMFErrorSeverity.ERROR,
                    "Impossible to create the xPath Expression.");
        }
        return xPathExpression;
    }

    @Override
    protected void tearDown() {
    }

    public String dataSourceTest(final Integer dataSourceId)
            throws EMFUserError {
        String toReturn = "";
        setDataSourceParameter(dataSourceId);
        String documentPath = readDataSourceParameterValue(FILE);
        setDocument(documentPath);
        toReturn = "Document URI: " + documentPath;
        return toReturn;
    }

    public String operationTest(final Integer operationId) throws EMFUserError {
        String toReturn = "";
        setOperationParameter(operationId);
        String xPathQuery = readOperationParameterValue(XPATH);
        setXPathExpression(xPathQuery);
        toReturn = "XPathExpression: " + getXPathExpression();
        return toReturn;
    }
}
