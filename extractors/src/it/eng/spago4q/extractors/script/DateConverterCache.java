/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.script;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.Script;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QDAO;

import java.util.HashMap;

import org.apache.log4j.Logger;

public class DateConverterCache {

	private static transient Logger logger = Logger
			.getLogger(DateConverterCache.class);

	private HashMap<Integer, String> dateFormat;

	public DateConverterCache() {
		dateFormat = new HashMap<Integer, String>();
	}

	public String getDateFormat(final Integer scriptId) {
		String toReturn = dateFormat.get(scriptId);
		if (toReturn == null) {
			toReturn = getScriptFromDB(scriptId);
			addScript(scriptId, toReturn);
		}
		return toReturn;
	}

	private void addScript(final Integer scriptId, final String format) {
		dateFormat.put(scriptId, format);
	}

	private String getScriptFromDB(final Integer scriptId) {
		String toReturn = null;

		IES4QDAO scriptDAO;
		try {
			scriptDAO = (IES4QDAO) DAOFactory.getDAO("EScriptDAO");
			Script script = (Script) scriptDAO.loadObjectById(scriptId);
			if (script != null && script.getScript() != null) {
				toReturn = script.getScript();
			}
		} catch (ClassCastException e) {
			logger.error("Problem douring creation of script with id"
					+ scriptId, e);
		} catch (EMFUserError e1) {
			logger.error("Problem douring load of Script Object with id"
					+ scriptId, e1);
		}
		return toReturn;
	}
}
