/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.script;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public final class ScriptCreator {
	
	private ScriptCreator() {}

	public static IMappingScript createScriptObject(final String script)
			throws ScriptException {
		IMappingScript toReturn = null;
		ScriptEngineManager scriptManager = new ScriptEngineManager();
		ScriptEngine groovyScriptEngine = scriptManager
				.getEngineByName("groovy");

		toReturn = (IMappingScript) groovyScriptEngine.eval(script);

		return toReturn;
	}

	// public static String execute2(String script, String value) throws
	// InstantiationException, IllegalAccessException {
	// String toReturn = "";
	//	
	// GroovyClassLoader gcl = new GroovyClassLoader();
	// Class clazz = gcl.parseClass(script);
	// Object aScript = clazz.newInstance();
	//
	// IMappingScript ifc = (IMappingScript) aScript;
	// toReturn = ifc.execute(value);
	//	
	// return toReturn;
	// }

}
