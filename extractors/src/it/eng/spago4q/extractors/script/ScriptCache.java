/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.script;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.Script;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QDAO;

import java.util.HashMap;

import javax.script.ScriptException;

import org.apache.log4j.Logger;

public class ScriptCache {
	
	private static transient Logger logger = Logger.getLogger(ScriptCache.class);

	private HashMap<Integer, IMappingScript> scripts;

	public ScriptCache() {
		scripts = new HashMap<Integer, IMappingScript>();
	}

	public IMappingScript getScript(final Integer scriptId) {
		IMappingScript toReturn = scripts.get(scriptId);
		if (toReturn == null) {
			toReturn = getScriptFromDB(scriptId);
			addScript(scriptId, toReturn);
		}
		return toReturn;
	}

	private void addScript(final Integer scriptId, final IMappingScript script) {
		scripts.put(scriptId, script);
	}

	private IMappingScript getScriptFromDB(final Integer scriptId) {
		IMappingScript toReturn = null;

		IES4QDAO scriptDAO;
		try {
			scriptDAO = (IES4QDAO) DAOFactory.getDAO("EScriptDAO");
			Script script = (Script) scriptDAO.loadObjectById(scriptId);
			if (script != null && script.getScript() != null) {
				toReturn = ScriptCreator
						.createScriptObject(script.getScript());
			}
		} catch (ClassCastException e) {
			logger.error("Problem douring creation of script with id" + scriptId , e);
		} catch (ScriptException e) {
			logger.error("Problem douring creation of script with id" + scriptId , e);
		} catch (EMFUserError e1) {
			logger.error("Problem douring load of Script Object with id" + scriptId , e1);
		}
		return toReturn;
	}
}
