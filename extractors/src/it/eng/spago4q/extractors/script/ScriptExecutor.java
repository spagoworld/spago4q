/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.script;

import org.apache.log4j.Logger;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.dao.EOperationFieldDAOHibImpl;
import it.eng.spago4q.extractors.MapMakerException;
import it.eng.spago4q.extractors.utilities.ExtractorConstants;

public class ScriptExecutor {

	private ScriptCache groovyCache;
	private DateConverterCache dateConverterCache;
	private static Logger logger = Logger
			.getLogger(EOperationFieldDAOHibImpl.class);

	public ScriptExecutor() {
		groovyCache = new ScriptCache();
		dateConverterCache = new DateConverterCache();
	}

	public String execute(final Integer scriptId, final String scriptType,
			final String value) throws MapMakerException{
		String toReturn = "";
		if (scriptType != null && scriptType.equals(ExtractorConstants.GROOVY)) {
			toReturn = executeGroovy(scriptId, value);
		}
		if (scriptType != null
				&& scriptType.equals(ExtractorConstants.DATE_CONVERTER)) {
			toReturn = executeDateConverter(scriptId, value);
		}
		return toReturn;
	}

	private ScriptCache getGroovyCache() {
		return groovyCache;
	}

	private DateConverterCache getDateConverterCache() {
		return dateConverterCache;
	}

	private String executeGroovy(final Integer scriptId, final String value) throws MapMakerException{
		String toReturn = "";
		IMappingScript script = getGroovyCache().getScript(scriptId);
		if (script != null) {
			try {
				toReturn = script.execute(value);
			} catch (Exception e) {
				toReturn = null;
				logger.warn("Error while execute groovy script with ID "
						+ scriptId + " to the value " + value + " ."
						+ e.getLocalizedMessage());
				throw new MapMakerException("Mapping script execution error");
			}
		} else {
			toReturn = value;
		}
		return toReturn;
	}

	private String executeDateConverter(final Integer scriptId,
			final String value) throws MapMakerException{
		String toReturn = null;
		String format = getDateConverterCache().getDateFormat(scriptId);
		try {
			toReturn = DateConverterScript.execute(value, format);
		} catch (EMFUserError e) {
			logger.warn("Error while execute dataConverter script with ID "
					+ scriptId + " to the value " + value + " ."
					+ e.getLocalizedMessage());
			throw new MapMakerException("Date conversion error");
		}
		return toReturn;
	}

}
