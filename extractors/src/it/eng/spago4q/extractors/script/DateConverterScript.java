/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.script;

import it.eng.spago.base.SourceBean;
import it.eng.spago.configuration.ConfigSingleton;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;

import java.text.DateFormat;
import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateConverterScript {

	private DateConverterScript() {
	}

	public static String execute(final String date, final String format)
			throws EMFUserError {
		String toReturn = null;
		if (date != null && !(date.trim().equals(""))) {

			DateFormat formatter = new SimpleDateFormat(format.trim());

			ConfigSingleton configSingleton = ConfigSingleton.getInstance();
			SourceBean dbFormatConfigSourceBean = (SourceBean) configSingleton
					.getFilteredSourceBeanAttribute(
							"SPAGO4Q.DB-DATE-FORMAT.FORMAT", "name", "format");
			String dBFormat = (String) dbFormatConfigSourceBean
					.getAttribute("formatconverter");
			try {
				Date inputDate = (Date) formatter.parse(date);
				formatter = new SimpleDateFormat(dBFormat);
				toReturn = formatter.format(inputDate);
			} catch (ParseException e) {
				throw new EMFUserError(EMFErrorSeverity.ERROR, e.getMessage());
			}
		}
		return toReturn;
	}
}
