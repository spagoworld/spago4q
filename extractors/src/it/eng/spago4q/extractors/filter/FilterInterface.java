/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.filter;

import it.eng.spago4q.extractors.bo.InterfaceItem;

// TODO: Auto-generated Javadoc
/**
 * The Interface FilterInterface.
 */
public interface FilterInterface {
	
	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage();
	
	/**
	 * Validate.
	 *
	 * @param interfaceItem the interface item
	 * @return true, if successful
	 */
	public boolean validate(InterfaceItem interfaceItem);

}
