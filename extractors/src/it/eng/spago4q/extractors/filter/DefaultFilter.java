/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.filter;

import it.eng.spago4q.extractors.bo.InterfaceFieldInstance;
import it.eng.spago4q.extractors.bo.InterfaceItem;

import java.util.HashMap;
import java.util.Set;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultFilter.
 */
public class DefaultFilter implements FilterInterface {

	/** The message. */
	private String message = null;

	/* (non-Javadoc)
	 * @see it.eng.spago4q.extractors.filter.FilterInterface#getMessage()
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Sets the message.
	 *
	 * @param m the new message
	 */
	private void setMessage(final String m){
	    message = m;
	}

	/* (non-Javadoc)
	 * @see it.eng.spago4q.extractors.filter.FilterInterface#validate(it.eng.spago4q.extractors.bo.InterfaceItem)
	 */
	public boolean validate(final InterfaceItem interfaceItem) {
		boolean toReturn = true;
		HashMap<String, InterfaceFieldInstance> data = interfaceItem
				.getHashMap();
		Set<String> keys = data.keySet();
		for (String columnName : keys) {
			InterfaceFieldInstance instance = data.get(columnName);
			if (instance != null && instance.isKey()
					&& instance.getValue() == null) {
				toReturn = false;
				setMessage("Key column " + columnName + " is null");
				break;
			}
		}
		return toReturn;
	}

}
