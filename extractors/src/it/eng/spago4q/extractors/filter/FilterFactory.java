/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.filter;

import it.eng.spago4q.bo.EOperation;

import org.apache.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating Filter objects.
 */
public final class FilterFactory {
    
    /**
     * Instantiates a new filter factory.
     */
    private FilterFactory() {
    }
    
    /** The logger. */
    private static Logger logger = Logger.getLogger(FilterFactory.class);
    
    /**
     * Creates the.
     *
     * @param operation the operation
     * @return the filter interface
     */
    public static FilterInterface create(final EOperation operation) {
        FilterInterface filter = null;
        if (operation != null && operation.getInterfaceType() != null
                && operation.getInterfaceType().getFilterClass() != null) {
            String filterClassName = operation.getInterfaceType()
                    .getFilterClass();
            try {
                if (filterClassName != null
                        && !filterClassName.trim().isEmpty()) {
                    filter = (FilterInterface) Class.forName(filterClassName)
                            .newInstance();
                } else {
                    filter = new DefaultFilter();
                }
                
            } catch (InstantiationException e) {
                logger.error(
                        "Error while Instantiate class " + filterClassName, e);
                // throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
            } catch (IllegalAccessException e) {
                logger.error(
                        "Error while Instantiate class " + filterClassName, e);
                // throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
            } catch (ClassNotFoundException e) {
                logger.error(
                        "Error while Instantiate class " + filterClassName, e);
                // throw new EMFUserError(EMFErrorSeverity.ERROR, 101);
            }
        }
        return filter;
    }
}
