/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.parameterfiller;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.DataSourceParameter;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.bo.EOperationParameter;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.extractors.bo.ExtractorParameter;

import java.util.ArrayList;
import java.util.List;

public class DefaultExtractionParameterFiller implements
        ExtractionParameterFiller {
    private static final String EOPERATIONPARAMETER_DAO = "EOperationParameterDAO";
    private static final String EDATASOURCEPARAMETER_DAO = "EDataSourceParameterDAO";
    private EOperation operation;

    public void setOperation(final EOperation operation) {
        this.operation = operation;
    }

    public List<ExtractorParameter> getDataSourceParameters(
            final Integer dataSourceId) throws EMFUserError {
        List<ExtractorParameter> dataSourceParameters = null;
        if (dataSourceId != null) {
            IES4QFKDAO dataSourceParameterDAO = (IES4QFKDAO) DAOFactory
                    .getDAO(EDATASOURCEPARAMETER_DAO);
            List ListEDataSourceParameter = dataSourceParameterDAO
                    .loadObjectList(dataSourceId, null, null);
            dataSourceParameters = new ArrayList<ExtractorParameter>();
            for (DataSourceParameter eDataSourceParameter : (List<DataSourceParameter>) ListEDataSourceParameter) {
                ExtractorParameter dsParameter = new ExtractorParameter(
                        eDataSourceParameter.getName(), eDataSourceParameter
                                .getValue());
                dataSourceParameters.add(dsParameter);
            }
        }
        return dataSourceParameters;
    }

    public List<ExtractorParameter> getDataSourceParameters()
            throws EMFUserError {
        List<ExtractorParameter> dataSourceParameters = null;
        if (operation != null && operation.getDataSource() != null) {
            dataSourceParameters = getDataSourceParameters(operation
                    .getDataSource().getId());
        }
        return dataSourceParameters;
    }

    public List<ExtractorParameter> getOperationParameters()
            throws EMFUserError {
        List<ExtractorParameter> operationParameters = null;
        if (operation != null) {
            operationParameters = getOperationParameters(operation.getId());
        }
        return operationParameters;
    }

    public List<ExtractorParameter> getOperationParameters(
            final Integer operationId) throws EMFUserError {
        List<ExtractorParameter> operationParameters = null;
        if (operationId != null) {
            IES4QFKDAO operationParameterDAO = (IES4QFKDAO) DAOFactory
                    .getDAO(EOPERATIONPARAMETER_DAO);
            List ListEOperationParameter = operationParameterDAO
                    .loadObjectList(operationId, null, null);
            operationParameters = new ArrayList<ExtractorParameter>();
            for (EOperationParameter eOperationParameter : (List<EOperationParameter>) ListEOperationParameter) {
                ExtractorParameter oParameter = new ExtractorParameter(
                        eOperationParameter.getName(), eOperationParameter
                                .getValue());
                operationParameters.add(oParameter);
            }
        }
        return operationParameters;

    }
}
