/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.parameterfiller;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.extractors.bo.ExtractorParameter;

import java.util.List;

public interface ExtractionParameterFiller {

    public void setOperation(EOperation operation);
    
    public List<ExtractorParameter> getOperationParameters() throws EMFUserError;

    public List<ExtractorParameter> getDataSourceParameters() throws EMFUserError;
}
