/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import java.util.Date;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.ELog;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;

public class LogWriter {

	private ELog log;
	private IES4QFKDAO eLogDao;

	public LogWriter() throws EMFUserError {
		log = new ELog();
		eLogDao = (IES4QFKDAO) DAOFactory.getDAO("ELogDAO");
	}

	protected void writeExtractedItems(final Integer processId,
			final Integer operationId, final Integer extractedItem,
			final Date operationDate, final Date executionDate,
			final String archivePath) throws EMFUserError {
		log.setProcessId(processId);
		log.setOperationId(operationId);
		log.setExtracted(extractedItem);
		log.setOperationDate(operationDate);
		log.setExecutionDate(executionDate);
		log.setArchivePath(archivePath);
		log.setCompleted(false);

		Integer logId = eLogDao.insertObject(log);

		log.setIdLog(logId);
	}

	protected Integer getLogId() {
		Integer toReturn = null;
		if (log != null) {
			toReturn = log.getIdLog();
		}
		return toReturn;
	}

	protected void writeMappingError(final int rejected,
	        final String rejectedPath)
			throws EMFUserError {
		log.setRejected(rejected);
		log.setRejectedArchivePath(rejectedPath);
		eLogDao.modifyObject(log);
	}

	protected void wirteInsertedItems(final Integer insertedItem)
			throws EMFUserError {
		log.setInserted(insertedItem);
		log.setCompleted(true);
		eLogDao.modifyObject(log);
	}
}
