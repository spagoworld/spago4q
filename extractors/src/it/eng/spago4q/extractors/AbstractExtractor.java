/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.DataSourceParameter;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.bo.EOperationParameter;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.dao.ILogDAO;
import it.eng.spago4q.extractors.bo.ExtractorParameter;
import it.eng.spago4q.extractors.bo.GenericItemInterface;
import it.eng.spago4q.extractors.parameterfiller.DefaultExtractionParameterFiller;
import it.eng.spago4q.extractors.parameterfiller.ExtractionParameterFiller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

public abstract class AbstractExtractor implements ExtractorInterface {
    
    private EOperation eOperation = null;
    private List<ExtractorParameter> operationParameter = null;
    private List<ExtractorParameter> dataSourceParameter = null;
    private static final String EOPERATIONPARAMETER_DAO = "EOperationParameterDAO";
    private static final String EDATASOURCEPARAMETER_DAO = "EDataSourceParameterDAO";
    private static final String ELOG_DAO = "ELogDAO";
    private static transient Logger logger = Logger
            .getLogger(AbstractExtractor.class);
    
    public List<GenericItemInterface> execute() throws EMFUserError {
        List<GenericItemInterface> toReturn = null;
        setUp();
        try {
            toReturn = extract();
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            throw new EMFUserError(EMFErrorSeverity.ERROR, e
                    .getLocalizedMessage());
        } finally {
            tearDown();
        }
        return toReturn;
    }
    
    public void init(final List<ExtractorParameter> iDataSourceParameter,
            final List<ExtractorParameter> iOperationParameter) {
        if (iDataSourceParameter != null) {
            dataSourceParameter = new ArrayList<ExtractorParameter>();
            dataSourceParameter.addAll(iDataSourceParameter);
        }
        
        if (iOperationParameter != null) {
            operationParameter = new ArrayList<ExtractorParameter>();
            operationParameter.addAll(iOperationParameter);
        }
    }
    
    @Deprecated
    public void init(final EOperation operation) throws EMFUserError {
        this.eOperation = operation;
        ExtractionParameterFiller extractionParameterFiller = new DefaultExtractionParameterFiller();
        extractionParameterFiller.setOperation(operation);
        setOperationParameter(extractionParameterFiller
                .getOperationParameters());
        setDataSourceParameter(extractionParameterFiller
                .getDataSourceParameters());
        
    }
    
    protected EOperation getEOperation() {
        return eOperation;
    }
    
    public List<ExtractorParameter> getOperationParameter() {
        return operationParameter;
    }
    
    protected void setOperationParameter(
            final List<ExtractorParameter> iOperationParameter) {
        operationParameter = iOperationParameter;
    }
    
    protected void setDataSourceParameter(
            final List<ExtractorParameter> iDataSourceParameter) {
        dataSourceParameter = iDataSourceParameter;
    }
    
    public void setOperationParameter(final Integer operationId)
            throws EMFUserError {
        IES4QFKDAO operationParameterDAO = (IES4QFKDAO) DAOFactory
                .getDAO(EOPERATIONPARAMETER_DAO);
        List ListEOperationParameter = operationParameterDAO.loadObjectList(
                operationId, null, null);
        operationParameter = new ArrayList<ExtractorParameter>();
        for (EOperationParameter eOperationParameter : (List<EOperationParameter>) ListEOperationParameter) {
            ExtractorParameter oParameter = new ExtractorParameter(
                    eOperationParameter.getName(), eOperationParameter
                            .getValue());
            operationParameter.add(oParameter);
        }
    }
    
    public List<ExtractorParameter> getDataSourceParameter() {
        return dataSourceParameter;
    }
    
    public void setDataSourceParameter(final Integer dataSourceId)
            throws EMFUserError {
        IES4QFKDAO dataSourceParameterDAO = (IES4QFKDAO) DAOFactory
                .getDAO(EDATASOURCEPARAMETER_DAO);
        List ListEDataSourceParameter = dataSourceParameterDAO.loadObjectList(
                dataSourceId, null, null);
        dataSourceParameter = new ArrayList<ExtractorParameter>();
        for (DataSourceParameter eDataSourceParameter : (List<DataSourceParameter>) ListEDataSourceParameter) {
            ExtractorParameter oParameter = new ExtractorParameter(
                    eDataSourceParameter.getName(), eDataSourceParameter
                            .getValue());
            dataSourceParameter.add(oParameter);
        }
    }
    
    protected String readDataSourceParameterValue(final String key) {
        String toReturn = null;
        Iterator<ExtractorParameter> it = (Iterator<ExtractorParameter>) getDataSourceParameter()
                .iterator();
        while (it.hasNext()) {
            ExtractorParameter currentDataSourceParameter = (ExtractorParameter) it
                    .next();
            if (currentDataSourceParameter.getKey().equals(key)) {
                toReturn = currentDataSourceParameter.getValue();
            }
        }
        return toReturn;
    }
    
    protected List<String> readDataSourceParameterValueList(final String key) {
        List<String> toReturn = new ArrayList<String>();
        Iterator<ExtractorParameter> it = (Iterator<ExtractorParameter>) getDataSourceParameter()
                .iterator();
        while (it.hasNext()) {
            ExtractorParameter currentDataSourceParameter = (ExtractorParameter) it
                    .next();
            if (currentDataSourceParameter.getKey().equals(key)) {
                toReturn.add(currentDataSourceParameter.getValue());
            }
        }
        return toReturn;
    }
    
    protected String readOperationParameterValue(final String key) {
        String toReturn = null;
        Iterator<ExtractorParameter> it = (Iterator<ExtractorParameter>) getOperationParameter()
                .iterator();
        while (it.hasNext()) {
            ExtractorParameter currentOperationParameter = (ExtractorParameter) it
                    .next();
            if (currentOperationParameter.getKey().equals(key)) {
                toReturn = currentOperationParameter.getValue();
            }
        }
        return toReturn;
    }
    
    protected List<String> readOperationParameterValueList(final String key) {
        List<String> toReturn = new ArrayList<String>();
        Iterator<ExtractorParameter> it = (Iterator<ExtractorParameter>) getOperationParameter()
                .iterator();
        while (it.hasNext()) {
            ExtractorParameter currentOperationParameter = (ExtractorParameter) it
                    .next();
            if (currentOperationParameter.getKey().equals(key)) {
                toReturn.add(currentOperationParameter.getValue());
            }
        }
        return toReturn;
    }
    
    protected abstract void setUp() throws EMFUserError;
    
    protected abstract void tearDown() throws EMFUserError;
    
    protected abstract List<GenericItemInterface> extract() throws EMFUserError;
    
    public String dataSourceTest(final Integer dataSourceId)
            throws EMFUserError {
        return "No data source test found!";
    }
    
    public String operationTest(final Integer operationId) throws EMFUserError {
        return "No operation test found!";
    }
    
    public Date getLastExtractionDate() throws EMFUserError {
        Date toReturn = null;
        if (eOperation != null) {
            ILogDAO logDAO = (ILogDAO) DAOFactory.getDAO(ELOG_DAO);
            toReturn = logDAO.getLastOperationExecutionDate(eOperation.getId());
        }
        return toReturn;
    }
}
