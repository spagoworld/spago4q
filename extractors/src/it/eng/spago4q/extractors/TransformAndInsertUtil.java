/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import it.eng.spago.error.EMFUserError;
import it.eng.spago.security.IEngUserProfile;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.extractors.bo.GenericItemInterface;
import it.eng.spago4q.extractors.bo.InterfaceItem;
import it.eng.spago4q.extractors.script.ScriptExecutor;
import it.eng.spago4q.extractors.utilities.OperationUtil;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class TransformAndInsertUtil {
    
    private String archiveDir;

    public void init(final String archDir) {
        archiveDir = archDir;
    }

    protected void transformAndInsertData(final Integer processId,
            final EOperation operation, final Date operationDate,
            final Date executionDate, final ScriptExecutor scriptExecutor,
            final IEngUserProfile userProfile, String archivePath,
            final List<GenericItemInterface> genericItemList)
            throws EMFUserError {
        DomainValueCacheCreator domainValueCacheCreator = new DomainValueCacheCreator(
                operation.getInterfaceType().getId());
        if (operation.getArchive()) {
            archivePath = OperationUtil.createBeckUpArchive(genericItemList,
                    operationDate, operation, archiveDir);
        }

        LogWriter logWriter = new LogWriter();

        logWriter.writeExtractedItems(processId, operation.getId(),
                genericItemList.size(), operationDate, executionDate,
                archivePath);

        MapMaker mapMaker = new MapMaker();
        List<InterfaceItem> interfaceItem = mapMaker.mapping(operation,
                genericItemList, scriptExecutor, domainValueCacheCreator);

        String rejectedPath = null;
        if (mapMaker.getMappingErrorManager().getNumberOfRejected() > 0
                && operation.getRejected()) {
            rejectedPath = mapMaker
                    .getMappingErrorManager()
                    .createRejectedArchive(executionDate, operation, archiveDir);
        }
        logWriter.writeMappingError(mapMaker.getMappingErrorManager()
                .getNumberOfRejected(), rejectedPath);

        FactTableManager factTableManager = new FactTableManager(operation
                .getInterfaceType().getId());
        int numberOfInsert = 0;

        for (Iterator iterator2 = interfaceItem.iterator(); iterator2.hasNext();) {
            InterfaceItem interfaceItem2 = (InterfaceItem) iterator2.next();
            if (factTableManager.loadData(operationDate, operation
                    .getInterfaceType().getId(), interfaceItem2, logWriter
                    .getLogId(), userProfile)) {
                numberOfInsert++;
            }
        }
        logWriter.wirteInsertedItems(numberOfInsert);
    }

}
