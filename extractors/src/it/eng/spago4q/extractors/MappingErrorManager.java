/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.extractors.bo.GenericItemInterface;
import it.eng.spago4q.extractors.utilities.OperationUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MappingErrorManager {

    private List<GenericItemInterface> genericItemList;
    private List<String> errorMessage;

    public MappingErrorManager() {
        genericItemList = new ArrayList<GenericItemInterface>();
        errorMessage = new ArrayList<String>();
    }

    public void addError(final String operationFieldName,
            final String interfaceFieldName,
            final String message,
            final GenericItemInterface genericItem) {
        genericItemList.add(genericItem);
        if (operationFieldName != null && interfaceFieldName != null) {
            errorMessage.add("Generic Item Field: [" + operationFieldName
                    + "] - Interface Item Field :[" + interfaceFieldName
                    + "] - Message: [" + message + "]");
        } else {
            if (operationFieldName != null) {
                errorMessage.add("Generic Item Field: [" + operationFieldName
                        + "] - Message: [" + message + "]");
            } else {
                errorMessage.add("Message: [" + message + "]");
            }
        }
    }

    public int getNumberOfRejected() {
        int toReturn = 0;
        if (errorMessage != null) {
            toReturn = errorMessage.size();
        }
        return toReturn;
    }

    public String createRejectedArchive(final Date executionDate,
            final EOperation operation, final String extractorDir) {
        return OperationUtil.createRejectedArchive(genericItemList,
                errorMessage, executionDate, operation, extractorDir);
    }

    public void print() {
        for (int i = 0; i < genericItemList.size(); i++) {
            System.out.println(genericItemList.get(i));
            System.out.println(errorMessage.get(i));
        }
    }
}
