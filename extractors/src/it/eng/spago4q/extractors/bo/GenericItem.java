/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.bo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class GenericItem implements GenericItemInterface {

	private HashMap<String,String> data = new HashMap<String,String>();
	
	public String getValue(final String field) {
		return data.get(field.toUpperCase());
	}

	public void setValue(final String name, final String val){
		data.put(name.toUpperCase(), val);
	}
	
	public Set<String> getKeys(){
		return data.keySet();
	}
	
	@Override
	public String toString(){
		Iterator iter=data.keySet().iterator();
		String ris="";
		while(iter.hasNext()){
			String k=(String)iter.next();
			ris=ris+k+"="+data.get(k)+" | ";
		}
		return ris;
	}

}
