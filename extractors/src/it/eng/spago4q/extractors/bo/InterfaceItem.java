/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.bo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class InterfaceItem {
	
	private HashMap<String,InterfaceFieldInstance> data = new HashMap<String,InterfaceFieldInstance>();
	
	public InterfaceFieldInstance getValue(final String field) {
		return data.get(field);
	}

	public void setValue(final String name, final InterfaceFieldInstance val){
		data.put(name, val);
	}
	
	public HashMap<String,InterfaceFieldInstance> getHashMap(){
		return data;
	}
	
	public int getNumberOfSensibleValue(){
		int toReturn = 0;
		Set keys = data.keySet();
		for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (data.get(key).isSensible()){
				toReturn ++;
			}
		}
		return toReturn;
	}

}
