/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.bo;

import it.eng.spago.base.SourceBean;
import it.eng.spago.base.SourceBeanException;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;

public final class GenericItemUtil {

	private static final String GENERIC_ITEMS = "GenericItems";
	private static final String GENERIC_ITEM = "GenericItem";
	private static final String ERROR = "error";
	private static final String XML_FILE_NAME = "genericItem.xml";
	
	private static final Logger LOGGER = Logger.getLogger(GenericItemUtil.class);

	private GenericItemUtil() {
	};

	public static SourceBean genericItemListToSourceBean(
			final List<GenericItemInterface> genericItemList) {
		SourceBean genericItemsSB = null;
		try {
			genericItemsSB = new SourceBean(GENERIC_ITEMS);
			for (GenericItemInterface genericItem : genericItemList) {
				Set<String> keys = genericItem.getKeys();
				SourceBean genericItemSB = new SourceBean(GENERIC_ITEM);
				for (String key : keys) {
					SourceBean keySB = new SourceBean(key);
					keySB.setCharacters(genericItem.getValue(key));
					genericItemSB.setAttribute(keySB);
				}
				genericItemsSB.setAttribute(genericItemSB);
			}
		} catch (SourceBeanException e) {
			LOGGER.debug(e.getLocalizedMessage());
		}
		return genericItemsSB;
	}
	
	public static SourceBean genericItemListWithErrorsToSourceBean(
			final List<GenericItemInterface> genericItemList,
			final List<String> messageErrors){
		SourceBean genericItemsSB = null;
		try {
			genericItemsSB = new SourceBean(GENERIC_ITEMS);
			for (int i = 0; i < genericItemList.size(); i ++) {
				Set<String> keys = genericItemList.get(i).getKeys();
				SourceBean genericItemSB = new SourceBean(GENERIC_ITEM);
				genericItemSB.setAttribute("error", messageErrors.get(i));
				for (String key : keys) {
					SourceBean keySB = new SourceBean(key);
					keySB.setCharacters(genericItemList.get(i).getValue(key));
					genericItemSB.setAttribute(keySB);
				}
				genericItemsSB.setAttribute(genericItemSB);
			}
		} catch (SourceBeanException e) {
			LOGGER.debug(e.getLocalizedMessage());
		}
		return genericItemsSB;
	}

	public static String sourceBeanToZipFile(final String archiveDir,
			final String zipFileName, final SourceBean sourceBean) {
		String toReturn = null;
		try {
			File tempDirectory = new File(archiveDir);
			tempDirectory.mkdir();
			File archive = new File(tempDirectory, zipFileName);
			toReturn = archive.getAbsolutePath();
			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
					new FileOutputStream(archive)));
			out.putNextEntry(new ZipEntry(XML_FILE_NAME));
			byte[] sourceBeanByte = sourceBean.toString().getBytes();
			out.write(sourceBeanByte);
			out.flush();
			out.close();
		} catch (IOException e) {
			LOGGER.debug(e.getLocalizedMessage());
		}
		return toReturn;
	}

}
