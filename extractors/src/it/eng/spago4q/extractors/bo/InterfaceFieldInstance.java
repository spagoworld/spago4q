/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.bo;

public class InterfaceFieldInstance {
	
	private Object value;
	private Boolean sensible;
	private Boolean key;
	private String type;
	
	public Object getValue() {
		return value;
	}
	private void setValue(final Object value) {
		this.value = value;
	}
	public Boolean isSensible() {
		return sensible;
	}
	private void setSensible(final Boolean sensible) {
		this.sensible = sensible;
	}
	public Boolean isKey() {
		return key;
	}
	private void setKey(final Boolean key) {
		this.key = key;
	}
	
	public InterfaceFieldInstance(final Object value, final Boolean sensible,
			final Boolean keyValue, final String type) {
		setValue(value);
		setSensible(sensible);
		setKey(keyValue);
		setType(type);
	}
	
	public String getType() {
		return type;
	}
	public void setType(final String type) {
		this.type = type;
	}
	
	

}
