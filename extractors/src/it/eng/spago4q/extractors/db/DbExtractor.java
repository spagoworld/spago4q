/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.db;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.AbstractExtractor;
import it.eng.spago4q.extractors.bo.GenericItem;
import it.eng.spago4q.extractors.bo.GenericItemInterface;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class DbExtractor extends AbstractExtractor {
    
    private static Logger logger = Logger
    .getLogger(DbExtractor.class);

	public static final String URL = "URL";
	public static final String DRIVER = "DRIVER";
	public static final String QUERY = "QUERY";
	public static final String USER = "USER";
	public static final String PASSWORD = "PASSWORD";

	private String driver;
	private String url;
	private String user;
	private String password;
	private String query;

	private Connection openConnection() throws EMFUserError {
	    Connection conn = null;
		try {
			java.lang.Class.forName(driver);
			conn = java.sql.DriverManager.getConnection(getUrl(),
					getUser(), getPassword());
		} catch (ClassNotFoundException e) {
            logger.error(e.getLocalizedMessage());
            throw new EMFUserError("ERROR", e.getLocalizedMessage());
		} catch (SQLException e) {
		    logger.error(e.getLocalizedMessage());
            throw new EMFUserError("ERROR", e.getLocalizedMessage());
		}
		return conn;
	}

	@Override
	protected List<GenericItemInterface> extract() throws EMFUserError {
		List<GenericItemInterface> toReturn = null;
		Connection conn = null;
		ResultSet rs = null;
		ResultSetMetaData metadata = null;
		java.sql.PreparedStatement statement = null;
		int columnCount = 0;
		try {
			conn = openConnection();
			statement = conn.prepareStatement(query);
			rs = statement.executeQuery();
			metadata = rs.getMetaData();
			columnCount = metadata.getColumnCount();
			toReturn = new ArrayList<GenericItemInterface>();
			while (rs.next()) {
				GenericItem genericItem = new GenericItem();
				for (int i = 1; i <= columnCount; i++) {
					String columName = metadata.getColumnName(i);
					String value = null;
					try{
						value = rs.getString(i);
					} catch (SQLException e) {
					    logger.error(e.getLocalizedMessage());
					    throw new EMFUserError("ERROR", e.getLocalizedMessage());
					}
					genericItem.setValue(columName, value);
				}
				toReturn.add(genericItem);
			}
			rs.close();
			statement.close();
		} catch (SQLException e) {
            logger.error(e.getLocalizedMessage());
            throw new EMFUserError("ERROR", e.getLocalizedMessage());
		}
		return toReturn;
	}

	@Override
	protected void setUp() {
		setDriver(readDataSourceParameterValue(DRIVER));
		setUrl(readDataSourceParameterValue(URL));
		setUser(readDataSourceParameterValue(USER));
		setPassword(readDataSourceParameterValue(PASSWORD));
		setQuery(readOperationParameterValue(QUERY));
	}

	@Override
	protected void tearDown() {
	}
	
	@Override
	public String dataSourceTest(final Integer dataSourceId) throws EMFUserError {
		String toReturn = "";
		setDataSourceParameter(dataSourceId);
		String driverPar = readDataSourceParameterValue(DRIVER);
		if (driverPar == null || driverPar.equals("")){
			throw new EMFUserError("ERROR", "Missing DRIVER parameter");
		}
		String urlPar = readDataSourceParameterValue(URL);
		if (urlPar == null || urlPar.equals("")){
			throw new EMFUserError("ERROR", "Missing URL parameter");
		}
		String userPar = readDataSourceParameterValue(USER);
		if (userPar == null || userPar.equals("")){
			throw new EMFUserError("ERROR", "Missing USER parameter");
		}
		String passwordPar = readDataSourceParameterValue(PASSWORD);
		if (passwordPar == null || passwordPar.equals("")){
			throw new EMFUserError("ERROR", "Missing PASSWORD parameter");
		}
		toReturn = "<b>Driver:</b> " + driverPar + "<br>"
		+ "<b>URL:</b> " + urlPar + "<br>"
		+ "<b>USER:</b> " + userPar + "<br>"
		+ "<b>PASSWORD:</b> " + passwordPar;	
		return toReturn;
	}

	@Override
	public String operationTest(final Integer operationId) throws EMFUserError {
		String toReturn = "";
		setOperationParameter(operationId);
		String queryPar = readOperationParameterValue(QUERY);
		if (queryPar == null || queryPar.equals("")){
			throw new EMFUserError("ERROR", "Missing QUERY parameter");
		}
		toReturn = "Query: " + queryPar.trim();
		return toReturn;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(final String driver) {
		this.driver = driver;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(final String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(final String query) {
		this.query = query;
	}

}
