/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import it.eng.spago.error.EMFUserError;
import it.eng.spago.security.IEngUserProfile;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.bo.EProcess;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.extractors.parameterfiller.DefaultExtractionParameterFiller;
import it.eng.spago4q.extractors.parameterfiller.ExtractionParameterFiller;
import it.eng.spago4q.extractors.script.ScriptExecutor;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

public class DefaultCoordinator extends AbstractCoordinator {

    private List operationsList = null;
    private EProcess eProcess;
    private static final String EOPERATION_DAO = "EOperationDAO";

    private static Logger logger = Logger.getLogger(DefaultCoordinator.class);

    public void init(final EProcess eProcessPar, final String archDir)
            throws EMFUserError {
        init(archDir);
        this.eProcess = eProcessPar;
        IES4QFKDAO eOperationDAO = (IES4QFKDAO) DAOFactory
                .getDAO(EOPERATION_DAO);
        this.operationsList = eOperationDAO.loadObjectList(eProcessPar.getId(),
                null, null);
    }

    public List getOperationsList() {
        return operationsList;
    }

    public void run(final IEngUserProfile userProfile) throws EMFUserError {
        ScriptExecutor scriptExecutor = new ScriptExecutor();
        List currentOperationsList = getOperationsList();
        // get the extraction date
        Date executionDate = Calendar.getInstance().getTime();
        for (Iterator iterator = currentOperationsList.iterator(); iterator
                .hasNext();) {
            EOperation operation = (EOperation) iterator.next();
            executeOperation(eProcess.getId(), operation, executionDate,
                    scriptExecutor, userProfile);
        }
    }

    @Override
    protected ExtractionParameterFiller getExtractionParameterFiller() {
        return new DefaultExtractionParameterFiller();
    }

}
