/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.tablehandlers.mysql;

import it.eng.spago4q.extractors.tablehandlers.AbstractTableHandler;
import it.eng.spago4q.extractors.tablehandlers.SqlCodeGeneratorInterface;

public class MySqlTableHandler extends AbstractTableHandler {
	
	private SqlCodeGeneratorInterface codeGenerator;
	
	public MySqlTableHandler() {
		codeGenerator = new MySqlCodeGenerator();
	}

	@Override
	public SqlCodeGeneratorInterface getSqlCodeGenerator() {
		return codeGenerator;
	}


}
