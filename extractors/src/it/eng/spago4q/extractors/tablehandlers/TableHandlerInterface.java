/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.tablehandlers;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import it.eng.spago.base.SourceBean;
import it.eng.spago.dbaccess.sql.DataConnection;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.bo.InterfaceItem;

public interface TableHandlerInterface {

	public void dropFactTable(DataConnection connection,
			String interfaceTableName) throws EMFUserError;

	public void createFactTable(DataConnection dataConnection,
			String tableName, List interfaceFieldList) throws EMFUserError;

	public void updateFactTable(Date executionDate, DataConnection dataConnection,
			String tableName, InterfaceItem interfaceItem) throws EMFUserError;

	public void insertIntoFactTable(Date executionDate, DataConnection dataConnection,
			String tableName, InterfaceItem interfaceItem, Integer logId) throws EMFUserError;

	public SourceBean selectFromFactTable(DataConnection dataConnection,
			String tableName, InterfaceItem interfaceItem, Date operationDate) throws EMFUserError;

	public boolean hasThisTable(Connection connection, String tableName) throws EMFUserError;

	public String transformTableName(String tableName);

}
