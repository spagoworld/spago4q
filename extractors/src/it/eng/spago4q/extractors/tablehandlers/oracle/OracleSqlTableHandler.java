/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.tablehandlers.oracle;

import java.util.Date;
import java.util.List;

import it.eng.spago.base.SourceBean;
import it.eng.spago.dbaccess.Utils;
import it.eng.spago.dbaccess.sql.DataConnection;
import it.eng.spago.dbaccess.sql.SQLCommand;
import it.eng.spago.dbaccess.sql.result.DataResult;
import it.eng.spago.dbaccess.sql.result.ScrollableDataResult;
import it.eng.spago.error.EMFInternalError;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.bo.InterfaceItem;
import it.eng.spago4q.extractors.tablehandlers.AbstractTableHandler;
import it.eng.spago4q.extractors.tablehandlers.SqlCodeGeneratorInterface;

public class OracleSqlTableHandler extends AbstractTableHandler {
    
    private SqlCodeGeneratorInterface codeGenerator;
    private String alterSessionQuery;

    public OracleSqlTableHandler() {
        codeGenerator = new OracleSqlCodeGenerator();
        alterSessionQuery = OracleSqlCodeGenerator.getAlterSessionQuery();
    }

    @Override
    public void createFactTable(final DataConnection dataConnection,
            final String tableName, final List interfaceFieldList)
            throws EMFUserError {
        // sequence
        String sequenceQuery = OracleSqlCodeGenerator
                .generateCreateSequenceQuery(tableName);
        // table
        String createQuery = getSqlCodeGenerator().generateCreateTableQuery(
                tableName, interfaceFieldList);
        // trigger
        String triggerQuery = OracleSqlCodeGenerator
                .generateCreateTriggerQuery(tableName);
        String[] queryArray = { sequenceQuery, createQuery, triggerQuery };
        
        queryListExecutor(dataConnection, queryArray);
    }

    @Override
    public void dropFactTable(final DataConnection connection,
            final String tableName) throws EMFUserError {
        // sequence
        String dropSequenceQuery = OracleSqlCodeGenerator
                .generateDropSequenceQuery(tableName);
        // table
        String dropQuery = getSqlCodeGenerator().generateDropTableQuery(
                tableName);
        // trigger
        String dropTrigger = OracleSqlCodeGenerator
                .generateDropTriggerQuery(tableName);
        String[] queryArray = { dropTrigger, dropSequenceQuery, dropQuery };
        
        queryListExecutor(connection, queryArray);
    }

    @Override
    public SqlCodeGeneratorInterface getSqlCodeGenerator() {
        return codeGenerator;
    }

    @Override
    protected SourceBean selectQueryExecutor(
            final DataConnection dataConnection, final String query)
            throws EMFUserError {
        SourceBean result = null;
        SQLCommand sqlCommand = null;
        DataResult dataResult = null;
        try {
            sqlCommand = dataConnection.createInsertCommand(alterSessionQuery);
            sqlCommand.execute();
            sqlCommand = dataConnection.createSelectCommand(query);
            dataResult = sqlCommand.execute();
            ScrollableDataResult scrollableDataResult = (ScrollableDataResult) dataResult
                    .getDataObject();
            result = scrollableDataResult.getSourceBean();
        } catch (EMFInternalError e) {
            throw new EMFUserError(e.getCategory(), e.getDescription());
        } finally {
            Utils.releaseResources(dataConnection, sqlCommand, dataResult);
        }
        return result;
    }

    @Override
    public void updateFactTable(Date executionDate,
            DataConnection dataConnection, String tableName,
            InterfaceItem interfaceItem) throws EMFUserError {
        String updateQuery = getSqlCodeGenerator().generateUpdateQuery(
                executionDate, tableName, interfaceItem);
        String[] queryArray = { alterSessionQuery, updateQuery };

        queryListExecutor(dataConnection, queryArray);
    }

    @Override
    public void insertIntoFactTable(Date executionDate,
            DataConnection dataConnection, String tableName,
            InterfaceItem interfaceItem, Integer logId) throws EMFUserError {
        String insertQuery = getSqlCodeGenerator().generateInsertQuery(
                executionDate, tableName, interfaceItem, logId);
        String[] queryArray = { alterSessionQuery, insertQuery };
        
        queryListExecutor(dataConnection, queryArray);
    }

}
