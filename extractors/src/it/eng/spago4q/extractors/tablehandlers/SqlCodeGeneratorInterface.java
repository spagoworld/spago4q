/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.tablehandlers;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.bo.InterfaceItem;

import java.util.Date;
import java.util.List;

public interface SqlCodeGeneratorInterface {
	
	public String generateDropTableQuery(String tableName);
	
	public String generateCreateTableQuery(String tableName,
			List interfaceFieldList) throws EMFUserError;
	
	public String generateUpdateQuery(Date executionDate, String tableName,
			InterfaceItem interfaceItem);
	
	public String generateInsertQuery(Date executionDate, String tableName,
			InterfaceItem interfaceItem, Integer logId);
	
	public String generateSelectQuery(String tableName,
			InterfaceItem interfaceItem, Date operationDate);
	
}
