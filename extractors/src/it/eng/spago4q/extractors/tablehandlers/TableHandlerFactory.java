/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.tablehandlers;

import it.eng.spago4q.extractors.tablehandlers.mysql.MySqlTableHandler;
import it.eng.spago4q.extractors.tablehandlers.oracle.OracleSqlTableHandler;
import it.eng.spago4q.extractors.tablehandlers.postgresql.PostgreSqlTableHandler;

public final class TableHandlerFactory {
	
    private static final String MYSQLINNODBDIALECT = "org.hibernate.dialect.MySQLInnoDBDialect";
    private static final String POSTGREDBDIALECT = "org.hibernate.dialect.PostgreSQLDialect";
    private static final String ORACLEDBDIALECT = "org.hibernate.dialect.Oracle9Dialect";
    
    
	private TableHandlerFactory(){}

	public static TableHandlerInterface getTableHandler(final String tableHandlerEntry){
		TableHandlerInterface toReturn = null;
		
		if(tableHandlerEntry.equals(MYSQLINNODBDIALECT)) {
			toReturn = new MySqlTableHandler();
		}
		
		if(tableHandlerEntry.equals(POSTGREDBDIALECT)) {
			toReturn = new PostgreSqlTableHandler();
		}

		if(tableHandlerEntry.equals(ORACLEDBDIALECT)) {
	            toReturn = new OracleSqlTableHandler();
		}
		
		return toReturn;
	}
}