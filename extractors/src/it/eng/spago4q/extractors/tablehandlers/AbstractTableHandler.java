/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.tablehandlers;

import it.eng.spago.base.SourceBean;
import it.eng.spago.dbaccess.sql.DataConnection;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.bo.InterfaceItem;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public abstract class AbstractTableHandler extends QueryExecutor implements TableHandlerInterface {

	public String transformTableName(final String tableName) {
		return "FT_" + tableName.trim().replace(" ", "").toUpperCase();
	}

	public boolean hasThisTable(final Connection connection,
			final String tableName) throws EMFUserError {
		boolean toReturn = false;
		DatabaseMetaData dbmd = null;
		try {
			dbmd = connection.getMetaData();
			// Specify the type of object; in this case we want tables
			String[] types = { "TABLE" };
			ResultSet resultSet = dbmd.getTables(null, null, "%", types);

			// Get the table names
			while (resultSet.next()) {
				// Get the table name
				String dbTableName = resultSet.getString(3);
				if (dbTableName.toUpperCase().equals(tableName)) {
					toReturn = true;
					break;
				}
			}
		} catch (SQLException e) {
			throw new EMFUserError(EMFErrorSeverity.ERROR, 100);

		}
		return toReturn;
	}

	public void dropFactTable(final DataConnection dataConnection,
			final String tableName) throws EMFUserError {
		String query = getSqlCodeGenerator().generateDropTableQuery(tableName);
		queryExecutor(dataConnection, query);
	}

	public void createFactTable(final DataConnection dataConnection,
			final String tableName, final List interfaceFieldList)
			throws EMFUserError {
		String query = getSqlCodeGenerator().generateCreateTableQuery(
				tableName, interfaceFieldList);
		queryExecutor(dataConnection, query);
	}

	public void updateFactTable(final Date executionDate,
			final DataConnection dataConnection, final String tableName,
			final InterfaceItem interfaceItem) throws EMFUserError {
		String updateQuery = getSqlCodeGenerator().generateUpdateQuery(
				executionDate, tableName, interfaceItem);
		queryExecutor(dataConnection, updateQuery);
	}

	public void insertIntoFactTable(final Date executionDate,
			final DataConnection dataConnection, final String tableName,
			final InterfaceItem interfaceItem, final Integer logId)
			throws EMFUserError {
		String insertQuery = getSqlCodeGenerator().generateInsertQuery(
				executionDate, tableName, interfaceItem, logId);
		queryExecutor(dataConnection, insertQuery);
	}

	public SourceBean selectFromFactTable(final DataConnection dataConnection,
			final String tableName, final InterfaceItem interfaceItem,
			final Date operationDate) throws EMFUserError {
		String selectQuery = getSqlCodeGenerator().generateSelectQuery(
				tableName, interfaceItem, operationDate);
		SourceBean selectResult = selectQueryExecutor(dataConnection,
				selectQuery);
		return selectResult;
	}

	public abstract SqlCodeGeneratorInterface getSqlCodeGenerator();

}
