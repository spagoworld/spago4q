/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.tablehandlers;

import it.eng.spago.base.SourceBean;
import it.eng.spago.dbaccess.Utils;
import it.eng.spago.dbaccess.sql.DataConnection;
import it.eng.spago.dbaccess.sql.SQLCommand;
import it.eng.spago.dbaccess.sql.result.DataResult;
import it.eng.spago.dbaccess.sql.result.ScrollableDataResult;
import it.eng.spago.error.EMFInternalError;
import it.eng.spago.error.EMFUserError;

public class QueryExecutor {
	
	protected void queryExecutor(final DataConnection dataConnection,
			final String query) throws EMFUserError {
		SQLCommand sqlCommand = null;
		DataResult dataResult = null;
		try {
			sqlCommand = dataConnection.createInsertCommand(query);
			sqlCommand.execute();
		} catch (EMFInternalError e) {
			throw new EMFUserError(e.getCategory(), e.getDescription());
		} finally {
			Utils.releaseResources(dataConnection, sqlCommand, dataResult);
		}
	}
	
	protected void queryListExecutor(final DataConnection dataConnection,
            final String[] query) throws EMFUserError {
        SQLCommand sqlCommand = null;
        DataResult dataResult = null;
        try {
            for (int i = 0; i < query.length; i++) {
                sqlCommand = dataConnection.createInsertCommand(query[i]);
                sqlCommand.execute();
            }
        } catch (EMFInternalError e) {
            throw new EMFUserError(e.getCategory(), e.getDescription());
        } finally {
            Utils.releaseResources(dataConnection, sqlCommand, dataResult);
        }
    }

	protected SourceBean selectQueryExecutor(
			final DataConnection dataConnection, final String query)
			throws EMFUserError {
		SourceBean result = null;
		SQLCommand sqlCommand = null;
		DataResult dataResult = null;
		try {
			sqlCommand = dataConnection.createSelectCommand(query);
			dataResult = sqlCommand.execute();
			ScrollableDataResult scrollableDataResult = (ScrollableDataResult) dataResult
					.getDataObject();
			result = scrollableDataResult.getSourceBean();
		} catch (EMFInternalError e) {
			throw new EMFUserError(e.getCategory(), e.getDescription());
		} finally {
			Utils.releaseResources(dataConnection, sqlCommand, dataResult);
		}
		return result;
	}

}
