/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.tablehandlers;

import it.eng.spago.base.SourceBean;
import it.eng.spago.configuration.ConfigSingleton;
import it.eng.spago.dbaccess.SQLStatements;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.InterfaceField;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.extractors.bo.InterfaceItem;
import it.eng.spago4q.extractors.utilities.ExtractorConstants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

public abstract class AbstractSqlCodeGenerator implements
		SqlCodeGeneratorInterface {

	private static Logger logger = Logger
	.getLogger(AbstractSqlCodeGenerator.class);
	
	public static final String TABLE_NAME_PLACE_HOLDER = "@tablename";
	public static final String COLUMN_NAME_TYPE_PLACE_HOLDER = "@columnNameType";
	public static final String COLUMN_NAME_PLACE_HOLDER = "@columns";
	public static final String VALUES_PLACE_HOLDER = "@values";
	public static final String WHERE_PLACE_HOLDER = "@where";
	public static final String SELECT_PLACE_HOLDER = "@select";
	public static final String DATE_PLACE_HOLDER = "@date";

	public String generateDropTableQuery(final String tableName) {
		String query = SQLStatements.getStatement("DROP_TABLE");
		query = query.replace(TABLE_NAME_PLACE_HOLDER, tableName);
		return query;
	}

	public String generateCreateTableQuery(final String tableName,
			final List interfaceFieldList) throws EMFUserError {
		String columnNameType = "";
		for (Iterator iterator = interfaceFieldList.iterator(); iterator
				.hasNext();) {
			InterfaceField interfaceField = (InterfaceField) iterator.next();

			if (!isValidColumName(interfaceField.getName())) {
				throw new EMFUserError(EMFErrorSeverity.ERROR, 10035);
			}

			columnNameType += ", " + interfaceField.getName() + " ";
			IES4QFKDAO eDomainValueDAO = (IES4QFKDAO) DAOFactory
					.getDAO("EDomainValueDAO");
			List domainValueList = eDomainValueDAO.loadObjectList(
					interfaceField.getId(), null, null);

			if (domainValueList == null || (domainValueList.isEmpty())) {
				columnNameType += typeConverter(interfaceField.getFieldType());
			} else {
				columnNameType += typeConverter(ExtractorConstants.INTEGER);
			}
		}

		String commandString = SQLStatements.getStatement("CREATE_TABLE");
		commandString = commandString.replace(TABLE_NAME_PLACE_HOLDER,
				tableName);
		commandString = commandString.replace(COLUMN_NAME_TYPE_PLACE_HOLDER,
				columnNameType);

		logger.debug("Return this Create query:" + commandString);
		return commandString;
	}

	public String generateUpdateQuery(final Date executionDate,
			final String tableName, final InterfaceItem interfaceItem) {
		String where = "";

		Set<String> keySet = interfaceItem.getHashMap().keySet();

		for (Iterator<String> iterator = keySet.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (interfaceItem.getValue(key).isKey()) {
				String value = getDBValue(interfaceItem, key);
				if (value.equals("null")){
					where += " AND " + key + " is " + value;
				}else{
					where += " AND " + key + " = " + value;	
				}
			}
		}

		String commandString = SQLStatements.getStatement("UPDATE_EXTRACTION");
		commandString = commandString.replace(TABLE_NAME_PLACE_HOLDER,
				tableName);
		commandString = commandString.replace(WHERE_PLACE_HOLDER, where);
		commandString = commandString.replace(DATE_PLACE_HOLDER, "'"
				+ dateFormatter(executionDate) + "'");

		logger.debug("Return this Update query:" + commandString);
		return commandString;
	}

	public String generateInsertQuery(final Date executionDate,
			final String tableName, final InterfaceItem interfaceItem,
			final Integer logId) {
		String columns = "";
		String values = logId + ", ";
		Set<String> keySet = interfaceItem.getHashMap().keySet();

		for (Iterator<String> iterator = keySet.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();

			columns += key;

			values += getDBValue(interfaceItem, key);

			if (iterator.hasNext()) {
				columns += ",";
				values += ",";
			}
		}

		String commandString = SQLStatements.getStatement("INSERT_EXTRACTION");
		commandString = commandString.replace(TABLE_NAME_PLACE_HOLDER,
				tableName);
		commandString = commandString
				.replace(COLUMN_NAME_PLACE_HOLDER, columns);
		commandString = commandString.replace(VALUES_PLACE_HOLDER, values);
		commandString = commandString.replace(DATE_PLACE_HOLDER, "'"
				+ dateFormatter(executionDate) + "'");

		logger.debug("Return this Insert query:" + commandString);
		return commandString;
	}

	public String generateSelectQuery(final String tableName,
			final InterfaceItem interfaceItem, final Date operationDate) {
		String select = "";
		String where = "";
		String orWhere = "";
		
		//SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String operationDateStr = dateFormatter(operationDate);

		Set<String> keySet = interfaceItem.getHashMap().keySet();

		for (Iterator<String> iterator = keySet.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (interfaceItem.getValue(key).isKey()) {
				// add key fields in where conditions
				String value = getDBValue(interfaceItem, key);
				if (value.equals("null")) {
					where += " AND " + key + " is " + value + "";
					orWhere += " AND " + key + " is " + value + "";
				} else {
					where += " AND " + key + " = " + value + "";
					orWhere += " AND " + key + " = " + value + "";
				}
			}
			if (interfaceItem.getValue(key).isSensible()) {
				select += ", " + key;
				// add sensible fields in where conditions
				String value = getDBValue(interfaceItem, key);
				if (value.equals("null")) {
					where += " AND " + key + " is " + value + "";

				} else {
					where += " AND " + key + " = " + value + "";
				}
			}
		}

		where += " AND BEGIN_TIME <= '" + operationDateStr + "' ";
		/*
		 * TO verify
		+ "' )"
				+ " OR (BEGIN_TIME >= '" + operationDateStr + "' " + orWhere;
		 */
		String commandString = SQLStatements.getStatement("SELECT_EXTRACTION");
		commandString = commandString.replace(TABLE_NAME_PLACE_HOLDER,
				tableName);
		commandString = commandString.replace(SELECT_PLACE_HOLDER, select);
		commandString = commandString.replace(WHERE_PLACE_HOLDER, where);

		logger.debug("Return this Select query:" + commandString);
		return commandString;
	}

	public String getDBValue(final InterfaceItem interfaceItem, final String key) {
		String toReturn = "";
		if (interfaceItem.getValue(key).getValue() != null) {
		    if (interfaceItem.getValue(key).getType().equalsIgnoreCase(ExtractorConstants.DOUBLE)){
		        toReturn = interfaceItem.getValue(key).getValue().toString();    
		    }else {
		        toReturn = "'" + interfaceItem.getValue(key).getValue().toString()
					+ "'";
		    }
		} else {
			toReturn = "null";
		}
		return toReturn;
	}

	public boolean isValidColumName(final String columName) {
		boolean toReturn = true;
		if (columName != null && columName.toUpperCase().equals("ID")) {
			toReturn = false;
		}
		if (columName != null && columName.toUpperCase().equals("BEGIN_TIME")) {
			toReturn = false;
		}
		if (columName != null && columName.toUpperCase().equals("END_TIME")) {
			toReturn = false;
		}
		if (columName != null && columName.toUpperCase().equals("ID_LOG")) {
			toReturn = false;
		}
		return toReturn;
	}

	private String dateFormatter(final Date date) {
		ConfigSingleton configSingleton = ConfigSingleton.getInstance();
		SourceBean dbFormatConfigSourceBean = (SourceBean) configSingleton
				.getFilteredSourceBeanAttribute(
						"SPAGO4Q.DB-DATE-FORMAT.FORMAT", "name", "format");
		String dBFormat = (String) dbFormatConfigSourceBean
				.getAttribute("formatconverter");

		DateFormat formatter = new SimpleDateFormat(dBFormat);
		return formatter.format(date);
	}

	public String typeConverter(final String type) {
		String toReturn = "";
		ConfigSingleton configSingleton = ConfigSingleton.getInstance();
		SourceBean daoConfigSourceBean = (SourceBean) configSingleton
				.getFilteredSourceBeanAttribute("SPAGO4Q.DB-CONF."
						+ getDBConfEnvelopeName() + ".DB-TYPE", "name", type);
		toReturn = (String) daoConfigSourceBean.getAttribute("conversion");

		return toReturn;
	}

	public abstract String getDBConfEnvelopeName();

}
