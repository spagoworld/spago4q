/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.tablehandlers.postgresql;

import it.eng.spago.dbaccess.SQLStatements;
import it.eng.spago.error.EMFErrorSeverity;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.InterfaceField;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.extractors.tablehandlers.AbstractSqlCodeGenerator;
import it.eng.spago4q.extractors.utilities.ExtractorConstants;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

public class PostgreSqlCodeGenerator extends AbstractSqlCodeGenerator {
	
	private static String POSTGRESQL = "POSTGRESQL";

	private static final String SEQUENCE_NAME_PLACE_HOLDER = "@sequenceName";
	private static final String CONSTRAINT_NAME_PLACE_HOLDER = "@constraintName";
	
	private static Logger logger = Logger
    .getLogger(PostgreSqlCodeGenerator.class);

	private static String createSequenceName(final String tableName) {
		return tableName + "_SEQ";
	}

	private static String createPrimaryKeyConstraintName(final String tableName) {
		return "XPK" + tableName;
	}

	public static String generateCreateSequenceQuery(final String tableName) {

		String query = SQLStatements
				.getStatement("POSTGRE_CREATE_TABLE_SEQUENCE");
		query = query.replace(SEQUENCE_NAME_PLACE_HOLDER,
				createSequenceName(tableName));
		return query;
	}

	public String generateCreateTableQuery(final String tableName,
			final List interfaceFieldList) throws EMFUserError {
		String columnNameType = "";
		for (Iterator iterator = interfaceFieldList.iterator(); iterator
				.hasNext();) {
			InterfaceField interfaceField = (InterfaceField) iterator.next();

			if (!isValidColumName(interfaceField.getName())) {
				throw new EMFUserError(EMFErrorSeverity.ERROR, 10035);
			}

			columnNameType += ", " + interfaceField.getName() + " ";
			IES4QFKDAO eDomainValueDAO = (IES4QFKDAO) DAOFactory
					.getDAO("EDomainValueDAO");
			List domainValueList = eDomainValueDAO.loadObjectList(
					interfaceField.getId(), null, null);

			if (domainValueList == null
					|| (domainValueList.isEmpty())) {
				columnNameType += typeConverter(interfaceField.getFieldType());
			} else {
				columnNameType += typeConverter(ExtractorConstants.INTEGER);
			}
		}

		String commandString = SQLStatements
				.getStatement("POSTGRE_CREATE_TABLE");
		commandString = commandString.replace(TABLE_NAME_PLACE_HOLDER,
				tableName);
		commandString = commandString.replace(COLUMN_NAME_TYPE_PLACE_HOLDER,
				columnNameType);
		commandString = commandString.replace(CONSTRAINT_NAME_PLACE_HOLDER,
				createPrimaryKeyConstraintName(tableName));

		commandString = commandString.replace(SEQUENCE_NAME_PLACE_HOLDER,
				createSequenceName(tableName));

		logger.debug("Return this Create query:" + commandString);
		return commandString;
	}

	public static String generateDropSequenceQuery(final String tableName) {
		String query = SQLStatements
				.getStatement("POSTGRE_DROP_TABLE_SEQUENCE");
		query = query.replace(SEQUENCE_NAME_PLACE_HOLDER,
				createSequenceName(tableName));
		return query;
	}

	@Override
	public String getDBConfEnvelopeName() {
		return POSTGRESQL;
	}

}
