/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.tablehandlers.postgresql;

import it.eng.spago.dbaccess.sql.DataConnection;
import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.extractors.tablehandlers.AbstractTableHandler;
import it.eng.spago4q.extractors.tablehandlers.SqlCodeGeneratorInterface;

import java.util.List;

public class PostgreSqlTableHandler extends AbstractTableHandler {

	private SqlCodeGeneratorInterface codeGenerator;

	public PostgreSqlTableHandler() {
		codeGenerator = new PostgreSqlCodeGenerator();
	}

	@Override
	public void createFactTable(final DataConnection dataConnection,
			final String tableName, final List interfaceFieldList) throws EMFUserError {
		// sequence
		String sequenceQuery = PostgreSqlCodeGenerator
				.generateCreateSequenceQuery(tableName);
		// table
		String createQuery = getSqlCodeGenerator().generateCreateTableQuery(
				tableName, interfaceFieldList);
		queryExecutor(dataConnection, sequenceQuery + createQuery);
	}

	@Override
	public void dropFactTable(final DataConnection connection, final String tableName)
			throws EMFUserError {
		// sequence
		String dropSequenceQuery = PostgreSqlCodeGenerator
				.generateDropSequenceQuery(tableName);
		// table
		String dropQuery = getSqlCodeGenerator()
				.generateDropTableQuery(tableName);
		queryExecutor(connection, dropQuery + dropSequenceQuery);
	}

	@Override
	public SqlCodeGeneratorInterface getSqlCodeGenerator() {
		return codeGenerator;
	}

}
