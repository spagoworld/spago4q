/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import it.eng.spago4q.extractors.bo.ExtractorParameter;
import it.eng.spago4q.extractors.xml.SaxXmlExtractor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import junit.framework.TestCase;

public class AbstractExtractorTest extends TestCase {


	private AbstractExtractor extractor;

	public AbstractExtractorTest(final String value) {
		super(value);
	}


	@Override
	protected void setUp() throws Exception {
		super.setUp();
			List<ExtractorParameter> iDataSourceParameters = new ArrayList<ExtractorParameter>();
			iDataSourceParameters.add(new ExtractorParameter("DSPAR","1"));
			iDataSourceParameters.add(new ExtractorParameter("DSPAR","2"));
			
			List<ExtractorParameter> iOperationParameters = new ArrayList<ExtractorParameter>();
			iOperationParameters.add(new ExtractorParameter("OPAR", "a"));
			iOperationParameters.add(new ExtractorParameter("OPAR", "b"));
			iOperationParameters.add(new ExtractorParameter("OPAR", "c"));
			// Create the extractor
			extractor = new SaxXmlExtractor();
			extractor.init(iDataSourceParameters, iOperationParameters);

	}

	public void testReadDataSourceParameterValueList() {
		// Check the number of Data Source parameters
		assertEquals(2, extractor.getDataSourceParameter().size());
		
		List<String> dsPars = extractor.readDataSourceParameterValueList("DSPAR");

		assertEquals(2, dsPars.size());

		Integer value = 1;
		for (Iterator iterator = dsPars.iterator(); iterator.hasNext();) {
			assertEquals(value.toString(),(String) iterator.next());
			value ++;
		}
	}

	public void testReadOperationParameterValueList() {
		assertEquals(3, extractor.getOperationParameter().size());

		List<String> oPars = extractor.readOperationParameterValueList("OPAR"); 
		assertEquals(3, oPars.size());
		
		Character value = 'a';
		for (Iterator iterator = oPars.iterator(); iterator.hasNext();) {
			assertEquals(value.toString(),(String) iterator.next());
			value ++;
		}
	}
}
