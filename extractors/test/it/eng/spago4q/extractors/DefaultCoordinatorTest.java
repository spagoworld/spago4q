/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors;

import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.extractors.bo.ExtractorParameter;
import it.eng.spago4q.extractors.bo.GenericItemInterface;
import it.eng.spago4q.extractors.bo.InterfaceItem;
import it.eng.spago4q.extractors.parameterfiller.DefaultExtractionParameterFiller;
import it.eng.spago4q.extractors.parameterfiller.ExtractionParameterFiller;
import it.eng.spago4q.extractors.script.ScriptExecutor;
import it.eng.spago4q.extractors.utilities.OperationUtil;
import it.eng.spago4q.testUtils.AbstractDBTestCase;

import java.util.List;

public class DefaultCoordinatorTest extends AbstractDBTestCase {

	private static final String FLAT_FILE = "test/flatFile/s4qFlatFile.xml";

	public DefaultCoordinatorTest(final String value) {
		super(value);
	}

	public void testExecute() {
		try {

 			ScriptExecutor scriptExecutor = new ScriptExecutor();
			
	        IES4QFKDAO operationDAO = (IES4QFKDAO) DAOFactory.getDAO("EOperationDAO");
	        EOperation operation = (EOperation)operationDAO.loadObjectById(2);
	        
	    	DomainValueCacheCreator domainValueCacheCreator = new DomainValueCacheCreator(
					operation.getInterfaceType().getId());
	    	
	    	ExtractionParameterFiller extractorParameterFiller = new DefaultExtractionParameterFiller();
	        // set the operation
	        extractorParameterFiller.setOperation(operation);
	        // get data source parameters
	        List<ExtractorParameter> dataSourceParameters = extractorParameterFiller
	                .getDataSourceParameters();
	        // get operations parameters
	        List<ExtractorParameter> operationParameters = extractorParameterFiller
	                .getOperationParameters();

	        List<GenericItemInterface> genericItemList = OperationUtil
	                .executeOperation(operation, dataSourceParameters,
	                        operationParameters);

			MapMaker mapMaker = new MapMaker();
			List<InterfaceItem> interfaceItem = mapMaker.mapping(operation,
					genericItemList, scriptExecutor, domainValueCacheCreator);
			assertEquals("Verify the number of interface Item extracted",1,interfaceItem.size());

		} catch (Exception e) {
			fail();
		}

	}

	protected String getFlatFile() {
		return FLAT_FILE;
	}

}
