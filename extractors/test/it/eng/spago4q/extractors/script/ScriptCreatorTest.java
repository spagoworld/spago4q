/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.script;

import javax.script.ScriptException;

import junit.framework.TestCase;

public class ScriptCreatorTest extends TestCase {

	public ScriptCreatorTest(final String name) {
		super(name);
	}

	public void testExecute() {
		String script = "import it.eng.spago4q.extractors.script.IMappingScript; \n";
		script += "\n";
		script += "public class Tester implements IMappingScript { \n";
		script += "\n";
		script += "public String execute(String value) {\n";
		script += "return value + \" ciao\"; \n";
		script += "}\n";
		script += "}\n";
		script += "\n";
		script += "Object o = new Tester();";
		
		IMappingScript scriptTest;
		try {
			scriptTest = ScriptCreator.createScriptObject(script);
			String value = "ciao";
			assertEquals("ciao ciao", scriptTest.execute(value));
		} catch (ScriptException e) {
			fail();
		}
	}
}
