/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.bo;

import it.eng.spago.base.SourceBean;
import it.eng.spago.configuration.ConfigSingleton;
import it.eng.spago.configuration.FileCreatorConfiguration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class GenericItemUtilTest extends TestCase {

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		ClassLoader loader = this.getClass().getClassLoader();
		URL xmlUrl = loader.getResource("master.xml");
		System.setProperty("AF_CONFIG_FILE", xmlUrl.getPath());
		ConfigSingleton.setConfigurationCreation(new FileCreatorConfiguration(
				ConfigSingleton.getRootPath()));
	}

	public GenericItemUtilTest(final String name) {
		super(name);
	}

	public void testGenericItemListToSourceBean() {
		List<GenericItemInterface> genericItemList = new ArrayList<GenericItemInterface>();
		GenericItem g1 = new GenericItem();
		g1.setValue("prova11", "provax");
		g1.setValue("prova12", "provay");
		g1.setValue("prova13", "prova1");

		GenericItem g2 = new GenericItem();
		g2.setValue("prova21", "provaa");
		g2.setValue("prova22", "provab");
		g2.setValue("prova23", "prova2");

		genericItemList.add(g1);
		genericItemList.add(g2);
		SourceBean result = null;

		try {
			result = GenericItemUtil
					.genericItemListToSourceBean(genericItemList);
		} catch (Exception e) {
			fail();
		}
	}
	
	public void testGenericItemListWithErrorsToSourceBean() {
		List<GenericItemInterface> genericItemList = new ArrayList<GenericItemInterface>();
		List<String> errorMessages = new ArrayList<String>();
		GenericItem g1 = new GenericItem();
		g1.setValue("prova11", "prova1");
		g1.setValue("prova12", "prova1");
		g1.setValue("prova13", "prova1");

		GenericItem g2 = new GenericItem();
		g2.setValue("prova21", "prova2");
		g2.setValue("prova22", "prova2");
		g2.setValue("prova23", "prova2");

		genericItemList.add(g1);
		genericItemList.add(g2);
		
		errorMessages.add("g1 error1");
		errorMessages.add("g2 error2");
		SourceBean result = null;

		try {
			result = GenericItemUtil
					.genericItemListWithErrorsToSourceBean(genericItemList, errorMessages);
		} catch (Exception e) {
			fail();
		}
	}

}
