/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.xml;

import it.eng.spago4q.extractors.AbstractExtractor;
import it.eng.spago4q.extractors.bo.ExtractorParameter;
import it.eng.spago4q.extractors.bo.GenericItemInterface;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class Dom4JXmlExtractorTest extends TestCase {

	public Dom4JXmlExtractorTest(final String value) {
		super(value);
	}

	public void testExecute() {
		try {
			// Create the parameters
			List<ExtractorParameter> operationParameter = new ArrayList<ExtractorParameter>();
			operationParameter.add(new ExtractorParameter("XPATH", "//book"));
			List<ExtractorParameter> dataSourceParameter = new ArrayList<ExtractorParameter>();
			dataSourceParameter.add(new ExtractorParameter("FILE",
					"test/xml/books.xml"));

			// Create the extractor
			AbstractExtractor extractor = new Dom4JXmlExtractor();
			extractor.init(dataSourceParameter, operationParameter);
			// Check the number of parameters
			assertEquals(1, extractor.getDataSourceParameter().size());
			assertEquals(1, extractor.getOperationParameter().size());

			List<GenericItemInterface> genericItemExtracted = extractor
					.execute();

			assertEquals(3, genericItemExtracted.size());
			assertEquals("Snow Crash", genericItemExtracted.get(0).getValue(
					"title"));
		} catch (Exception e) {
			fail();
		}
	}
}
