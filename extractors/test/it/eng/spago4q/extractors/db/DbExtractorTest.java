/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.extractors.db;

import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QFKDAO;
import it.eng.spago4q.extractors.AbstractExtractor;
import it.eng.spago4q.extractors.DefaultCoordinator;
import it.eng.spago4q.extractors.OperationCoordinatorFactory;
import it.eng.spago4q.extractors.OperationCoordinatorInterface;
import it.eng.spago4q.extractors.bo.GenericItemInterface;
import it.eng.spago4q.testUtils.AbstractDBTestCase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DbExtractorTest extends AbstractDBTestCase {

	private static final String FLAT_FILE = "test/flatFile/s4qFlatFile.xml";

	public DbExtractorTest(final String name) {
		super(name);
	}

	public void testExecute() {
		try {
			OperationCoordinatorInterface oCI = OperationCoordinatorFactory
					.create(2,"test");
			assertEquals(DefaultCoordinator.class, oCI.getClass());

			assertEquals(1, oCI.getOperationsList().size());

			assertEquals("it.eng.spago4q.extractors.db.DbExtractor",
					((EOperation) oCI.getOperationsList().get(0))
							.getDataSource().getSourceType()
							.getExtractorClass());

			AbstractExtractor extractor = (AbstractExtractor) Class.forName(
					((EOperation) oCI.getOperationsList().get(0))
							.getDataSource().getSourceType()
							.getExtractorClass()).newInstance();

			extractor.init((EOperation) oCI.getOperationsList().get(0));

			assertEquals(4, extractor.getDataSourceParameter().size());
			assertEquals(1, extractor.getOperationParameter().size());

			List<GenericItemInterface> genericItemList = extractor.execute();

			assertEquals(1, genericItemList.size());
		} catch (Exception e) {
			fail();
		}

	}

	public void testGetLastExecutionDate() {
		try {
			// get the operation from the DB
			IES4QFKDAO operationDAO = (IES4QFKDAO) DAOFactory
					.getDAO("EOperationDAO");
			EOperation operation = (EOperation) operationDAO.loadObjectById(2);

			// Create the extractor
			AbstractExtractor extractor = new DbExtractor();
			extractor.init(operation);
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = (Date) formatter.parse("2009-04-28 11:36:52.0");

			assertEquals(date, extractor.getLastExtractionDate());
		} catch (Exception e) {
			fail();
		}
	}

	@Override
	protected String getFlatFile() {
		return FLAT_FILE;
	}
}
