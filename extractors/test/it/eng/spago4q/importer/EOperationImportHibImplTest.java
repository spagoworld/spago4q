/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.importer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;

import it.eng.spago4q.exporter.OperationExporter;
import it.eng.spago4q.metadata.S4qEOperation;
import it.eng.spago4q.metadata.S4qEOperationField;
import junit.framework.TestCase;

public class EOperationImportHibImplTest extends TestCase {

	public EOperationImportHibImplTest(final String value) {
		super(value);
	}
	
	public void test1ImportDataOWTrue() {
		try{
	String xml = "<operation>"
	  +"<name>PRJ_MANAGMENT_OPERATION</name>"
	  +"<description>Patatine fritte</description>"
	  +"<archive>false</archive>"
	  
	  +"<process>"
	  +"<name>PRJ_EXTRACTION_PROCESS</name>"
	  +"<description></description>"
	  +"<coordinatorClass>it.eng.spago4q.extractors.DefaultCoordinator</coordinatorClass>"
	  +"</process>"
	  
	  +"<dataSource>S4Q_DS_DATA_BASE-S4Q_DATA_BASE</dataSource>"
	  
	  +"<interfaceType>PROJECT_MANAGEMENT</interfaceType>"
	  
	  +"<operationParameters>"
	  +"<operationParameter>"
	  +"<name>QUERY</name>"
	  +"<value>select * FROM spago4q.DT_PROJECT_MAN; bla bla</value>"
	  +"<cript>false</cript>"
	  +"</operationParameter>"
	  +"</operationParameters>"
	  +"<operationFields>"
	  
	  +"<operationField>"
	  +"<name>PRJ</name>"
	  +"<interfaceField>PRJ</interfaceField>"
	  +"<script>PROJECT_NAME_CONVERTER</script>"
	  +"</operationField>"
	  
	  +"<operationField>"
	  +"<name>DATE bla bal</name>"
	  +"<interfaceField>DATE</interfaceField>"
	  +"<script>DATE_CONVERTER</script>"
	  +"</operationField>"
	  
	  +"<operationField>"
	  +"<name>PV pluto</name>"
	  +"<interfaceField>PV</interfaceField>"
	  +"</operationField>"
	  
	  +"<operationField>"
	  +"<name>AC</name>"
	  +"<interfaceField>AC</interfaceField>"
	  +"</operationField>"
	  
	  +"<operationField>"
	  +"<name>EV</name>"
	  +"<interfaceField>EV</interfaceField>"
	  +"</operationField>"
	  
	  +"</operationFields>"
	  +"</operation>";
	
	OperationExporter operationExporter = new OperationExporter();
	S4qEOperation opeartionToImport = operationExporter.importOperation(xml);
	assertEquals("PRJ_MANAGMENT_OPERATION", opeartionToImport.getName());
	assertEquals("Patatine fritte", opeartionToImport.getDescription());
	assertFalse(opeartionToImport.getArchive());
	
	assertEquals("PRJ_EXTRACTION_PROCESS", opeartionToImport.getS4qEProcess().getName());
	assertEquals("", opeartionToImport.getS4qEProcess().getDescription());
	assertEquals("it.eng.spago4q.extractors.DefaultCoordinator", opeartionToImport.getS4qEProcess().getCoordinatorClass());
	
	assertEquals("S4Q_DS_DATA_BASE-S4Q_DATA_BASE", opeartionToImport.getS4qEDataSource().getName()+"-"+opeartionToImport.getS4qEDataSource().getS4qESourceType().getName());
	
	assertEquals("PROJECT_MANAGEMENT", opeartionToImport.getS4qEInterfaceType().getName());
	
	assertEquals(1, opeartionToImport.getS4qEOperationParameters().size());
	assertEquals(5, opeartionToImport.getS4qEOperationFields().size());
	
			}catch(Exception e){
			fail();
		}
	}

	public void testOperationFromFile() {
		try {
			String xml = readFileAsString("test/it/eng/spago4q/importer/xml/operation.xml");
			OperationExporter operationExporter = new OperationExporter();
			S4qEOperation operationToImport = operationExporter.importOperation(xml);
			Set<S4qEOperationField> fields =  (Set<S4qEOperationField>)operationToImport.getS4qEOperationFields();
			assertEquals(10, fields.size());
		} catch (IOException e) {
			fail();
		}
	}
	
    private String readFileAsString(String filePath)
    throws java.io.IOException{
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
    }

}
