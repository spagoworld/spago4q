/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.importer;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.exporter.DataSourceExporter;
import it.eng.spago4q.metadata.S4qEDataSource;
import it.eng.spago4q.metadata.S4qEDataSourceParameter;
import it.eng.spago4q.metadata.S4qESourceType;
import it.eng.spago4q.testUtils.AbstractDBTestCase;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashSet;

import org.dbunit.Assertion;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory;

public class EDataSourceImportHibImplTest extends AbstractDBTestCase{

	public EDataSourceImportHibImplTest(final String value) {
		super(value);
	}

	private static final String FLAT_FILE = "test/flatFile/s4qFlatFile.xml";
	
	@Override
	protected String getFlatFile() {
		return FLAT_FILE;
	}

	public void testImportDataOverWriteTrue() {
		try{
		S4qESourceType s4qSourceType = new S4qESourceType();
		s4qSourceType.setIdSourceType(100);
		s4qSourceType.setName("EXTRACTOR_DB");
		s4qSourceType.setDescription("Source Type Description");
		s4qSourceType.setExtractorClass("class");
		
		S4qEDataSource s4qDataSource = new S4qEDataSource();
		s4qDataSource.setIdDataSource(99);
		s4qDataSource.setName("TEST_HSQLDB_DATASOURCE");
		s4qDataSource.setDescription("Data Source Description");
		s4qDataSource.setS4qESourceType(s4qSourceType);
		
		HashSet dataSourceParameters = new HashSet();
		
		S4qEDataSourceParameter dataSourceParameter = new S4qEDataSourceParameter();
		dataSourceParameter.setIdDataSourceParameter(88);
		dataSourceParameter.setName("URL");
		dataSourceParameter.setValue("pazzo");
		dataSourceParameter.setCript(false);
		dataSourceParameters.add(dataSourceParameter);
		
		s4qDataSource.setS4qEDataSourceParameters(dataSourceParameters);
		DataSourceExporter dataSourceExporter = new DataSourceExporter();
		String xml = dataSourceExporter.exportDataSource(s4qDataSource);
		
		S4qEDataSource s4qDataSourceFromInput = dataSourceExporter.importDataSource(xml);
		
		EDataSourceImport dataSourceImport = new EDataSourceImportHibImpl();
		
		dataSourceImport.importData(s4qDataSourceFromInput, true);
		
		assertDataSourceTables("test/flatFile/DataSource/Test1OWTrue.xml");
		}catch(EMFUserError e){
			fail();
		}
	}

	public void testImportDataOverWriteFalse() {
		try{
		String xmlFileExported = "<dataSource>"
		  +"<name>TEST_HSQLDB_DATASOURCE</name>"
		  +"<description>Data Source Description</description>"
		  +"<sourceType>"
		  +"<name>EXTRACTOR_DB</name>"
		  +"<description>Source Type Description</description>"
		  +"<extractorClass>class</extractorClass>"
		  +"</sourceType>"
		  +"<dataSourceParameters>"
		  +"<dataSourceParameter>"
		  +"<name>URL</name>"
          +"<value>pazzo</value>"
          +"<cript>false</cript>"
		  +"</dataSourceParameter>"
		  +"</dataSourceParameters>"
		  +"</dataSource>";
		
		S4qEDataSource s4qDataSourceFromInput = new DataSourceExporter().importDataSource(xmlFileExported);
		
		EDataSourceImport dataSourceImport = new EDataSourceImportHibImpl();
		
		dataSourceImport.importData(s4qDataSourceFromInput, false);
		
		assertDataSourceTables(FLAT_FILE);
		}catch(EMFUserError e){
			fail();
		}
		
	}	
	
	private void assertDataSourceTables(final String flatFileNamePath){
		Connection jdbcConnection = null;
		try{
		// Fetch database data after executing your code
		Class driverClass = Class.forName("org.hsqldb.jdbcDriver");
		jdbcConnection = DriverManager.getConnection(
				"jdbc:hsqldb:mem:/spagobi","sa","");
		IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
		DatabaseConfig config = connection.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
				new HsqldbDataTypeFactory());
        IDataSet databaseDataSet = connection.createDataSet();
        

        // Load expected data from an XML data set
        IDataSet expectedDataSet = new FlatXmlDataSet(new File(flatFileNamePath));
        ITable actualSourceTypeTable = databaseDataSet.getTable("S4Q_E_SOURCE_TYPE");
        ITable expectedSourceTypeTable = expectedDataSet.getTable("S4Q_E_SOURCE_TYPE");

        ITable actualDataSourceTable = databaseDataSet.getTable("S4Q_E_DATA_SOURCE");
        ITable expectedDataSourceTable = expectedDataSet.getTable("S4Q_E_DATA_SOURCE");
        
        ITable actualDataSourceParameterTable = databaseDataSet.getTable("S4Q_E_DATA_SOURCE_PARAMETER");
        ITable expectedDataSourceParameterTable = expectedDataSet.getTable("S4Q_E_DATA_SOURCE_PARAMETER");
        
        // Assert actual database table match expected table
        Assertion.assertEquals(actualSourceTypeTable, expectedSourceTypeTable);
        
        Assertion.assertEquals(actualDataSourceTable, expectedDataSourceTable);
        
        Assertion.assertEquals(actualDataSourceParameterTable, expectedDataSourceParameterTable);
		}catch(Exception e){
			fail();
		}finally{
			if (jdbcConnection != null){
				try {
					jdbcConnection.close();
				} catch (SQLException e) {
					fail();
				}
			}
		}
		
	}
	
}