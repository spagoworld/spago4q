/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.importer;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.Script;
import it.eng.spago4q.dao.EScriptDAOHibImpl;
import it.eng.spago4q.metadata.S4qEScript;
import it.eng.spago4q.testUtils.AbstractDBTestCase;

import java.util.List;

public class EScriptImporterHibImplTest extends AbstractDBTestCase {

	public EScriptImporterHibImplTest(final String value) {
		super(value);
	}

	private static final String FLAT_FILE = "test/flatFile/s4qFlatFile.xml";

	@Override
	protected String getFlatFile() {
		return FLAT_FILE;
	}

	public void test1ImportDataOWFalse() {
		try {
			S4qEScript script = new S4qEScript();
			script.setIdScript(10);
			script.setName("TEST SCRIPT");
			script.setDescription("script description");
			script.setScriptType("groovy");
			script.setScript("bla bla bla");

			EScriptImport scriptImporter = new EScriptImportHibImpl();
			scriptImporter.importData(script, false);
			Script actualScript = getScriptStoredIntoDB("TEST SCRIPT");
			
			assertEquals(script.getName(), actualScript.getName());
			assertEquals(script.getScriptType(), actualScript.getScriptType());
			assertFalse(script.getDescription().equals(actualScript.getDescription()));
			assertFalse(script.getScript().equals(actualScript.getScript()));
			
		} catch (Exception e) {
			fail();
		}
	}

	private Script getScriptStoredIntoDB(String scriptName) throws EMFUserError {
		EScriptDAOHibImpl eScriptDAO = new EScriptDAOHibImpl();
		List scripts = eScriptDAO.loadObjectList(null, null);
		Script toFind = null;
		for (Script script : (List<Script>) scripts) {
			if (script.getName().equals(scriptName)) {
				toFind = script;
				break;
			}
		}
		return toFind;
	}


}
