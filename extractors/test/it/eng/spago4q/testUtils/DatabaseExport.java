/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.testUtils;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.search.TablesDependencyHelper;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;

public final class DatabaseExport {

	private DatabaseExport() {
	}

	public static final void main(final String[] args) throws Exception {
		// database connection
		Class driverClass = Class.forName("com.mysql.jdbc.Driver");
		Connection jdbcConnection = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/s4q-qualipso", "s4q-qualipso", "s4q-qualipso");
		IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
		DatabaseConfig config = connection.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
				new MySqlDataTypeFactory());

		// dependent tables database export: export table X and all tables that
		// have a PK which is a FK on X, in the right order for insertion
//		 String[] depTableNames =
//		 TablesDependencyHelper.getAllDependentTables(
//		 connection, "s4q_e_script");
		String[] depTableNames = {"S4Q_E_INTERFACE_TYPE",
				"S4Q_E_INTERFACE_FIELD",
				"S4Q_E_SOURCE_TYPE",
				"S4Q_E_DATA_SOURCE",
				"S4Q_E_PROCESS",
				"S4Q_E_OPERATION",
				"S4Q_E_SCRIPT",
				"S4Q_E_OPERATION_PARAMETER",
				"S4Q_E_OPERATION_FIELD"};

		IDataSet depDataset = connection.createDataSet(depTableNames);
		FlatXmlDataSet.write(depDataset, new FileOutputStream(
				"test/s4qFlatFile200.xml"));
	}
}
