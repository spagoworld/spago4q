/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.testUtils;

import it.eng.spago4q.exporter.OperationExporterTest;
import it.eng.spago4q.exporter.ScriptExportTest;
import it.eng.spago4q.extractors.AbstractExtractorTest;
import it.eng.spago4q.extractors.bo.GenericItemUtilTest;
import it.eng.spago4q.extractors.script.DateConverterScriptTest;
import it.eng.spago4q.extractors.script.ScriptCreatorTest;
import it.eng.spago4q.extractors.xml.Dom4JXmlExtractorTest;
import it.eng.spago4q.extractors.xml.SaxXmlExtractorTest;
import it.eng.spago4q.extractors.xml.XmlExtractorTest;
import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for it.eng.spago4q.testUtils");
		suite.addTestSuite(ScriptExportTest.class);
		suite.addTestSuite(OperationExporterTest.class);
		suite.addTestSuite(AbstractExtractorTest.class);
		suite.addTestSuite(GenericItemUtilTest.class);
		suite.addTestSuite(DateConverterScriptTest.class);
		suite.addTestSuite(ScriptCreatorTest.class);
		suite.addTestSuite(Dom4JXmlExtractorTest.class);
		suite.addTestSuite(SaxXmlExtractorTest.class);
		suite.addTestSuite(XmlExtractorTest.class);
		return suite;
	}

}
