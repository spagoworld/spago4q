/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.testUtils;

import it.eng.spago.configuration.ConfigSingleton;
import it.eng.spago.configuration.FileCreatorConfiguration;
import it.eng.spago4q.dao.DAOFactory;
import it.eng.spago4q.dao.IES4QDAO;

import java.io.FileInputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;

import junit.framework.TestCase;

public abstract class AbstractDBTestCase extends TestCase {

	public AbstractDBTestCase(final String value) {
		super(value);
	}

	private Connection jdbcConnection;
	
	private void setConnection (final Connection connection) {
		this.jdbcConnection = connection;
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		ClassLoader loader = this.getClass().getClassLoader();
		URL xmlUrl = loader.getResource("master.xml");
		System.setProperty("AF_CONFIG_FILE", xmlUrl.getPath());
        ConfigSingleton.setConfigurationCreation(new FileCreatorConfiguration(ConfigSingleton.getRootPath())); 
		IES4QDAO foo = (IES4QDAO) DAOFactory.getDAO("ESourceTypeDAO");
		
		foo.loadObjectList(null, null);

		// initialize your database connection here
		setConnection( DriverManager.getConnection(
				"jdbc:hsqldb:mem:/spagobi", "sa", ""));

		IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
		DatabaseConfig config = connection.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
				new HsqldbDataTypeFactory());

		// initialize your dataset here
		IDataSet dataSet = new FlatXmlDataSet(new FileInputStream(
				getFlatFile()));

		try {
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
		} finally {
			connection.close();
		}
	}

	protected abstract String getFlatFile();

}
