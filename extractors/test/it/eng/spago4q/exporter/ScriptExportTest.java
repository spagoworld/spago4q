/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import it.eng.spago4q.metadata.S4qEScript;
import junit.framework.TestCase;

public class ScriptExportTest extends TestCase {

	public ScriptExportTest(final String value) {
		super(value);
	}

	public void testExportScript() {
		String xml = "<script>\n" + "  <name>PROJECT_NAME_CONVERTER</name>\n"
				+ "  <description>Convert P1 to PRJ-1</description>\n"
				+ "  <scriptType>Groovy</scriptType>\n"
				+ "  <script>a Groovy Script</script>\n" + "</script>";

		S4qEScript script = new S4qEScript();
		script.setName("PROJECT_NAME_CONVERTER");
		script.setDescription("Convert P1 to PRJ-1");
		script.setScriptType("Groovy");
		script.setScript("a Groovy Script");
		ScriptExporter scriptExporter = new ScriptExporter();
		S4qEScript actual = scriptExporter.importScript(scriptExporter
				.exportScript(script));
		assertEquals(xml, scriptExporter.exportScript(script));
		assertEquals(script.getName(), actual.getName());
		assertEquals(script.getDescription(), actual.getDescription());
		assertEquals(script.getScriptType(), actual.getScriptType());
		assertEquals(script.getScript(), actual.getScript());
	}
}
