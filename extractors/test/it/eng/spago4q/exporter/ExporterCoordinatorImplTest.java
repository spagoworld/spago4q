/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import java.io.File;
import java.util.HashSet;


import it.eng.spago4q.testUtils.AbstractDBTestCase;

public class ExporterCoordinatorImplTest extends AbstractDBTestCase {

	private static final String FLAT_FILE = "test/flatFile/s4qFlatFile.xml";
	private static final String OPERATION_URL = "test/operationUrl";
	private static final String DATASOURCE_URL = "test/dataSourceUrl";
	private static final String ITERFACETYPE_URL = "test/interfaceTypeUrl";
	private static final String SCRIPT_URL = "test/scriptUrl";
	public ExporterCoordinatorImplTest(final String value) {
		super(value);
	}

	@Override
	protected String getFlatFile() {
		return FLAT_FILE;
	}
	

	public void testExportDataSource() {
		ExporterCoordinatorImpl exporterCoordinator = new ExporterCoordinatorImpl();
		HashSet<Integer> ids = new HashSet<Integer>();
		ids.add(2);
		exporterCoordinator
				.exportOperation(ids, OPERATION_URL,
						DATASOURCE_URL, ITERFACETYPE_URL,
						SCRIPT_URL);
		if (!new File("test/operationUrl").exists()){
			fail();
		}
		if (!new File(DATASOURCE_URL).exists()){
			fail();
		}
		if (!new File(ITERFACETYPE_URL).exists()){
			fail();
		}
		if (!new File(SCRIPT_URL).exists()){
			fail();
		}
	}
	
	@Override
	protected void tearDown() throws Exception {
		new File(OPERATION_URL).delete();
		new File(DATASOURCE_URL).delete();
		new File(ITERFACETYPE_URL).delete();
		new File(SCRIPT_URL).delete();
	}

}
