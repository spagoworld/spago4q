/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import it.eng.spago4q.metadata.S4qEDataSource;
import it.eng.spago4q.metadata.S4qEInterfaceField;
import it.eng.spago4q.metadata.S4qEInterfaceType;
import it.eng.spago4q.metadata.S4qEOperation;
import it.eng.spago4q.metadata.S4qEOperationField;
import it.eng.spago4q.metadata.S4qEOperationParameter;
import it.eng.spago4q.metadata.S4qEProcess;
import it.eng.spago4q.metadata.S4qESourceType;

import java.util.HashSet;

import junit.framework.TestCase;

public class OperationExporterTest extends TestCase {

	public OperationExporterTest(final String value) {
		super(value);
	}

	public void testExportOperation() {
		String xmlToProduce = "<operation>\n"
				+ "  <name>TEST DB OPERATION</name>\n"
				+ "  <description>extract data from S4Q_E_OPERATION Table</description>\n"
				+ "  <archive>false</archive>\n"
				+ "  <process>\n"
				+ "    <name>DB PROCESS TEST</name>\n"
				+ "    <description>extract data from this DB</description>\n"
				+ "    <coordinatorClass>it.eng.spago4q.extractors.DefaultCoordinator</coordinatorClass>\n"
				+ "  </process>\n"
				+ "  <dataSource>TEST_HSQLDB_DATASOURCE-EXTRACTOR_DB</dataSource>\n"
				+ "  <interfaceType>Test Interface</interfaceType>\n"
				+ "  <operationParameters>\n" + "    <operationParameter>\n"
				+ "      <name>QUERY</name>\n"
				+ "      <value>SELECT * FROM S4Q_E_OPERATION</value>\n"
				+ "      <cript>false</cript>\n"
				+ "    </operationParameter>\n" + "  </operationParameters>\n"
				+ "  <operationFields>\n" + "    <operationField>\n"
				+ "      <name>ID_OPERATION</name>\n"
				+ "      <interfaceField>pluto</interfaceField>\n"
				+ "    </operationField>\n" + "  </operationFields>\n"
				+ "</operation>";

		OperationExporter operationExporter = new OperationExporter();

		S4qEOperation operationToExport = new S4qEOperation();
		operationToExport.setName("TEST DB OPERATION");
		operationToExport
				.setDescription("extract data from S4Q_E_OPERATION Table");
		operationToExport.setArchive(false);

		S4qEInterfaceType interfaceToExport = new S4qEInterfaceType();
		interfaceToExport.setName("Test Interface");

		S4qEProcess processToExport = new S4qEProcess();

		processToExport.setName("DB PROCESS TEST");
		processToExport.setDescription("extract data from this DB");
		processToExport
				.setCoordinatorClass("it.eng.spago4q.extractors.DefaultCoordinator");

		S4qEDataSource dataSource = new S4qEDataSource();
		dataSource.setName("TEST_HSQLDB_DATASOURCE");
		dataSource.setS4qESourceType(new S4qESourceType());
		dataSource.getS4qESourceType().setName("EXTRACTOR_DB");

		HashSet exportParameters = new HashSet();
		S4qEOperationParameter operationParameter = new S4qEOperationParameter();
		operationParameter.setName("QUERY");
		operationParameter.setValue("SELECT * FROM S4Q_E_OPERATION");
		operationParameter.setCript(false);

		exportParameters.add(operationParameter);

		HashSet exportOperationFields = new HashSet();
		S4qEOperationField operationField = new S4qEOperationField();
		operationField.setName("ID_OPERATION");

		S4qEInterfaceField interfaceField = new S4qEInterfaceField();
		interfaceField.setName("pluto");
		operationField.setS4qEInterfaceField(interfaceField);

		exportOperationFields.add(operationField);

		operationToExport.setS4qEDataSource(dataSource);
		operationToExport.setS4qEProcess(processToExport);
		operationToExport.setS4qEOperationParameters(exportParameters);
		operationToExport.setS4qEOperationFields(exportOperationFields);
		operationToExport.setS4qEInterfaceType(interfaceToExport);
		String xml = operationExporter.exportOperation(operationToExport);
		assertEquals(xmlToProduce, xml);
		S4qEOperation operationToImport = operationExporter
				.importOperation(xml);

		assertEquals(operationToExport.getName(), operationToImport.getName());
		assertEquals(operationToExport.getDescription(), operationToImport
				.getDescription());
		assertEquals(operationToExport.getArchive(), operationToImport
				.getArchive());

	}

}
