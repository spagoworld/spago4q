/**

  Spago4Q - The Open Source platform for Software Quality

  Copyright (C) 2012 Engineering Ingegneria Informatica S.p.A.

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0, without the "Incompatible With Secondary Licenses"
  notice.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at http://mozilla.org/MPL/2.0/.

**/
package it.eng.spago4q.exporter;

import java.util.List;

import it.eng.spago.error.EMFUserError;
import it.eng.spago4q.bo.EOperation;
import it.eng.spago4q.dao.EOperationDAOHibImpl;
import it.eng.spago4q.dao.EOperationFieldDAOHibImpl;
import it.eng.spago4q.testUtils.AbstractDBTestCase;

public class ImporterCoordinatorImplTest extends AbstractDBTestCase {
	private static final String FLAT_FILE = "test/flatFile/s4qFlatFile.xml";
	private static final String OPERATION_URL = "test/it/eng/spago4q/exporter/xml/operation.xml";
	private static final String DATASOURCE_URL = "test/it/eng/spago4q/exporter/xml/dataSource.xml";
	private static final String ITERFACETYPE_URL = "test/it/eng/spago4q/exporter/xml/interfaceType.xml";
	private static final String SCRIPT_URL = "test/it/eng/spago4q/exporter/xml/script.xml";
	
	public ImporterCoordinatorImplTest(String value) {
		super(value);
	}

	@Override
	protected String getFlatFile() {
		return FLAT_FILE;
	}
	
	public void testOperationImporter(){
		ImporterCoordinator importerCoordinator = new ImporterCoordinatorImpl();
		try {
			importerCoordinator.operationImporter(OPERATION_URL, DATASOURCE_URL, ITERFACETYPE_URL, SCRIPT_URL, null, true);
			EOperationDAOHibImpl dao = new EOperationDAOHibImpl();
			EOperation operation = (EOperation) dao.loadObjectById(3);
			assertEquals("ACTIONKPI",operation.getName());
			
			EOperationFieldDAOHibImpl daoOpField = new EOperationFieldDAOHibImpl();
			List list = daoOpField.loadObjectList(operation.getId(), null, null);
			assertEquals(10,list.size());
			
		} catch (EMFUserError e) {
			fail();
		}
		
	}

}
