<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:element name="Projects">
			<xsl:for-each select="Project/Tasks/Task[1]">
				<xsl:element name="Project">
					<xsl:for-each select="../../*[name()!='Tasks']">
						<xsl:copy>
							<xsl:apply-templates select="." />
						</xsl:copy>
					</xsl:for-each>
					<xsl:for-each select="*">
						<xsl:variable name="elementName" select="name()" />
						<xsl:element name="Task_{$elementName}">
							<xsl:apply-templates select="." />
						</xsl:element>
					</xsl:for-each>
				</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
